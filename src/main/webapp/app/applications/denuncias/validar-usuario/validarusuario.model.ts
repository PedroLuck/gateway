import { BaseEntity } from '../../../shared';

export class ValidarUsuarioModel {
    constructor(
        public authenticationError: boolean,
        public password: string,
        public rememberMe: boolean,
        public username: string,
        public credentials: any,
    ) {
    }
}

import { BaseEntity } from '../../../shared';
import { Usuario } from '../../../entities/usuario';
import { ComboModel } from '../../general/combobox.model';

export class PernatModel {
    constructor(
        public tipoDoc?: ComboModel,
        public numDoc?: String,
        public nombres?: String,
        public genero?: ComboModel,
        public generoDes?: String,
        public estadoCivil?: ComboModel,
        public estadoCivilDes?: String,
        public discapacidad?: Boolean,
        public nacionalidad?: String,
        public codPaisNacimiento?: ComboModel,
        public fecNacimiento?: Date,
        public codPaisResidencia?: ComboModel,
        public flagJefeHogar?: Boolean,
        public edad?: Number,
        public correo?: String,
    ) {
    }
}

import { BaseEntity } from '../../../shared';
import { ComboModel } from '../../general/combobox.model';

export class InforlabModel {
    constructor(
        public salarioMin?: ComboModel,
        public ocupacion1?: String,
        public ocupacion2?: String,
        public tiempoDesempleado?: String,
    ) {
    }
}

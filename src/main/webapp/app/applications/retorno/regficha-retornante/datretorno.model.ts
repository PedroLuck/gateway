import { BaseEntity } from '../../../shared';
import { ComboModel } from '../../general/combobox.model';

export class DatretornoModel {
    constructor(
        public depaOpc1?: ComboModel,
        public depaOpc2?: ComboModel,
        public fecSalidaPeru?: Date,
        public fecRetornoPeru?: Date,
        public flagRetConFami?: Boolean,
    ) {
    }
}

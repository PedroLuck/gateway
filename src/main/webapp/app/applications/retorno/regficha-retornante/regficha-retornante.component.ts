import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Validators, FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiAlertService, JhiLanguageService } from 'ng-jhipster';
import { RegfichaRetornanteModel } from './regficha-retornante.model';
import { StateStorageService } from '../../../shared/auth/state-storage.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../../shared';
import { Message } from 'primeng/components/common/api';
import { MessageService } from 'primeng/components/common/messageservice';
import { RegfichaRetornanteService } from './regficha-retornante.service';
import { ComboModel } from '../../general/combobox.model';
import { UbigeodepaModel } from '../../general/ubigeodepa.model';
import { UbigeoprovModel } from '../../general/ubigeoprov.model';
import { UbigeodistModel } from '../../general/ubigeodist.model';
import { PernatModel } from './pernat.model';
import { DatretornoModel } from './datretorno.model';
import { NiveleducaModel } from './niveleduca.model';
import { ExplaboralModel } from './explaboral.model';
import { FamiretModel } from './famiret.model';
import { InforlabModel } from './inforlab.model';
import { forEach } from '@angular/router/src/utils/collection';
import { ES } from './../../applications.constant';
import { Pernatural } from '../../../entities/pernatural';

@Component({
    selector: 'jhi-regficharetorno',
    templateUrl: './regficha-retornante.component.html'
})

export class RegfichaRetornanteComponent implements OnInit {
    model: RegfichaRetornanteModel;
    messages: Message[] = [];
    messagesDialog: Message[] = [];
    tipodocs: ComboModel[];
    block: boolean;
    selectedTipodoc: ComboModel;
    pernatural: PernatModel;
    niveleduca: NiveleducaModel;
    inforlab: InforlabModel;
    explaboral: ExplaboralModel;
    datretorno: DatretornoModel;
    tipoDocList: ComboModel[];
    generoList: ComboModel[];
    estadoCivilList: ComboModel[];
    paisNacimientoList: ComboModel[];
    paisResidenciaList: ComboModel[];
    gradoInstruccionList: ComboModel[];
    salarioMinList: ComboModel[];
    tiempoDesempleadoList: ComboModel[];
    famiretModelList: FamiretModel[];
    vinculoList: ComboModel[];
    departs: ComboModel[];
    provins: ComboModel[];
    distris: ComboModel[];
    famiretModelDialog: FamiretModel;
    selectedDeparts: ComboModel;
    selectedProvins: ComboModel;
    selectedDistris: ComboModel;
    displayDialog: Boolean;
    es: any;
    constructor(
        private eventManager: JhiEventManager,
        private messageService: MessageService,
        private regfichaRetornanteService: RegfichaRetornanteService,
    ) {
        this.pernatural = new PernatModel();
    }

    loadAll() {
        this.block = true;
    }
    ngOnInit() {
        this.es = ES;
        this.block = false;
        this.pernatural = new PernatModel();
        this.niveleduca = new NiveleducaModel();
        this.explaboral = new ExplaboralModel();
        this.datretorno = new DatretornoModel();
        this.inforlab = new InforlabModel();
        this.famiretModelDialog = new FamiretModel();
        this.displayDialog = false;
        this.departs = [];
        this.provins = [];
        this.distris = [];
        this.tipoDocList = [];
        this.generoList = [];
        this.paisNacimientoList = [];
        this.paisResidenciaList = [];
        this.estadoCivilList = [];
        this.gradoInstruccionList = [];
        this.salarioMinList = [];
        this.tiempoDesempleadoList = [];
        this.famiretModelList = [];
        this.vinculoList = [];
        this.vinculoList.push(new ComboModel('Padre', 'Padre', 0));
        this.vinculoList.push(new ComboModel('Madre', 'Madre', 0));
        this.vinculoList.push(new ComboModel('Hijo(a)', 'Hijo(a)', 0));
        this.vinculoList.push(new ComboModel('Primo(a)', 'Primo(a)', 0));
        this.vinculoList.push(new ComboModel('Tio(a)', 'Tio(a)', 0));
        this.tiempoDesempleadoList.push(new ComboModel('1 Año', '1', 0));
        this.tiempoDesempleadoList.push(new ComboModel('2 Años', '2', 0));
        this.tiempoDesempleadoList.push(new ComboModel('3 Años', '3', 0));
        this.tiempoDesempleadoList.push(new ComboModel('4 Años', '4', 0));
        this.salarioMinList.push(new ComboModel('600', '600', 0));
        this.salarioMinList.push(new ComboModel('800', '800', 0));
        this.salarioMinList.push(new ComboModel('1000', '1000', 0));
        this.salarioMinList.push(new ComboModel('2000', '2000', 0));
        this.salarioMinList.push(new ComboModel('3000', '3000', 0));
        this.salarioMinList.push(new ComboModel('4000', '4000', 0));
        this.salarioMinList.push(new ComboModel('5000', '5000', 0));
        this.salarioMinList.push(new ComboModel('6000', '6000', 0));
        this.salarioMinList.push(new ComboModel('7000', '7000', 0));
        this.salarioMinList.push(new ComboModel('8000', '8000', 0));
        this.salarioMinList.push(new ComboModel('9000', '9000', 0));
        this.gradoInstruccionList.push(new ComboModel('Ninguno', 'Ninguno', 0));
        this.gradoInstruccionList.push(new ComboModel('Bachiller', 'Bachiller', 0));
        this.gradoInstruccionList.push(new ComboModel('Magister', 'Magister', 0));
        this.gradoInstruccionList.push(new ComboModel('Doctorado', 'Doctorado', 0));
        this.estadoCivilList.push(new ComboModel('Soltero', 'Soltero', 0));
        this.estadoCivilList.push(new ComboModel('Casado', 'Casado', 0));
        this.estadoCivilList.push(new ComboModel('Viudo', 'Viudo', 0));
        this.estadoCivilList.push(new ComboModel('Divorciado', 'Divorciado', 0));
        this.tipoDocList.push(new ComboModel('DNI', 'DNI', 0));
        this.tipoDocList.push(new ComboModel('CE', 'CE', 0));
        this.generoList.push(new ComboModel('Masculino', 'M', 0));
        this.generoList.push(new ComboModel('Femenino', 'F', 0));
        this.paisNacimientoList.push(new ComboModel('Peru', '1', 0));
        this.paisNacimientoList.push(new ComboModel('Chile', '2', 0));
        this.paisResidenciaList.push(new ComboModel('Peru', '1', 0));
        this.paisResidenciaList.push(new ComboModel('Chile', '2', 0));
        this.block = true;
        this.regfichaRetornanteService.consultaDepas().subscribe(
            (deps: any) => {
                this.departs = [];
                // tslint:disable-next-line:forin
                for (const i in deps) {
                    this.departs.push(new ComboModel(deps[i].vDesdep, deps[i].vCoddep, 0));
                }
                console.log(this.departs);
                this.block = false;
            },
            (dist: ResponseWrapper) => {
                this.onError([{ severity: 'error', summary: 'Mensaje de Error', message: dist }]);
                this.block = false;
            });
    }

    onSubmit() {
    }

    submitNuevoUsuario() {
    }

    addRow() {
        this.famiretModelDialog = new FamiretModel();
        this.displayDialog = true;
    }

    saveRow() {
        if (this.validarRow() === true) {
            const famiretModelList = [...this.famiretModelList];
            famiretModelList.push(this.famiretModelDialog);
            this.famiretModelList = famiretModelList;
            this.famiretModelDialog = new FamiretModel();
            this.displayDialog = false;
        }
    }

    closeDialog() {
        this.famiretModelDialog = new FamiretModel();
        this.displayDialog = false;
    }

    delete(index: Number) {
        const famiretModelList: FamiretModel[] = [];
        // tslint:disable-next-line:forin
        for (const i in this.famiretModelList) {
            if (Number(i) !== index) {
                famiretModelList.push(this.famiretModelList[i]);
            }
        }
        this.famiretModelList = famiretModelList;
    }

    guardarTodo() {
        if (this.validarFormulario() === true) {
            this.block = true;
            this.regfichaRetornanteService.consultaPersonaValidaServicio(
                {
                    TipoDoc: this.pernatural.tipoDoc.name,
                    vNombres: this.pernatural.nombres,
                }).subscribe(
                (res: Pernatural) => {
                    const pernaturalSer: Pernatural = res;
                    if (pernaturalSer.Resultado) {
                        console.log(pernaturalSer);
                    } else {
                        this.onError([{
                            severity: 'error', summary: 'Mensaje de Error',
                            detail: 'Los datos del documento de identidad no corresponden a los ingresados.'
                        }])
                        this.block = false;
                    }
                },
                (res: ResponseWrapper) => { this.onError([{ severity: 'error', summary: 'Mensaje de Error', detail: res.json }]); this.block = false; });
        }
    }

    private validarFormulario(): Boolean {
        let flag: Boolean = true;
        if (this.pernatural.tipoDoc === undefined) {
            flag = false;
            this.onError({ message: 'Debe seleccionar un tipo de documento.' });
        } else if (this.pernatural.numDoc === undefined) {
            flag = false;
            this.onError({ message: 'Debe ingresar el numero de documento.' });
        } else if (this.pernatural.nombres === undefined) {
            flag = false;
            this.onError({ message: 'Debe ingresar el nombre completo tal cual su coumento de identidad.' });
        } else if (this.pernatural.genero === undefined) {
            flag = false;
            this.onError({ message: 'Debe seleccionar el genero.' });
        } else if (this.pernatural.estadoCivil === undefined) {
            flag = false;
            this.onError({ message: 'Debe seleccionar el estado civil.' });
        } else if (this.pernatural.nacionalidad === undefined) {
            flag = false;
            this.onError({ message: 'Debe ingresar la nacionalidad.' });
        } else if (this.pernatural.codPaisNacimiento === undefined) {
            flag = false;
            this.onError({ message: 'Debe seleccionar el pais de nacimiento.' });
        } else if (this.pernatural.fecNacimiento === undefined) {
            flag = false;
            this.onError({ message: 'Debe ingresar la fecha de nacimiento.' });
        } else if (this.pernatural.codPaisResidencia === undefined) {
            flag = false;
            this.onError({ message: 'Debe seleccionar el pais de residencia.' });
        } else if (this.pernatural.correo === undefined) {
            flag = false;
            this.onError({ message: 'Debe ingresar el correo electronico.' });
        } else if (this.niveleduca.gradoInstruccion1 === undefined) {
            flag = false;
            this.onError({ message: 'Debe ingresar el maximo grado de instruccion en peru.' });
        } else if (this.niveleduca.profesion1 === undefined) {
            flag = false;
            this.onError({ message: 'Debe ingresar la profesion en peru.' });
        } else if (this.niveleduca.gradoInstruccion2 === undefined) {
            flag = false;
            this.onError({ message: 'Debe ingresar el maximo grado de instruccion en el extranjero.' });
        } else if (this.niveleduca.profesion2 === undefined) {
            flag = false;
            this.onError({ message: 'Debe ingresar la profesion en el extranjero.' });
        } else if (this.explaboral.desOcupaPeru === undefined) {
            flag = false;
            this.onError({ message: 'Debe ingresar la ultima ocupacion en el Peru.' });
        } else if (this.explaboral.fecIniPeru === undefined) {
            flag = false;
            this.onError({ message: 'Debe ingresar la fecha de inicio de su ultima ocupacion en el Peru.' });
        } else if (this.explaboral.fecFinPeru === undefined) {
            flag = false;
            this.onError({ message: 'Debe ingresar la fecha de fin de su ultima ocupacion en el Peru.' });
        } else if (this.explaboral.desOcupaExtr === undefined) {
            flag = false;
            this.onError({ message: 'Debe ingresar la ultima ocupacion en el extranjero.' });
        } else if (this.explaboral.fecIniExtr === undefined) {
            flag = false;
            this.onError({ message: 'Debe ingresar la fecha de inicio de su ultima ocupacion en el extranjero.' });
        } else if (this.explaboral.fecFinExtr === undefined) {
            flag = false;
            this.onError({ message: 'Debe ingresar la fecha de fin de su ultima ocupacion en el extranjero.' });
        } else if (this.explaboral.fecFinExtr === undefined) {
            flag = false;
            this.onError({ message: 'Debe ingresar la fecha de fin de su ultima ocupacion en el extranjero.' });
        } else if (this.datretorno.depaOpc1 === undefined) {
            flag = false;
            this.onError({ message: 'Debe seleccionar un departamento de retorno opcion 1.' });
        } else if (this.datretorno.depaOpc2 === undefined) {
            flag = false;
            this.onError({ message: 'Debe seleccionar un departamento de retorno opcion 2.' });
        } else if (this.datretorno.fecSalidaPeru === undefined) {
            flag = false;
            this.onError({ message: 'Debe ingresar la fecha de salida del Peru.' });
        } else if (this.datretorno.fecRetornoPeru === undefined) {
            flag = false;
            this.onError({ message: 'Debe ingresar la fecha de retorno al Peru.' });
        } else if (this.datretorno.flagRetConFami === true && this.famiretModelList.length === 0) {
            flag = false;
            this.onError({ message: 'Debe ingresar a un miento de su familia.' });
        } else if (this.inforlab.salarioMin === undefined) {
            flag = false;
            this.onError({ message: 'Debe seleccionar el salario minimo que aspira ganar.' });
        } else if (this.inforlab.ocupacion1 === undefined) {
            flag = false;
            this.onError({ message: 'Debe ingresar como minimo una ocupacion en la podria desempeñarse.' });
        } else if (this.inforlab.ocupacion1 === undefined) {
            flag = false;
            this.onError({ message: 'Debe ingresar como minimo una ocupacion en la podria desempeñarse.' });
        } else if (this.inforlab.tiempoDesempleado === undefined) {
            flag = false;
            this.onError({ message: 'Debe seleccionar el tiempo de desempleado.' });
        }
        return flag;
    }
    private validarRow(): Boolean {
        let flag: Boolean = true;
        if (this.famiretModelDialog.edad === undefined) {
            flag = false;
            this.onErrorDialog({ message: 'Debe ingresar la edad del registro faltante en la lista de familia retorno' });
        } else if (this.famiretModelDialog.genero === undefined) {
            flag = false;
            this.onErrorDialog({ message: 'Debe ingresar el genero del registro faltante en la lista de familia retorno' });
        } else if (this.famiretModelDialog.nacionalidad === undefined) {
            flag = false;
            this.onErrorDialog({ message: 'Debe ingresar la nacionalidad del registro faltante en la lista de familia retorno' });
        } else if (this.famiretModelDialog.vinculo === undefined) {
            flag = false;
            this.onErrorDialog({ message: 'Debe ingresar el vinculo del registro faltante en la lista de familia retorno' });
        }
        return flag;
    }
    private onErrorDialog(error: any) {
        this.messagesDialog = [];
        this.messagesDialog.push({ severity: 'error', summary: 'Mensaje de Error', detail: error.message });
    }
    private onError(error: any) {
        this.messages = [];
        this.messages.push({ severity: 'error', summary: 'Mensaje de Error', detail: error.message });
    }
}

import { BaseEntity } from '../../../shared';
import { Usuario } from '../../../entities/usuario';

export class ExplaboralModel {
    constructor(
        public desOcupaPeru?: String,
        public fecIniPeru?: Date,
        public fecFinPeru?: Date,
        public desOcupaExtr?: String,
        public fecIniExtr?: Date,
        public fecFinExtr?: Date,
    ) {
    }
}

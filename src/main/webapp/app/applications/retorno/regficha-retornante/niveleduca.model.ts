import { BaseEntity } from '../../../shared';
import { ComboModel } from '../../general/combobox.model';

export class NiveleducaModel {
    constructor(
        public gradoInstruccion1?: ComboModel,
        public profesion1?: String,
        public gradoInstruccion2?: ComboModel,
        public profesion2?: String,
    ) {
    }
}

import { BaseEntity } from '../../../shared';
import { ComboModel } from '../../general/combobox.model';

export class FamiretModel {
    constructor(
        public vinculo?: ComboModel,
        public nacionalidad?: ComboModel,
        public genero?: ComboModel,
        public edad?: Number
    ) {
    }
}

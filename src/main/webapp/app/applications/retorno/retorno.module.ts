import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { customHttpProvider } from '../../blocks/interceptor/http.provider';
import { RegfichaRetornanteModule } from './regficha-retornante/regficha-retornante.module';

@NgModule({
    imports: [
        RegfichaRetornanteModule
    ],
    declarations: [],
    entryComponents: [],
    providers: [customHttpProvider()],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GatewayRetornoModule {}

import { Datlab } from './../models/datlab.model';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { JhiEventManager, JhiParseLinks } from 'ng-jhipster';
import { Message } from 'primeng/primeng';

import { Atencion } from '../models/atencion.model';
import { Trabajador } from '../models/trabajador.model';
import { Sucesor } from '../models/sucesor.model';
import { Empleador } from '../models/empleador.model';
import { Pernatural } from '../models/pernatural.model';
import { Tipdocident } from '../models/tipdocident.model';
import { ComboModel } from '../../general/combobox.model';
import { Motateselec } from '../models/motateselec.model';
import { Docinperdlb } from '../models/docinperdlb.model';
import { Docpresate } from '../models/docpresate.model';
import { Accadoate } from '../models/accadoate.model';
import { ITEMS_PER_PAGE,  ResponseWrapper } from '../../../shared';
import { AtencionTrabajadorService } from './atencion-trabajador.service';
import { RegistroAtencionWizardService } from './atencion-trabajador-wizard/registro-atencion-wizard.service';

@Component({
    selector: 'jhi-atencion-trabajador',
    templateUrl: './atencion-trabajador.component.html'
})
export class AtencionTrabajadorComponent implements OnInit {

    atenciones: Atencion[];
    selecAten: any;
    selectedSucesion: any;
    trabajadorSelec: Object;
    trabajadores: Trabajador[];
    tipoBusqueda = '1';
    tipodocs: Tipdocident[];
    selectedTipodoc: Tipdocident;
    vNumdoc: string;
    vNombre: string;
    vApaterno: string;
    vAmaterno: string;
    displayDialog: boolean;
    maxlengthDocIdent: number;
    direcciones: any[];
    pases: any[];
    numOficina = 5;
    paganterior = '0';
    motateselec: Motateselec[];
    mensajes: Message[] = [];
    block: boolean;
    fechaActual = new Date();

    constructor(
        private atencionTrabajadorService: AtencionTrabajadorService,
        private eventManager: JhiEventManager,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private registroAtencionWizardService: RegistroAtencionWizardService
    ) {
    }

    cargarTipoDocumentos() {
        this.atencionTrabajadorService.consultaTipoDocIdentidad().subscribe(
            (res: ResponseWrapper) => { this.tipodocs = res.json; },
            (res: ResponseWrapper) => { this.onError(res.json); }
        )
    };
    ngOnInit() {
        localStorage.setItem('atencion', null);
        localStorage.setItem('trabajador', null);
        localStorage.setItem('sucesor', null);
        this.fechaActual.setDate(this.fechaActual.getDate() - 1);
        console.log('FECHAACTUAL');
        console.log(this.fechaActual);
        this.cargarTipoDocumentos();
        this.registroAtencionWizardService.cambiarActividad(null);
        this.registroAtencionWizardService.cambiarBandPagAnterior(this.paganterior);
        this.registroAtencionWizardService.cambiarAtencion(new Atencion());
        this.registroAtencionWizardService.cambiarAccionadop(<Accadoate []>[]);
        this.registroAtencionWizardService.cambiarDatlab(new Datlab());
        this.registroAtencionWizardService.cambiarDocumentosPres(<Docpresate []>[]);
        this.registroAtencionWizardService.cambiarMotivos(<Motateselec []>[]);
        this.registroAtencionWizardService.cambiarEmpleador(new Empleador());
        this.registroAtencionWizardService.cambiarTrabajador(new Trabajador());
        this.registroAtencionWizardService.cambiarSucesor(new Sucesor());
    }

    inicializaTablas() {
        this.atenciones = [];
        this.trabajadores = undefined;
    }
    inicializarFormulario() {
        this.inicializaTablas();
        this.vNumdoc = '';
        this.vApaterno = '';
        this.vAmaterno = '';
        this.vNombre = '';
        // this.tippersona = '0';
        this.displayDialog = false;
        if (this.selectedTipodoc !== undefined) {
            this.maxlengthDocIdent = this.selectedTipodoc.nNumdigi;
        }
    }
    buscarTrabajador() {
        if (this.tipoBusqueda === '1') {
            if (this.selectedTipodoc === undefined) {
                this.mensajes = [];
                this.mensajes.push({ severity: 'warn', summary: 'Mensaje', detail: 'Seleccione tipo documento' });
                return;
            } else  if (this.vNumdoc === undefined || this.vNumdoc === null || this.vNumdoc === '') {
                this.mensajes = [];
                this.mensajes.push({severity: 'warn', summary: 'Mensaje de Alerta', detail: 'No se ha ingresado el número de documento'});
                return;
            } else if (this.vNumdoc.length !== Number(this.selectedTipodoc.nNumdigi) ) {
                let mensajeerror;
                mensajeerror = 'Para ' + this.selectedTipodoc.vDescorta + ' Debe ingresar ' + this.selectedTipodoc.nNumdigi + ' dígitos';
                this.mensajes = [];
                this.mensajes.push({ severity: 'warn', summary: 'Mensaje', detail: mensajeerror });
                return;
            }
            this.inicializaTablas();
            this.atencionTrabajadorService.findTrabajadorsByDocIdent(Number(this.selectedTipodoc.id), this.vNumdoc ).subscribe(
                (res: ResponseWrapper)  => {
                    // console.log(res.json);
                    this.trabajadores = res.json;
                },
                (res: ResponseWrapper) => { this.onError(res.json); }
            );
        } else {
            this.inicializaTablas();
            this.atencionTrabajadorService.findTrabajadorsByName(this.vNombre.toUpperCase(), this.vApaterno.toUpperCase(), this.vAmaterno.toUpperCase() ).subscribe(
                (res: ResponseWrapper) => {
                    this.trabajadores = res.json;
                },
                (res: ResponseWrapper) => { this.onError(res.json); }
            );
        }
    }

    seleccionarTrabajador(event) {
        this.consultaAtenciones(event.data.id);
        this.consultaPases(event.data.id);
        this.consultaDirecciones(event.data.id);
        // this.data.cambiarPase(event.data);
    }
    deseleccionarFilas(event) {
        this.trabajadorSelec = undefined;
        this.displayDialog = false;
    }
    cargarAtenciones() {
        if (this.trabajadorSelec === undefined) {
            this.mensajes = [];
            this.mensajes.push({severity: 'warn', summary: 'Mensaje de Alerta', detail: 'No se ha seleccionado al trabajador'});
            return;
        }
        this.displayDialog = true;
    }

    cancelar() {
        this.displayDialog = false;
    }

    cargarRegistroAtencion(actividad: string) {
        this.registroAtencionWizardService.cambiarActividad(null);
        // Validar si se envia una nueva atención(1) o se selecciona una atencion (2)
        const atencionEnviar: Atencion = (actividad === '1') ?  new Atencion() : this.selecAten.aten;
        this.registroAtencionWizardService.cambiarAtencion(atencionEnviar);
        localStorage.setItem('atencion', JSON.stringify(atencionEnviar));
        console.log('CAMBIO DE RUTA:ATENCION');
        console.log(atencionEnviar);
        this.registroAtencionWizardService.cambiarActividad(actividad);
        this.router.navigate(['/consultas/registro-atencion-trabajador', { outlets: { wizard: ['datos-trabajador'] } }]);
    }
    editarAtencion(res) {
        this.registroAtencionWizardService.cambiarAtencion(res.aten);
        this.registroAtencionWizardService.cambiarActividad('3');
        this.router.navigate(['/consultas/registro-atencion-trabajador', { outlets: { wizard: ['datos-trabajador'] } }]);
    }
    consultaAtenciones(id: string) {
        this.atencionTrabajadorService.findAtencionsByTrabajador(id).subscribe(
            (res: ResponseWrapper) => { this.atenciones = res.json; },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    consultaDirecciones(id: number) {
            this.atencionTrabajadorService.buscarDireccionesPerNat(id).subscribe(
                (res: ResponseWrapper) => { this.direcciones = res.json; },
                (res: ResponseWrapper) => { this.onError(res.json); }
            );
    }
    consultaPases(id: number) {
        this.atencionTrabajadorService.findPasesByTrabajador(id).subscribe(
            (res: ResponseWrapper) => { this.pases = res.json; },
            (res: ResponseWrapper) => { this.onError(res.json); }
        );
    }
    private onError(error) {
        // this.jhiAlertService.error(error.message, null, null);
    }
}

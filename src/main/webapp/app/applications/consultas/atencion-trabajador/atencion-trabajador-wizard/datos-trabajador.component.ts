
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DatePipe } from '@angular/common';

import { Subscription, Observable } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { Trabajador } from '../../models/trabajador.model';
import { AtencionTrabajadorService } from './../atencion-trabajador.service';
import { RegistroAtencionWizardService } from './registro-atencion-wizard.service';
import { SelectItem, DataTable } from 'primeng/primeng';
import { Message } from 'primeng/components/common/api';
import { ResponseWrapper } from '../../../../shared';

import { Sucesor } from '../../models/sucesor.model';
import { Atencion } from '../../models/atencion.model';
import { Dirpernat } from '../../models/dirpernat.model';
import { Pernatural } from '../../models/pernatural.model';
import { Tipdocident } from '../../models/tipdocident.model';
import { Cartrab } from '../../models/cartrab.model';
import { ComboModel } from '../../../general/combobox.model';
import { ES } from './../../../applications.constant';

@Component({
    selector: 'jhi-datos-trabajador',
    templateUrl: './datos-trabajador.component.html'
})
export class DatosTrabajadorComponent implements OnInit, OnDestroy {
    // export class DatosTrabajadorComponent {

    atencion: any;
    // trabajador: Trabajador;
    trabajador: any = new Trabajador();
    pernatura: Pernatural = new Pernatural();
    sucesor: any = new Sucesor();
    pernatural: Pernatural;
    listadocident: Tipdocident[] = [];
    listacargo: Cartrab[] = [];
    tipodocs: Tipdocident[];
    // selectedTipodoc: Tipdocident;
    selectedTipodoc = new Tipdocident();
    selectedTipodocsu: Tipdocident;
    private subscription: Subscription;
    private atenSubscription: Subscription;
    private eventSubscriber: Subscription;
    private bandPantSuscriber: Subscription;
    private atenSuscriber: Subscription;
    vNumdocumento: string;
    vNumdocumentosu: string;
    routeSub: any;
    direccion: any;
    displayDialog: boolean;
    displayDialogsu: boolean;
    newDirec: boolean;
    newDirecsu: boolean;
    departamentos: SelectItem[];
    departs: ResponseWrapper;
    provins: ResponseWrapper;
    distris: ResponseWrapper;
    dirpernat: Dirpernat[];
    dirpersucesor: Dirpernat[];
    dirper = new Dirpernat();
    dirpersu = new Dirpernat();
    selecDirperDep: any;
    selecDirperProv: any;
    selecDirperDist: any;
    selecDirperDepsu: any;
    selecDirperProvsu: any;
    selecDirperDistsu: any;
    selecDirper: Dirpernat;
    selecDirpersu: Dirpernat;

    actividadSelec: string;

    fechoy: Date;
    maxlengthDocIdent: number;
    maxlengthDocIdentsu: number;
    paganterior: string;
    isVisible: boolean;

    es: any;
    dFecnac: Date;
    // dFecnac: String;
    accion: number;
    accionsu: number;
    predecesor: string;

    block: boolean;
    mensajes: Message[] = [];
    messagesDireccion: Message[] = [];

    isVisiblesu: boolean;
    DirDep: any;

    bandirec: any; // 0: nuevo modal direccion; 1: modificar modal direccion

    constructor(
        private eventManager: JhiEventManager,
        private atencionTrabajadorService: AtencionTrabajadorService,
        private router: Router,
        private registroAtencionWizard: RegistroAtencionWizardService,
        private datePipe: DatePipe,
        // private cartrabService: CartrabService,
        private route: ActivatedRoute
    ) {
    }

    loadTipoDoc() {
        this.atencionTrabajadorService.consultaTipoDocIdentidad().subscribe(
            (res: ResponseWrapper) => { this.tipodocs = res.json; },
            (res: ResponseWrapper) => { this.onError(res.json); }
        );
    }
    loadDirecPerNatu(id: any) {
        if (id !== undefined) {
                this.atencionTrabajadorService.buscarDireccionesPernatural(id).subscribe(
                (res: ResponseWrapper) => { this.dirpernat = res.json; this.isVisible = (this.dirpernat !== undefined) ? true : false; },
                (res: ResponseWrapper) => { this.onError(res.json); }
            );
        }
    }
    loadDirecPerSucesor(id: any) {
        this.atencionTrabajadorService.buscarDireccionesSucesor(id).subscribe(
            (res: ResponseWrapper) => { this.dirpersucesor = res.json; this.isVisiblesu = (this.dirpersucesor !== undefined) ? true : false; },
            (res: ResponseWrapper) => { this.onError(res.json); }
        );
    }

    inicializaTablas() {
        this.dirpernat = [];
        this.dirper = new Dirpernat();
        this.isVisible = false;
        this.isVisiblesu = false;
        this.dirpersucesor = [];
        this.dirpersu = new Dirpernat();
    }
    inicializarFormulario() {
        this.inicializaTablas();
        this.displayDialog = false;
        if (this.trabajador !== null) {
            this.trabajador.nFlgsuces = false;
            this.trabajador.id = undefined;
            this.trabajador.pernatural = new Pernatural();
            this.trabajador.cartrab = new Cartrab();
        }
    }

    changeTipdocident() {
        this.vNumdocumento = '';
        if (this.selectedTipodoc !== undefined) {
            this.maxlengthDocIdent = this.selectedTipodoc.nNumdigi;
        }
        this.inicializarFormulario();
    }

    changeTipdocidentsu() {
        this.vNumdocumentosu = '';
        if (this.selectedTipodocsu !== undefined) {
            this.maxlengthDocIdentsu = this.selectedTipodocsu.nNumdigi;
        }
        // this.inicializarFormulario();
        this.dirpersucesor = [];
        this.dirpersu = new Dirpernat();
    }

    buscaPernaturalByDocIdent() {
        if (this.selectedTipodocsu.id === undefined || this.vNumdocumentosu === undefined) {
            return;
        } else if (this.vNumdocumentosu === this.vNumdocumento) {
            return;
        }
        this.atencionTrabajadorService.findPernaturalByDocIdent(this.selectedTipodocsu.id, this.vNumdocumentosu).subscribe((personanatural) => {
            console.log('PERNATURALLOG')
            console.log(personanatural);
            this.sucesor = new Sucesor();
            if (personanatural.id !== undefined) {
                this.sucesor.trabajador = this.trabajador;
                this.sucesor.pernatural = personanatural;
                this.loadDirecPerSucesor(this.sucesor.pernatural.id);
                if (this.sucesor.pernatural.dFecnac !== undefined) {
                    this.sucesor.pernatural.dFecnac = new Date(this.sucesor.pernatural.dFecnac);
                } else {
                    this.sucesor.pernatural.dFecnac = null;
                }
            } else {
                this.sucesor.pernatural = new Pernatural();
                this.sucesor.trabajador = new Trabajador();
            }
            console.log('Sucesor luego de la busqueda');
            console.log(this.sucesor);
            // this.registerChangeSucesor();
        }, (res: Response) => {
            console.log('4567')
            this.buscarPersonasReniec2(this.selectedTipodocsu.id, this.vNumdocumentosu);
        //    this.errorBuscaPersona();
        });
    }
    validarBuscarTrabajador(): boolean {
        if (this.selectedTipodoc.id === undefined) {
            this.mensajes = [];
            this.mensajes.push({ severity: 'warn', summary: 'Mensaje', detail: 'Seleccione tipo documento' });
            return false;
        } else if (this.vNumdocumento.length !== Number(this.selectedTipodoc.nNumdigi) ) {
            let mensajeerror;
            mensajeerror = 'Para ' + this.selectedTipodoc.vDescorta + ' debe ingresar ' + this.selectedTipodoc.nNumdigi + ' dígitos';
            this.mensajes = [];
            this.mensajes.push({ severity: 'warn', summary: 'Mensaje', detail: mensajeerror });
            return false;
        } else {
            return true;
        }
    }
    generarFecha(fecha) {
        return new Date(fecha.getFullYear(), fecha.getMonth(), fecha.getDate());
    }

    buscaTrabajadorByDocIdent() {
        if (this.validarBuscarTrabajador()) {

            const tipodoc = this.selectedTipodoc.id; // 1;
            const numdoc =  this.vNumdocumento; //  '12345678';

            this.trabajador = new Trabajador();
            this.trabajador.pernatural = new Pernatural();
            this.sucesor = new Sucesor();
            this.sucesor.pernatural = new Pernatural();

            this.atencionTrabajadorService.findTrabajadorByDocIdent(tipodoc, numdoc).subscribe((trabajador: any) => {
                this.trabajador = trabajador;
                if (trabajador.pernatural.dFecnac) {
                    this.trabajador.pernatural.dFecnac = this.generarFecha(this.trabajador.pernatural.dFecnac);
                }
                // Si el trabajador es encontrado en los archivos locales.
                if (this.trabajador.id !== undefined) {
                    this.predecesor =       this.trabajador.pernatural.vNombres + ' ' +
                                            this.trabajador.pernatural.vApepat + ' ' + this.trabajador.pernatural.vApemat;
                    if (this.trabajador.nFlgsuces) {
                        this.atencionTrabajadorService.findSucesorByTrabajador(this.trabajador.id).subscribe((sucesor: any) => {
                            console.log('fecha nacimiento sucesoar: ' + sucesor.pernatural.dFecnac);
                            this.sucesor = sucesor;
                            if (this.sucesor.id !== undefined) {
                                this.sucesor.pernatural = sucesor.pernatural;
                                this.sucesor.trabajador = this.trabajador
                                this.selectedTipodocsu = this.sucesor.pernatural.tipdocident;
                                this.vNumdocumentosu = this.sucesor.pernatural.vNumdoc;
                                this.dirpersu.pernatural = this.sucesor.pernatural;
                                this.dirpersu = new Dirpernat();
                                if (this.sucesor.pernatural.dFecnac) {
                                    this.sucesor.pernatural.dFecnac = this.generarFecha(this.sucesor.pernatural.dFecnac);
                                }
                                this.dirpersucesor = [];
                                this.loadDirecPerSucesor(this.sucesor.pernatural.id);
                            } else {
                                this.sucesor = new Sucesor();
                                this.sucesor.pernatural.dFecnac = new Date();
                                this.sucesor.pernatural = new Pernatural();
                                this.sucesor.trabajador = new Trabajador();
                            }
                            // this.registerChangeSucesor();
                        });
                    } else {
                        this.loadDirecPerNatu(this.trabajador.pernatural.id);
                    }
                }
                console.log('1');
                // this.registerChangeInTrabajador();
                // this.registroAtencionWizard.trabajadorSeleccionado.subscribe((loadtrabajado) => {
                //     this.trabajador = loadtrabajado;
                // });
            }, (res: Response) => {
                console.log('ERROR TRABAJADOR');
                this.buscaPersonaNatural_Sistema(tipodoc, numdoc);
            });
        }
    }

    buscaPersonaNatural_Sistema(tipodoc, numdoc) {
        this.atencionTrabajadorService.findPernaturalByDocIdent(tipodoc, numdoc).subscribe((reprepersona) => {
            console.log('1234')
            this.pernatural = reprepersona;
            if (this.pernatural.id !== undefined) {
                this.predecesor = this.pernatural.vNombres + ' ' + this.pernatural.vApepat + ' ' + this.pernatural.vApemat;
                this.trabajador = new Trabajador();
                this.trabajador.pernatural = new Pernatural();
                this.trabajador.pernatural = this.pernatural;
                this.dirper.pernatural = this.pernatural;
                if (this.trabajador.pernatural.dFecnac) {
                    this.trabajador.pernatural.dFecnac = this.generarFecha(this.trabajador.pernatural.dFecnac);
                }
                this.loadDirecPerNatu(this.pernatural.id);
            }
        }, (res: Response) => {
            console.log('4567')
           this.buscarPersonasReniec(tipodoc, numdoc);
        //    this.errorBuscaPersona();
        });
    }
    buscarPersonasReniec(tipodoc, numdoc) {
        let tipodocumento = '';
        console.log('TIPODOC' + tipodoc);
        if (tipodoc === 1) {
            tipodocumento = 'DNI';
        }
        this.atencionTrabajadorService.getPersonaServicio({
            TipoDoc: tipodocumento, vNumdoc: numdoc,
        }).subscribe(
            (res: any) => {
                console.log(res);
                this.pernatural = res;
                this.predecesor = this.pernatural.vNombres + ' ' + this.pernatural.vApepat + ' ' + this.pernatural.vApemat;
                this.trabajador = new Trabajador();
                this.trabajador.pernatural = this.pernatural;
                this.dirper.pernatural = this.pernatural;
                const fechaNac = this.trabajador.pernatural.dFecnac;
                if (fechaNac) {
                    // this.trabajador.pernatural.dFecnac = this.generarFecha(fechaNac);
                    // console.log(fechaNac.substring(4, 6));
                    // console.log(fechaNac.substring(6, 8));
                    this.trabajador.pernatural.dFecnac = new Date(fechaNac.substring(0, 4), (fechaNac.substring(4, 6)) - 1, fechaNac.substring(6, 8));
                }
                // this.denunte = res;
                // this.nombreDenu = this.denunte.vNombres + ' ' + this.denunte.vApepat + ' ' + this.denunte.vApemat;
                // this.validRegDenunte = true;
                this.block = false;
            },
            (res: any) => {
                // this.numDocDenu = '';
                // this.nombreDenu = '';
                // this.onErrorDenunciante('Numero o tipo de documento incorrecto.');
                this.block = false;
            });
    }
    buscarPersonasReniec2(tipodoc, numdoc) {
        let tipodocumento = '';
        console.log('TIPODOC' + tipodoc);
        if (tipodoc === 1) {
            tipodocumento = 'DNI';
        }
        this.atencionTrabajadorService.getPersonaServicio({
            TipoDoc: tipodocumento, vNumdoc: numdoc,
        }).subscribe(
            (res: any) => {
                console.log(res);
                this.pernatural = res;
                this.sucesor = new Sucesor();
                this.sucesor.pernatural = this.pernatural;
                this.dirper.pernatural = this.pernatural;
                const fechaNac = this.sucesor.pernatural.dFecnac;
                if (fechaNac) {
                    // this.sucesor.pernatural.dFecnac = this.generarFecha(fechaNac);
                    // console.log(fechaNac.substring(4, 6));
                    // console.log(fechaNac.substring(6, 8));
                    this.sucesor.pernatural.dFecnac = new Date(fechaNac.substring(0, 4), (fechaNac.substring(4, 6)) - 1, fechaNac.substring(6, 8));
                }
                // this.denunte = res;
                // this.nombreDenu = this.denunte.vNombres + ' ' + this.denunte.vApepat + ' ' + this.denunte.vApemat;
                // this.validRegDenunte = true;
                this.block = false;
            },
            (res: any) => {
                // this.numDocDenu = '';
                // this.nombreDenu = '';
                // this.onErrorDenunciante('Numero o tipo de documento incorrecto.');
                this.block = false;
            });
    }

    ngOnInit() {
        this.accion = 1;
        this.es = ES;
        this.fechoy = new Date();
        this.atencion = new Atencion();
        this.atencion.vNumticket = '';
        this.trabajador.pernatural = new Pernatural();
        this.sucesor.pernatural = new Pernatural();
        this.sucesor.trabajador = new Trabajador();
        // this.inicializaTablas();
        this.loadTipoDoc();
        this.loadDepartamentos();

        // Se carga el tipo de actividad a realizar y los datos de la atención
        this.subscription = this.registroAtencionWizard.actividadSelec.subscribe((actividadsel) => {
            this.actividadSelec = actividadsel;
            if (actividadsel === null) { // Si la página se refresca se pierde la actividad y se redirige al inicio
                this.router.navigate(['/consultas/atencion-trabajador']);
                return;
            }
            console.log('de actividad');
            this.atencion = JSON.parse(localStorage.getItem('atencion'));
            const atencion = this.atencion;
            // this.atenSubscription = this.registroAtencionWizard.atenSeleccionado.subscribe((atencion) => {
            //     this.atencion = atencion;
                console.log('de atencion');
                console.log(atencion)
                const loadtrabajador = JSON.parse(localStorage.getItem('trabajador'));
                // this.registroAtencionWizard.trabajadorSeleccionado.subscribe((loadtrabajador) => {
                    console.log('de trabajador');
                    console.log(loadtrabajador);
                    if (atencion.vNumticket !== undefined) {
                        this.atencion.vNumticket = atencion.vNumticket.toUpperCase();
                    }
                    if (actividadsel === '1') { // Si el flujo es generado al clickear el boton nuevo registro se instanciaran los datos en blanco
                        console.log('trabajador undefined');
                        this.isVisible = false;
                        this.isVisiblesu = false;
                        this.dirpernat = [];
                        this.dirper = new Dirpernat();
                        console.log('LOADTRABAJADOR');
                        console.log(loadtrabajador)
                        if (loadtrabajador !== null) {
                            this.trabajador = loadtrabajador;
                            this.predecesor = loadtrabajador.pernatural.vNombres + ' ' + loadtrabajador.pernatural.vApepat + ' ' + loadtrabajador.pernatural.vApemat;
                            if (this.trabajador.pernatural.dFecnac) {
                                this.trabajador.pernatural.dFecnac = this.generarFecha(new Date(loadtrabajador.pernatural.dFecnac));
                            }
                            console.log('LOAD');
                            console.log(this.trabajador);
                            this.selectedTipodoc = this.trabajador.pernatural.tipdocident;
                            this.vNumdocumento = this.trabajador.pernatural.vNumdoc;
                            if (JSON.parse(localStorage.getItem('sucesor')) !== null ) {
                                this.sucesor = JSON.parse(localStorage.getItem('sucesor'))
                                if (loadtrabajador.nFlgsuces) {
                                    if (this.sucesor.pernatural.dFecnac) {
                                        this.sucesor.pernatural.dFecnac = this.generarFecha(new Date(this.sucesor.pernatural.dFecnac));
                                    }
                                }
                                this.selectedTipodocsu = this.sucesor.pernatural.tipdocident;
                                this.vNumdocumentosu = this.sucesor.pernatural.vNumdoc;
                            } else {
                                this.sucesor = new Sucesor();
                                this.sucesor.pernatural = new Pernatural();
                                this.sucesor.trabajador = new Trabajador();
                            }
                        }
                        this.paganterior = '1';
                    } else if (actividadsel === '2') {

                        let trabajadorAten = loadtrabajador;
                        if (loadtrabajador === null) {
                            trabajadorAten = (atencion.datlab !== undefined ) ? this.atencion.datlab.trabajador : this.atencion.trabajador;
                        }
                        this.trabajador = trabajadorAten;
                        console.log(trabajadorAten);

                        if (this.trabajador.pernatural.dFecnac) {
                            this.trabajador.pernatural.dFecnac = this.generarFecha(new Date(trabajadorAten.pernatural.dFecnac));
                        }
                        this.selectedTipodoc = trabajadorAten.pernatural.tipdocident;
                        this.vNumdocumento = trabajadorAten.pernatural.vNumdoc;
                        this.dirper = new Dirpernat();
                        this.dirper.pernatural = trabajadorAten.pernatural;
                        this.loadDirecPerNatu(trabajadorAten.pernatural.id);
                        this.predecesor = trabajadorAten.pernatural.vNombres + ' ' + trabajadorAten.pernatural.vApepat + ' ' + trabajadorAten.pernatural.vApemat;
                        this.dirpersu = new Dirpernat();
                        this.dirpersucesor = [];
                        if (JSON.parse(localStorage.getItem('sucesor')) !== null ) {
                            this.sucesor = JSON.parse(localStorage.getItem('sucesor'))
                        } else {
                            this.sucesor = new Sucesor();
                            this.sucesor.pernatural = new Pernatural();
                            this.sucesor.trabajador = new Trabajador();
                        }
                        console.log('TrabajadorFlag' + trabajadorAten.nFlgsuces);
                        if (trabajadorAten.nFlgsuces) {
                            console.log('Sucesor');
                            console.log(this.sucesor);
                            if (this.sucesor.pernatural.dFecnac) {
                                this.sucesor.pernatural.dFecnac = this.generarFecha(new Date(this.sucesor.pernatural.dFecnac));
                            }
                            this.selectedTipodocsu = this.sucesor.pernatural.tipdocident;
                            this.vNumdocumentosu = this.sucesor.pernatural.vNumdoc;
                            if (this.sucesor.id === undefined) {
                                this.atencionTrabajadorService.findSucesorByTrabajador(this.trabajador.id).subscribe((sucesor) => {
                                    this.sucesor = sucesor;
                                    console.log('SUCESOR1');
                                    console.log(this.sucesor);
                                    this.selectedTipodocsu = this.sucesor.pernatural.tipdocident;
                                    this.vNumdocumentosu = this.sucesor.pernatural.vNumdoc;
                                    this.dirpersu.pernatural = this.sucesor.pernatural;
                                    this.sucesor.pernatural.dFecnac = new Date(this.sucesor.pernatural.dFecnac);
                                    this.loadDirecPerSucesor(this.sucesor.pernatural.id);
                                });
                            }
                        }
                    }

                    //     if (atencion.datlab !== undefined ) { // Si la atencion datos laborales se obtienen los datos del trabajador de esta entidad
                    //         // this.trabajador =  this.atencion.datlab.trabajador;
                    //         // this.trabajador.pernatural = this.atencion.datlab.trabajador.pernatural;
                    //         // this.selectedTipodoc = this.atencion.datlab.trabajador.pernatural.tipdocident;
                    //         // this.vNumdocumento = this.atencion.datlab.trabajador.pernatural.vNumdoc;
                    //         // this.dirper = new Dirpernat();
                    //         // this.dirper.pernatural = this.trabajador.pernatural;
                    //         // this.loadDirecPerNatu(this.trabajador.pernatural.id);
                    //         // this.predecesor = this.trabajador.pernatural.vNombres + ' ' + this.trabajador.pernatural.vApepat + ' ' + this.trabajador.pernatural.vApemat;
                    //         // this.dirpersu = new Dirpernat();
                    //         // this.dirpersucesor = [];
                    //         // this.sucesor = new Sucesor();
                    //         // this.sucesor.pernatural = new Pernatural();
                    //         // this.sucesor.trabajador = new Trabajador();
                    //         if (this.trabajador.nFlgsuces) {
                    //             // console.log('atencion datlab trabajador con sucesor');
                    //             this.registroAtencionWizard.sucesorSeleccionado.subscribe((loadSucesor) => {
                    //                 this.sucesor = loadSucesor;
                    //                 // this.sucesor.pernatural = loadSucesor.pernatural;
                    //                 // console.log('Atencion.datlab.Sucesor1: ' + JSON.stringify(this.sucesor));
                    //                 if (this.sucesor.id === undefined) {
                    //                     // console.log('atencion datlab trabajador sucesor aun no grabado');
                    //                     this.sucesor = new Sucesor();
                    //                     this.sucesor.pernatural = new Pernatural();
                    //                     this.sucesor.trabajador = new Trabajador();
                    //                     // console.log('buscar sucesor por id trabajador: ' + this.trabajador.id);
                    //                     this.atencionTrabajadorService.findSucesorByTrabajador(this.trabajador.id).subscribe((sucesor) => {
                    //                         // console.log('sucesor: ', sucesor);
                    //                         this.sucesor = sucesor;
                    //                         // console.log('Atencion.datlab.Sucesor2: ' + JSON.stringify(this.sucesor));
                    //                         if (this.sucesor.id !== undefined) {
                    //                             // console.log('atencion datlab trabajdor sucesor encontrado: ');
                    //                             this.sucesor.pernatural = sucesor.pernatural;
                    //                             this.sucesor.trabajador = sucesor.trabajador;
                    //                             this.selectedTipodocsu = this.sucesor.pernatural.tipdocident;
                    //                             this.vNumdocumentosu = this.sucesor.pernatural.vNumdoc;
                    //                             console.log('nueva prueba');
                    //                             this.dirpersu.pernatural = this.sucesor.pernatural;
                    //                             // console.log(this.sucesor.pernatural.dFecnac);
                    //                             // console.log(this.sucesor);
                    //                             // if (this.sucesor.pernatural.dFecnac !== undefined) {
                    //                                 console.log('estamos aca y queremos probar lo que necesitamos');
                    //                                 this.sucesor.pernatural.dFecnac = new Date(this.sucesor.pernatural.dFecnac);
                    //                             // }
                    //                             this.loadDirecPerSucesor(this.sucesor.pernatural.id);
                    //                             // this.registerChangeSucesor();
                    //                         } else {
                    //                             // console.log('atencion datlab trabajador sucesor no encntrado');
                    //                             this.dirpersu = new Dirpernat();
                    //                             this.dirpersucesor = [];
                    //                             this.sucesor = new Sucesor();
                    //                             this.sucesor.pernatural = new Pernatural();
                    //                             this.sucesor.trabajador = new Trabajador();
                    //                         }
                    //                     });
                    //                 } else if (this.sucesor.id !== undefined) {
                    //                         // console.log('Existe Sucesor!');
                    //                         // console.log('atencion datlab trabajador sucesor ya grabado');
                    //                         this.sucesor.pernatural = loadSucesor.pernatural;
                    //                         this.sucesor.trabajador = loadSucesor.trabajador;
                    //                         console.log('estamos aca y queremos probar lo que necesitamos');
                    //                         this.sucesor.pernatural.dFecnac = new Date(this.sucesor.pernatural.dFecnac);
                    //                         this.selectedTipodocsu = this.sucesor.pernatural.tipdocident;
                    //                         this.vNumdocumentosu = this.sucesor.pernatural.vNumdoc;
                    //                         this.dirpersu.pernatural = this.sucesor.pernatural;
                    //                         this.loadDirecPerSucesor(this.sucesor.pernatural.id);
                    //                 }
                    //             });
                    //         } else {
                    //             // console.log('atencion datlab trabajador sin sucesor');
                    //             this.dirpersu = new Dirpernat();
                    //             this.dirpersucesor = [];
                    //             this.sucesor = new Sucesor();
                    //             this.sucesor.pernatural = new Pernatural();
                    //             this.sucesor.trabajador = new Trabajador();
                    //         }
                    //     } else { // Si la atención no tiene datos laborales se carga la información de la propia atención.
                    //         if (this.atencion.trabajador !== undefined) {
                    //         console.log('datos de la atencion');
                    //         this.trabajador =  this.atencion.trabajador;
                    //         this.trabajador.pernatural = this.atencion.trabajador.pernatural;
                    //         this.selectedTipodoc = this.atencion.trabajador.pernatural.tipdocident;
                    //         this.vNumdocumento = this.atencion.trabajador.pernatural.vNumdoc;
                    //         // this.dFecnac = new Date(this.atencion.trabajador.pernatural.dFecnac);
                    //         this.dFecnac = this.atencionTrabajadorService.convertFechas(this.atencion.trabajador.pernatural.dFecnac);
                    //         this.dirper = new Dirpernat();
                    //         this.dirper.pernatural = this.trabajador.pernatural;
                    //         this.loadDirecPerNatu(this.trabajador.pernatural.id);
                    //         this.predecesor = this.trabajador.pernatural.vNombres + ' ' + this.trabajador.pernatural.vApepat + ' ' + this.trabajador.pernatural.vApemat;
                    //         this.sucesor = new Sucesor();
                    //         this.sucesor.pernatural = new Pernatural();
                    //         this.sucesor.trabajador = new Trabajador();
                    //         if (this.trabajador.nFlgsuces) {
                    //             // console.log('atencion trabajador con sucesor');
                    //             this.registroAtencionWizard.sucesorSeleccionado.subscribe((loadSucesor) => {
                    //                 this.sucesor = loadSucesor;
                    //                 // console.log('Atencion.trabajador.Sucesor3: ' + JSON.stringify(this.sucesor));
                    //                 if (this.sucesor.id === undefined) {
                    //                     // console.log('atencion trabajador sucesor aun no grabado');
                    //                     this.sucesor = new Sucesor();
                    //                     this.sucesor.pernatural = new Pernatural();
                    //                     this.sucesor.trabajador = new Trabajador();
                    //                     this.atencionTrabajadorService.findSucesorByTrabajador(this.trabajador.id).subscribe((sucesor) => {
                    //                         this.sucesor = sucesor;
                    //                         // console.log('Atencion.trabajador.Sucesor4: ' + JSON.stringify(this.sucesor));
                    //                         if (this.sucesor.id !== undefined) {
                    //                             // console.log('atencion trabajador sucesor encontrado');
                    //                             this.sucesor.pernatural = sucesor.pernatural;
                    //                             this.sucesor.trabajador = sucesor.trabajador;
                    //                             this.sucesor.pernatural.dFecnac = new Date(this.sucesor.pernatural.dFecnac);
                    //                             this.selectedTipodocsu = this.sucesor.pernatural.tipdocident;
                    //                             this.vNumdocumentosu = this.sucesor.pernatural.vNumdoc;
                    //                             this.dirpersu.pernatural = this.sucesor.pernatural;
                    //                             this.loadDirecPerSucesor(this.sucesor.pernatural.id);
                    //                             // this.registerChangeSucesor();
                    //                         } else {
                    //                             // console.log('atencion trabajador sucesor no encontrado');
                    //                             this.dirpersu = new Dirpernat();
                    //                             this.dirpersucesor = [];
                    //                             this.sucesor = new Sucesor();
                    //                             this.sucesor.pernatural = new Pernatural();
                    //                             this.sucesor.trabajador = new Trabajador();
                    //                         }
                    //                     });
                    //                 }
                    //                 if (this.sucesor.id !== undefined) {
                    //                     // console.log('atencion trabajador sucesor ya grabado');
                    //                     this.sucesor.pernatural = loadSucesor.pernatural;
                    //                     this.sucesor.trabajador = loadSucesor.trabajador;
                    //                     this.selectedTipodocsu = this.sucesor.pernatural.tipdocident;
                    //                     this.vNumdocumentosu = this.sucesor.pernatural.vNumdoc;
                    //                     this.dirpersu.pernatural = this.sucesor.pernatural;
                    //                     this.loadDirecPerSucesor(this.sucesor.pernatural.id);
                    //                 }
                    //             });
                    //         } else {
                    //             // console.log('atencion trabajador sin sucesor');
                    //             this.dirpersu = new Dirpernat();
                    //             this.dirpersucesor = [];
                    //             this.sucesor = new Sucesor();
                    //             this.sucesor.pernatural = new Pernatural();
                    //             this.sucesor.trabajador = new Trabajador();
                    //         }
                    //         }
                    //     }
                    // } else {
                    //     this.trabajador = loadtrabajador;
                    //     if (loadtrabajador !== undefined) {
                    //         this.trabajador = loadtrabajador;
                    //         if (loadtrabajador.pernatural !== undefined) {
                    //             // this.trabajador.pernatural = new Pernatural();
                    //             this.trabajador.pernatural = loadtrabajador.pernatural;
                    //             console.log('pernatural id del load' + JSON.stringify(this.trabajador.pernatural));
                    //             if (this.trabajador.pernatural.id !== undefined) {
                    //                 this.isVisible = true;
                    //                 this.dirper = new Dirpernat();
                    //                 this.dirpernat = [];
                    //                 this.dirper.pernatural = this.trabajador.pernatural;
                    //                 this.dirpersu = new Dirpernat();
                    //                 this.dirpersucesor = [];
                    //                 // console.log('Load Trabajador.Personanatural: ' + JSON.stringify(this.trabajador.pernatural));
                    //                 this.selectedTipodoc = this.trabajador.pernatural.tipdocident;
                    //                 this.vNumdocumento = this.trabajador.pernatural.vNumdoc;
                    //                 // this.trabajador.pernatural.dFecnac = new Date(this.trabajador.pernatural.dFecnac);
                    //                 // this.dFecnac = new Date(this.trabajador.pernatural.dFecnac);
                    //                 // this.dFecnac = this.atencionTrabajadorService.convertFechas(this.trabajador.pernatural.dFecnac);
                    //                     this.loadDirecPerNatu(this.trabajador.pernatural.id);
                    //                 this.predecesor = this.trabajador.pernatural.vNombres + ' '
                    //                 + this.trabajador.pernatural.vApepat + ' ' + this.trabajador.pernatural.vApemat;
                    //                 this.sucesor = new Sucesor();
                    //                 this.sucesor.pernatural = new Pernatural();
                    //                 this.sucesor.trabajador = new Trabajador();
                    //                 if (this.trabajador.nFlgsuces) {
                    //                     // console.log('Trabajador con sucesor 1');
                    //                     this.registroAtencionWizard.sucesorSeleccionado.subscribe((loadSucesor) => {
                    //                         this.sucesor = loadSucesor;
                    //                     //  if (this.sucesor.id !== undefined) {
                    //                         if (this.sucesor !== undefined) {
                    //                             if (this.sucesor.pernatural !== undefined) {
                    //                             // console.log('sucesor ya grabado 1');
                    //                             this.sucesor.pernatural = loadSucesor.pernatural;
                    //                             this.sucesor.trabajador = loadSucesor.trabajador;
                    //                             this.selectedTipodocsu = this.sucesor.pernatural.tipdocident;
                    //                             this.vNumdocumentosu = this.sucesor.pernatural.vNumdoc;
                    //                             // this.dFecnac = this.atencionTrabajadorService.convertFechas(this.trabajador.pernatural.dFecnac);
                    //                             this.dirpersu.pernatural = this.sucesor.pernatural;
                    //                                 this.isVisiblesu = true;
                    //                                 this.loadDirecPerSucesor(this.sucesor.pernatural.id);
                    //                             }
                    //                         } else {
                    //                             // console.log('tiene sucesor pero aun no se ha grabado');
                    //                             // this.sucesor = new Sucesor();
                    //                             // this.sucesor.pernatural = new Pernatural();
                    //                             // this.sucesor.trabajador = new Trabajador();
                    //                             // this.selectedTipodocsu = undefined;
                    //                             // this.vNumdocumentosu = '';
                    //                         }
                    //                     });
                    //                 }
                    //             } else {
                    //                 console.log('id pernarural undefined');
                    //                 this.trabajador.pernatural = new Pernatural();
                    //             }
                    //         }else {
                    //             console.log('pernatural undefined');
                    //             this.trabajador.pernatural = new Pernatural();
                    //             // this.selectedTipodoc = new Tipdocident();
                    //             // this.vNumdocumento = '';
                    //             // this.dirper = new Dirpernat();
                    //         }
                    //     }
                    // }
                this.registerChangePaganterior();
                // });
            // });
            console.log('antes de registrar trabajador: ' + JSON.stringify(this.trabajador));
        });
        this.registerChangeInTrabajador();
    }
    ngOnDestroy() {
        if (this.subscription !== undefined) {
            this.subscription.unsubscribe();
        }
        if (this.atenSubscription !== undefined) {
            this.atenSubscription.unsubscribe();
        }
        if (this.eventSubscriber !== undefined) {
            this.eventSubscriber.unsubscribe();
        }
    }

    load(id) {
        this.atencionTrabajadorService.findTrabajadorById(id).subscribe((trabajador) => {
            this.trabajador = trabajador;
        });
    }
    loadDepartamentos() {
        this.atencionTrabajadorService.consDep().subscribe((departamentos) => {
            this.departs = departamentos.json;
            console.log(departamentos);
        });
    }
    /*loadProvincias(init: boolean, idDept) {
        this.atencionTrabajadorService.consProv(this.padWithZero(idDept)).subscribe((provincias) => {
            this.provins = provincias.json;
            if (init) {
                this.dirper.nCodprov = Number(this.provins[0].vCodpro);
                // this.loadDistritos(true, this.provins[0].vCodpro);
            }
        });
    }*/

    loadProvincias() {
        this.atencionTrabajadorService.consProv(this.padWithZero(this.selecDirperDep.vCoddep)).subscribe((provincias) => {
            this.dirper.nCoddepto =  Number(this.selecDirperDep.vCoddep);
            console.log(this.selecDirperDep);
            this.dirper.nCodprov = undefined;
            this.dirper.nCoddist = undefined;
            this.distris = undefined;
            this.provins = provincias.json;
        });
    }

    loadProvinciasUpdate() {
        this.atencionTrabajadorService.consProv(this.padWithZero(this.selecDirperDep.vCoddep)).subscribe((provincias) => {
            this.dirper.nCoddepto =  Number(this.selecDirperDep.vCoddep);
            console.log(this.selecDirperDep);
            // this.dirper.nCodprov = undefined;
            // this.dirper.nCoddist = undefined;
            // this.distris = undefined;
            this.provins = provincias.json;
        });
    }

    /*loadDistritos(init: boolean, idProv) {
        this.atencionTrabajadorService.consDis(this.padWithZero(this.dirper.nCoddepto), this.padWithZero(idProv)).subscribe((distritos) => {
            this.distris = distritos.json;
            if (init) {
                this.dirper.nCoddist = Number(this.distris[0].vCoddis);
            }
        });
    }*/

    loadDistritos() {
        console.log('antes del loaddistritos');
        console.log(this.selecDirperDep.vCoddep);
        console.log(this.selecDirperProv.vCodpro);
        this.atencionTrabajadorService.consDis(this.padWithZero(this.selecDirperDep.vCoddep), this.padWithZero(this.selecDirperProv.vCodpro)).subscribe((distritos) => {
            console.log(this.selecDirperProv);
            this.dirper.nCodprov = Number(this.selecDirperProv.vCodpro);
            this.dirper.nCoddist = undefined;
            this.distris = distritos.json;
            console.log(this.distris);
        });
    }

    loadDistritosUpdate() {
        console.log('antes del loaddistritos');
        console.log(this.selecDirperDep.vCoddep);
        console.log(this.selecDirperProv.vCodpro);
        this.atencionTrabajadorService.consDis(this.padWithZero(this.selecDirperDep.vCoddep), this.padWithZero(this.selecDirperProv.vCodpro)).subscribe((distritos) => {
            console.log(this.selecDirperProv);
            this.dirper.nCodprov = Number(this.selecDirperProv.vCodpro);
            // this.dirper.nCoddist = undefined;
            this.distris = distritos.json;
            console.log(this.distris);
        });
    }

    // loadProvinciassu(init: boolean, idDept) {
    //     this.atencionTrabajadorService.consProv(this.padWithZero(idDept)).subscribe((provincias) => {
    //         this.provins = provincias.json;
    //         if (init) {
    //             this.dirpersu.nCodprov = Number(this.provins[0].vCodpro);
    //             this.loadDistritossu(true, this.provins[0].vCodpro);
    //         }
    //     });
    // }

    loadProvinciassu() {
        console.log(this.selecDirperDepsu);
        this.atencionTrabajadorService.consProv(this.padWithZero(this.selecDirperDepsu.vCoddep)).subscribe((provincias) => {
            this.dirper.nCoddepto =  Number(this.selecDirperDepsu.vCoddep);
            console.log(this.selecDirperDepsu);
            this.dirper.nCodprov = undefined;
            this.dirper.nCoddist = undefined;
            this.distris = undefined;
            this.provins = provincias.json;
        });
    }

    loadProvinciasUpdatesu() {
        this.atencionTrabajadorService.consProv(this.padWithZero(this.selecDirperDepsu.vCoddep)).subscribe((provincias) => {
            this.dirper.nCoddepto =  Number(this.selecDirperDepsu.vCoddep);
            console.log(this.selecDirperDepsu);
            // this.dirper.nCodprov = undefined;
            // this.dirper.nCoddist = undefined;
            // this.distris = undefined;
            this.provins = provincias.json;
        });
    }

    // loadDistritossu(init: boolean, idProv) {
    //     this.atencionTrabajadorService.consDis(this.padWithZero(this.dirpersu.nCoddepto), this.padWithZero(idProv)).subscribe((distritos) => {
    //         this.distris = distritos.json;
    //         if (init) {
    //             this.dirpersu.nCoddist = Number(this.distris[0].vCoddis);
    //         }
    //     });
    // }

    loadDistritossu() {
        console.log('antes del loaddistritos');
        console.log(this.selecDirperDepsu.vCoddep);
        console.log(this.selecDirperProvsu.vCodpro);
        this.atencionTrabajadorService.consDis(this.padWithZero(this.selecDirperDepsu.vCoddep), this.padWithZero(this.selecDirperProvsu.vCodpro)).subscribe((distritos) => {
            console.log(this.selecDirperProvsu);
            this.dirper.nCodprov = Number(this.selecDirperProvsu.vCodpro);
            this.dirper.nCoddist = undefined;
            this.distris = distritos.json;
            console.log(this.distris);
        });
    }

    loadDistritosUpdatesu() {
        console.log('antes del loaddistritos');
        console.log(this.selecDirperDepsu.vCoddep);
        console.log(this.selecDirperProvsu.vCodpro);
        this.atencionTrabajadorService.consDis(this.padWithZero(this.selecDirperDepsu.vCoddep), this.padWithZero(this.selecDirperProvsu.vCodpro)).subscribe((distritos) => {
            console.log(this.selecDirperProvsu);
            this.dirper.nCodprov = Number(this.selecDirperProvsu.vCodpro);
            // this.dirper.nCoddist = undefined;
            this.distris = distritos.json;
            console.log(this.distris);
        });
    }

    showDialogToAdd() {
        this.newDirec = true;
        this.displayDialog = true;
        this.accion = 1;
        this.selecDirperDep = undefined;
        this.selecDirperProv = undefined;
        this.selecDirperDist = undefined;
        this.provins = undefined;
        this.distris = undefined;
    }

    showDialogToAddsu() {
        this.newDirecsu = true;
        this.displayDialogsu = true;
        this.accionsu = 1;
        this.selecDirperDepsu = undefined;
        this.selecDirperProvsu = undefined;
        this.selecDirperDistsu = undefined;
        this.provins = undefined;
        this.distris = undefined;
    }

    showDialogToAction(rowIndex, accion: number) {
        this.accion = accion;
        this.direccion = this.dirpernat[rowIndex];
        this.dirper = this.direccion.direc
        this.newDirec = false;
        this.displayDialog = true;
    }

    showDialogToActionsu(rowIndex, accion: number) {
        this.accionsu = accion;
        this.direccion = this.dirpersucesor[rowIndex];
        this.dirpersu = this.direccion.direc
        console.log('direccion de sucesor: ' + JSON.stringify(this.dirpersu));
        this.newDirecsu = false;
        this.displayDialogsu = true;
    }

    onRowSelect(event) {
        this.newDirec = false;
        this.dirper = this.cloneDirec(event.data.direc);
        this.selecDirperDep = {vCoddep: this.padWithZero(this.dirper.nCoddepto), vDesdep: event.data.dpto};
        this.loadProvinciasUpdate();
        this.selecDirperProv = {vCodpro: this.padWithZero(this.dirper.nCodprov), vCoddep: this.padWithZero(this.dirper.nCoddepto), vDespro: event.data.prov};
        console.log('antes de cargar la info de distritos');
        console.log(this.selecDirperDep);
        console.log(this.selecDirperProv);
        this.loadDistritosUpdate();
        this.selecDirperDist = {vCoddep: this.padWithZero(this.dirper.nCoddepto),
        vCoddis: this.padWithZero(this.dirper.nCoddist),
        vCodpro: this.padWithZero(this.dirper.nCodprov), vDesdis: event.data.dist };
        this.displayDialog = true;
    }

    onRowSelectsu(event) {
        this.newDirecsu = false;
        this.dirpersu = this.cloneDirec(event.data.direc);
        this.selecDirperDepsu = {vCoddep: this.padWithZero(this.dirpersu.nCoddepto), vDesdep: event.data.dpto};
        this.loadProvinciasUpdatesu();
        this.selecDirperProvsu = {vCodpro: this.padWithZero(this.dirpersu.nCodprov), vCoddep: this.padWithZero(this.dirpersu.nCoddepto), vDespro: event.data.prov};
        console.log('antes de cargar la info de distritos');
        console.log(this.selecDirperDepsu);
        console.log(this.selecDirperProvsu);
        this.loadDistritosUpdatesu();
        this.selecDirperDistsu = {vCoddep: this.padWithZero(this.dirpersu.nCoddepto),
        vCoddis: this.padWithZero(this.dirpersu.nCoddist),
        vCodpro: this.padWithZero(this.dirpersu.nCodprov), vDesdis: event.data.dist };
        this.displayDialogsu = true;
    }

    save() {
        if (this.ValidarDireccion() === true ) {
            this.dirper.vDircomple.toUpperCase();
            this.dirper.nCoddepto = Number(this.selecDirperDep.vCoddep);
            this.dirper.nCodprov = Number(this.selecDirperProv.vCodpro);
            this.dirper.nCoddist = Number(this.selecDirperDist.vCoddis);
            if (this.newDirec) {
                this.subscribeToSaveResponse(
                    this.atencionTrabajadorService.createDirPerNat(this.dirper));
            } else {
                this.subscribeToSaveResponse(
                    this.atencionTrabajadorService.updateDirPerNat(this.dirper));
            }
        }
    }

    ValidarDireccion() {
         if (this.selecDirperDep.vCoddep === undefined) {
            this.onErrorDireccion('Debe seleccionar un departamento');
            return false;
        } else if (this.selecDirperProv.vCodpro === undefined) {
            this.onErrorDireccion('Debe seleccionar una provincia');
            return false;
        } else if (this.selecDirperDist.vCoddis === undefined) {
            this.onErrorDireccion('Debe seleccionar una distrito');
            return false;
        } else if (this.dirper.vDircomple === undefined || this.dirper.vDircomple === '' ) {
            this.onErrorDireccion('Debe ingresar una dirección');
            return false;
        } else {
            return true;
        }
    }

    ValidarDireccionsu() {
        if (this.selecDirperDepsu.vCoddep === undefined) {
           this.onErrorDireccion('Debe seleccionar un departamento');
           return false;
       } else if (this.selecDirperProvsu.vCodpro === undefined) {
           this.onErrorDireccion('Debe seleccionar una provincia');
           return false;
       } else if (this.selecDirperDistsu.vCoddis === undefined) {
           this.onErrorDireccion('Debe seleccionar una distrito');
           return false;
       } else if (this.dirpersu.vDircomple === undefined || this.dirpersu.vDircomple === '' ) {
           this.onErrorDireccion('Debe ingresar una dirección');
           return false;
       } else {
           return true;
       }
   }

    private onErrorDireccion(error: any) {
        this.messagesDireccion = [];
        this.messagesDireccion.push({ severity: 'warn', summary: 'Mensaje de Error:', detail: error });
    }

    savesu() {
        // if (this.newDirecsu) {
        //     console.log('grabando direccion del sucesor');
        //     this.subscribeToSaveResponsesu(
        //          this.atencionTrabajadorService.createDirPerNat(this.dirpersu));
        // } else {
        //     console.log('actualizando direccion del sucesor');
        //     this.subscribeToSaveResponsesu(
        //         this.atencionTrabajadorService.updateDirPerNat(this.dirpersu));
        // }
        // this.dirpersu = new Dirpernat();

        if (this.ValidarDireccionsu() === true ) {
            this.dirpersu.vDircomple.toUpperCase();
            this.dirpersu.nCoddepto = Number(this.selecDirperDepsu.vCoddep);
            this.dirpersu.nCodprov = Number(this.selecDirperProvsu.vCodpro);
            this.dirpersu.nCoddist = Number(this.selecDirperDistsu.vCoddis);
            console.log(this.dirpersu);
            if (this.newDirecsu) {
                this.subscribeToSaveResponsesu(
                    this.atencionTrabajadorService.createDirPerNat(this.dirpersu));
            } else {
                this.subscribeToSaveResponsesu(
                    this.atencionTrabajadorService.updateDirPerNat(this.dirpersu));
            }
        }
    }

    close() {
        this.dirper = new Dirpernat();
        this.dirper.pernatural = this.trabajador.pernatural;
        this.displayDialog = false;
    }

    closesu() {
        this.dirpersu = new Dirpernat();
        this.dirpersu.pernatural = this.trabajador.pernatural;
        this.displayDialogsu = false;
    }

    delete() {
        // this.newDirec = false;
        this.dirper.nFlgactivo = false;
        this.save();
    }

    deletesu() {
        this.dirpersu.nFlgactivo = false;
        this.savesu();
    }
    errorBuscaPersona()  {
        this.mensajes = [];
        this.mensajes.push({severity: 'error', summary: 'Mensaje de Búsqueda', detail: 'No se encotró persona con los datos de búsqueda.'});
    }
    registerChangeSucesor() {
        // console.log('Registrar Sucesor: ' + JSON.stringify(this.sucesor));
        // this.sucesor.pernatural.tipdocident = this.selectedTipodocsu;
        // this.sucesor.pernatural.vNumdoc = this.vNumdocumentosu;
        this.registroAtencionWizard.cambiarSucesor(this.sucesor);
        // this.registroAtencionWizard.sucesorSeleccionado.subscribe((loadSucesor) => {
        //     this.sucesor = loadSucesor;
        // });
    }

    registerChangeInTrabajador() {
        this.eventSubscriber = this.eventManager.subscribe('saveTrabajador',
        (response) => {
            console.log('PasarTrabajador' + JSON.stringify(this.trabajador));
            this.trabajador.pernatural.tipdocident = this.selectedTipodoc;
            this.trabajador.pernatural.vNumdoc = this.vNumdocumento;
            localStorage.setItem('trabajador', JSON.stringify(this.trabajador));
            console.log(this.vNumdocumento);
            // this.registroAtencionWizard.cambiarEstadoStep();
            this.registroAtencionWizard.cambiarTrabajador(this.trabajador);
            if (this.sucesor !== undefined ) {
                console.log('PasarSucesor');
                console.log(this.sucesor);
                localStorage.setItem('sucesor', JSON.stringify(this.sucesor));
                this.registroAtencionWizard.cambiarSucesor(this.sucesor);
            }
        });
    }

    registerChangeAtencion(atencion: Atencion) {
        // this.atenSuscriber = this.eventManager.subscribe('saveAten',
        // (response) => {
            this.registroAtencionWizard.cambiarAtencion(atencion);
        // });
    }

    registerChangePaganterior() {
        // this.bandPantSuscriber = this.eventManager.subscribe('savePageAnte',
        // (response) => {
            this.registroAtencionWizard.cambiarBandPagAnterior(this.paganterior);
        // });
    }

    private subscribeToSaveResponse(result: Observable<Dirpernat>) {
        result.subscribe((res: Dirpernat) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private subscribeToSaveResponsesu(result: Observable<Dirpernat>) {
        result.subscribe((res: Dirpernat) =>
            this.onSaveSuccesssu(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: Dirpernat) {
        // this.loadDirecPerNatu(this.trabajador.pernatural.id);
        // console.log('direccion result: ' + JSON.stringify(result));
        this.loadDirecPerNatu(result.pernatural.id);
        this.dirper = new Dirpernat();
        this.close();
    }
    private onSaveSuccesssu(result: Dirpernat) {
        // this.loadDirecPerNatu(this.trabajador.pernatural.id);
        console.log('direccion result: ' + JSON.stringify(result));
        this.loadDirecPerNatu(result.pernatural.id);
        this.closesu();
    }
    private onSaveError() {
        // console.log('saveerror');
    }

    previousState() {
        window.history.back();
    }
    trackTipoDocumentoIdentidad(index: number, item: Tipdocident) {
        return item.vDescorta;
    }

    trackCargos(index: number, item: Cartrab) {
        return item.vDescartra;
    }

    cloneDirec(dir: Dirpernat): Dirpernat {
        const direc = new Dirpernat();
        for (const prop in dir) {
            if ( prop) {
                direc[prop] = dir[prop];
            }
        }
        return direc;
    }
    // cloneDirecsu(dir: Dirpernat): Dirpernat {
    //     const direc = new Dirpernat();
    //     for (const prop in dir) {
    //         if ( prop) {
    //             direc[prop] = dir[prop];
    //         }
    //     }
    //     return direc;
    // }
    padWithZero(number) {
        let num_form = '' + number;
        if (num_form.length < 2) {
            num_form = '0' + num_form;
        }
        return num_form;
    }

    private onError(error: any) {
        // this.jhiAlertService.error(error.message, null, null);
        // this.messages = [];
        // this.messages.push({ severity: 'error', summary: 'Mensaje de Error', detail: error.message });
    }
}

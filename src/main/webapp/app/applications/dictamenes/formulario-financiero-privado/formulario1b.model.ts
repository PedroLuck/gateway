import { BaseEntity } from '../../../shared/index';
import { Tabla } from './tabla.model';

export class Formulario1B implements BaseEntity {
    constructor(
        public id?: number,
        public listaProductos?: Tabla[],
    ) { }
}

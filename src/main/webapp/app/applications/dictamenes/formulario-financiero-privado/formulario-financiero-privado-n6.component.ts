import { Component, OnInit, OnDestroy, } from '@angular/core';
import { JhiAlertService, JhiEventManager } from 'ng-jhipster';
import { Principal, ResponseWrapper } from '../../../shared/index';
import { Subscription } from 'rxjs/Subscription';
import { ActivatedRoute, Router } from '@angular/router';
import { SessionStorage, LocalStorage } from 'ng2-webstorage';
import { ComboModel } from '../../general/combobox.model';
import { Message } from 'primeng/components/common/api';
import { DatePipe } from '@angular/common';
import { FormularioFinancieroPrivadoService } from './index';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Formperfil, FormperfilService } from '../entities/entities/formperfil/index';
import { Solicform, SolicformService } from '../entities/entities/solicform/index';
import { Solicitud, SolicitudService } from '../entities/entities/solicitud/index';
import { Constants } from './constants';
import { FormControl } from '@angular/forms';
import { Tabla } from './tabla.model';
import { Componente } from './componente.model';
import { Formulario6 } from './formulario6.model';
import { FormfinancDetalleService } from '../entities/index';

@Component({
    selector: 'jhi-formulario-financiero-privado-n6',
    templateUrl: './formulario-financiero-privado-n6.component.html',
    styleUrls: ['formulario-financiero-privado.scss']
})

export class FormularioFinancieroPrivadoN6Component implements OnInit, OnDestroy {
    currentAccount: Account;
    eventSubscriber: Subscription;
    subscription: Subscription;

    // Mensajes
    messages: Message[] = [];
    messagesForm: Message[] = [];
    messageList: any;

    // Variables de edicion y bloqueo
    block: boolean;
    display6b: boolean;
    display6c: boolean;

    // Datos de Perfil
    @LocalStorage('solicitud')
    solicitud: Solicitud;
    @LocalStorage('solicform')
    solicForm: Solicform;
    @LocalStorage('formperfil')
    formPerfil: Formperfil;
    @LocalStorage('observacion')
    observacion: boolean;
    @LocalStorage('tipoUsu')
    tipoUsuario: string;
    obsForm: string;

    anios: number[];
    formGroup: FormGroup;
    formulario: Formulario6;
    constantes: Constants;

    nCodffina: number;

    constructor(
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal,
        private route: ActivatedRoute,
        private router: Router,
        private formularioFinancieroPrivadoService: FormularioFinancieroPrivadoService,
        private fb: FormBuilder,
        private datepipe: DatePipe,
        // Servicios
        private solicitudService: SolicitudService,
        private formperfilService: FormperfilService,
        private formfinancdetalleService: FormfinancDetalleService,
        private solicformService: SolicformService,
    ) {
        this.formGroup = this.fb.group({ 'name': ['', Validators.required] });
    }

    loadAll() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['nCodffina']);
        });
    }

    load(nCodffina) {
        this.nCodffina = nCodffina;
        this.solicformService.obtenerSolicitudFormularioPorCodFfina(nCodffina).subscribe((solicForm) => {
            this.solicForm = solicForm;
            const nCodsolic = solicForm.nCodsolic;
            if (this.solicitud === null) {
                this.solicitudService.find(nCodsolic).subscribe((solicitud) => {
                    this.solicitud = solicitud;
                });
            }
        });
    }

    ngOnInit() {
        this.loadAll();
        this.inicializarVariables();
        this.display6b = false;
        this.display6c = false;
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
    }

    inicializarVariables() {
        this.constantes = new Constants();
        const fechaReg = this.datepipe.transform(this.solicitud.tFecreg, 'yyyy');
        const fecha: number = Number(fechaReg);
        this.anios = Array<number>();
        this.anios.push(fecha - 3);
        this.anios.push(fecha - 2);
        this.anios.push(fecha - 1);
        this.anios.push(fecha);
        this.construirFormulario();
    }

    abrirModal6B() {
        this.display6b = true;
    }

    abrirModal6C() {
        this.display6c = true;
    }

    construirFormulario() {
        this.formulario = new Formulario6();

        /*const desc: string[] = [this.constantes.FORM6_CR,
        this.constantes.FORM6_CNR,
        this.constantes.FORM6_CT,
        this.constantes.FORM6_ACS,
        this.constantes.FORM6_CTS,
        this.constantes.FORM6_OB,
        this.constantes.FORM6_GPCT,
        this.constantes.FORM6_TOTAL];
        const cod: string[] = [this.constantes.FORM6_COD_CR,
        this.constantes.FORM6_COD_CNR,
        this.constantes.FORM6_COD_CT,
        this.constantes.FORM6_COD_ACS,
        this.constantes.FORM6_COD_CTS,
        this.constantes.FORM6_COD_OB,
        this.constantes.FORM6_COD_GPCT,
        this.constantes.FORM6_COD_TOTAL];
        this.formulario.listaGastosPersonal = this.crearlistacomponentes(desc, cod, true);*/
    }

    // Funcionaes para la creacion de Formularios
    // -------------------------------------------------------------------
    crearlistacomponentes(desc: string[], cod: string[], subtotal: boolean): Tabla[] {
        const t = new Array<Tabla>();
        for (let j = 0; j < desc.length; j++) {
            t[j] = new Tabla();
            t[j].descripcion = desc[j];
            t[j].componentes = new Array<Componente>();
            for (let i = 0; i < (this.anios.length * 3); i++) {
                t[j].componentes[i] = new Componente();
                t[j].componentes[i].codigo = cod[j] + '_' + i + '_' + this.anios[i];
                t[j].componentes[i].cantidad = 0;
                t[j].componentes[i].año = this.anios[i];
                this.formfinancdetalleService.obtenerComponente(this.nCodffina, t[j].componentes[i].codigo).subscribe(
                    (formfinancdetalle) => {
                        if (formfinancdetalle !== undefined) {
                            t[j].componentes[i].cantidad = formfinancdetalle.nValffina;
                            t[j].componentes[i].id = formfinancdetalle.nCodfdetal;
                            t[j].componentes[i].vUsureg = formfinancdetalle.vUsuareg;
                            t[j].componentes[i].tFecReg = formfinancdetalle.tFecreg;
                            t[j].componentes[i].nSedeReg = formfinancdetalle.nSedereg;
                        }
                    }
                );
                if (subtotal && j === desc.length - 1) {
                    // logica no necesaria
                } else {
                    const fc: FormControl = new FormControl();
                    this.formGroup.addControl(cod[j] + '_' + i + '_' + this.anios[i], new FormControl);
                    this.formGroup.controls[cod[j] + '_' + i + '_' + this.anios[i]].setValue(0);
                }
            }

        }
        return t;
    }
    // -------------------------------------------------------------------
    // Calculos del Formulario
    // -------------------------------------------------------------------
    subtotal(event: any) {
        const columna: string[] = event.target.id.split('_');
        const idx = columna[2];
        const idy = this.formulario.listaGastosPersonal.length - 1;
        let suma = 0;

        for (let i = 0; i < this.formulario.listaGastosPersonal.length - 1; i++) {
            for (let j = 0; j < this.formulario.listaGastosPersonal[i].componentes.length; j++) {
                if (this.formulario.listaGastosPersonal[i].componentes[j].codigo === event.target.id) {
                    this.formulario.listaGastosPersonal[i].componentes[j].cantidad = Number(event.target.value);
                }
            }
        }

        for (let i = 0; i < this.formulario.listaGastosPersonal.length - 1; i++) {
            for (let j = 0; j < this.formulario.listaGastosPersonal[i].componentes.length; j++) {
                if (this.formulario.listaGastosPersonal[i].componentes[j].codigo.indexOf(columna[2] + '_' + columna[3]) !== -1) {
                    suma += Number(this.formulario.listaGastosPersonal[i].componentes[j].cantidad);
                }
            }
        }
        this.formulario.listaGastosPersonal[idy].componentes[idx].cantidad = suma;
    }
    // -------------------------------------------------------------------
    // Solo numeros
    keyPress(event: any) {
        const pattern = /[0-9]/;
        const inputChar = String.fromCharCode(event.charCode);
        if (!pattern.test(inputChar)) {
            event.preventDefault();
        }
    }

    // --------------------------- Errores ------------------------------------
    private onErrorMultiple(errorList: any) {
        for (let i = 0; i < errorList.length; i++) {
            this.messagesForm.push(errorList[i]);
        }
    }

    private onError(error: any) {
        this.messages = [];
        this.messages.push({ severity: 'error', summary: 'Mensaje de Error', detail: error.message });
    }

    guardarFormulario() {
        if (!this.observacion) {
            this.formfinancdetalleService.desactivarFormulario(this.nCodffina, 'f3').subscribe(
                (res: ResponseWrapper) => {
                    this.formfinancdetalleService.guardarFormFinancieroTablas(this.datepipe, this.formulario.listaGastosPersonal, this.nCodffina);
                    this.verControlInformacion();
                },
                (res: ResponseWrapper) => this.onError(res.json)
            );
        } else {
            this.solicForm.vObserva = this.obsForm;
            this.solicForm.vFlgest = 'O';
            this.solicformService.update(this.solicForm).subscribe((res: Solicform) => { this.verControlInformacion() });
        }
    }

    verControlInformacion() {
        this.router.navigate(['../../dictamenes/control-informacion/' + this.solicitud.nCodsolic, this.observacion, this.tipoUsuario])
    }

    ngOnDestroy() { }

    guardarObservacion() {
        // logica de guardado de observaciones
        this.formularioFinancieroPrivadoService.guardarObservaciones(this.obsForm, this.solicForm, this.solicformService, this.tipoUsuario);
        this.verControlInformacion();
    }
}

import { BaseEntity } from '../../../shared/index';
import { Tabla } from './tabla.model';

export class Formulario1A implements BaseEntity {
    constructor(
        public id?: number,
        public listaNacional?: Tabla[],
        public listaInternacional?: Tabla[],
        public subtotalNacional?: Tabla[],
        public subtotalInternacional?: Tabla[],
        public ingresoTotal?: Tabla[],
    ) { }
}

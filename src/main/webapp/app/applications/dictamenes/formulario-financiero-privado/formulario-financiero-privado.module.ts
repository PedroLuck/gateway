import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GatewaySharedModule } from '../../../shared';
import { DialogModule, TabMenuModule, MenuItem, DropdownModule } from 'primeng/primeng';

import {
    FormularioFinancieroPrivadoN1Component,
    FormularioFinancieroPrivadoN1AComponent,
    FormularioFinancieroPrivadoN1BComponent,
    FormularioFinancieroPrivadoN1CComponent,
    FormularioFinancieroPrivadoN1DComponent,
    FormularioFinancieroPrivadoN2Component,
    FormularioFinancieroPrivadoN2AComponent,
    FormularioFinancieroPrivadoN3Component,
    FormularioFinancieroPrivadoN4Component,
    FormularioFinancieroPrivadoN5Component,
    FormularioFinancieroPrivadoRoute,
    FormularioFinancieroPrivadoService,
} from './';
import { FormfinancService, FormfinancDetalleService } from '../entities/index';

const ENTITY_STATES = [
    ...FormularioFinancieroPrivadoRoute,
];

@NgModule({
    declarations: [
        FormularioFinancieroPrivadoN1Component,
        FormularioFinancieroPrivadoN1AComponent,
        FormularioFinancieroPrivadoN1BComponent,
        FormularioFinancieroPrivadoN1CComponent,
        FormularioFinancieroPrivadoN1DComponent,
        FormularioFinancieroPrivadoN2Component,
        FormularioFinancieroPrivadoN2AComponent,
        FormularioFinancieroPrivadoN3Component,
        FormularioFinancieroPrivadoN4Component,
        FormularioFinancieroPrivadoN5Component
    ],
    imports: [
        GatewaySharedModule,
        RouterModule.forChild(ENTITY_STATES),
        DialogModule,
        TabMenuModule,
        DropdownModule,
    ],
    entryComponents: [
        FormularioFinancieroPrivadoN1Component,
        FormularioFinancieroPrivadoN1AComponent,
        FormularioFinancieroPrivadoN1BComponent,
        FormularioFinancieroPrivadoN1CComponent,
        FormularioFinancieroPrivadoN1DComponent,
        FormularioFinancieroPrivadoN2Component,
        FormularioFinancieroPrivadoN2AComponent,
        FormularioFinancieroPrivadoN3Component,
        FormularioFinancieroPrivadoN4Component,
        FormularioFinancieroPrivadoN5Component
    ],
    providers: [
        FormularioFinancieroPrivadoService,
        FormfinancService,
        FormfinancDetalleService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GatewayFormularioFinancieroPrivadoModule { }

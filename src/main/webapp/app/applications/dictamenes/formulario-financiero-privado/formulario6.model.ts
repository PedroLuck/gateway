import { BaseEntity } from '../../../shared/index';
import { Tabla } from './tabla.model';

export class Formulario6 implements BaseEntity {
    constructor(
        public id?: number,
        public listaGastosPersonal?: Tabla[],
    ) { }
}

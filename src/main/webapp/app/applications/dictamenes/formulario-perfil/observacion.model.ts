import { BaseEntity } from '../../../shared/index';

export class Observacion implements BaseEntity {

        constructor(
            public id?: number,
            public observacionfp1?: string,
            public observacionfp2?: string,
            public observacionfp3?: string,
            public observacionfp4?: string,
            public observacionfp5?: string,
            public observacionfp6?: string,
        ) {
            observacionfp1 = '';
            observacionfp2 = '';
            observacionfp3 = '';
            observacionfp4 = '';
            observacionfp5 = '';
            observacionfp6 = '';
        }
    }

import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GatewaySharedModule } from '../../../shared';
import { DialogModule, TabViewModule, DropdownModule, MessagesModule, MessageModule, BlockUIModule, MultiSelectModule, FileUploadModule } from 'primeng/primeng';

import {
    SolicitudService,
    SolicitudComponent,
    solicitudRoute,
} from '../entities/entities/solicitud/';

import {
    ListadoSolicitudesService,
    ListadoSolicitudesComponent,
    ListadoSolicitudesRoute,
} from './';

import {
    SolicformService,
    solicformRoute,
    SolicformComponent,
} from '../entities/entities/solicform/';

import {
    DireccionService,
    direccionRoute,
    DireccionComponent,
} from '../entities/entities/direccion/index';

import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ControlInformacionComponent } from '../control-informacion/index';
import { FormularioPerfilComponent, FormularioPerfilService } from '../formulario-perfil/index';
import {
    FormperfilComponent,
    formperfilRoute,
    FormperfilService
} from '../entities/entities/formperfil/index';

import {
    ActieconService,
    ActieconComponent,
    actieconRoute,
} from '../entities/entities/actiecon';
import { FormularioPerfil2Component } from '../formulario-perfil/formulario-perfil2.component';
import { FormularioPerfil3Component } from '../formulario-perfil/formulario-perfil3.component';
import { FormularioPerfil4Component } from '../formulario-perfil/formulario-perfil4.component';
import { FormularioPerfil5Component } from '../formulario-perfil/formulario-perfil5.component';
import { FormularioPerfil6Component } from '../formulario-perfil/formulario-perfil6.component';
import { UndnegocioComponent, UndnegocioService } from '../entities/entities/undnegocio/index';
import { HechoinverService, HechoinverComponent } from '../entities/entities/hechoinver/index';
import { ParticipaService, ParticipaComponent } from '../entities/entities/participa/index';
import { NegocolectService, NegocolectComponent } from '../entities/entities/negocolect/index';
import { ResulnegocService, ResulnegocComponent } from '../entities/entities/resulnegoc/index';
import { RespinformaService, RespinformaComponent } from '../entities/entities/respinforma/index';
import { AnexlaboralService, AnexlaboralComponent } from '../entities/entities/anexlaboral/index';
import { DecimalMask } from '../../general/decimal.directive';
import { TipdocService, TipdocComponent } from '../entities/entities/tipdoc/index';
import { Empresa } from '../../general/servicesmodel/empresa.model';
import { ReglaboralService } from '../entities/entities/reglaboral/index';
import { PerreglabService } from '../entities/entities/perreglab/index';
import {
    FormularioFinancieroPrivadoN1Component,
    FormularioFinancieroPrivadoN1AComponent,
    FormularioFinancieroPrivadoN1BComponent,
    FormularioFinancieroPrivadoN1CComponent,
    FormularioFinancieroPrivadoN1DComponent,
    FormularioFinancieroPrivadoN2Component,
    FormularioFinancieroPrivadoN2AComponent,
    FormularioFinancieroPrivadoN3Component,
    FormularioFinancieroPrivadoN4Component,
    FormularioFinancieroPrivadoN5Component,
    FormularioFinancieroPrivadoN6Component,
    FormularioFinancieroPrivadoService
} from '../formulario-financiero-privado/index';
import { FormfinancService, FormfinancDetalleService } from '../entities/index';
import {
    FormularioFinancieroFinancieroN1Component,
    FormularioFinancieroFinancieroN2Component,
    FormularioFinancieroFinancieroN2AComponent,
    FormularioFinancieroFinancieroN3Component,
    FormularioFinancieroFinancieroN4Component,
    FormularioFinancieroFinancieroN5Component,
    FormularioFinancieroFinancieroN6Component,
    FormularioFinancieroFinancieroN7Component,
    FormularioFinancieroFinancieroN8Component,
    FormularioFinancieroFinancieroService
} from '../formulario-financiero-financiero/index';
import { PagosNacionService } from '../entities/entities/pagosnacion/index';

const ENTITY_STATES = [
    ...ListadoSolicitudesRoute,
];

@NgModule({
    declarations: [
        ListadoSolicitudesComponent,
        SolicitudComponent,
        ControlInformacionComponent,
        SolicformComponent,
        FormularioPerfilComponent,
        FormularioPerfil2Component,
        FormularioPerfil3Component,
        FormularioPerfil4Component,
        FormularioPerfil5Component,
        FormularioPerfil6Component,
        FormularioFinancieroPrivadoN1Component,
        FormularioFinancieroPrivadoN1AComponent,
        FormularioFinancieroPrivadoN1BComponent,
        FormularioFinancieroPrivadoN1CComponent,
        FormularioFinancieroPrivadoN1DComponent,
        FormularioFinancieroPrivadoN2Component,
        FormularioFinancieroPrivadoN2AComponent,
        FormularioFinancieroPrivadoN3Component,
        FormularioFinancieroPrivadoN4Component,
        FormularioFinancieroPrivadoN5Component,
        FormularioFinancieroPrivadoN6Component,
        FormularioFinancieroFinancieroN1Component,
        FormularioFinancieroFinancieroN2Component,
        FormularioFinancieroFinancieroN2AComponent,
        FormularioFinancieroFinancieroN3Component,
        FormularioFinancieroFinancieroN4Component,
        FormularioFinancieroFinancieroN5Component,
        FormularioFinancieroFinancieroN6Component,
        FormularioFinancieroFinancieroN7Component,
        FormularioFinancieroFinancieroN8Component,
        DireccionComponent,
        HechoinverComponent,
        ParticipaComponent,
        FormperfilComponent,
        ActieconComponent,
        UndnegocioComponent,
        NegocolectComponent,
        ResulnegocComponent,
        RespinformaComponent,
        AnexlaboralComponent,
        DecimalMask,
        TipdocComponent,
    ],
    imports: [
        GatewaySharedModule,
        RouterModule.forChild(ENTITY_STATES),
        DialogModule,
        FormsModule,
        ReactiveFormsModule,
        TabViewModule,
        DropdownModule,
        MessagesModule,
        MessageModule,
        BlockUIModule,
        MultiSelectModule,
        FormsModule,
        ReactiveFormsModule,
        FileUploadModule,
    ],
    entryComponents: [
        ListadoSolicitudesComponent,
    ],
    providers: [
        ListadoSolicitudesService,
        DireccionService,
        UndnegocioService,
        HechoinverService,
        ParticipaService,
        SolicitudService,
        SolicformService,
        FormperfilService,
        ActieconService,
        NegocolectService,
        ResulnegocService,
        RespinformaService,
        AnexlaboralService,
        TipdocService,
        FormularioPerfilService,
        ReglaboralService,
        PerreglabService,
        FormularioFinancieroPrivadoService,
        FormularioFinancieroFinancieroService,
        FormfinancService,
        FormfinancDetalleService,
        PagosNacionService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GatewayListadoSolicitudesModule { }

import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { ListadoSolicitudesComponent } from './listado-solicitudes.component';
import { UserRouteAccessService } from '../../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';
import { ControlInformacionComponent } from '../control-informacion/index';
import { FormularioPerfilComponent } from '../formulario-perfil/index';
import { FormularioPerfil2Component } from '../formulario-perfil/formulario-perfil2.component';
import { FormularioPerfil3Component } from '../formulario-perfil/formulario-perfil3.component';
import { FormularioPerfil4Component } from '../formulario-perfil/formulario-perfil4.component';
import { FormularioPerfil5Component } from '../formulario-perfil/formulario-perfil5.component';
import { FormularioPerfil6Component } from '../formulario-perfil/formulario-perfil6.component';
import {
    FormularioFinancieroPrivadoN1Component,
    FormularioFinancieroPrivadoN1AComponent,
    FormularioFinancieroPrivadoN1BComponent,
    FormularioFinancieroPrivadoN1CComponent,
    FormularioFinancieroPrivadoN1DComponent,
    FormularioFinancieroPrivadoN2Component,
    FormularioFinancieroPrivadoN2AComponent,
    FormularioFinancieroPrivadoN3Component,
    FormularioFinancieroPrivadoN4Component,
    FormularioFinancieroPrivadoN5Component,
    FormularioFinancieroPrivadoN6Component
} from '../formulario-financiero-privado/index';
import {
    FormularioFinancieroFinancieroN1Component,
    FormularioFinancieroFinancieroN2Component,
    FormularioFinancieroFinancieroN2AComponent,
    FormularioFinancieroFinancieroN3Component,
    FormularioFinancieroFinancieroN4Component,
    FormularioFinancieroFinancieroN5Component,
    FormularioFinancieroFinancieroN6Component,
    FormularioFinancieroFinancieroN7Component,
    FormularioFinancieroFinancieroN8Component
} from '../formulario-financiero-financiero/index';

export const ListadoSolicitudesRoute: Routes = [
    {
        path: 'dictamenes/listado-solicitudes',
        component: ListadoSolicitudesComponent,
        children: []
    }, {
        path: 'dictamenes/control-informacion/:nCodsolic/:ncodObs/:ncodTipoUsu',
        component: ControlInformacionComponent,
        children: []
    }, {
        path: 'dictamenes/formulario-perfil/:nCodfperf/:ncodObs/:ncodTipoUsu',
        component: FormularioPerfilComponent,
    }, {
        path: 'dictamenes/formulario-perfil2/:ncodObs/:ncodTipoUsu',
        component: FormularioPerfil2Component,
    }, {
        path: 'dictamenes/formulario-perfil3/:ncodObs/:ncodTipoUsu',
        component: FormularioPerfil3Component,
    }, {
        path: 'dictamenes/formulario-perfil4/:ncodObs/:ncodTipoUsu',
        component: FormularioPerfil4Component,
    }, {
        path: 'dictamenes/formulario-perfil5/:ncodObs/:ncodTipoUsu',
        component: FormularioPerfil5Component,
    }, {
        path: 'dictamenes/formulario-perfil6/:ncodObs/:ncodTipoUsu',
        component: FormularioPerfil6Component,
    }, {
        path: 'dictamenes/formulario-financiero-privado-n1/:nCodffina/:ncodObs/:ncodTipoUsu',
        component: FormularioFinancieroPrivadoN1Component,
    }, {
        path: 'dictamenes/formulario-financiero-privado-n1a/:nCodffina/:ncodObs/:ncodTipoUsu',
        component: FormularioFinancieroPrivadoN1AComponent,
    }, {
        path: 'dictamenes/formulario-financiero-privado-n1b/:nCodffina/:ncodObs/:ncodTipoUsu',
        component: FormularioFinancieroPrivadoN1BComponent,
    }, {
        path: 'dictamenes/formulario-financiero-privado-n1c/:nCodffina/:ncodObs/:ncodTipoUsu',
        component: FormularioFinancieroPrivadoN1CComponent,
    }, {
        path: 'dictamenes/formulario-financiero-privado-n1d/:nCodffina/:ncodObs/:ncodTipoUsu',
        component: FormularioFinancieroPrivadoN1DComponent,
    }, {
        path: 'dictamenes/formulario-financiero-privado-n2/:nCodffina/:ncodObs/:ncodTipoUsu',
        component: FormularioFinancieroPrivadoN2Component,
    }, {
        path: 'dictamenes/formulario-financiero-privado-n2a/:nCodffina/:ncodObs/:ncodTipoUsu',
        component: FormularioFinancieroPrivadoN2AComponent,
    }, {
        path: 'dictamenes/formulario-financiero-privado-n3/:nCodffina/:ncodObs/:ncodTipoUsu',
        component: FormularioFinancieroPrivadoN3Component,
    }, {
        path: 'dictamenes/formulario-financiero-privado-n4/:nCodffina/:ncodObs/:ncodTipoUsu',
        component: FormularioFinancieroPrivadoN4Component,
    }, {
        path: 'dictamenes/formulario-financiero-privado-n5/:nCodffina/:ncodObs/:ncodTipoUsu',
        component: FormularioFinancieroPrivadoN5Component,
    },  {
        path: 'dictamenes/formulario-financiero-privado-n6/:nCodffina/:ncodObs/:ncodTipoUsu',
        component: FormularioFinancieroPrivadoN6Component,
    }, {
        path: 'dictamenes/formulario-financiero-financiero-n1/:nCodffina/:ncodObs/:ncodTipoUsu',
        component: FormularioFinancieroFinancieroN1Component,
    }, {
        path: 'dictamenes/formulario-financiero-financiero-n2/:nCodffina/:ncodObs/:ncodTipoUsu',
        component: FormularioFinancieroFinancieroN2Component,
    }, {
        path: 'dictamenes/formulario-financiero-financiero-n2a/:nCodffina/:ncodObs/:ncodTipoUsu',
        component: FormularioFinancieroFinancieroN2AComponent,
    }, {
        path: 'dictamenes/formulario-financiero-financiero-n3/:nCodffina/:ncodObs/:ncodTipoUsu',
        component: FormularioFinancieroFinancieroN3Component,
    }, {
        path: 'dictamenes/formulario-financiero-financiero-n4/:nCodffina/:ncodObs/:ncodTipoUsu',
        component: FormularioFinancieroFinancieroN4Component,
    }, {
        path: 'dictamenes/formulario-financiero-financiero-n5/:nCodffina/:ncodObs/:ncodTipoUsu',
        component: FormularioFinancieroFinancieroN5Component,
    }, {
        path: 'dictamenes/formulario-financiero-financiero-n6/:nCodffina/:ncodObs/:ncodTipoUsu',
        component: FormularioFinancieroFinancieroN6Component,
    }, {
        path: 'dictamenes/formulario-financiero-financiero-n7/:nCodffina/:ncodObs/:ncodTipoUsu',
        component: FormularioFinancieroFinancieroN7Component,
    }, {
        path: 'dictamenes/formulario-financiero-financiero-n8/:nCodffina/:ncodObs/:ncodTipoUsu',
        component: FormularioFinancieroFinancieroN8Component,
    },
];

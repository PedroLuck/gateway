import { Component, OnInit, OnDestroy, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiAlertService, JhiLanguageService, JhiDateUtils } from 'ng-jhipster';
import { ListadoSolicitudesService } from './listado-solicitudes.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../../shared';
import { Solicitud, SolicitudService } from '../entities/entities/solicitud/index';
import { SessionStorage } from 'ng2-webstorage';
import { Solicform } from '../entities/entities/solicform/index';
import { Formperfil } from '../entities/entities/formperfil/index';
import { Direccion } from '../entities/entities/direccion/index';
import { Voucher } from './voucher.model';
import { PagosNacionService } from '../entities/entities/pagosnacion/index';
import { DatePipe } from '@angular/common';
import { DictamenesConstants } from '../entities/index';

@Component({
    selector: 'jhi-listado-solicitudes',
    templateUrl: './listado-solicitudes.component.html',
    styleUrls: ['listado-solicitudes.scss']
})

export class ListadoSolicitudesComponent implements OnInit, OnDestroy {
    @SessionStorage('solicitud')
    solicitud2: Solicitud;
    @SessionStorage('solicform')
    solicForm: Solicform;
    @SessionStorage('formperfil')
    formPerfil: Formperfil;
    @SessionStorage('direcciones')
    direcciones: Direccion[];
    solicituds: Solicitud[];
    solicitud: Solicitud;
    currentAccount: Account;
    eventSubscriber: Subscription;
    currentSearch: string;
    display = false;

    block: boolean;
    mensaje: string;
    voucher: Voucher;
    ruc: string;
    constantes: DictamenesConstants;

    showDialog(obj: Solicitud) {
        this.voucher = new Voucher();
        this.mensaje = '';
        this.display = true;
        this.solicitud = obj;

        this.voucher.vCodTrib = '06076';
        this.voucher.vNumDoc = '20100047056';
        this.voucher.nImpComp = '177.5';
        this.voucher.vCodComp = '394377';
        this.voucher.vCodCaj = '4993';
        this.voucher.vCodOfc = '0085';
    }

    constructor(
        private solicitudService: SolicitudService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private activatedRoute: ActivatedRoute,
        private principal: Principal,
        private router: Router,
        private pagosNacionService: PagosNacionService,
        private datepipe: DatePipe,
        private dateUtils: JhiDateUtils,
    ) { }

    loadAll() {
        this.obtenerListaSolicitud();
    }

    ngOnInit() {
        this.voucher = new Voucher();
        this.constantes = new DictamenesConstants();
        this.block = false;
        this.ruc = '20100130204';
        this.loadAll();
        this.display = false;
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
    }

    ngOnDestroy() {
        // this.eventManager.destroy(this.eventSubscriber);
        // this.eventSubscriber.unsubscribe();
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }

    obtenerListaSolicitud() {
        this.solicitudService.obtenerlistaSolicitudesPorRuc(this.ruc).subscribe(
            (res: ResponseWrapper) => this.solicituds = res.json,
            (res: ResponseWrapper) => this.onError(res.json),
        );
    }

    verificarVoucher(solicitud: Solicitud) {
        let verificar = true;

        const var1 = String(this.voucher.vCodTrib);
        const var2 = String(this.voucher.vNumDoc);
        const var3 = String(this.voucher.nImpComp);
        const var4 = String(this.voucher.vCodComp);
        const var5 = String(this.voucher.vFecComp);
        const var6 = String(this.voucher.vCodCaj);
        const var7 = String(this.voucher.vCodOfc);

        if (this.voucher != null) {
            if (var1.length === 0) {
                verificar = false;
            } else if (var2.length === 0) {
                verificar = false;
            } else if (var3.length === 0) {
                verificar = false;
            } else if (var4.length === 0) {
                verificar = false;
            } else if (var5.length === 0) {
                verificar = false;
            } else if (var6.length === 0) {
                verificar = false;
            } else if (var7.length === 0) {
                verificar = false;
            }
        } else {
            verificar = false;
        }

        const fechaComp = new Date(var5);
        const fechaHoy = new Date();
        const anio = fechaComp.getFullYear();
        const mes = fechaComp.getUTCMonth();
        const dia = fechaComp.getUTCDate();
        const fecha = anio + this.padLeft(String(mes + 1), '0', 2) + this.padLeft(String(dia), '0', 2);
        /* obj.tFecplazo = this.dateUtils.toDate(this.datepipe.transform(fec.getDate() +
         this.constantes.DICTAMENES_DIAS_PAGO +
         this.constantes.DICTAMENES_DIAS_PLAZO,
         'yyyy-MM-dd HH:mm:ss'));*/
         console.log('verificar: ' + verificar);
        if (verificar) {
            this.pagosNacionService.verificarPago(this.padLeft(String(this.voucher.vCodComp), '0', 10),
                this.padLeft(String(this.voucher.vCodOfc), '0', 4),
                fecha,
                this.voucher.vNumDoc,
                this.voucher.nImpComp,
                this.padLeft(String(this.voucher.vCodTrib), '0', 5),
                this.padLeft(String(this.voucher.vCodCaj), '0', 4)).subscribe(
                (resultado: Number) => {
                    if (resultado > 0) {
                        // Se realizo el pago dentro del rango de dias de pago
                        if (fechaComp <= (solicitud.tFecnotif.getDate() + this.constantes.DICTAMENES_DIAS_PAGO)) {
                            // Se encuentra en el rango de pago
                        }
                        if (fechaComp > (solicitud.tFecnotif.getDate() + this.constantes.DICTAMENES_DIAS_PAGO) &&
                            fechaComp <= (solicitud.tFecplazo.getDate())) {
                            // Nueva Fecha de Plazo
                            solicitud.tFecplazo = this.dateUtils.toDate(this.datepipe.transform(fechaHoy.getDate() +
                                this.constantes.DICTAMENES_DIAS_PLAZO,
                                'yyyy-MM-dd HH:mm:ss'));
                        }
                        // if (!(fechaComp > (solicitud.tFecplazo.getDate()))) {
                        if ((fechaComp > (solicitud.tFecplazo.getDate()))) {
                            solicitud.nFlgestado = 2;
                            solicitud.vUsuaupd = 'UsuMod';
                            solicitud.vVoucher = this.padLeft(String(this.voucher.vCodComp), '0', 10);
                            solicitud.tFecupd = this.dateUtils.toDate(this.datepipe.transform((new Date), 'yyyy-MM-dd HH:mm:ss'));
                            solicitud.nSedeupd = 0;
                            this.solicitudService.update(solicitud).subscribe(
                                (res: Solicitud) => {
                                    this.obtenerListaSolicitud();
                                    this.display = false;
                                }

                            );
                        } else {
                            this.mensaje = 'La fecha pago ha superado el plazo de pago';
                        }
                    } else {
                        this.mensaje = 'El pago aún no se ha registrado.';
                    }
                }
                );
        } else {
            this.mensaje = 'Debe ingresar todos los campos correctamente';
        }
    }

    verControlInformacion(obj: Solicitud) {
        this.router.navigate(['../../dictamenes/control-informacion/' + obj.nCodsolic, 0, 'EM'])
    }

    padLeft(text: string, padChar: string, size: number): string {
        return (String(padChar).repeat(size) + text).substr((size * -1), size);
    }
}

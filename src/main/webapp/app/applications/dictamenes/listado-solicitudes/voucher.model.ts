import { BaseEntity } from '../../../shared/index';

export class Voucher implements BaseEntity {
    constructor(
        public id?: number,
        public vCodTrib?: String,
        public vNumDoc?: String,
        public nImpComp?: String,
        public vCodComp?: String,
        public vFecComp?: String,
        public vCodCaj?: String,
        public vCodOfc?: String,
    ) {
        vCodTrib = '',
        vNumDoc = '',
        nImpComp = '',
        vCodComp = '',
        vFecComp = '',
        vCodCaj = '',
        vCodOfc = ''
    }
}

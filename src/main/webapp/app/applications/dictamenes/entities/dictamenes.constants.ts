export class DictamenesConstants {

    public readonly DICTAMENES_RESOURCE = 'dictamenes';
    // public readonly DICTAMENES_RESOURCE = '';
    public readonly DICTAMENES_DIAS_PLAZO = 8; // 8 dias
    public readonly DICTAMENES_DIAS_PAGO = 2; // 2 dias (48 horas)
    // FORMULARIOS FINANCIEROS
    public readonly FORMULARIO_FINANCIERO_1 = 'ESTADO DE RESULTADOS'; // F1
    public readonly FORMULARIO_FINANCIERO_2 = 'ESTADO DE SITUACIÓN FINANCIERA'; // F2
    public readonly FORMULARIO_FINANCIERO_2A = 'DETALLE DE CUENTAS'; // F2A
    public readonly FORMULARIO_FINANCIERO_3 = 'ESTADOS DE FLUJOS DE EFECTIVO'; // F3
    public readonly FORMULARIO_FINANCIERO_4 = 'ESTADO DE CAMBIOS EN EL PATRIMONIO'; // F4
    public readonly FORMULARIO_FINANCIERO_5 = 'ESTRUCTURA DE LOS CRÉDITOS DIRECTOS POR MODALIDAD'; // F5
    public readonly FORMULARIO_FINANCIERO_6 = 'ESTRUCTURA DE LOS CRÉDITOS DIRECTOS SEGÚN TIPO Y MODALIDAD'; // F6
    public readonly FORMULARIO_FINANCIERO_7 = 'MOROSIDAD SEGÚN TIPO Y MODALIDAD DE CRÉDITO'; // F7
    public readonly FORMULARIO_FINANCIERO_8 = 'CRÉDITOS DIRECTOS POR TIPO DE CRÉDITO Y SECTOR ECONÓMICO'; // F8
    public readonly FORMULARIO_COD_FINANCIERO_1 = 'FF1';
    public readonly FORMULARIO_COD_FINANCIERO_2 = 'FF2';
    public readonly FORMULARIO_COD_FINANCIERO_2A = 'FF2A';
    public readonly FORMULARIO_COD_FINANCIERO_3 = 'FF3';
    public readonly FORMULARIO_COD_FINANCIERO_4 = 'FF4';
    public readonly FORMULARIO_COD_FINANCIERO_5 = 'FF5';
    public readonly FORMULARIO_COD_FINANCIERO_6 = 'FF6';
    public readonly FORMULARIO_COD_FINANCIERO_7 = 'FF7';
    public readonly FORMULARIO_COD_FINANCIERO_8 = 'FF8';

    // FORMULARIOS PRIVADOS
    public readonly FORMULARIO_PRIVADO_1 = 'ESTADO DE RESULTADOS'; // F1
    public readonly FORMULARIO_PRIVADO_1A = 'INGRESOS POR VENTAS NETAS Y/O SERVICIOS'; // F1A
    public readonly FORMULARIO_PRIVADO_1B = 'VOLUMEN  FÍSICO  DE  PRODUCCIÓN  Y/O  SERVICIOS'; // F1B
    public readonly FORMULARIO_PRIVADO_1C = 'MATERIA PRIMA CONSUMIDA EN EL PROCESO DE PRODUCCIÓN'; // F1C
    public readonly FORMULARIO_PRIVADO_1D = 'DETALLE DE CUENTAS'; // F1D
    public readonly FORMULARIO_PRIVADO_2 = 'ESTADO DE SITUACIÓN FINANCIERA'; // F2
    public readonly FORMULARIO_PRIVADO_2A = 'DETALLE DE CUENTAS'; // F2A
    public readonly FORMULARIO_PRIVADO_3 = 'ESTADOS DE FLUJOS DE EFECTIVO'; // F3
    public readonly FORMULARIO_PRIVADO_4 = 'ESTADO DE CAMBIOS EN EL PATRIMONIO'; // F4
    public readonly FORMULARIO_PRIVADO_5 = 'GASTOS DE PERSONAL RELACIONADOS AL COSTO DE PRODUCCIÓN O SERVICIOS'; // F5
    public readonly FORMULARIO_PRIVADO_6 = 'NÚMERO DE TRABAJADORES Y HORAS HOMBRE LABORADAS'; // F6
    public readonly FORMULARIO_COD_PRIVADO_1 = 'FP1';
    public readonly FORMULARIO_COD_PRIVADO_1A = 'FP1A';
    public readonly FORMULARIO_COD_PRIVADO_1B = 'FP1B';
    public readonly FORMULARIO_COD_PRIVADO_1C = 'FP1C';
    public readonly FORMULARIO_COD_PRIVADO_1D = 'FP1D';
    public readonly FORMULARIO_COD_PRIVADO_2 = 'FP2';
    public readonly FORMULARIO_COD_PRIVADO_2A = 'FP2A';
    public readonly FORMULARIO_COD_PRIVADO_3 = 'FP3';
    public readonly FORMULARIO_COD_PRIVADO_4 = 'FP4';
    public readonly FORMULARIO_COD_PRIVADO_5 = 'FP5';
    public readonly FORMULARIO_COD_PRIVADO_6 = 'FP6';
    // FORMULARIOS PUBLICOS

}

import { BaseEntity } from '../../../../../shared';
import { Formulario } from '../../../formulario-perfil/formulario.model';
import { Direccion } from '../direccion/index';

export class Formperfil implements BaseEntity {
    constructor(
        public id?: number,
        public nCodfperf?: number,
        public vNomcomer?: string,
        public vDesemple?: string,
        public vCodciiu?: string,
        public vPartreg?: string,
        public vGruecono?: string,
        public vSector?: string,
        public vSectorfront?: string,
        public vPlancont?: string,
        public vPlancontfront?: string,
        public vReglabo?: string,
        public vUsuareg?: string,
        public tFecreg?: any,
        public nFlgactivo?: boolean,
        public nSedereg?: number,
        public vUsuaupd?: string,
        public tFecupd?: any,
        public nSedeupd?: number,
        public direccions?: BaseEntity[],
        public hechoInversions?: BaseEntity[],
        public anexoLaborals?: BaseEntity[],
        public unidadNegocios?: BaseEntity[],
        public respInfos?: BaseEntity[],
        public resulNegociacions?: BaseEntity[],
        public participacions?: BaseEntity[],
        public negColectivas?: BaseEntity[],
        // agregados
        public direcciones?: BaseEntity[],
        public direcciones2?: BaseEntity[],
        public direcciones3?: BaseEntity[],
        public participacionAccionaria?: BaseEntity[],
        public participacionMercado?: BaseEntity[],
        public obras?: BaseEntity[],
        public proyectos?: BaseEntity[],
        public organizaciones?: BaseEntity[],
        public solicitante?: BaseEntity,
        public resultadoNegociacion?: BaseEntity[],
        public responInfoFinanciera?: BaseEntity,
        public responInfoLaboral?: BaseEntity,
        public formulario?: Formulario[]
    ) {
        this.nFlgactivo = false;
    }
}

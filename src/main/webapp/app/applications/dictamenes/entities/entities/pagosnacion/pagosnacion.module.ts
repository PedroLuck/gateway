import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GatewaySharedModule } from '../../../../../shared';
import {
    PagosNacionService,
    PagosNacionComponent,
    pagosnacionRoute,
} from './';

const ENTITY_STATES = [
    ...pagosnacionRoute,
];

@NgModule({
    imports: [
        GatewaySharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        PagosNacionComponent,
    ],
    entryComponents: [
        PagosNacionComponent,
    ],
    providers: [
        PagosNacionService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GatewayPagosNacionModule {}

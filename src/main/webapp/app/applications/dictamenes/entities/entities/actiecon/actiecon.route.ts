import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../../../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { ActieconComponent } from './actiecon.component';

export const actieconRoute: Routes = [
    {
        path: 'actiecon',
        component: ActieconComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.actiecon.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

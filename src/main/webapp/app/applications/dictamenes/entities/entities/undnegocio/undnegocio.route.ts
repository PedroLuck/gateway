import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../../../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UndnegocioComponent } from './undnegocio.component';

export const undnegocioRoute: Routes = [
    {
        path: 'undnegocio',
        component: UndnegocioComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.undnegocio.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

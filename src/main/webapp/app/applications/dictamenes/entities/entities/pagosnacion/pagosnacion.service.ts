import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils } from 'ng-jhipster';
import { PagosNacion } from './pagosnacion.model';
import { ResponseWrapper, createRequestOption } from '../../../../../shared';
import { DictamenesConstants } from '../../index';
import { SERVER_API_URL } from '../../../../../app.constants';

@Injectable()
export class PagosNacionService {

    private constants: DictamenesConstants;
    private resourceDictamenes: string;
    private resourceUrl: string;
    private resourceSearchUrl: string;

    constructor(private http: Http, private dateUtils: JhiDateUtils) {
        this.constants = new DictamenesConstants();
        this.resourceDictamenes = this.constants.DICTAMENES_RESOURCE;
        this.resourceUrl = SERVER_API_URL + this.resourceDictamenes + '/api/pagosnacions';
        this.resourceSearchUrl = SERVER_API_URL + this.resourceDictamenes + '/api/_search/pagosnacions';
    }

    create(pagosnacion: PagosNacion): Observable<PagosNacion> {
        const copy = this.convert(pagosnacion);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(pagosnacion: PagosNacion): Observable<PagosNacion> {
        const copy = this.convert(pagosnacion);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<PagosNacion> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to PagosNacion.
     */
    private convertItemFromServer(json: any): PagosNacion {
        const entity: PagosNacion = Object.assign(new PagosNacion(), json);
        entity.dFecreg = this.dateUtils
            .convertDateTimeFromServer(json.dFecreg);
        entity.dFeccomp = this.dateUtils
            .convertDateTimeFromServer(json.dFeccomp);
        return entity;
    }

    /**
     * Convert a PagosNacion to a JSON which can be sent to the server.
     */
    private convert(pagosnacion: PagosNacion): PagosNacion {
        const copy: PagosNacion = Object.assign({}, pagosnacion);

        copy.dFecreg = this.dateUtils.toDate(pagosnacion.dFecreg);

        copy.dFeccomp = this.dateUtils.toDate(pagosnacion.dFeccomp);
        return copy;
    }

    verificarPago(vcodcomp: String,
                  vcodofc: String,
                  vfeccomp: String,
                  vnumdoc: String,
                  nimpcomp: String,
                  vcodtrib: String,
                  vcodcaj: String): Observable<Number> {
        const options = createRequestOption();
        const url = SERVER_API_URL + this.resourceDictamenes + 'api/verificarPago';
        return this.http.get(url +
                            '?vcodcomp=' + vcodcomp +
                            '&vcodofc=' + vcodofc +
                            '&vfeccomp=' + vfeccomp +
                            '&vnumdoc=' + vnumdoc +
                            '&nimpcomp=' + nimpcomp +
                            '&vcodtrib=' + vcodtrib +
                            '&vcodcaj=' + vcodcaj , options)
            .map((res: Response) => Number(res.json()));
    }

    /*eliminar(codFormPerfil: number) {
        const options = createRequestOption();
        const url = SERVER_API_URL + this.resourceDictamenes +  'api/eliminarPagosNacion';
        return this.http.get(url + '?codFormPerfil=' + codFormPerfil, options)
            .map((res: Response) => {
                const jsonResponse = res.json();
                return Object.assign(Number, jsonResponse);
        });
    }*/
}

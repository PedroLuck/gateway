import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GatewaySharedModule } from '../../../../../shared';
import {
    PernaturalService,
    PernaturalComponent,
    pernaturalRoute,
} from './';

const ENTITY_STATES = [
    ...pernaturalRoute,
];

@NgModule({
    imports: [
        GatewaySharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        PernaturalComponent,
    ],
    entryComponents: [
        PernaturalComponent,
    ],
    providers: [
        PernaturalService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GatewayPernaturalModule {}

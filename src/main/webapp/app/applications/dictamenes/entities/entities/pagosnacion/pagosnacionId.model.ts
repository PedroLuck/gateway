import { BaseEntity } from '../../../../../shared';

export class PagosNacionId implements BaseEntity {
    constructor(
        public id?: number,
        public vAnocomp?: string,
        public vCodcomp?: string,
        public vCodofc?: string,
        public vFeccomp?: string,
    ) { }
}

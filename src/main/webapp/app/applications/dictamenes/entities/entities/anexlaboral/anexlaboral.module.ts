import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GatewaySharedModule } from '../../../../../shared';
import {
    AnexlaboralService,
    AnexlaboralComponent,
    anexlaboralRoute,
} from './';

const ENTITY_STATES = [
    ...anexlaboralRoute,
];

@NgModule({
    imports: [
        GatewaySharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        AnexlaboralComponent,
    ],
    entryComponents: [
        AnexlaboralComponent,
    ],
    providers: [
        AnexlaboralService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GatewayAnexlaboralModule {}

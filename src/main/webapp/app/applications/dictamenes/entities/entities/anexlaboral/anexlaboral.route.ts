import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../../../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { AnexlaboralComponent } from './anexlaboral.component';

export const anexlaboralRoute: Routes = [
    {
        path: 'anexlaboral',
        component: AnexlaboralComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.anexlaboral.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../../../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { PernaturalComponent } from './pernatural.component';

export const pernaturalRoute: Routes = [
    {
        path: 'pernatural',
        component: PernaturalComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.pernatural.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GatewaySharedModule } from '../../../../../shared';
import {
    NegocolectService,
    NegocolectComponent,
    negocolectRoute,
} from './';

const ENTITY_STATES = [
    ...negocolectRoute,
];

@NgModule({
    imports: [
        GatewaySharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        NegocolectComponent,
    ],
    entryComponents: [
        NegocolectComponent,
    ],
    providers: [
        NegocolectService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GatewayNegocolectModule {}

import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../../../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { NegocolectComponent } from './negocolect.component';

export const negocolectRoute: Routes = [
    {
        path: 'negocolect',
        component: NegocolectComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.negocolect.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

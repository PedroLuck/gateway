import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers, Request, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils } from 'ng-jhipster';
import { Formperfil } from './formperfil.model';
import { ResponseWrapper, createRequestOption } from '../../../../../shared';
import { DictamenesConstants } from '../../index';
import { SERVER_API_URL } from '../../../../../app.constants';
import { Direccion } from '../direccion/index';

@Injectable()
export class FormperfilService {

    private constants: DictamenesConstants;
    private resourceDictamenes: string;
    private resourceUrl: string;
    private resourceSearchUrl: string;

    constructor(private http: Http, private dateUtils: JhiDateUtils) {
        this.constants = new DictamenesConstants();
        this.resourceDictamenes = this.constants.DICTAMENES_RESOURCE;
        this.resourceUrl = SERVER_API_URL + this.resourceDictamenes + '/api/formperfils';
        this.resourceSearchUrl = SERVER_API_URL + this.resourceDictamenes + '/api/_search/formperfils';
    }

    create(formperfil: Formperfil): Observable<Formperfil> {
        const copy = this.convert(formperfil);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(formperfil: Formperfil): Observable<Formperfil> {
        const copy = this.convert(formperfil);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<Formperfil> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to Formperfil.
     */
    private convertItemFromServer(json: any): Formperfil {
        const entity: Formperfil = Object.assign(new Formperfil(), json);
        entity.tFecreg = this.dateUtils
            .convertDateTimeFromServer(json.tFecreg);
        entity.tFecupd = this.dateUtils
            .convertDateTimeFromServer(json.tFecupd);
        return entity;
    }

    /**
     * Convert a Formperfil to a JSON which can be sent to the server.
     */
    private convert(formperfil: Formperfil): Formperfil {
        const copy: Formperfil = Object.assign({}, formperfil);
        copy.tFecreg = this.dateUtils.toDate(formperfil.tFecreg);
        copy.tFecupd = this.dateUtils.toDate(formperfil.tFecupd);
        return copy;
    }

    private convertDireccion(obj: Direccion[]): Direccion[] {
        const direccion: Direccion[] = new Array<Direccion>();
        for (let i = 0; i < obj.length; i++) {
            const copy: Direccion = Object.assign({}, obj[i]);
            copy.tFecreg = this.dateUtils.toDate(obj[i].tFecreg);
            copy.tFecupd = this.dateUtils.toDate(obj[i].tFecupd);
            direccion.push(copy);
        }
        return direccion;
    }

    obtenerFormularioPerfil(codPerfil: number): Observable<Formperfil> {
        const copy = '';
        const url = SERVER_API_URL + this.resourceDictamenes + '/api/obtenerFormularioPerfilPorCodigo';
        return this.http.put(url + '?nCodfperf=' + codPerfil, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    guardarFormularioPerfil(formperfil: Formperfil, direcciones: Direccion[]): Observable<Formperfil> {
        let data = '';
        data = 'formperfil: ';
        const copy = JSON.stringify(this.convert(formperfil));
        data += copy + ', direcciones: ';
        const copy2 = JSON.stringify(direcciones);
        data += copy2;
        console.log(data);
        // trssdt
        const headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Accept', 'application/json');
        const options = new RequestOptions();
        options.headers = headers;
        const url = SERVER_API_URL + this.resourceDictamenes + 'api/guardarFormularioPerfil';
        return this.http.post(url, data, options).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

}

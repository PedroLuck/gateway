import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../../../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { PagosNacionComponent } from './pagosnacion.component';

export const pagosnacionRoute: Routes = [
    {
        path: 'pagosnacion',
        component: PagosNacionComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.pagosnacion.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GatewaySharedModule } from '../../../../../shared';
import {
    UndnegocioService,
    UndnegocioComponent,
    undnegocioRoute,
} from './';

const ENTITY_STATES = [
    ...undnegocioRoute,
];

@NgModule({
    imports: [
        GatewaySharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        UndnegocioComponent,
    ],
    entryComponents: [
        UndnegocioComponent,
    ],
    providers: [
        UndnegocioService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GatewayUndnegocioModule {}

import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GatewaySharedModule } from '../../../../../shared';
import {
    OrganizacioService,
    OrganizacioComponent,
    organizacioRoute,
} from './';

const ENTITY_STATES = [
    ...organizacioRoute,
];

@NgModule({
    imports: [
        GatewaySharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        OrganizacioComponent,
    ],
    entryComponents: [
        OrganizacioComponent,
    ],
    providers: [
        OrganizacioService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GatewayOrganizacioModule {}

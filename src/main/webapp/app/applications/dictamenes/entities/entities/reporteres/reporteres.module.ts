import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GatewaySharedModule } from '../../../../../shared';
import {
    ReporteresService,
    ReporteresComponent,
    reporteresRoute,
} from './';

const ENTITY_STATES = [
    ...reporteresRoute,
];

@NgModule({
    imports: [
        GatewaySharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        ReporteresComponent,
    ],
    entryComponents: [
        ReporteresComponent,
    ],
    providers: [
        ReporteresService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GatewayReporteresModule {}

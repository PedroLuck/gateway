import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { InputMaskModule } from 'primeng/primeng';

import { GatewaySharedModule } from '../../../../../shared';
import {
    UsuarioService,
    UsuarioComponent,
    usuarioRoute,
    UsuarioResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...usuarioRoute,
];

@NgModule({
    imports: [
        GatewaySharedModule,
        InputMaskModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        UsuarioComponent,
    ],
    entryComponents: [
        UsuarioComponent,
    ],
    providers: [
        UsuarioService,
        UsuarioResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GatewayUsuarioModule {}

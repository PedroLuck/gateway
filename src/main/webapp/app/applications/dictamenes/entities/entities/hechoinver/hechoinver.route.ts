import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../../../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { HechoinverComponent } from './hechoinver.component';

export const hechoinverRoute: Routes = [
    {
        path: 'hechoinver',
        component: HechoinverComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.hechoinver.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

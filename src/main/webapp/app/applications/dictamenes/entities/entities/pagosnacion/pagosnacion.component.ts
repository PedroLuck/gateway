import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { PagosNacion } from './pagosnacion.model';
import { PagosNacionService } from './pagosnacion.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../../../../shared';

@Component({
    selector: 'jhi-pagosnacion',
    templateUrl: './pagosnacion.component.html'
})
export class PagosNacionComponent implements OnInit, OnDestroy {
pagosnacions: PagosNacion[];
    currentAccount: any;
    eventSubscriber: Subscription;
    currentSearch: string;

    constructor(
        private pagosnacionService: PagosNacionService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private activatedRoute: ActivatedRoute,
        private principal: Principal
    ) {
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
    }

    loadAll() {
        if (this.currentSearch) {
            this.pagosnacionService.search({
                query: this.currentSearch,
                }).subscribe(
                    (res: ResponseWrapper) => this.pagosnacions = res.json,
                    (res: ResponseWrapper) => this.onError(res.json)
                );
            return;
       }
        this.pagosnacionService.query().subscribe(
            (res: ResponseWrapper) => {
                this.pagosnacions = res.json;
                this.currentSearch = '';
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.currentSearch = query;
        this.loadAll();
    }

    clear() {
        this.currentSearch = '';
        this.loadAll();
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInPagosNacions();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: PagosNacion) {
        return item.id;
    }
    registerChangeInPagosNacions() {
        this.eventSubscriber = this.eventManager.subscribe('pagosnacionListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}

import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GatewaySharedModule } from '../../../../../shared';
import {
    ResulnegocService,
    ResulnegocComponent,
    resulnegocRoute,
} from './';

const ENTITY_STATES = [
    ...resulnegocRoute,
];

@NgModule({
    imports: [
        GatewaySharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        ResulnegocComponent,
    ],
    entryComponents: [
        ResulnegocComponent,
    ],
    providers: [
        ResulnegocService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GatewayResulnegocModule {}

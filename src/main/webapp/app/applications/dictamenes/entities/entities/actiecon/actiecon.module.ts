import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GatewaySharedModule } from '../../../../../shared';
import {
    ActieconService,
    ActieconComponent,
    actieconRoute,
} from './';

const ENTITY_STATES = [
    ...actieconRoute,
];

@NgModule({
    imports: [
        GatewaySharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        ActieconComponent,
    ],
    entryComponents: [
        ActieconComponent,
    ],
    providers: [
        ActieconService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GatewayActieconModule {}

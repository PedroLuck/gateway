import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { AsignacionSolicitudesComponent } from './asignacion-solicitudes.component';
import { UserRouteAccessService } from '../../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

export const AsignacionSolicitudesRoute: Routes = [
    {
        path: 'dictamenes/asignacion-solicitudes',
        component: AsignacionSolicitudesComponent,
        children: []
    }
];

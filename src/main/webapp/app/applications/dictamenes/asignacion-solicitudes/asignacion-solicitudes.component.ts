import { Component, OnInit, OnDestroy, Output, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiAlertService, JhiLanguageService, JhiDateUtils } from 'ng-jhipster';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../../shared';
import { SolicitudService, Solicitud } from '../entities/entities/solicitud/index';
import { UsusolService, Ususol } from '../entities/entities/ususol/index';
import { FieldsetModule, Message, BlockableUI, ProgressBarModule } from 'primeng/primeng';
import { Empresa } from '../../general/servicesmodel/empresa.model';
import { Persona } from '../../general/servicesmodel/persona.model';
import { AsignacionSolicitudesService } from './index';
import { Organizacio } from '../entities/entities/organizacio/index';
import { Usuario } from '../entities/entities/usuario/index';
import { Pernatural } from '../entities/entities/pernatural/index';
import { DatePipe } from '@angular/common';
import { UsusolId } from '../entities/entities/ususol/ususolid.model';
import { Formperfil, FormperfilService } from '../entities/entities/formperfil/index';
import { SolicformService, Solicform } from '../entities/entities/solicform/index';
import { DictamenesConstants } from '../entities/index';

@Component({
    selector: 'jhi-asignacion-solicitudes',
    templateUrl: './asignacion-solicitudes.component.html',
    styleUrls: ['asignacion-solicitudes.scss']
})

export class AsignacionSolicitudesComponent implements OnInit, OnDestroy, BlockableUI {
    currentAccount: Account;
    eventSubscriber: Subscription;
    display: boolean
    rucSearch: string;
    razonSocialSearch: string;

    @ViewChild('pnldialog') pnldialog;

    block: boolean;
    blockDialog: boolean;
    editar: boolean;
    displayAsignarRevisor: boolean;
    displayAsignarAnalista: boolean;

    // Mensajes
    messages: Message[] = [];
    messagesForm: Message[] = [];
    messageList: any;

    // Solicitudes
    listaSolicitudes: Solicitud[];
    solicitudTemporal: Solicitud;

    empresa: Empresa;
    arbitro: Empresa;
    sindicato: Organizacio;
    // coordinador: Usuario;

    nombreArbitro: string;
    nombreEmpresa: string;
    nombreSindicato: string;
    // nombreCoordinador: string;

    numdoc: string;

    // Usuario Solicitud
    tipoUsuario = 'ADMINISTRADOR';

    vigenciaInicio: any;
    vigenciaFinal: any;

    selectedRevisor: number;
    selectedAnalista: number;
    selectedRevisorText: string;
    selectedAnalistaText: string;

    constantes: DictamenesConstants;

    solicitudRevisor: Solicitud;
    solicitudAnalista: Solicitud;

    constructor(
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private activatedRoute: ActivatedRoute,
        private principal: Principal,
        private router: Router,
        private solicitudService: SolicitudService,
        private ususolService: UsusolService,
        private asignacionSolicitudesService: AsignacionSolicitudesService,
        private formperfilService: FormperfilService,
        private solicformService: SolicformService,
        private datepipe: DatePipe,
        private dateUtils: JhiDateUtils,
    ) { }

    loadAll() {
        this.obtenerListaSolicitud();
    }

    ngOnInit() {
        this.constantes = new DictamenesConstants();
        this.loadAll();
        this.block = false;
        this.editar = false;
        this.displayAsignarRevisor = false;
        this.displayAsignarAnalista = false;
        this.selectedRevisorText = '';
        this.selectedAnalistaText = '';
        this.blockDialog = false;
        this.rucSearch = '';
        this.razonSocialSearch = '';
        this.display = false;
        this.arbitro = new Empresa;
        this.empresa = new Empresa;
        this.sindicato = new Organizacio;
        this.selectedRevisor = 0;
        this.selectedAnalista = 0;

        // this.coordinador.pernatural = new Pernatural;
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
    }

    ngOnDestroy() { }

    // Solo numeros
    keyPress(event: any) {
        const pattern = /[0-9]/;
        const inputChar = String.fromCharCode(event.charCode);
        if (!pattern.test(inputChar)) {
            event.preventDefault();
        }
    }

    buscarSolicitudes() {
        this.block = true;
        const estado: number[] = [4, 6];
        this.solicitudService.buscarSolicitudesPorEstado(estado, this.rucSearch, this.razonSocialSearch).subscribe(
            (res: ResponseWrapper) => this.listaSolicitudes = res.json,
            (res: ResponseWrapper) => this.onError(res.json),
        );
        this.block = false;
    }

    obtenerListaSolicitud() {
        const estado: number[] = [4, 6];
        this.solicitudService.obtenerSolicitudPorEstado(estado).subscribe(
            (res: ResponseWrapper) => {
                this.listaSolicitudes = res.json
                for (let i = 0; i < this.listaSolicitudes.length; i++) {
                    this.ususolService.obtenerUsuarioPorTipo(this.listaSolicitudes[i].nCodsolic, 'RF').subscribe(
                        (ususol) => {
                            if (ususol !== undefined && ususol !== null) {
                                this.listaSolicitudes[i].vNombresRevisorEconomico = ususol.vNombres;
                            }
                        }
                    );
                    this.ususolService.obtenerUsuarioPorTipo(this.listaSolicitudes[i].nCodsolic, 'AF').subscribe(
                        (ususol) => {
                            if (ususol !== undefined && ususol !== null) {
                                this.listaSolicitudes[i].vNombresAnalistaEconomico = ususol.vNombres;
                            }
                        }
                    );
                }
            },
            (res: ResponseWrapper) => this.onError(res.json),
        );
    }

    mostrarModalAsignacion() { }

    asignarRevisorEconomico(obj: Solicitud) {
        this.solicitudRevisor = obj;
        this.displayAsignarRevisor = true;
    }

    asignarAnalistaEconomico(obj: Solicitud) {
        this.solicitudAnalista = obj;
        this.displayAsignarAnalista = true;
    }

    selectRevisorchange(event: any) {
        this.selectedRevisorText = event.target[event.target.value].label;
    }

    guardarAsignacionRevisor() {
        const ususol = new Ususol();
        const ususolId = new UsusolId();

        ususolId.nCodsolic = this.solicitudRevisor.nCodsolic;
        ususolId.vCodusu = this.selectedRevisor.toString();
        ususol.nSedereg = 0;
        ususol.vUsuareg = 'Usureg1';
        ususol.ususolId = ususolId;
        ususol.vTipousu = 'RF';
        ususol.tFecreg = this.datepipe.transform((new Date), 'yyyy-MM-dd HH:mm:ss');
        ususol.vNombres = this.selectedRevisorText;
        ususol.nFlgactivo = true;

        this.ususolService.create(ususol).subscribe(
            (res: Ususol) => {
                this.obtenerListaSolicitud();
            }
        );
        this.solicitudRevisor = new Solicitud();
        this.selectedRevisor = 0;
    }

    guardarAsignacionAnalista() {
        const ususol = new Ususol();
        const ususolId = new UsusolId();

        ususolId.nCodsolic = this.solicitudAnalista.nCodsolic;
        ususolId.vCodusu = this.selectedAnalista.toString();
        ususol.nSedereg = 0;
        ususol.vUsuareg = 'Usureg1';
        ususol.ususolId = ususolId;
        ususol.vTipousu = 'AF';
        ususol.tFecreg = this.datepipe.transform((new Date), 'yyyy-MM-dd HH:mm:ss');
        ususol.vNombres = this.selectedAnalistaText;
        ususol.nFlgactivo = true;

        this.ususolService.create(ususol).subscribe(
            (res: Ususol) => {
                this.obtenerListaSolicitud();
            }
        );
        this.solicitudAnalista = new Solicitud();
        this.selectedAnalista = 0;
    }

    private onError(error: any) {
        this.messages = [];
        this.messages.push({ severity: 'error', summary: 'Mensaje de Error', detail: error.message });
    }

    private onErrorMultiple(errorList: any) {
        for (let i = 0; i < errorList.length; i++) {
            this.messagesForm.push(errorList[i]);
        }
    }

    getBlockableElement(): HTMLElement {
        throw new Error('Method not implemented.');
    }
}

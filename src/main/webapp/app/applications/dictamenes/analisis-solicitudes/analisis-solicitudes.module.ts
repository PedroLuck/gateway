import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { GatewaySharedModule } from '../../../shared';
import {DialogModule, TabViewModule, DropdownModule, MessagesModule, BlockUIModule, FieldsetModule, BlockableUI, ProgressBarModule} from 'primeng/primeng';
import { AnalisisSolicitudesRoute
       , AnalisisSolicitudesComponent
       , AnalisisSolicitudesService } from './';
import { SolicitudService, SolicitudComponent } from '../entities/entities/solicitud/index';
import { UsusolService } from '../entities/entities/ususol/index';

const ENTITY_STATES = [
    ...AnalisisSolicitudesRoute,
];

@NgModule({
    declarations: [
        AnalisisSolicitudesComponent,
    ],
    imports: [
        GatewaySharedModule,
        RouterModule.forChild(ENTITY_STATES),
        DialogModule,
        FieldsetModule,
        TabViewModule,
        DropdownModule,
        MessagesModule,
        BlockUIModule,
        ProgressBarModule

    ],
    entryComponents: [
        AnalisisSolicitudesComponent,
    ],
    providers: [
        AnalisisSolicitudesService,
        UsusolService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GatewayAnalisisSolicitudesModule {}

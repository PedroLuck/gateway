import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { AnalisisSolicitudesComponent } from './analisis-solicitudes.component';
import { UserRouteAccessService } from '../../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

export const AnalisisSolicitudesRoute: Routes = [
    {
        path: 'dictamenes/analisis-solicitudes',
        component: AnalisisSolicitudesComponent,
        children: []
    }
];

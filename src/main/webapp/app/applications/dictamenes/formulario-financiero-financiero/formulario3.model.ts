import { BaseEntity } from '../../../shared/index';
import { Tabla } from './tabla.model';

export class Formulario3 implements BaseEntity {
    constructor(
        public id?: number,
        // Tablas
        public listaAjustes?: Tabla[],
        public listaVariacion?: Tabla,
        public listaIncrementoNetoActivos?: Tabla[],
        public listaIncrementoNetoPasivos?: Tabla[],
        public listaResultadoPeriodo?: Tabla[],
        // Totales
        public resultadoNeto?: Tabla,
        public totalPasivo?: Tabla,
        public totalPasivoPatrimonio?: Tabla,
    ) { }
}

import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { JhiDateUtils } from 'ng-jhipster';
import { Solicform, SolicformService } from '../entities/entities/solicform/index';

@Injectable()
export class FormularioFinancieroFinancieroService {

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    guardarObservaciones(observacion: string,
        solicForm: Solicform,
        solicformService: SolicformService,
        tipoUsuario: string) {

        if (solicForm.vObserva.indexOf(tipoUsuario) !== -1) {
            let observacionFinal = '';
            const cadena = solicForm.vObserva.split('===');
            let indexRF = -1;
            let cont = -1;
            for (let i = 0; i < cadena.length; i++) {
                if (cadena[i].indexOf(tipoUsuario) > -1) {
                    cont = cadena[i].indexOf(tipoUsuario);
                    indexRF = i;
                    break;
                }
            }
            if (cont !== -1) {
                const obscadena = cadena[indexRF].split('=_=');
                obscadena[1] = observacion === null ? '' : observacion;
                cadena[indexRF] = tipoUsuario + '=_=' + obscadena[1]
                    + '=_=' + obscadena[2]
                    + '=_=' + obscadena[3]
                    + '=_=' + obscadena[4]
                    + '=_=' + obscadena[5]
                    + '=_=' + obscadena[6];
            }
            for (let j = 0; j < cadena.length; j++) {
                observacionFinal += cadena[j] + '===';
            }
            solicForm.vObserva = observacionFinal;
        } else {
            solicForm.vObserva = (solicForm.vObserva === null ? '' : solicForm.vObserva) + tipoUsuario
                + '=_=' + observacion === null ? '' : observacion + '===';
        }

        solicformService.update(solicForm).subscribe(
            (res: Solicform) => { }
        );
    }
}

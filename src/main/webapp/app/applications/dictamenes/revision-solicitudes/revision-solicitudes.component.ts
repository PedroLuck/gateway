import { Component, OnInit, OnDestroy, Output, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiAlertService, JhiLanguageService, JhiDateUtils } from 'ng-jhipster';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../../shared';
import { SolicitudService, Solicitud } from '../entities/entities/solicitud/index';
import { UsusolService, Ususol } from '../entities/entities/ususol/index';
import { FieldsetModule, Message, BlockableUI, ProgressBarModule } from 'primeng/primeng';
import { Empresa } from '../../general/servicesmodel/empresa.model';
import { Persona } from '../../general/servicesmodel/persona.model';
import { RevisionSolicitudesService } from './index';
import { Organizacio } from '../entities/entities/organizacio/index';
import { Usuario } from '../entities/entities/usuario/index';
import { Pernatural } from '../entities/entities/pernatural/index';
import { DatePipe } from '@angular/common';
import { UsusolId } from '../entities/entities/ususol/ususolid.model';
import { Formperfil, FormperfilService } from '../entities/entities/formperfil/index';
import { SolicformService, Solicform } from '../entities/entities/solicform/index';
import { DictamenesConstants } from '../entities/index';

@Component({
    selector: 'jhi-revision-solicitudes',
    templateUrl: './revision-solicitudes.component.html',
    styleUrls: ['revision-solicitudes.scss']
})

export class RevisionSolicitudesComponent implements OnInit, OnDestroy, BlockableUI {
    currentAccount: Account;
    eventSubscriber: Subscription;
    display: boolean
    rucSearch: string;
    razonSocialSearch: string;

    @ViewChild('pnldialog') pnldialog;

    block: boolean;
    blockDialog: boolean;
    editar: boolean;
    displayEliminar: boolean;

    // Mensajes
    messages: Message[] = [];
    messagesForm: Message[] = [];
    messageList: any;

    // Solicitudes
    listaSolicitudes: Solicitud[];
    solicitudTemporal: Solicitud;

    empresa: Empresa;
    arbitro: Empresa;
    sindicato: Organizacio;
    // coordinador: Usuario;

    nombreArbitro: string;
    nombreEmpresa: string;
    nombreSindicato: string;
    // nombreCoordinador: string;

    numdoc: string;

    // Usuario Solicitud
    vCodUsuario = '1';

    vigenciaInicio: any;
    vigenciaFinal: any;

    constantes: DictamenesConstants;

    constructor(
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private activatedRoute: ActivatedRoute,
        private principal: Principal,
        private router: Router,
        private solicitudService: SolicitudService,
        private ususolService: UsusolService,
        private revisionSolicitudesService: RevisionSolicitudesService,
        private formperfilService: FormperfilService,
        private solicformService: SolicformService,
        private datepipe: DatePipe,
        private dateUtils: JhiDateUtils,
    ) { }

    loadAll() {
        this.obtenerListaSolicitud();
    }

    ngOnInit() {
        this.constantes = new DictamenesConstants();
        this.loadAll();
        this.block = false;
        this.editar = false;
        this.displayEliminar = false;
        this.blockDialog = false;
        this.rucSearch = '';
        this.razonSocialSearch = '';
        this.display = false;
        this.arbitro = new Empresa;
        this.empresa = new Empresa;
        this.sindicato = new Organizacio;

        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
    }

    ngOnDestroy() { }

    // Solo numeros
    keyPress(event: any) {
        const pattern = /[0-9]/;
        const inputChar = String.fromCharCode(event.charCode);
        if (!pattern.test(inputChar)) {
            event.preventDefault();
        }
    }

    buscarSolicitudes() {
        this.block = true;
        const estado: number[] = [4];
        this.solicitudService.buscarSolicitudesPorEstadoUsuario(estado, this.rucSearch, this.razonSocialSearch, this.vCodUsuario).subscribe(
            (res: ResponseWrapper) => this.listaSolicitudes = res.json,
            (res: ResponseWrapper) => this.onError(res.json),
        );
        this.block = false;
    }

    obtenerListaSolicitud() {
        const estado: number[] = [4];
        this.solicitudService.obtenerSolicitudPorEstadoUsuario(estado,  this.vCodUsuario).subscribe(
            (res: ResponseWrapper) => {
                this.listaSolicitudes = res.json
            },
            (res: ResponseWrapper) => this.onError(res.json),
        );
    }

    refrescarSolicitudes(event: any) {
        this.obtenerListaSolicitud();
    }

    revisarSolicitud(obj: Solicitud) {
        // logica de revision ir a control de informacion
        this.router.navigate(['../../dictamenes/control-informacion/' + obj.nCodsolic, 1 , 'RF']);
    }

    private onError(error: any) {
        this.messages = [];
        this.messages.push({ severity: 'error', summary: 'Mensaje de Error', detail: error.message });
    }

    private onErrorMultiple(errorList: any) {
        for (let i = 0; i < errorList.length; i++) {
            this.messagesForm.push(errorList[i]);
        }
    }

    darConformidad(obj: Solicitud) {
        obj.nFlgestado = 6;
        this.solicitudService.update(obj).subscribe();
        // boton de Envio de informacion
    }

    getBlockableElement(): HTMLElement {
        throw new Error('Method not implemented.');
    }
}

import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { RevisionSolicitudesComponent } from './revision-solicitudes.component';
import { UserRouteAccessService } from '../../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

export const RevisionSolicitudesRoute: Routes = [
    {
        path: 'dictamenes/revision-solicitudes',
        component: RevisionSolicitudesComponent,
        children: []
    }
];

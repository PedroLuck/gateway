import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { GatewaySharedModule } from '../../../shared';
import {DialogModule, TabViewModule, DropdownModule, MessagesModule, BlockUIModule, FieldsetModule, BlockableUI, ProgressBarModule} from 'primeng/primeng';
import { RevisionSolicitudesRoute
       , RevisionSolicitudesComponent
       , RevisionSolicitudesService } from './';
import { SolicitudService, SolicitudComponent } from '../entities/entities/solicitud/index';
import { UsusolService } from '../entities/entities/ususol/index';

const ENTITY_STATES = [
    ...RevisionSolicitudesRoute,
];

@NgModule({
    declarations: [
        RevisionSolicitudesComponent,
    ],
    imports: [
        GatewaySharedModule,
        RouterModule.forChild(ENTITY_STATES),
        DialogModule,
        FieldsetModule,
        TabViewModule,
        DropdownModule,
        MessagesModule,
        BlockUIModule,
        ProgressBarModule

    ],
    entryComponents: [
        RevisionSolicitudesComponent,
    ],
    providers: [
        RevisionSolicitudesService,
        UsusolService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GatewayRevisionSolicitudesModule {}

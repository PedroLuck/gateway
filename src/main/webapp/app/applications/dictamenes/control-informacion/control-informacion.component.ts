import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiAlertService, JhiLanguageService, JhiDateUtils } from 'ng-jhipster';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../../shared';
import { Solicitud, SolicitudService } from '../entities/entities/solicitud/index';
import { Solicform, SolicformService } from '../entities/entities/solicform/';
import { LocalStorage } from 'ng2-webstorage';
import { Undnegocio } from '../entities/entities/undnegocio/index';
import { Participa } from '../entities/entities/participa/index';
import { Hechoinver } from '../entities/entities/hechoinver/index';
import { Direccion } from '../entities/entities/direccion/index';
import { Negocolect } from '../entities/entities/negocolect/index';
import { Resulnegoc } from '../entities/entities/resulnegoc/index';
import { Respinforma } from '../entities/entities/respinforma/index';
import { ModelAnexo } from '../entities/entities/anexlaboral/modelanexo.model';
import { Formperfil, FormperfilService } from '../entities/entities/formperfil/index';
import { ComboModel } from '../../general/combobox.model';
import { Formulario } from '../formulario-perfil/formulario.model';
import { FormGroup } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { forEach } from '@angular/router/src/utils/collection';
import { Message } from 'primeng/components/common/api';
import { FileUploadModule } from 'primeng/primeng';
import { DictamenesConstants, Formfinanc, FormfinancService } from '../entities/index';

@Component({
    selector: 'jhi-control-informacion',
    templateUrl: './control-informacion.component.html',
    styleUrls: ['control-informacion.scss']
})

export class ControlInformacionComponent implements OnInit, OnDestroy {
    solicitud: Solicitud;
    solicFormsPerfil: Solicform[];
    solicFormsFSFinanciero: Solicform[];
    solicFormsFSPrivado: Solicform[];
    solicFormsFSPublico: Solicform[];
    solicFormsOpcional: Solicform[];
    currentAccount: Account;
    eventSubscriber: Subscription;
    private subscription: Subscription;
    // Mensajes
    messages: Message[] = [];
    messagesForm: Message[] = [];
    messageList: any;

    // Archivos
    uploadedFiles: any[] = [];

    displayEnviar: boolean;
    displayObservacion: boolean;

    // Datos de Perfil
    @LocalStorage('solicitud')
    solicitudLS: Solicitud;
    @LocalStorage('solicform')
    solicForm: Solicform;
    @LocalStorage('formperfil')
    formPerfil: Formperfil;

    // Listados de dato
    @LocalStorage('undNegocios')
    undNegocios: Undnegocio[];
    @LocalStorage('participacionesAccionarias')
    participacionesAccionarias: Participa[];
    @LocalStorage('participacionesMercado')
    participacionesMercados: Participa[];
    @LocalStorage('obras')
    obras: Hechoinver[];
    @LocalStorage('proyectos')
    proyectos: Hechoinver[];
    @LocalStorage('direcciones')
    direcciones: Direccion[];
    @LocalStorage('solicitante')
    solicitante: Negocolect;
    @LocalStorage('organizaciones')
    organizaciones: Negocolect[];
    @LocalStorage('resultadoNegociaciones')
    resultadoNegociaciones: Resulnegoc[];
    @LocalStorage('responInfoFinanciera')
    responInfoFinanciera: Respinforma;
    @LocalStorage('responeInfoLaboral')
    responeInfoLaboral: Respinforma;
    @LocalStorage('anexoLaboral')
    formulario: Formulario[];

    @LocalStorage('inicioDir')
    inicioDir: boolean;
    @LocalStorage('inicioUnidad')
    inicioUnidad: boolean;
    @LocalStorage('inicioAccionaria')
    inicioAccionaria: boolean;
    @LocalStorage('inicioMercado')
    inicioMercado: boolean;
    @LocalStorage('inicioObra')
    inicioObra: boolean;
    @LocalStorage('inicioProyecto')
    inicioProyecto: boolean;
    @LocalStorage('inicioFinanciero')
    inicioFinanciero: boolean;
    @LocalStorage('inicioLaboral')
    inicioLaboral: boolean;
    @LocalStorage('inicioResultado')
    inicioResultado: boolean;
    @LocalStorage('inicioSolicitante')
    inicioSolicitante: boolean;
    @LocalStorage('inicioOrganizacion')
    inicioOrganizacion: boolean;
    @LocalStorage('regimenLaboral')
    selectedRegimen: ComboModel[];
    @LocalStorage('observacion')
    observacion: boolean;
    @LocalStorage('tipoUsu')
    tipoUsuario: string;

    observaciones: string;

    constructor(
        private solicitudService: SolicitudService,
        private solicformService: SolicformService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private activatedRoute: ActivatedRoute,
        private principal: Principal,
        private router: Router,
        private route: ActivatedRoute,
        private datepipe: DatePipe,
        private dateUtils: JhiDateUtils,
        private formperfilService: FormperfilService,
        private formfinancService: FormfinancService,
    ) { }

    loadAll() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['nCodsolic']);
            this.observacion = params['ncodObs'];
            this.tipoUsuario = params['ncodTipoUsu'];
        });
    }

    ngOnInit() {
        this.solicFormsPerfil = new Array<Solicform>();
        this.solicFormsFSFinanciero = new Array<Solicform>();
        this.solicFormsFSPrivado = new Array<Solicform>();
        this.solicFormsFSPublico = new Array<Solicform>();
        this.observaciones = '';
        this.displayObservacion = false;
        this.displayEnviar = false;
        this.limpiarLocalStorage();
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
    }

    load(nCodsolic) {
        this.solicitudService.find(nCodsolic).subscribe((solicitud) => {
            this.solicitud = solicitud;
            this.solicitudLS = solicitud;
            this.solicformService.obtenerformularioperfil(this.solicitud.nCodsolic).subscribe(
                (res: ResponseWrapper) => this.solicFormsPerfil = res.json,
                (res: ResponseWrapper) => this.onError(res.json)
            );
            this.solicformService.obtenerlistaFormulariosObligatorios(1, this.solicitud.nCodsolic, 'FF').subscribe(
                (res: ResponseWrapper) => this.solicFormsFSFinanciero = res.json,
                (res: ResponseWrapper) => this.onError(res.json)
            );

            this.solicformService.obtenerlistaFormulariosObligatorios(1, this.solicitud.nCodsolic, 'FP').subscribe(
                (res: ResponseWrapper) => this.solicFormsFSPrivado = res.json,
                (res: ResponseWrapper) => this.onError(res.json)
            );

            this.solicformService.obtenerlistaFormulariosObligatorios(1, this.solicitud.nCodsolic, 'FU').subscribe(
                (res: ResponseWrapper) => this.solicFormsFSPublico = res.json,
                (res: ResponseWrapper) => this.onError(res.json)
            );
            /*this.solicformService.obtenerlistaFormulariosObligatorios(0, this.solicitud.nCodsolic).subscribe(
                (res: ResponseWrapper) => this.solicFormsOpcional = res.json,
                (res: ResponseWrapper) => this.onError(res.json)
            );*/
        });
    }

    limpiarLocalStorage() {
        this.solicitudLS = null;
        this.solicForm = null;
        this.formPerfil = null;
        this.undNegocios = null;
        this.participacionesAccionarias = null;
        this.participacionesMercados = null;
        this.obras = null;
        this.proyectos = null;
        this.direcciones = null;
        this.solicitante = null;
        this.organizaciones = null;
        this.resultadoNegociaciones = null;
        this.responInfoFinanciera = null;
        this.responeInfoLaboral = null;
        this.formulario = null;
        this.selectedRegimen = null;
        // Inicios
        this.inicioDir = null;
        this.inicioAccionaria = null;
        this.inicioMercado = null;
        this.inicioObra = null;
        this.inicioProyecto = null;
        this.inicioUnidad = null;
        this.inicioFinanciero = null;
        this.inicioLaboral = null;
        this.inicioResultado = null;
        this.inicioSolicitante = null;
        this.inicioOrganizacion = null;
    }

    previousState() {
        window.history.back();
    }

    ngOnDestroy() { }

    listarSolicitudes() {
        this.router.navigate(['./dictamenes/listado-solicitudes'])
    }

    requirirProrroga() {
        this.solicitud.nFlgprorro = 1;
        this.solicitudService.update(this.solicitud).subscribe(
            (res: Solicitud) => {
                this.solicitud = res;
            }
        );
    }

    showEnviar() {
        this.displayEnviar = true;
    }

    enviarInformacion() {
        let verificar = true;
        for (let i = 0; i < this.solicFormsPerfil.length; i++) {
            if (this.solicFormsPerfil[i].vFlgest !== 'E') {
                verificar = false;
            }
        }
        if (this.solicFormsFSFinanciero.length > 0) {
            for (let i = 0; i < this.solicFormsFSFinanciero.length; i++) {
                if (this.solicFormsFSFinanciero[i].vFlgest !== 'E') {
                    verificar = false;
                }
            }
        }
        if (this.solicFormsFSPrivado.length > 0) {
            for (let i = 0; i < this.solicFormsFSPrivado.length; i++) {
                if (this.solicFormsFSPrivado[i].vFlgest !== 'E') {
                    verificar = false;
                }
            }
        }
        if (this.solicFormsFSPublico.length > 0) {
            for (let i = 0; i < this.solicFormsFSPublico.length; i++) {
                if (this.solicFormsFSPublico[i].vFlgest !== 'E') {
                    verificar = false;
                }
            }
        }
        if (verificar) {
            if (this.solicitud.nFlgestado === 5) {
                this.solicitud.tFecobserv = this.dateUtils.toDate(this.datepipe.transform(new Date(), 'yyyy-MM-dd HH:mm:ss'));
            } else {
                this.solicitud.tFecenvio = this.dateUtils.toDate(this.datepipe.transform(new Date(), 'yyyy-MM-dd HH:mm:ss'));
            }
            this.solicitud.nFlgestado = 4;
            this.solicitud.vUsuaupd = 'UsuMod1';
            this.solicitud.tFecupd = this.dateUtils.toDate(this.datepipe.transform(new Date(), 'yyyy-MM-dd HH:mm:ss'));
            this.solicitudService.update(this.solicitud).subscribe(
                (res: Solicitud) => { }
            );
            this.listarSolicitudes();
        } else {
            this.onErrorMultiple([{ severity: 'error', summary: 'Mensaje de Error', detail: 'Debe ingresar la informacion de todos los formularios obligatorios' }]);
            this.messagesForm.push();
            this.displayEnviar = false;
        }
    }

    confirmarFormulario(obj: Solicform) {
        obj.vFlgest = 'C';
        this.solicformService.update(obj).subscribe(
            (res: Solicform) => { this.listarSolicitudes(); }
        );
    }

    abrirFormulario(obj: Solicform) {
        if (obj.vTipoform === 'G' &&
            obj.nCodfperf != null &&
            (obj.vFlgest === 'P' || obj.vFlgest === 'E' || obj.vFlgest === 'O')
        ) {
            // Formularios Perfil
            this.router.navigate(['../../dictamenes/formulario-perfil/' + obj.nCodfperf, this.observacion, this.tipoUsuario]);
        } else if (obj.vTipoform === 'FP1' &&
            (obj.vFlgest === 'P' || obj.vFlgest === 'E' || obj.vFlgest === 'O')
        ) {
            // Formularios Financiero 1 Sector Privado
            this.router.navigate(['../../dictamenes/formulario-financiero-privado-n1/' + obj.nCodffina, this.observacion, this.tipoUsuario]);
        } else if (obj.vTipoform === 'FP1A' &&
            (obj.vFlgest === 'P' || obj.vFlgest === 'E' || obj.vFlgest === 'O')
        ) {
            // Formularios Financiero Anexo 1A Sector Privado
            this.router.navigate(['../../dictamenes/formulario-financiero-privado-n1a/' + obj.nCodffina, this.observacion, this.tipoUsuario]);
        } else if (obj.vTipoform === 'FP1B' &&
            (obj.vFlgest === 'P' || obj.vFlgest === 'E' || obj.vFlgest === 'O')
        ) {
            // Formularios Financiero Anexo 1B Sector Privado
            this.router.navigate(['../../dictamenes/formulario-financiero-privado-n1b/' + obj.nCodffina, this.observacion, this.tipoUsuario]);
        } else if (obj.vTipoform === 'FP1C' &&
            (obj.vFlgest === 'P' || obj.vFlgest === 'E' || obj.vFlgest === 'O')
        ) {
            // Formularios Financiero Anexo 1C Sector Privado
            this.router.navigate(['../../dictamenes/formulario-financiero-privado-n1c/' + obj.nCodffina, this.observacion, this.tipoUsuario]);
        } else if (obj.vTipoform === 'FP1D' &&
            (obj.vFlgest === 'P' || obj.vFlgest === 'E' || obj.vFlgest === 'O')
        ) {
            // Formularios Financiero Anexo 1D Sector Privado
            this.router.navigate(['../../dictamenes/formulario-financiero-privado-n1d/' + obj.nCodffina, this.observacion, this.tipoUsuario]);
        } else if (obj.vTipoform === 'FP2' &&
            (obj.vFlgest === 'P' || obj.vFlgest === 'E' || obj.vFlgest === 'O')
        ) {
            // Formularios Financiero 2 Sector Privado
            this.router.navigate(['../../dictamenes/formulario-financiero-privado-n2/' + obj.nCodffina, this.observacion, this.tipoUsuario]);
        } else if (obj.vTipoform === 'FP2A' &&
            (obj.vFlgest === 'P' || obj.vFlgest === 'E' || obj.vFlgest === 'O')
        ) {
            // Formularios Financiero Anexo 2A Sector Privado
            this.router.navigate(['../../dictamenes/formulario-financiero-privado-n2a/' + obj.nCodffina, this.observacion, this.tipoUsuario]);
        } else if (obj.vTipoform === 'FP3' &&
            (obj.vFlgest === 'P' || obj.vFlgest === 'E' || obj.vFlgest === 'O')
        ) {
            // Formularios Financiero Anexo 2B Sector Privado
            this.router.navigate(['../../dictamenes/formulario-financiero-privado-n3/' + obj.nCodffina, this.observacion, this.tipoUsuario])
        } else if (obj.vTipoform === 'FP4' &&
            (obj.vFlgest === 'P' || obj.vFlgest === 'E' || obj.vFlgest === 'O')
        ) {
            // Formularios Financiero Aenxo 2C Sector Privado
            this.router.navigate(['../../dictamenes/formulario-financiero-privado-n4/' + obj.nCodffina, this.observacion, this.tipoUsuario])
        } else if (obj.vTipoform === 'FP5' &&
            (obj.vFlgest === 'P' || obj.vFlgest === 'E' || obj.vFlgest === 'O')
        ) {
            // Formularios Financiero 3 Sector Privado
            this.router.navigate(['../../dictamenes/formulario-financiero-privado-n5/' + obj.nCodffina, this.observacion, this.tipoUsuario])
        } else if (obj.vTipoform === 'FP6' &&
            (obj.vFlgest === 'P' || obj.vFlgest === 'E' || obj.vFlgest === 'O')
        ) {
            // Formularios Financiero 3 Sector Privado
            this.router.navigate(['../../dictamenes/formulario-financiero-privado-n6/' + obj.nCodffina, this.observacion, this.tipoUsuario])
        } else if (obj.vTipoform === 'FF1' &&
            (obj.vFlgest === 'P' || obj.vFlgest === 'E' || obj.vFlgest === 'O')
        ) {
            // Formularios Financiero 1 Sector Financiero
            this.router.navigate(['../../dictamenes/formulario-financiero-financiero-n1/' + obj.nCodffina, this.observacion, this.tipoUsuario])
        } else if (obj.vTipoform === 'FF2' &&
            (obj.vFlgest === 'P' || obj.vFlgest === 'E' || obj.vFlgest === 'O')
        ) {
            // Formularios Financiero 2 Sector Financiero
            this.router.navigate(['../../dictamenes/formulario-financiero-financiero-n2/' + obj.nCodffina, this.observacion, this.tipoUsuario])
        } else if (obj.vTipoform === 'FF2A' &&
            (obj.vFlgest === 'P' || obj.vFlgest === 'E' || obj.vFlgest === 'O')
        ) {
            // Formularios Financiero Anexo 2A Sector Financiero
            this.router.navigate(['../../dictamenes/formulario-financiero-financiero-n2a/' + obj.nCodffina, this.observacion, this.tipoUsuario])
        } else if (obj.vTipoform === 'FF3' &&
            (obj.vFlgest === 'P' || obj.vFlgest === 'E' || obj.vFlgest === 'O')
        ) {
            // Formularios Financiero Anexo 2B Sector Financiero
            this.router.navigate(['../../dictamenes/formulario-financiero-financiero-n3/' + obj.nCodffina, this.observacion, this.tipoUsuario])
        } else if (obj.vTipoform === 'FF4' &&
            (obj.vFlgest === 'P' || obj.vFlgest === 'E' || obj.vFlgest === 'O')
        ) {
            // Formularios Financiero Anexo 2C Sector Financiero
            this.router.navigate(['../../dictamenes/formulario-financiero-financiero-n4/' + obj.nCodffina, this.observacion, this.tipoUsuario])
        } else if (obj.vTipoform === 'FF5' &&
            (obj.vFlgest === 'P' || obj.vFlgest === 'E' || obj.vFlgest === 'O')
        ) {
            // Formularios Financiero 3 Sector Financiero
            this.router.navigate(['../../dictamenes/formulario-financiero-financiero-n5/' + obj.nCodffina, this.observacion, this.tipoUsuario])
        } else if (obj.vTipoform === 'FF6' && (obj.vFlgest === 'P' || obj.vFlgest === 'E' || obj.vFlgest === 'O')
        ) {
            // Formularios Financiero 4 Sector Financiero
            this.router.navigate(['../../dictamenes/formulario-financiero-financiero-n6/' + obj.nCodffina, this.observacion, this.tipoUsuario])
        } else if (obj.vTipoform === 'FF7' &&
            (obj.vFlgest === 'P' || obj.vFlgest === 'E' || obj.vFlgest === 'O')
        ) {
            // Formularios Financiero 5 Sector Financiero
            this.router.navigate(['../../dictamenes/formulario-financiero-financiero-n7/' + obj.nCodffina, this.observacion, this.tipoUsuario])
        } else if (obj.vTipoform === 'FF8' &&
            (obj.vFlgest === 'P' || obj.vFlgest === 'E' || obj.vFlgest === 'O')
        ) {
            // Formularios Financiero 6 Sector Financiero
            this.router.navigate(['../../dictamenes/formulario-financiero-financiero-n8/' + obj.nCodffina, this.observacion, this.tipoUsuario])
        } else {
            this.router.navigate(['./dictamenes/control-informacion/' + obj.nCodsolic, this.observacion, this.tipoUsuario])
        }
    }

    refrescarLista(event: any) {
        this.loadAll();
    }

    // --------------------------- Errores ------------------------------------
    private onErrorMultiple(errorList: any) {
        for (let i = 0; i < errorList.length; i++) {
            this.messagesForm.push(errorList[i]);
        }
    }

    private onError(error: any) {
        this.messages = [];
        this.messages.push({ severity: 'error', summary: 'Mensaje de Error', detail: error.message });
    }

    regresarRevisionSolicitudes() {
        this.router.navigate(['./dictamenes/revision-solicitudes']);
    }

    verObservaciones(obj: Solicform) {
        this.observaciones = '';
        if (obj != null) {
            if (obj.vObserva !== null) {
                const cadena = obj.vObserva.split('===');
                let indexRF = -1;
                let cont = -1;
                for (let i = 0; i < cadena.length; i++) {
                    if (cadena[i].indexOf(this.tipoUsuario) > -1) {
                        cont = cadena[i].indexOf(this.tipoUsuario);
                        indexRF = i;
                        break;
                    }
                }
                if (cont !== -1) {
                    const obscadena = cadena[indexRF].split('=_=');
                    this.observaciones += obscadena[1] + ', ';
                    this.observaciones += obscadena[2] + ', ';
                    this.observaciones += obscadena[3] + ', ';
                    this.observaciones += obscadena[4] + ', ';
                    this.observaciones += obscadena[5] + ', ';
                    this.observaciones += obscadena[6] + ', ';
                }
            }
        }
        this.displayObservacion = true;
    }

    enviarFormulario(obj: Solicform) {
        obj.vFlgest = 'E';
        this.solicformService.update(obj).subscribe();
    }

    enviarFormularioPerfil(obj: Solicform) {
        this.formperfilService.obtenerFormularioPerfil(obj.nCodfperf).subscribe(
            (formperfil: Formperfil) => {
                const constantes: DictamenesConstants = new DictamenesConstants();
                const formularioArray = new Array<Formfinanc>();
                switch (this.formPerfil.vSector) {
                    case '1':
                        // Privado
                        for (let i = 0; i < 11; i++) {
                            let nomForm = '';
                            let codForm = '';
                            const form1 = new Formfinanc();
                            form1.nFlgactivo = 1;
                            form1.vUsuareg = 'Usureg1';
                            form1.nSedereg = 0;
                            form1.tFecreg = this.datepipe.transform((new Date), 'yyyy-MM-dd HH:mm:ss');
                            this.formfinancService.create(form1).subscribe(
                                (formfinanc1: Formfinanc) => {
                                    switch (i) {
                                        case 0: nomForm = constantes.FORMULARIO_PRIVADO_1;
                                            codForm = constantes.FORMULARIO_COD_PRIVADO_1;
                                            break;
                                        case 1: nomForm = constantes.FORMULARIO_PRIVADO_1A;
                                            codForm = constantes.FORMULARIO_COD_PRIVADO_1A;
                                            break;
                                        case 2: nomForm = constantes.FORMULARIO_PRIVADO_1B;
                                            codForm = constantes.FORMULARIO_COD_PRIVADO_1B;
                                            break;
                                        case 3: nomForm = constantes.FORMULARIO_PRIVADO_1C;
                                            codForm = constantes.FORMULARIO_COD_PRIVADO_1C;
                                            break;
                                        case 4: nomForm = constantes.FORMULARIO_PRIVADO_1D;
                                            codForm = constantes.FORMULARIO_COD_PRIVADO_1D;
                                            break;
                                        case 5: nomForm = constantes.FORMULARIO_PRIVADO_2;
                                            codForm = constantes.FORMULARIO_COD_PRIVADO_2;
                                            break;
                                        case 6: nomForm = constantes.FORMULARIO_PRIVADO_2A;
                                            codForm = constantes.FORMULARIO_COD_PRIVADO_2A;
                                            break;
                                        case 7: nomForm = constantes.FORMULARIO_PRIVADO_3;
                                            codForm = constantes.FORMULARIO_COD_PRIVADO_3;
                                            break;
                                        case 8: nomForm = constantes.FORMULARIO_PRIVADO_4;
                                            codForm = constantes.FORMULARIO_COD_PRIVADO_4;
                                            break;
                                        case 9: nomForm = constantes.FORMULARIO_PRIVADO_5;
                                            codForm = constantes.FORMULARIO_COD_PRIVADO_5;
                                            break;
                                        case 10: nomForm = constantes.FORMULARIO_PRIVADO_6;
                                            codForm = constantes.FORMULARIO_COD_PRIVADO_6;
                                            break;
                                    }

                                    const solicFormForm1 = new Solicform();
                                    solicFormForm1.nCodsolic = obj.nCodsolic;
                                    solicFormForm1.nCodffina = formfinanc1.nCodffina;
                                    solicFormForm1.nFlgactivo = 1;
                                    solicFormForm1.vUsuareg = 'Usureg';
                                    solicFormForm1.nSedereg = 0;
                                    solicFormForm1.vNomform = nomForm;
                                    solicFormForm1.vTipoform = codForm;
                                    solicFormForm1.nFlgoblig = 1;
                                    solicFormForm1.tFecreg = this.dateUtils.toDate(this.datepipe.transform((new Date), 'yyyy-MM-dd HH:mm:ss'));
                                    solicFormForm1.vFlgest = 'P';
                                    this.solicformService.create(solicFormForm1).subscribe();
                                }
                            );
                        }
                        break;
                    case '2':
                        // Publico
                        break;
                    case '3':
                        // Financiero
                        for (let i = 0; i < 9; i++) {
                            let nomForm = '';
                            let codForm = '';
                            const form1 = new Formfinanc();
                            form1.nFlgactivo = 1;
                            form1.vUsuareg = 'Usureg1';
                            form1.nSedereg = 0;
                            form1.tFecreg = this.datepipe.transform((new Date), 'yyyy-MM-dd HH:mm:ss');
                            this.formfinancService.create(form1).subscribe(
                                (formfinanc1: Formfinanc) => {
                                    switch (i) {
                                        case 0: nomForm = constantes.FORMULARIO_FINANCIERO_1;
                                            codForm = constantes.FORMULARIO_COD_FINANCIERO_1;
                                            break;
                                        case 1: nomForm = constantes.FORMULARIO_FINANCIERO_2;
                                            codForm = constantes.FORMULARIO_COD_FINANCIERO_2;
                                            break;
                                        case 2: nomForm = constantes.FORMULARIO_FINANCIERO_2A;
                                            codForm = constantes.FORMULARIO_COD_FINANCIERO_2A;
                                            break;
                                        case 3: nomForm = constantes.FORMULARIO_FINANCIERO_3;
                                            codForm = constantes.FORMULARIO_COD_FINANCIERO_3;
                                            break;
                                        case 4: nomForm = constantes.FORMULARIO_FINANCIERO_4;
                                            codForm = constantes.FORMULARIO_COD_FINANCIERO_4;
                                            break;
                                        case 5: nomForm = constantes.FORMULARIO_FINANCIERO_5;
                                            codForm = constantes.FORMULARIO_COD_FINANCIERO_5;
                                            break;
                                        case 6: nomForm = constantes.FORMULARIO_FINANCIERO_6;
                                            codForm = constantes.FORMULARIO_COD_FINANCIERO_6;
                                            break;
                                        case 7: nomForm = constantes.FORMULARIO_FINANCIERO_7;
                                            codForm = constantes.FORMULARIO_COD_FINANCIERO_7;
                                            break;
                                        case 8: nomForm = constantes.FORMULARIO_FINANCIERO_8;
                                            codForm = constantes.FORMULARIO_COD_FINANCIERO_8;
                                            break;
                                    }

                                    const solicFormForm1 = new Solicform();
                                    solicFormForm1.nCodsolic = obj.nCodsolic;
                                    solicFormForm1.nCodffina = formfinanc1.nCodffina;
                                    solicFormForm1.nFlgactivo = 1;
                                    solicFormForm1.vUsuareg = 'Usureg';
                                    solicFormForm1.nSedereg = 0;
                                    solicFormForm1.vNomform = nomForm;
                                    solicFormForm1.vTipoform = codForm;
                                    solicFormForm1.nFlgoblig = 1;
                                    solicFormForm1.tFecreg = this.dateUtils.toDate(this.datepipe.transform((new Date), 'yyyy-MM-dd HH:mm:ss'));
                                    solicFormForm1.vFlgest = 'P';
                                    this.solicformService.create(solicFormForm1).subscribe();
                                }
                            );
                        }
                        break;
                }

                obj.vFlgest = 'E';
                obj.tFecupd = this.dateUtils.toDate(this.datepipe.transform((new Date), 'yyyy-MM-dd HH:mm:ss'));
                obj.vUsuaupd = 'vUsua';
                obj.nSedeupd = 0;
                this.solicformService.update(obj).subscribe();
            }
        );
    }
}

import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GatewaySharedModule } from '../../../shared';
import {
    ConsultaNotificacionesComponent,
    ConsultaNotificacionesDialogComponent,
    ConsultaNotificacionesPopupComponent,
    ConsultaNotificacionesService,
    ConsultaNotificacionesPopupService,
    consultaNotificacionesRoute } from './';
import { TabViewModule, DataTableModule, RadioButtonModule, CheckboxModule, SpinnerModule, DropdownModule, CalendarModule, BlockUIModule } from 'primeng/primeng';
import {GrowlModule} from 'primeng/components/growl/growl';

const ENTITY_STATES = [
    ...consultaNotificacionesRoute
];

@NgModule({
    imports: [
        GatewaySharedModule,
        TabViewModule,
        DataTableModule,
        CheckboxModule,
        DropdownModule,
        CalendarModule,
        SpinnerModule,
        RadioButtonModule,
        BlockUIModule,
        GrowlModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        ConsultaNotificacionesComponent,
        ConsultaNotificacionesPopupComponent,
        ConsultaNotificacionesDialogComponent,

    ],
    entryComponents: [
        ConsultaNotificacionesComponent,
        ConsultaNotificacionesPopupComponent,
        ConsultaNotificacionesDialogComponent,
    ],
    providers: [
        ConsultaNotificacionesService,
        ConsultaNotificacionesPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ConsultaNotificacionesModule {}

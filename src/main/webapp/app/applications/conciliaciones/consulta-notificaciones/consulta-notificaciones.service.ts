
import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils } from 'ng-jhipster';
import { DatePipe } from '@angular/common';
import { ResponseWrapper, createRequestOption } from '../../../shared';

import { Direcnotif } from '../models/direcnotif.model';

@Injectable()
export class ConsultaNotificacionesService {

    private resource = '/defensa/api/';

    private resourceTipoDoc         = this.resource + 'tipdocidents';
    private resourceDirecnotif      = this.resource + 'direcnotifs';
    private resourceReportes        = '/defensa/api/reporte';

    constructor(private http: Http, private dateUtils: JhiDateUtils, private datePipe: DatePipe) { }

    consultaDirecNotificaciones(query: string): Observable<ResponseWrapper> {
        return this.http.get(`${this.resource}${query}`)
            .map((res: Response) => this.convertDirNotificaResponse(res));
    }
    private convertDirNotificaResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemDirNotificaFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }
    private convertItemDirNotificaFromServer(json: any): Direcnotif {
        const entity: Direcnotif = Object.assign(new Direcnotif(), json);
        entity.tFecreg = this.dateUtils.convertDateTimeFromServer(json.tFecreg);
        entity.tFecupd = this.dateUtils.convertDateTimeFromServer(json.tFecupd);
        return entity;
    }

    getReporte(nombre: any, parametro: any): any {
        window.open(`${this.resourceReportes}/${nombre}/${parametro}`);
        return {flag : true};
    }

}

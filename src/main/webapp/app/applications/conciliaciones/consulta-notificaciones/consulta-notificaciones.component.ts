import { Component, OnInit } from '@angular/core';
import { ES } from './../../applications.constant';

import { DatePipe } from '@angular/common';
import { Message } from 'primeng/primeng';
import { Direcnotif } from './../models/direcnotif.model';
import { ConsultaNotificacionesService } from './consulta-notificaciones.service';
import { ResponseWrapper } from '../../../shared/index';

@Component({
    selector: 'jhi-consulta-notificaciones',
    templateUrl: './consulta-notificaciones.component.html'
})
export class ConsultaNotificacionesComponent implements OnInit {

    expedientes: any;
    es: any;
    id = '14';

    block: boolean;
    mensajes: Message[] = [];

    direcNotificaciones: Direcnotif[];

    tipoBusqueda = '1';
    rangeDates: Date[];
    fechaActual = new Date();
    nAnio: number = this.fechaActual.getFullYear();
    nExpediente: string;
    totalRegistros: number;

    constructor(
        private consultaNotificacionesService: ConsultaNotificacionesService,
        private datePipe: DatePipe,
    ) {}

    ngOnInit() {
        this.es = ES;
    }

    buscarNotificaciones() {
        let queryString = '';
        if (this.tipoBusqueda === '1') {
            if (this.nExpediente === undefined || this.nExpediente === '' || this.nExpediente === null) {
                this.mensajes = [];
                this.mensajes.push({severity: 'warn', summary: 'Mensaje de Alerta', detail: 'No se ha ingresado número del expediente'});
                return;
            }
            queryString = 'direcnotifs/params?nro_exp=' + this.nExpediente + '&anio=' + this.nAnio;
        } else {
            if (this.rangeDates === undefined) {
                this.mensajes = [];
                this.mensajes.push({severity: 'warn', summary: 'Mensaje de Alerta', detail: 'No se han ingresados las fechas'});
                return;
            }
            if (this.rangeDates[1] === null) {
                this.mensajes.push({severity: 'warn', summary: 'Mensaje de Alerta', detail: 'Solo se ha ingresado una de las fecha, por favor seleccione otra'});
                return;
            }
            const fec_ini = this.datePipe.transform(this.rangeDates[0], 'dd-MM-yyyy');
            const fec_fin = this.datePipe.transform(this.rangeDates[1], 'dd-MM-yyyy');
            queryString = 'direcnotifs/params?fec_ini=' + fec_ini + '&fec_fin=' + fec_fin;
        }
        this.block = true;
        this.consultaNotificacionesService.consultaDirecNotificaciones(queryString).subscribe(
            (res: ResponseWrapper) => {
                this.direcNotificaciones = res.json;
                this.totalRegistros = this.direcNotificaciones.length;
                this.block = false;
            },
            (res: ResponseWrapper) => { this.onError('Error de conexión, por favor vuelva a intentarlo'); this.block = false; }
        );
    }

    private onError(error: any) {
        this.mensajes = [];
        this.mensajes.push({ severity: 'error', summary: 'Mensaje de Error', detail: error });
    }

    imprimirReporteCedula(direcNoti) {
        const nombrereporte = 'CelulaNotificacionCon';
        const parametroString = 'COD_NOT|' + direcNoti.id + '^';
        this.consultaNotificacionesService.getReporte(nombrereporte, parametroString);
    }

    cargaLentaDirecNotifica() {

    }

}

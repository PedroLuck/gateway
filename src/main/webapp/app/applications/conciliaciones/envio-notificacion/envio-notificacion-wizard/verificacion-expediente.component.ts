import { InteresperiDialogComponent } from './../../../../entities/interesperi/interesperi-dialog.component';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { SelectItem } from 'primeng/primeng';
import { Observable, Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { Message } from 'primeng/primeng';
import { ConfirmationService } from 'primeng/components/common/api';
import { padWithZero  } from './../../../applications.constant';

import { Tipnotif } from './../../models/tipnotif.model';
import { Tipenvnot } from './../../models/tipenvnot.model';

import { ResponseWrapper } from '../../../../shared';
import { DatosWizardService } from './datos-wizard.service';
import { Expediente, Empleador, Dirperjuri, Dirpernat, Notifica, Direcnotif } from './../';
import { EnvioNotificacionWizardService } from './envio-notificacion-wizard.service';
import { MesgDestinario, Item, Administrado } from './../../models/crearMensajeria.model';

@Component({
    selector: 'jhi-verificacion-expediente',
    templateUrl: './verificacion-expediente.component.html',
    providers: [ConfirmationService]
})
export class VerificacionExpedienteComponent implements OnInit, OnDestroy {

    private subscription: Subscription;
    private eventSubscriber: Subscription;

    expedientes: Expediente[];
    empleador: Empleador;
    notifica: Notifica;
    notificaEmp: Notifica;
    notificas: Notifica[] = [];
    direcnotif: Direcnotif;

    idNotificacion = [];
    tipoNotificacion: Tipnotif;
    tipoenvNotificacion: Tipenvnot;
    selectedTNotificacion: String;
    selectedTEnvio: String;

    block: boolean;
    mensajes: Message[] = [];

    dirperjuri: Dirperjuri[];
    dirpernat: Dirpernat[];

    msgDestinatario: MesgDestinario;

    direcSelec: MesgDestinario;

    constructor(
        private eventManager: JhiEventManager,
        private datosWizardService: DatosWizardService,
        private router: Router,
        private envioNotificacionService: EnvioNotificacionWizardService,
        private confirmationService: ConfirmationService,
    ) {}

    loadTipnotif() {
        this.datosWizardService.buscarTipnotif().subscribe(
            (res: ResponseWrapper) => {
                this.tipoNotificacion = res.json;
            },
            (res: ResponseWrapper) => { this.onError(res.json); }
        );
    }
    loadTipenvnotif() {
        this.datosWizardService.buscarTipenvnot().subscribe(
            (res: ResponseWrapper) => {
                this.tipoenvNotificacion = res.json;
            },
            (res: ResponseWrapper) => { this.onError(res.json); }
        );
    }
    conseguirDirecciones( idTrabajador, idEmpleador, isPersonaJuridica, indice ) {
        this.block = true;
        this.datosWizardService.buscarDirecciones(idTrabajador).subscribe(
            (res: ResponseWrapper) => {
                this.expedientes[indice].trabajadorDireccion = res.json;
                this.block = false;
            },
            (res: ResponseWrapper) => { this.onError(res.json); this.block = false; }
        );
        if ( isPersonaJuridica ) {
            this.datosWizardService.buscarDireccionesPerJur(idEmpleador).subscribe(
                (res: ResponseWrapper) => {
                    this.expedientes[indice].empleadorDireccion = res.json;
                },
                (res: ResponseWrapper) => { this.onError(res.json); this.block = false; }
            );
        } else {
            this.datosWizardService.buscarDirecciones(idEmpleador).subscribe(
                (res: ResponseWrapper) => { this.expedientes[indice].empleadorDireccion = res.json; },
                (res: ResponseWrapper) => { this.onError(res.json); this.block = false; }
            );
        }
    }

    ngOnInit() {
        this.loadTipnotif();
        this.loadTipenvnotif();
        this.subscription = this.envioNotificacionService.expedienteSeleccionado.subscribe((expedientes: any) => {
            this.expedientes = expedientes;
            if ( Object.keys(this.expedientes).length === 0) {
                this.router.navigate(['/conciliaciones/expediente/envio-notificacion' , { outlets: { wizard: ['seleccion-expediente'] } }]);
            } else {
                this.expedientes.forEach((expediente: any, index) => {
                    this.empleador = expediente.empleador;
                    let isPerjuridica = false;
                    if (this.empleador.pernatural === null) {
                        isPerjuridica = true;
                    }
                    const idTrabajadorPersona = expediente.trabajador.pernatural.id;
                    const idEmpleadorPersona =  (expediente.empleador.pernatural === null) ? expediente.empleador.perjuridica.id : expediente.empleador.pernatural.id;
                    this.conseguirDirecciones(idTrabajadorPersona, idEmpleadorPersona, isPerjuridica, index);
                });
            }
        });
        this.registerChangeInExpediente();
    }
    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    private onError(error: any) {
        // this.messages = [];
        // this.messages.push({ severity: 'error', summary: 'Mensaje de Error', detail: error.message });
    }

    registerChangeInExpediente() {
        this.eventSubscriber = this.eventManager.subscribe('envioNotificaciones', (response) => {
            this.expedientes.some((expediente) => {
                console.log('Tenvio' + expediente.nroFoliosTrab);
                this.mensajes = [];
                if (expediente.tipoNotificacionTrab === null || expediente.tipoNotificacionTrab === undefined) {
                    this.mensajes.push({ severity: 'warn', summary: 'Mensaje de Alerta', detail: 'No se ha ingresado el tipo de notificación de un Trabajador' });
                } else if (expediente.tipoEnvioTrab === null || expediente.tipoEnvioTrab === undefined) {
                    this.mensajes.push({ severity: 'warn', summary: 'Mensaje de Alerta', detail: 'No se ha ingresado uno de los tipos de envió' });
                } else if (expediente.nroFoliosTrab === null || expediente.nroFoliosTrab === undefined) {
                    this.mensajes.push({ severity: 'warn', summary: 'Mensaje de Alerta', detail: 'No se ha ingresado uno de los números de folios' });
                } else if (expediente.tipoNotificacionEmp === null || expediente.tipoNotificacionEmp === undefined) {
                    this.mensajes.push({ severity: 'warn', summary: 'Mensaje de Alerta', detail: 'No se ha ingresado uno de los tipos de notificación de un Empleador' });
                } else if (expediente.tipoEnvioEmp === null || expediente.tipoEnvioEmp === undefined) {
                    this.mensajes.push({ severity: 'warn', summary: 'Mensaje de Alerta', detail: 'No se ha ingresado uno de los tipos de envió de un Empleador' });
                } else if (expediente.nroFoliosEmp === null || expediente.nroFoliosEmp === undefined) {
                    this.mensajes.push({ severity: 'warn', summary: 'Mensaje de Alerta', detail: 'No se ha ingresado uno de los números de folios de un Empleador' });
                }
                if (this.mensajes.length > 0) {
                    return true;
                }
                console.log('Mensajes' + this.mensajes.length);
            });
            if (this.mensajes.length < 1) {
                this.confirmar();
            }
        });
    }
    confirmar() {
        this.confirmationService.confirm({
            message: '¿Esta seguro de enviar las notifiaciones?',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.cargarExpediente();
            }
        });
    }

    // Funcion encargada de parsear el objeto expediente al formato de mensajeria
    cargarExpediente() {
        let cont = 0;
        this.msgDestinatario = new MesgDestinario();
        this.msgDestinatario.item = [];

        this.expedientes.forEach((expediente, index) => {
            expediente.trabajadorDireccion.forEach((trabajadorDireccion) => {
                if (trabajadorDireccion.direc.nFlgnotifi === true) {
                    this.msgDestinatarioPersona(trabajadorDireccion, cont, expediente, true);
                    cont++;
                }
            });
            expediente.empleadorDireccion.forEach((empleadorDireccion) => {
                if (empleadorDireccion.direc.nFlgnotifi === true) {
                    this.msgDestinatarioPersona(empleadorDireccion, cont, expediente, false)
                    cont++;
                }
            });
        });
        console.log('SOAP' + JSON.stringify(this.msgDestinatario));
        this.subscribeToSoapNotificacion(
            this.datosWizardService.createNotifacion(this.msgDestinatario));
    }
    private subscribeToSoapNotificacion(result: Observable<any>) {
        this.block = true;
        const index = 0;

        const totalExpediente = this.expedientes.length;
        result.subscribe((res: any) => {
            // console.log('RESULT');
            // console.log(res);
            // console.log('EXPEDIENTES');
            // console.log(this.expedientes);
            this.expedientes.forEach((expediente, indice) => {
               this.notificas.push(this.convertirNotificacionTrabajador(expediente, indice, res));
               this.notificas.push(this.convertirNotificacionEmpleador(expediente, indice, res));
            });
            console.log('Notifica1');
            console.log(this.notificas);
            this.notificas.forEach((notifica, indice) => {
                const finalExp =  ( (indice + 1) === this.notificas.length) ? true : false;
                this.subscribeToSaveNotifica(
                    this.datosWizardService.createTableNotifica(notifica), 'Las notificaciónes se han enviado correctamente', notifica, finalExp);
            });
        }, (res: Response) => {
            this.onError('Error en la respuesta del servidor por favor vuelva a intentarlo');
            this.block = false;
        });
    }
    private subscribeToSaveNotifica(result: Observable<Notifica>, mensaje: string, notificaDir: Notifica, finalExp: boolean) {
        result.subscribe((res: any) => {
            console.log('GRABADO');
            console.log(res);
            this.idNotificacion.push(res.id);
            this.envioNotificacionService.cambiarNotificacion(this.idNotificacion);
            const totalDirecnotifs = notificaDir.direcnotifs.length;
            let finalDirec = false;
            notificaDir.direcnotifs.forEach((personaDireccion: any, indice) => {
                if (finalExp) {
                    if ( (indice + 1) === totalDirecnotifs) {
                        finalDirec = true;
                    }
                }
                // console.log('PersonaDireccion');
                // console.log(personaDireccion);
                const direcNotifica = new Direcnotif(); // Variable nueva que contendra los datos de la table direcnot
                if (personaDireccion.direc.pernatural !== undefined) {
                    direcNotifica.nflgtrabaj = true;
                    direcNotifica.vDestina = personaDireccion.direc.pernatural.vNombres + ' ' + personaDireccion.direc.pernatural.vApepat + ' ' +
                        personaDireccion.direc.pernatural.vApemat;
                } else {
                    direcNotifica.nflgtrabaj = false;
                    direcNotifica.vDestina = personaDireccion.direc.perjuridica.vRazsocial;
                }
                direcNotifica.notifica = res;
                direcNotifica.vDireccion = personaDireccion.direc.vDircomple;
                direcNotifica.vHojaenvio = personaDireccion.hojaEnvio;
                // console.log('Direcnotifica');
                // console.log(direcNotifica);
                this.subscribeToSaveDirecNotifica(
                    this.datosWizardService.createTableDirecNotifica(direcNotifica), 'Las direcciones se han enviado correctamente', finalDirec);
            });
        }, (res: Response) => {this.onError('Error en la respuesta del servidor por favor vuelva a intentarlo'); this.block = false; });
    }
    private subscribeToSaveDirecNotifica(result: Observable<Direcnotif>, mensaje: string, finalDirec: boolean) {
        result.subscribe((res: any) => {
            console.log('DIRECGRABADO');
            console.log(res);
            if (finalDirec) {
                this.block = false;
                this.eventManager.broadcast({ name: 'end', content: 'OK'});
            }
        }, (res: Response) => this.onError('Error en la respuesta del servidor por favor vuelva a intentarlo'));
    }

    private convertirNotificacionTrabajador(expediente: Expediente, indice: number, res): Notifica {
        let index = 0;
        console.log('ConvertirNotificacion');
        this.notifica = new Notifica();
        this.notifica.expediente = expediente;
        this.notifica.nNumfolios = expediente.nroFoliosTrab;
        this.notifica.tipenvnot = expediente.tipoEnvioTrab;
        this.notifica.tipnotif = expediente.tipoNotificacionTrab;
        this.notifica.vCodconci = expediente.vCodconci;
        this.notifica.direcnotifs = [];
        expediente.trabajadorDireccion.forEach((trabajadorDireccion) => {
            if (trabajadorDireccion.direc.nFlgnotifi === true) {
                trabajadorDireccion.hojaEnvio = res.item[index].doNohetxt;
                this.notifica.direcnotifs.push(trabajadorDireccion);
                index++;
            }
        });
        return this.notifica;
    }
    private convertirNotificacionEmpleador(expediente: Expediente, indice: number, res) {
        let index = 0;
        this.notifica = new Notifica();
        this.notifica.expediente = expediente;
        this.notifica.nNumfolios = expediente.nroFoliosEmp;
        this.notifica.tipenvnot = expediente.tipoEnvioEmp;
        this.notifica.tipnotif = expediente.tipoNotificacionEmp;
        this.notifica.vCodconci = expediente.vCodconci;
        this.notifica.direcnotifs = [];
        if (expediente.empleadorDireccion !== null) {
            expediente.empleadorDireccion.forEach((empleadorDireccion) => {
                if (empleadorDireccion.direc.nFlgnotifi === true) {
                    empleadorDireccion.hojaEnvio = res.item[index].doNohetxt;
                    this.notifica.direcnotifs.push(empleadorDireccion);
                    index++;
                }
            });
        }
        return this.notifica;
    }

    private msgDestinatarioPersona(personaDireccion, cont, expediente: Expediente, trabajador: boolean ) {
        console.log('ExpeidentePersonadireccion');
        console.log(expediente);
        this.msgDestinatario.item[cont] = new Item();
        this.msgDestinatario.item[cont].administrado = new Administrado();
        this.msgDestinatario.item[cont].administrado.departamento = padWithZero(personaDireccion.direc.nCoddepto);
        this.msgDestinatario.item[cont].administrado.distrito = padWithZero(personaDireccion.direc.nCoddist);
        this.msgDestinatario.item[cont].administrado.domicilio = personaDireccion.direc.vDircomple;
        if (personaDireccion.direc.pernatural !== undefined) {
            this.msgDestinatario.item[cont].administrado.nombreRemitente =
                personaDireccion.direc.pernatural.vNombres + ' ' + personaDireccion.direc.pernatural.vApepat + ' ' + personaDireccion.direc.pernatural.vApemat;
            this.msgDestinatario.item[cont].administrado.numeroDocumentoIdent = personaDireccion.direc.pernatural.vNumdoc;
        } else {
            this.msgDestinatario.item[cont].administrado.nombreRemitente =
                personaDireccion.direc.perjuridica.vRazsocial;
            this.msgDestinatario.item[cont].administrado.numeroDocumentoIdent = personaDireccion.direc.perjuridica.vNumdoc;
        }
        this.msgDestinatario.item[cont].administrado.pais = '173';
        this.msgDestinatario.item[cont].administrado.provincia = padWithZero(personaDireccion.direc.nCodprov);
        this.msgDestinatario.item[cont].administrado.tipoAdministrado = '1';
        this.msgDestinatario.item[cont].administrado.tipoDocumentoIdent = '2';
        this.msgDestinatario.item[cont].asunto = 'HOJA DE ENVIO DE PRUEBA POR WS1';
        this.msgDestinatario.item[cont].idClase = '1';
        this.msgDestinatario.item[cont].nivelSegu = 'NORMAL';
        this.msgDestinatario.item[cont].noDocumento = '78549';
        if (trabajador) {
            this.msgDestinatario.item[cont].numFolios = expediente.nroFoliosTrab;
            this.msgDestinatario.item[cont].tipoDespacho = expediente.tipoEnvioTrab.vDescrip;
        } else {
            this.msgDestinatario.item[cont].numFolios = expediente.nroFoliosEmp;
            this.msgDestinatario.item[cont].tipoDespacho = expediente.tipoEnvioEmp.vDescrip;
        }
        this.msgDestinatario.item[cont].numeroExpediente = '';
        this.msgDestinatario.item[cont].paraNombre = 'MARIANO MELGAR';
    }

}

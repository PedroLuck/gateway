import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';

import { Abogado } from './abogado.model';
import { AbogadoService } from './abogado.service';
import { JhiEventManager } from 'ng-jhipster';
import { ResponseWrapper } from '../../../shared';

@Component({
    selector: 'jhi-mantenimiento-abogado',
    templateUrl: './mantenimiento-abogado.component.html'
})
export class MantenimientoAbogadoComponent implements OnInit {

    abogados: Abogado[];
    eventSubscriber: Subscription;
    currentSearch: string;

    constructor(
        private activatedRoute: ActivatedRoute,
        private eventManager: JhiEventManager,
        private abogadoService: AbogadoService
    ) {
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
    }

    ngOnInit() {
        this.loadAll();
        this.registerChangeInAbogados();
    }
    loadAll() {
        if (this.currentSearch) {
            this.abogadoService.search({
                query: this.currentSearch,
                }).subscribe(
                    (res: ResponseWrapper) => this.abogados = res.json,
                    (res: ResponseWrapper) => this.onError(res.json)
                );
            return;
       }
        this.abogadoService.query().subscribe(
            (res: ResponseWrapper) => {
                this.abogados = res.json;
                console.log(this.abogados);
                this.currentSearch = '';
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    search(query) {
        if (!query) {
            return this.clear();
        }
        this.currentSearch = query;
        this.loadAll();
    }

    clear() {
        this.currentSearch = '';
        this.loadAll();
    }

    registerChangeInAbogados() {
        this.eventSubscriber = this.eventManager.subscribe('abogadoListModification', (response) => this.loadAll());
    }
    private onError(error) {
        // this.jhiAlertService.error(error.message, null, null);
    }
}

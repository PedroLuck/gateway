import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Abogado } from './abogado.model';
import { MantenimientoAbogadoPopupService } from './mantenimiento-abogado-popup.service';
import { AbogadoService } from './abogado.service';

@Component({
    selector: 'jhi-mantenimiento-abogado-delete-dialog',
    templateUrl: './mantenimiento-abogado-delete-dialog.component.html'
})
export class MantenimientoAbogadoDeleteDialogComponent {

    abogado: Abogado;

    constructor(
        private abogadoService: AbogadoService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(abogado) {
        this.abogadoService.updateLogic(abogado).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'abogadoListModification',
                content: 'Deleted an aplicacion'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-mantenimiento-abogado-delete-popup',
    template: ''
})
export class MantenimientoAbogadoDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private matenimientoAbogadoPopupService: MantenimientoAbogadoPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.matenimientoAbogadoPopupService
                .open(MantenimientoAbogadoDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}

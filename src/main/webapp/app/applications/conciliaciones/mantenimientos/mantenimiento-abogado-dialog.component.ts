import { ES } from './../../applications.constant';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Abogado } from './abogado.model';
import { Oficina,  } from './oficina.model';
import { MantenimientoAbogadoPopupService } from './mantenimiento-abogado-popup.service';
import { AbogadoService } from './abogado.service';
import { ResponseWrapper } from './../../../shared';

@Component({
    selector: 'jhi-mantenimiento-abogado-dialog',
    templateUrl: './mantenimiento-abogado-dialog.component.html'
})
export class MantenimientoAbogadoDialogComponent implements OnInit {

    es: any;
    fechaAudiencia: Date;
    fechaMP: Date;
    registro: Date;
    selectedValues1: string[];
    selectedValues2: string[];
    selectedValues3: string[];
    abogado: Abogado;
    isSaving: boolean;

    oficina: Oficina;

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private abogadoService: AbogadoService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.es = ES;
        this.isSaving = false;
        this.oficina = new Oficina(2, 'CONCILIACIONES');
    }

    save() {
        this.isSaving = true;
        this.abogado.oficina = this.oficina;
        if (this.abogado.id !== undefined) {
            this.subscribeToSaveResponse(
                this.abogadoService.update(this.abogado));
        } else {
            this.subscribeToSaveResponse(
                this.abogadoService.create(this.abogado));
            console.log(this.abogado);
        }
    }

    private subscribeToSaveResponse(result: Observable<Abogado>) {
        result.subscribe((res: Abogado) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: Abogado) {
        console.log('broadcast');
        this.eventManager.broadcast({ name: 'abogadoListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
        console.log('saveerror');
    }

    private onError(error: any) {
        // this.jhiAlertService.error(error.message, null, null);
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }
}

@Component({
    selector: 'jhi-mantenimiento-abogado-popup',
    template: ''
})
export class MantenimientoAbogadoPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private mantenimientoAbogadoPopupService: MantenimientoAbogadoPopupService
    ) { }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.mantenimientoAbogadoPopupService
                    .open(MantenimientoAbogadoDialogComponent as Component, params['id']);
            } else {
                this.mantenimientoAbogadoPopupService
                    .open(MantenimientoAbogadoDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}

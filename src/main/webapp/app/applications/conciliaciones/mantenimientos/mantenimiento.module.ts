import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GatewaySharedModule } from '../../../shared';
import {
    MantenimientoResultadoComponent,
    MantenimientoResultadoDialogComponent,
    MantenimientoResultadoPopupComponent,
    MantenimientoResultadoDeleteDialogComponent,
    MantenimientoResultadoDeletePopupComponent,
    MantenimientoResultadoPopupService,
    MantenimientoAbogadoComponent,
    MantenimientoAbogadoDialogComponent,
    MantenimientoAbogadoPopupComponent,
    MantenimientoAbogadoDeleteDialogComponent,
    MantenimientoAbogadoDeletePopupComponent,
    MantenimientoAbogadoPopupService,
    MantenimientoAudienciaComponent,
    AbogadoService,
    ResulconciService,
    TipresconcService,
    mantenimientoRoute } from './';
import { TabViewModule, DataTableModule, CheckboxModule, DropdownModule, CalendarModule } from 'primeng/primeng';

const ENTITY_STATES = [
    ...mantenimientoRoute
];

@NgModule({
    imports: [
        GatewaySharedModule,
        TabViewModule,
        DataTableModule,
        CheckboxModule,
        DropdownModule,
        CalendarModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        MantenimientoResultadoComponent,
        MantenimientoAbogadoComponent,
        MantenimientoAudienciaComponent,
        MantenimientoResultadoDialogComponent,
        MantenimientoResultadoPopupComponent,
        MantenimientoResultadoDeleteDialogComponent,
        MantenimientoResultadoDeletePopupComponent,
        MantenimientoAbogadoDialogComponent,
        MantenimientoAbogadoPopupComponent,
        MantenimientoAbogadoDeleteDialogComponent,
        MantenimientoAbogadoDeletePopupComponent
    ],
    entryComponents: [
        MantenimientoResultadoComponent,
        MantenimientoAudienciaComponent,
        MantenimientoAbogadoDialogComponent,
        MantenimientoAbogadoDeleteDialogComponent,
        MantenimientoResultadoDialogComponent,
        MantenimientoResultadoPopupComponent,
        MantenimientoResultadoDeleteDialogComponent,
        MantenimientoResultadoDeletePopupComponent
   ],
    providers: [
        AbogadoService,
        ResulconciService,
        MantenimientoResultadoPopupService,
        MantenimientoAbogadoPopupService,
        TipresconcService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MantenimientoModule {}


export * from './mantenimiento-resultado.component';
export * from './mantenimiento-resultado-dialog.component';
export * from './mantenimiento-resultado-delete-dialog.component';
export * from './mantenimiento-resultado-popup.service';

export * from './mantenimiento-abogado.component';
export * from './mantenimiento-abogado-dialog.component';
export * from './mantenimiento-abogado-delete-dialog.component';
export * from './mantenimiento-abogado-popup.service';

export * from './mantenimiento-audiencia.component';

export * from './mantenimiento.route';

export * from './abogado.model';
export * from './oficina.model';
export * from './abogado.service';
export * from './resulconci.model';
export * from './resulconci.service';
export * from './tipresconc.model';
export * from './tipresconc.service';

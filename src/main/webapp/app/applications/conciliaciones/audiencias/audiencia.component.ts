import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { ES } from './../../applications.constant';
import { DatePipe } from '@angular/common'
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { Concilia } from '../models/concilia.model';
import { Expediente } from '../models/expediente.model';
import { Pasegl } from '../models/pasegl.model';
import { Atencion } from './atencion.model';
import { Datlab } from '../models/datlab.model';
import { Empleador } from '../models/empleador.model';
import { Perjuridica } from '../models/perjuridica.model';
import { Pernatural } from '../models/pernatural.model';
import { Trabajador } from '../models/trabajador.model';
import { Horacon } from '../models/horacon.model';
import { Resulconci} from '../models/resulconci.model';
import { ConciliaService } from './concilia.service';
import { Message } from 'primeng/primeng';
import { ResponseWrapper } from '../../../shared';

@Component({
    selector: 'jhi-audiencia',
    templateUrl: './audiencia.component.html'
})
export class AudienciaComponent implements OnInit {

    expedientes: any;
    concilias: Concilia[];
    concilia: Concilia;
    expediente: Expediente;
    pasegl: Pasegl;
    atencion: Atencion;
    datlab: Datlab;
    empleador: Empleador;
    perjuridica: Perjuridica;
    pernaturalEMP: Pernatural;
    pernaturalTRA: Pernatural;
    trabajador: Trabajador;
    horacon: Horacon;
    resulconci: Resulconci;
    eventSubscriber: Subscription;

    block: boolean;
    mensajes: Message[] = [];

    id: any;
    currentUrl: String;
    es: any;
    fechaAudiencia: Date;
    date: Date;

    constructor(
        private router: Router,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private activatedRoute: ActivatedRoute,
        private conciliaService: ConciliaService,
        public datepipe: DatePipe
    ) {
        this.fechaAudiencia = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
    }

    ngOnInit() {
        this.es = ES;
        this.currentUrl = this.router.url;
        this.cargarAudiencias();
        this.registerChangeInConcilia();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.fechaAudiencia = query;
        this.date = new Date();
        // const latest_date = this.datepipe.transform(this.date, 'dd/MM/yyyy');
        // console.log('FechaHoy');
        // console.log(latest_date);

        console.log('fecha');
        console.log(this.fechaAudiencia);
        this.cargarAudiencias();
    }

    clear() {
        this.fechaAudiencia = null;
        this.cargarAudiencias();
    }

    cargarAudiencias() {
        this.id = null;
        switch (this.currentUrl) {
            case '/conciliaciones/audiencia/asignacion-abogado' : {
                this.listarAsignarAbogado();
                break;
            }
            case '/conciliaciones/audiencia/registrar-resultado' : {
                this.listarAsignarResultado();
                break;
            }
            case '/conciliaciones/audiencia/reprogramacion' : {
                console.log('asignar reprogramacion');
                this.listarAsignarReprogramacion();
                break;
            }
        }
    }

    listarAsignarAbogado() {
        console.log('ASIG');
        let fechaBusqueda;
        if (this.fechaAudiencia) {
            fechaBusqueda = this.datepipe.transform(this.fechaAudiencia, 'dd-MM-yyyy');
        }
        this.block = true;
        this.conciliaService.buscarConciliaAsignacion(fechaBusqueda).subscribe(
            (res: ResponseWrapper) => {
                this.concilias = res.json;
                this.block = false;
                if (res.json.length === 0) {
                    this.mensajes = [];
                    this.mensajes.push({severity: 'warn', summary: 'Mensaje de Alerta', detail: 'No se han encontrado audiencias en esa fecha'});
                }
                this.concilias.forEach((item, index) => {
                    this.datosFaltantes(item, index);
                });
            },
            (res: ResponseWrapper) => { this.onError(res.json); this.block = false; }
        );
    }

    listarAsignarResultado() {
        let fechaBusqueda;
        if (this.fechaAudiencia) {
            fechaBusqueda = this.datepipe.transform(this.fechaAudiencia, 'dd-MM-yyyy');
        }
        this.block = true;
        this.conciliaService.buscarConciliaResultado(fechaBusqueda).subscribe(
            (res: ResponseWrapper) => {
                this.concilias = res.json;
                if (res.json.length === 0) {
                    this.mensajes = [];
                    this.mensajes.push({severity: 'warn', summary: 'Mensaje de Alerta', detail: 'No se han encontrado audiencias en esa fecha'});
                }
                this.block = false;
                this.concilias.forEach((item, index) => {
                    this.datosFaltantes(item, index);
                });
            },
            (res: ResponseWrapper) => {this.onError(res.json); this.block = false; }
        );
    }

    listarAsignarReprogramacion() {
        let fechaBusqueda;
        if (this.fechaAudiencia) {
            fechaBusqueda = this.datepipe.transform(this.fechaAudiencia, 'dd-MM-yyyy');
        }
        this.block = true;
        this.conciliaService.buscarConciliaReprogramacion(fechaBusqueda).subscribe(
            (res: ResponseWrapper) => {
                this.concilias = res.json;
                if (res.json.length === 0) {
                    this.mensajes = [];
                    this.mensajes.push({severity: 'warn', summary: 'Mensaje de Alerta', detail: 'No se han encontrado audiencias en esa fecha'});
                }
                this.block = false;
                this.concilias.forEach((item, index) => {
                    this.datosFaltantes(item, index);
                });
                this.fechaAudiencia = null;
            },
            (res: ResponseWrapper) => {this.onError(res.json); this.block = false; }
        );
    }

    abrirModal(tipoProceso) {
        if (this.id === null) {
            this.mensajes = [];
            this.mensajes.push({severity: 'warn', summary: 'Mensaje de Alerta', detail: 'No ha seleccionado un expediente'});
            return;
        }
        switch (tipoProceso) {
            case 'asignacion':
                this.router.navigate(['/conciliaciones/audiencia/asignacion-abogado', { outlets: { popupexp: this.id } }]); break;
            case 'resultado':
                this.router.navigate(['/conciliaciones/audiencia/registrar-resultado', { outlets: { popupexp: this.id } }]); break;
            case 'reprogramacion':
                this.router.navigate(['/conciliaciones/audiencia/reprogramacion', { outlets: { popupexp: this.id } }]); break;
            default:
                break;
        }
    }

    seleccionarFila(event) {
        this.id = String(event.data.id);
    }

    registerChangeInConcilia() {
        this.eventSubscriber = this.eventManager.subscribe('conciliaListModification', (response) => this.cargarAudiencias());
    }

    private onError(error) {
        // console.log('error' + error.message);
        // this.jhiAlertService.error(error.message, null, null);
    }

    private datosFaltantes(item, index) {
        const concilia = item;
        console.log(concilia);
        this.expediente = concilia.expediente;
        this.resulconci = concilia.resulconci;
        this.pasegl = this.expediente.pasegl;
        this.atencion = this.pasegl.atencion;
        this.datlab = this.atencion.datlab;
        this.empleador = this.datlab.empleador;
        this.perjuridica = this.empleador.perjuridica;
        this.pernaturalEMP = this.empleador.pernatural;
        this.trabajador = this.datlab.trabajador;
        this.pernaturalTRA = this.trabajador.pernatural;
        this.horacon = concilia.horacon;
        if (this.perjuridica != null) {
            this.concilias[index].nrodocemp = this.perjuridica.vNumdoc;
            this.concilias[index].fullnameemp = this.perjuridica.vRazsocial;
            console.log(this.perjuridica.vNumdoc);
        }else {
            this.concilias[index].nrodocemp = this.pernaturalEMP.vNumdoc;
            this.concilias[index].fullnameemp = this.pernaturalEMP.vNombres + ' ' + this.pernaturalEMP.vApepat + ' ' + this.pernaturalEMP.vApemat ;
            console.log(this.pernaturalEMP.vNumdoc);
        }
        this.concilias[index].nrodoctrab = this.pernaturalTRA.vNumdoc;
        this.concilias[index].fullnametrab = this.pernaturalTRA.vNombres + ' ' + this.pernaturalTRA.vApepat + ' ' + this.pernaturalTRA.vApemat ;
        this.concilias[index].fechahoraconci = this.datepipe.transform(concilia.dFecconci, 'dd/MM/yyyy') +
        ' ' + this.horacon.vDescrip + ':00';
    }

}


export * from './audiencia.component';
export * from './audiencia-consulta.component';

export * from './audiencia-consulta-dialog.component';
export * from './audiencia-consulta-popup.service';

export * from './audiencia-asignacion-dialog.component';
export * from './audiencia-asignacion-popup.service';

export * from './audiencia-registro-resultado-dialog.component';
export * from './audiencia-registro-resultado-popup.service';

export * from './audiencia-reprogramacion-dialog.component';
export * from './audiencia-reprogramacion-popup.service';

export * from './audiencia-registro-escrito-dialog.component';
export * from './audiencia-registro-escrito-popup.service';

export * from './audiencia.route';

export * from './concilia.service';
export * from './../models/concilia.model';
export * from './expediente.service';
export * from './../models/expediente.model';
export * from './../models/pasegl.model';
export * from './pasegl.service';
export * from './atencion.model';
export * from './atencion.service';
export * from './../models/datlab.model';
export * from './datlab.service';
export * from './../models/empleador.model';
export * from './empleador.service';
export * from './../models/perjuridica.model';
export * from './perjuridica.service';
export * from './../models/pernatural.model';
export * from './pernatural.service';
export * from './../models/trabajador.model';
export * from './trabajador.service';
export * from './../models/horacon.model';
export * from './abogado.model';
export * from './abogado.service';
export * from './../models/resulconci.model';
export * from './resulconci.service';
export * from './../models/horacon.model';
export * from './horacon.service';
export * from './../models/dirperjuri.model';
export * from './dirperjuri.service';
export * from './../models/dirpernat.model';
export * from './dirpernat.service';

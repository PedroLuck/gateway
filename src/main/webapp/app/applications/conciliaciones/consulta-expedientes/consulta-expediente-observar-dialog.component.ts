import { ES } from './../../applications.constant';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';
import { Expediente } from './../models/expediente.model';

import { ConsultaExpedienteArchivarPopupService } from './consulta-expediente-archivar-popup.service';
import { ConsultaExpedienteService } from './consulta-expediente.service';
import { ResponseWrapper } from './../../../shared';

@Component({
    selector: 'jhi-consulta-expediente-observar-dialog',
    templateUrl: './consulta-expediente-observar-dialog.component.html'
})
export class ConsultaExpedienteObservarDialogComponent implements OnInit {

    es: any;
    expediente: Expediente;

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private consultaExpedienteService: ConsultaExpedienteService
    ) {

    }

    ngOnInit() {
        this.es = ES;
    }
    save() {
        console.log(this.expediente);
        this.subscribeToSaveResponse(
                this.consultaExpedienteService.actualizarExpediente(this.expediente));
    }
    private subscribeToSaveResponse(result: Observable<Expediente>) {
        result.subscribe((res: Expediente) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: Expediente) {
        console.log('broadcast');
        this.eventManager.broadcast({ name: 'consultaMotificacionObservacion', content: 'OK'});
        this.activeModal.dismiss(result);
    }
    private onSaveError() {
        console.log('saveerror');
    }
    clear() {
        this.activeModal.dismiss('cancel');
    }
}

@Component({
    selector: 'jhi-consulta-expediente-popup',
    template: ''
})
export class ConsultaExpedienteObservarPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private consultaExpedienteArchivarPopupService: ConsultaExpedienteArchivarPopupService
    ) { }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.consultaExpedienteArchivarPopupService
                    .open(ConsultaExpedienteObservarDialogComponent as Component, params['id']);
            } else {
                this.consultaExpedienteArchivarPopupService
                    .open(ConsultaExpedienteObservarDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}

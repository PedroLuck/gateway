import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { Expediente } from './../../../entities/expediente/expediente.model';
import { ConsultaExpedienteService } from './consulta-expediente.service';

@Injectable()
export class ConsultaExpedienteObservarPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private consultaExpedienteService: ConsultaExpedienteService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.consultaExpedienteService.consultarExpediente(id).subscribe((expediente) => {
                    if (expediente.dFecmespar) {
                        expediente.dFecmespar = {
                            year: expediente.dFecmespar.getFullYear(),
                            month: expediente.dFecmespar.getMonth() + 1,
                            day: expediente.dFecmespar.getDate()
                        };
                    }
                    expediente.dFecregexp = this.datePipe.transform(expediente.dFecregexp, 'yyyy-MM-dd');
                    expediente.dFecArchiv = this.datePipe.transform(expediente.dFecArchiv, 'yyyy-MM-dd');
                    expediente.tFecreg = this.datePipe.transform(expediente.tFecreg, 'yyyy-MM-ddTHH:mm:ss');
                    expediente.tFecupd = this.datePipe.transform(expediente.tFecupd, 'yyyy-MM-ddTHH:mm:ss');
                    this.ngbModalRef = this.consultaExpedienteModalRef(component, expediente);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.consultaExpedienteModalRef(component, new Expediente());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    consultaExpedienteModalRef(component: Component, expediente: Expediente): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.expediente = expediente;
        modalRef.result.then((result) => {
            this.router.navigate(['conciliaciones/expediente/consulta'], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate(['conciliaciones/expediente/consulta'], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}

import { Expediente } from './../models/expediente.model';
import { ES } from './../../applications.constant';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ConsultaExpedienteArchivarPopupService } from './consulta-expediente-archivar-popup.service';
import { ConsultaExpedienteService } from './consulta-expediente.service';
import { ResponseWrapper } from './../../../shared';

@Component({
    selector: 'jhi-consulta-expediente-archivar-dialog',
    templateUrl: './consulta-expediente-archivar-dialog.component.html'
})
export class ConsultaExpedienteArchivarDialogComponent implements OnInit {

    es: any;
    fechaAudiencia: Date;
    fechaMP: Date;
    registro: Date;
    expediente: Expediente;
    fechaActual = new Date();

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private consultaExpedienteService: ConsultaExpedienteService
    ) {

    }

    ngOnInit() {
        this.es = ES;
        this.expediente.dFecArchiv = this.fechaActual;
        console.log(this.expediente);
    }
    save() {
        this.expediente.nFlgarchiv = true;
        console.log(this.expediente);
        this.subscribeToSaveResponse(
                this.consultaExpedienteService.actualizarExpediente(this.expediente));
    }
    clear() {
        this.activeModal.dismiss('cancel');
    }
    private subscribeToSaveResponse(result: Observable<Expediente>) {
        result.subscribe((res: Expediente) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: Expediente) {
        console.log('broadcast');
        this.eventManager.broadcast({ name: 'consultaModificacion', content: 'OK'});
        this.activeModal.dismiss(result);
    }
    private onSaveError() {
        console.log('saveerror');
    }
}

@Component({
    selector: 'jhi-consulta-expediente-popup',
    template: ''
})
export class ConsultaExpedienteArchivarPopupComponent implements OnInit, OnDestroy {

    routeSub: any;
    constructor(
        private route: ActivatedRoute,
        private consultaExpedienteArchivarPopupService: ConsultaExpedienteArchivarPopupService
    ) { }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.consultaExpedienteArchivarPopupService
                    .open(ConsultaExpedienteArchivarDialogComponent as Component, params['id']);
            } else {
                this.consultaExpedienteArchivarPopupService
                    .open(ConsultaExpedienteArchivarDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}

import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';
import { ConsultaExpedienteService } from './consulta-expediente.service';
import { Message } from 'primeng/components/common/api';
import { ResponseWrapper } from '../../../shared';
import { Abogado } from './../models/abogado.model';
import { Resulconci } from './../models/resulconci.model';

@Component({
    selector: 'jhi-consulta-expediente',
    templateUrl: './consulta-expediente.component.html'
})
export class ConsultaExpedienteComponent implements OnInit {

    eventSubscriber: Subscription;
    abogados: Abogado[];
    selectedAbogado: Abogado;
    resultadoConci: Resulconci[];
    selectedResulconci: Resulconci;
    ubicacion: any;
    selectedUbicacion: any;
    trabajadores: any;
    trabajador: any;
    tipoBusqueda = '1';
    index = 0;

    expedienteSelected: any;
    expedienteSelected2: any;

    fechaActual = new Date();
    nAnio: number = this.fechaActual.getFullYear();
    nAnio2 = this.nAnio;

    block: boolean;
    mensajes: Message[] = [];
    expedientes: any;
    expedientes2: any;
    id: any;
    vNumdoc: string;
    vNombres: string;

    constructor(private consultaExpedienteService: ConsultaExpedienteService, private router: Router, private eventManager: JhiEventManager) {}

    cargarAbogados() {
        this.block = true;
        this.consultaExpedienteService.consultarAbogados().subscribe(
            (res: ResponseWrapper) => {
                this.abogados = res.json;
                this.abogados.unshift(new Abogado(0, 'TODOS'));
                console.log('ABOGADOS');
                console.log(this.abogados);
                this.block = false;
            },
            (res: ResponseWrapper) => { this.onError(res.json); this.block = false;  }
        );
    }
    cargarResultadosConci() {
        this.block = true;
        this.consultaExpedienteService.consultarResultadosConciliacion().subscribe(
            (res: ResponseWrapper) => {
                this.resultadoConci = res.json;
                this.resultadoConci.unshift(new Resulconci(0, 'TODOS'));
                console.log('RESULTADOSCONCI');
                console.log(this.resultadoConci);
                this.block = false;
            },
            (res: ResponseWrapper) => { this.onError(res.json); this.block = false;  }
        );
    }

    ngOnInit() {
        console.log(this.tipoBusqueda);
        this.cargarAbogados();
        this.cargarResultadosConci();
        this.ubicacion = [
            {id: '0', descUbicacion : 'TODOS' },
            {id: '1', descUbicacion : 'ARCHIVO' },
            {id: '2', descUbicacion : 'EN CONCILIACION' },
            {id: '3', descUbicacion : 'DEVUELTOS' },
            {id: '4', descUbicacion : 'OBSERVADOS' }
        ];
        this.registroCambios();
        this.registroCambiosObservacion();
        // this.expedientes = [
        //     {item: '1', codexpediente : '0000002169-10', fecha: '10/03/2010', conciliador: 'SLIZARRAGA',
        //         ruc: '20505158343', empleador: 'CONFECCIONES INCA COTTON S.A.C', nrodoc: '56897245', nomdoc: '' },
        //     {item: '2', codexpediente : '0000001699-06', fecha: '11/05/2006', conciliador: 'ACASSANA',
        //         ruc: '20251850993', empleador: 'GRUPO INTERNACIONAL SERVICE S.A.C.', nrodoc: '56897458', nomdoc: '' },
        //     {item: '3', codexpediente : '0000001698-07', fecha: '15/06/2007', conciliador: 'SLIZARRAGA',
        //         ruc: '20504257381', empleador: 'SYSTEM DATABASE S.A.', nrodoc: '56897845', nomdoc: '' }
        // ]
    }

    buscarTrabajador(event) {
        if (this.tipoBusqueda === '1') {
            this.consultaExpedienteService.consultarTrabajadores(event.query).subscribe(
                (res: ResponseWrapper) => {
                    console.log(res.json)
                    this.trabajadores = res.json;
                    this.block = false;
                },
                (res: ResponseWrapper) => { this.onError('Error de conexión, por favor vuelva a intentarlo'); this.block = false;
            });
        } else {
            this.consultaExpedienteService.consultarTrabajadoresNombres(event.query).subscribe(
                (res: ResponseWrapper) => {
                    console.log(res.json)
                    this.trabajadores = res.json;
                    this.block = false;
                },
                (res: ResponseWrapper) => { this.onError('Error de conexión, por favor vuelva a intentarlo'); this.block = false;
            });
        }
    }

    buscarExpedientes() {
        // let queryString = '';
        console.log(this.nAnio);
        if (this.selectedUbicacion === undefined) {
            this.mensajes = [];
            this.mensajes.push({severity: 'warn', summary: 'Mensaje de Alerta', detail: 'No se ha selecionado la ubicación'});
            return
        }
        if (this.nAnio === undefined || this.nAnio === null || String(this.nAnio) === '') {
            this.mensajes = [];
            this.mensajes.push({severity: 'warn', summary: 'Mensaje de Alerta', detail: 'No se ha ingresado el año'});
            return;
        } else if (this.nAnio < 1950 || this.nAnio > 2050) {
            this.mensajes = [];
            this.mensajes.push({severity: 'warn', summary: 'Mensaje de Alerta', detail: 'Ingrese un año con registros'});
        }
        if (this.selectedAbogado === undefined) {
            this.mensajes = [];
            this.mensajes.push({severity: 'warn', summary: 'Mensaje de Alerta', detail: 'No se ha selecionado el abogado'});
            return;
        }
        if (this.selectedResulconci === undefined) {
            this.mensajes = [];
            this.mensajes.push({severity: 'warn', summary: 'Mensaje de Alerta', detail: 'No se ha selecionado el resultado de la conciliación'});
            return;
        }
        // if (this.vNumdoc === undefined || this.vNumdoc === null) {
        //     this.mensajes = [];
        //     this.mensajes.push({severity: 'warn', summary: 'Mensaje de Alerta', detail: 'No se ha ingresado el número de documento'});
        //     return;
        // }
        // queryString = '/pase/param?tip_doc=' + this.selectedTipodoc.value + '&nro_doc=' + this.vNumdoc;
        this.block = true;
        console.log(this.selectedUbicacion);
        this.consultaExpedienteService.buscarExpedientes(this.nAnio, this.selectedResulconci.id, this.selectedUbicacion.id,
                 this.selectedAbogado.id).subscribe(
            (res: ResponseWrapper) => {
                console.log('EXPEDIENTES')
                console.log(res.json)
                this.expedientes = res.json;
                this.block = false;
            },
            (res: ResponseWrapper) => { this.onError('Error de conexión, por favor vuelva a intentarlo'); this.block = false; }
        );
    }
    buscarExpedientes2() {
        console.log(this.trabajador);
        if (this.trabajador.pernatural === undefined) {
            this.mensajes = [];
            this.mensajes.push({severity: 'warn', summary: 'Mensaje de Alerta', detail: 'No ha seleccionado correctamente al trabajador'});
            return
        }
        this.block = true;
        this.consultaExpedienteService.buscarExpedientesPorTrabajador(this.nAnio2, this.trabajador.pernatural.vNumdoc).subscribe(
            (res: ResponseWrapper) => {
                console.log(res.json)
                this.expedientes2 = res.json;
                this.block = false;
            },
            (res: ResponseWrapper) => { this.onError('Error de conexión, por favor vuelva a intentarlo'); this.block = false;
        });
    }
   registroCambios() {
        this.eventSubscriber = this.eventManager.subscribe('consultaModificacion', (response) => {
            if (this.index === 0 ) {
                this.buscarExpedientes();
            } else {
                this.buscarExpedientes2()
            }
            this.mensajes = [];
            this.mensajes.push({severity: 'success', summary: 'Mensaje de Alerta', detail: 'Expediente archivado correctamente'});
        });
    }
    registroCambiosObservacion() {
        this.eventSubscriber = this.eventManager.subscribe('consultaMotificacionObservacion', (response) => {
            this.mensajes = [];
            this.mensajes.push({severity: 'success', summary: 'Mensaje de Alerta', detail: 'Expediente observado correctamente'});
        });
    }
    abrirModal(tipoProceso) {
        if (this.id === null || this.id === undefined) {
            this.mensajes = [];
            this.mensajes.push({severity: 'warn', summary: 'Mensaje de Alerta', detail: 'No ha seleccionado un expediente'});
            return;
        }
        console.log('INDEX' + this.index);
        const flagarchiv = (this.index === 0) ? this.expedienteSelected.expediente.nFlgarchiv : this.expedienteSelected2.nFlgarchiv;
        switch (tipoProceso) {
            case 'archivar':
                if (flagarchiv) {
                    this.mensajes = [];
                    this.mensajes.push({severity: 'warn', summary: 'Mensaje de Alerta', detail: 'El expediente se encuentra archivado'});
                } else {
                    this.router.navigate(['/conciliaciones/expediente/consulta', { outlets: { popupexp: 'archivar/' + this.id } }]);
                }
                 break;
            case 'observar':
                this.router.navigate(['/conciliaciones/expediente/consulta', { outlets: { popupexp: 'observar/' + this.id } }]); break;
            case 'datos':
                this.router.navigate(['/conciliaciones/expediente/consulta', { outlets: { popupexp:  this.id } }]); break;
            default:
                break;
        }
    }
    seleccionarFila(expediente) {
        this.id = String(expediente.id);
    }
    actualizarSeleccion(e) {
        this.expedienteSelected = null;
        this.expedienteSelected2 = null;
        this.id = null;
        this.index = e.index;
    }

    private onError(error: any) {
        // this.messages = [];
        // this.messages.push({ severity: 'error', summary: 'Mensaje de Error', detail: error.message });
    }
}

import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils } from 'ng-jhipster';
import { DatePipe } from '@angular/common';
import { ResponseWrapper, createRequestOption } from '../../../shared';
import { Abogado } from '../models/abogado.model';
import { Trabajador } from '../models/trabajador.model';
import { Expediente } from '../models/expediente.model';
import { Resulconci} from '../models/resulconci.model';

@Injectable()
export class ConsultaExpedienteService {

    private resource = '/defensa/api/';
    private resourceAbogados         = this.resource + 'abogados';
    private resourceTrabajadores     = this.resource + 'trabajadors';
    private resourceResultadosConci  = this.resource + 'resulconcis';
    private resourceExpediente       = this.resource + 'expedientes';

    constructor(private http: Http, private dateUtils: JhiDateUtils, private datePipe: DatePipe) { }

    consultarAbogados(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceAbogados, options)
            .map((res: Response) => this.convertResponseAbogados(res));
    }

   private convertResponseAbogados(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServerAbogados(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }
    private convertItemFromServerAbogados(json: any): Abogado {
        const entity: Abogado = Object.assign(new Abogado(), json);
        entity.tFecreg = this.dateUtils.convertDateTimeFromServer(json.tFecreg);
        entity.tFecupd = this.dateUtils.convertDateTimeFromServer(json.tFecupd);
        return entity;
    }
    private convertAbogados(abogado: Abogado): Abogado {
        const copy: Abogado = Object.assign({}, abogado);
        copy.tFecreg = this.dateUtils.toDate(abogado.tFecreg);
        copy.tFecupd = this.dateUtils.toDate(abogado.tFecupd);
        return copy;
    }

    consultarExpediente(id: number): Observable<Expediente> {
        return this.http.get(`${this.resourceExpediente}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemExpedienteFromServer(jsonResponse);
        });
    }

    consultarTrabajadores(numero: any): Observable<ResponseWrapper> {
        return this.http.get(`${this.resourceTrabajadores}/buscarnumero/${numero}`)
            .map((res: Response) => this.convertResponseTrabajadores(res));
    }
    consultarTrabajadoresNombres(nombres: any): Observable<ResponseWrapper> {
        return this.http.get(`${this.resourceTrabajadores}/buscarnombres/${nombres}`)
            .map((res: Response) => this.convertResponseTrabajadores(res));
    }

   private convertResponseTrabajadores(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServerTrabajadores(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }
    private convertItemFromServerTrabajadores(json: any): Trabajador {
        const entity: Trabajador = Object.assign(new Trabajador(), json);
        entity.tFecreg = this.dateUtils.convertDateTimeFromServer(json.tFecreg);
        entity.tFecupd = this.dateUtils.convertDateTimeFromServer(json.tFecupd);
        return entity;
    }

    consultarResultadosConciliacion(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceResultadosConci, options)
            .map((res: Response) => this.convertResponseResulConci(res));
    }
    private convertResponseResulConci(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServerResulConci(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }
    private convertItemFromServerResulConci(json: any): Resulconci {
        const entity: Resulconci = Object.assign(new Resulconci(), json);
        entity.tFecreg = this.dateUtils
            .convertDateTimeFromServer(json.tFecreg);
        entity.tFecupd = this.dateUtils
            .convertDateTimeFromServer(json.tFecupd);
        return entity;
    }
    private convertResulConci(resulconci: Resulconci): Resulconci {
        const copy: Resulconci = Object.assign({}, resulconci);

        copy.tFecreg = this.dateUtils.toDate(resulconci.tFecreg);

        copy.tFecupd = this.dateUtils.toDate(resulconci.tFecupd);
        return copy;
    }
    actualizarExpediente(expediente: Expediente): Observable<Expediente> {
        const copy = this.convertExpediente(expediente);
        return this.http.put(this.resourceExpediente, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemExpedienteFromServer(jsonResponse);
        });
    }

    buscarExpedientes(anio: number, resultado, ubicacion, abogado): Observable<ResponseWrapper> {
        return this.http.get(`${this.resourceExpediente}/anio/${anio}/resultado/${resultado}/ubicacion/${ubicacion}/abogado/${abogado}`)
            .map((res: Response) => this.convertExpedienteResponse(res));
    }
    buscarExpedientesPorTrabajador(anio: number, trabajador): Observable<ResponseWrapper> {
        return this.http.get(`${this.resourceExpediente}/anio/${anio}/trabajador/${trabajador}`)
            .map((res: Response) => this.convertExpedienteResponse(res));
    }
    private convertExpediente(expediente: Expediente): Expediente {
        const copy: Expediente = Object.assign({}, expediente);
        copy.dFecmespar = this.dateUtils.convertLocalDateToServer(expediente.dFecmespar);
        const fecregexp = this.datePipe.transform((expediente.dFecregexp), 'yyyy-MM-dd HH:mm:ss');
        copy.dFecregexp = this.dateUtils.toDate(fecregexp);
        const fecarchiv = this.datePipe.transform((expediente.dFecArchiv), 'yyyy-MM-dd HH:mm:ss');
        copy.dFecArchiv = this.dateUtils.toDate(fecarchiv);
        copy.tFecreg = this.dateUtils.toDate(expediente.tFecreg);
        copy.tFecupd = this.dateUtils.toDate(expediente.tFecupd);
        return copy;
    }
    private convertExpedienteResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemExpedienteFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }
    private convertItemExpedienteFromServer(json: any): Expediente {
        const entity: Expediente = Object.assign(new Expediente(), json);
        entity.dFecregexp = this.dateUtils
            .convertLocalDateFromServer(json.dFecregexp);
        entity.dFecmespar = this.dateUtils
            .convertLocalDateFromServer(json.dFecmespar);
        entity.dFecArchiv = this.dateUtils
            .convertLocalDateFromServer(json.dFecArchiv);
        entity.tFecreg = this.dateUtils
            .convertDateTimeFromServer(json.tFecreg);
        entity.tFecupd = this.dateUtils
            .convertDateTimeFromServer(json.tFecupd);
        return entity;
    }
}

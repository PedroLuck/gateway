import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GatewaySharedModule } from '../../../shared';
import {
    reporteRoute } from './';
import { ReporteDocumentoComponent} from './../reportes/reporte-documento.component';
import { TabViewModule, DataTableModule, CheckboxModule, DropdownModule, CalendarModule , RadioButtonModule } from 'primeng/primeng';
import {GrowlModule} from 'primeng/components/growl/growl';

const ENTITY_STATES = [
    ...reporteRoute
];

@NgModule({
    imports: [
        GatewaySharedModule,
        TabViewModule,
        DataTableModule,
        CheckboxModule,
        DropdownModule,
        CalendarModule,
        RadioButtonModule,
        GrowlModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        ReporteDocumentoComponent,
        // ConsultaExpedientePopupComponent,
        // ConsultaExpedienteDialogComponent,
    ],
    entryComponents: [
        ReporteDocumentoComponent,
        // ConsultaExpedientePopupComponent,
        // ConsultaExpedienteDialogComponent,
    ],
    providers: [
        // ConsultaExpedientePopupService,
        // ConsultaExpedienteArchivarPopupService,
        // ConsultaExpedienteObservarPopupService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ReporteModule {}

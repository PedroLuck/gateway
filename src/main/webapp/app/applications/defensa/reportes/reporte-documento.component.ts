import { Component, OnInit } from '@angular/core';
import { ES } from './../../applications.constant';
import { Router, ActivatedRoute, NavigationStart, Event as NavigationEvent  } from '@angular/router';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';
import { DatePipe } from '@angular/common';
import { ResponseWrapper } from './../../../shared';
import { Message } from 'primeng/components/common/api';

import { ConciliaService } from './../audiencias/concilia.service';
import { Docexpedien } from './../models/docexpedien.model';

@Component({
    selector: 'jhi-reporte',
    templateUrl: './reporte-documento.component.html'
})
export class ReporteDocumentoComponent implements OnInit {

    tipoBusqueda= 'fechas';
    rangeDates: Date[];
    es: any;
    vNumexp: string;
    docexpediens: Docexpedien[];
    messagesExpediente: Message[] = [];

    constructor(
        private router: Router,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private activatedRoute: ActivatedRoute,
        private datePipe: DatePipe,
        private conciliaService: ConciliaService
    ) {

    }

    ngOnInit() {
        this.es = ES;
    }

    buscarReporte() {

        let queryString = '';
        const fec_ini = this.datePipe.transform(this.rangeDates[0], 'dd-MM-yyyy');
        const fec_fin = this.datePipe.transform(this.rangeDates[1], 'dd-MM-yyyy');

        if (fec_ini === null || fec_fin === null) {
            this.messagesExpediente = [];
            this.messagesExpediente.push({ severity: 'error', summary: 'Mensaje de Error', detail: 'Ingrese Rango de Fechas' });
        } else {
        queryString = 'FECHA_INICIO|' + fec_ini + '^' + 'FECHA_FIN|' + fec_fin + '^';
        let nombrereporte;
        nombrereporte = 'ReporteDocumentos';

        this.conciliaService.getReporte(nombrereporte, queryString);
        }
    }

    private onError(error) {
        // console.log('error' + error.message);
        // this.jhiAlertService.error(error.message, null, null);
    }
}

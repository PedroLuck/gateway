import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationStart, Event as NavigationEvent  } from '@angular/router';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';
import { Subscription } from 'rxjs/Rx';
import { DatePipe } from '@angular/common';
import { ResponseWrapper } from './../../../../shared';

import { ConciliaService } from './../../audiencias/concilia.service';

import { ES } from './../../../applications.constant';
import { Concilia } from './../../audiencias/concilia.model';
import { Expediente } from './../../audiencias/expediente.model';
import { Horacon } from './../../audiencias/horacon.model';
import { Abogado } from './../../audiencias/abogado.model';
import { Pasegl } from './../../audiencias/pasegl.model';
import { Atencion } from './../../audiencias/atencion.model';
import { Datlab } from './../../audiencias/datlab.model';
import { Empleador } from './../../audiencias/empleador.model';
import { Perjuridica } from './../../audiencias/perjuridica.model';
import { Pernatural } from './../../audiencias/pernatural.model';
import { Trabajador } from './../../audiencias/trabajador.model';
import { Resulconci } from './../../audiencias/resulconci.model';
import { Docexpedien } from './../../../../entities/docexpedien/docexpedien.model';
import { Tipdocexp } from './../../../../entities/tipdocexp/tipdocexp.model';
import { Tipproveid } from './../../../../entities/tipproveid/tipproveid.model';
import { Dettipprov } from './../../../../entities/dettipprov/dettipprov.model';
import { Message } from 'primeng/components/common/api';

@Component({
    selector: 'jhi-consulta-expediente',
    templateUrl: './asignar-resolutor.component.html'
})
export class AsignarResolutorComponent implements OnInit {

    expedientes: any;
    id = '';
    tipoBusqueda = '1';
    es: any;
    rangeDates: Date[];
    vNumexp: string;
    id_expediente: any;
    idconciliacion = '';
    idexpediente = '';

    concilia: Concilia;
    expediente: Expediente;
    horacon: Horacon;
    pasegl: Pasegl;
    atencion: Atencion;
    datlab: Datlab;
    empleador: Empleador;
    perjuridica: Perjuridica;
    pernaturalEMP: Pernatural;
    pernaturalTRA: Pernatural;
    trabajador: Trabajador;
    resulconci: Resulconci;
    docexpedien: Docexpedien;
    docexpediens: Docexpedien[];
    concilias: Concilia[];
    tipdocexp: Tipdocexp;
    tipproveid: Tipproveid;
    dettipprov: Dettipprov;
    eventSubscriber: Subscription;
    messagesExpediente: Message[] = [];

    fechaActual = new Date();
    nAnio: number = this.fechaActual.getFullYear();

    constructor(
        private router: Router,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private activatedRoute: ActivatedRoute,
        private datePipe: DatePipe,
        private conciliaService: ConciliaService
    ) {

    }

    ngOnInit() {
        this.es = ES;
        this.registerChangeInExpediente();
    }

    onRowSelect(event) {
        this.idexpediente = String(event.data.expediente.id); // Id expediente
    }

    AsignarResolutor() {
        if (this.idexpediente === null || this.idexpediente === undefined  || this.idexpediente === '' ) {
            this.messagesExpediente = [];
            this.messagesExpediente.push({ severity: 'error', summary: 'Mensaje', detail: 'Seleccione un registo de la tabla' });
        }else {
            this.id = this.idexpediente;
            this.router.navigate(['/defensa/expediente/asignar-resolutor', { outlets: { popup: this.id } }]);
        }
    }

    buscarAudiencia() {
        let queryString = '';
        if (this.tipoBusqueda === '1') {
            queryString = '/concilias/expediente/asignar/param?nro_exp=' + this.vNumexp + '&anio=' + this.nAnio;
        } else {
            const fec_ini = this.datePipe.transform(this.rangeDates[0], 'dd-MM-yyyy');
            const fec_fin = this.datePipe.transform(this.rangeDates[1], 'dd-MM-yyyy');
            queryString = '/concilias/expediente/asignar/param?fec_ini=' + fec_ini + '&fec_fin=' + fec_fin;
        }
        this.conciliaService.consultaAudiencia(queryString).subscribe(
            (res: ResponseWrapper) => {
                this.concilias = res.json;
                this.concilias.forEach((item, index) => {
                     this.DatosFaltantes(item, index);
                });

            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    registerChangeInExpediente() {
        this.eventSubscriber = this.eventManager.subscribe('ExpedienteResolutorListModification', (response) => this.buscarAudiencia());
    }

    private DatosFaltantes(item, index) {
        const concilia = item;
        this.expediente = concilia.expediente;
        this.resulconci = concilia.resulconci;
        this.pasegl = this.expediente.pasegl;
        this.atencion = this.pasegl.atencion;
        this.datlab = this.atencion.datlab;
        this.empleador = this.datlab.empleador;
        this.perjuridica = this.empleador.perjuridica;
        this.pernaturalEMP = this.empleador.pernatural;
        this.trabajador = this.datlab.trabajador;
        this.pernaturalTRA = this.trabajador.pernatural;
        this.horacon = concilia.horacon;
        if (this.perjuridica != null) {
            this.concilias[index].nrodocemp = this.perjuridica.vNumdoc;
            this.concilias[index].fullnameemp = this.perjuridica.vRazsocial;
        }else {
            this.concilias[index].nrodocemp = this.pernaturalEMP.vNumdoc;
            this.concilias[index].fullnameemp = this.pernaturalEMP.vNombres + ' ' + this.pernaturalEMP.vApepat + ' ' + this.pernaturalEMP.vApemat ;
        }
        this.concilias[index].nrodoctrab = this.pernaturalTRA.vNumdoc;
        this.concilias[index].fullnametrab = this.pernaturalTRA.vNombres + ' ' + this.pernaturalTRA.vApepat + ' ' + this.pernaturalTRA.vApemat ;
        this.concilias[index].fechahoraconci = this.datePipe.transform(concilia.dFecconci, 'dd/MM/yyyy') +
        ' ' + this.horacon.vDescrip + ':00';
    }

    private onError(error) {
        // console.log('error' + error.message);
        // this.jhiAlertService.error(error.message, null, null);
    }

}

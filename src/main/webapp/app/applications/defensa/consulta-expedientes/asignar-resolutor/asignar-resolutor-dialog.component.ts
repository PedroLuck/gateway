import { ES } from './../../../applications.constant';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { Response } from '@angular/http';
import { DatePipe } from '@angular/common'

import { JhiEventManager, JhiAlertService } from 'ng-jhipster';
import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { AsignarResolutorPopupService } from './asignar-resolutor-popup.service';
import { ResponseWrapper } from './../../../../shared';

import { Message } from 'primeng/components/common/api';
import { MessageService } from 'primeng/components/common/messageservice';
import { ComboModel } from './../../../general/combobox.model';
import { Concilia } from './../../audiencias/concilia.model';
import { Expediente } from './../../audiencias/expediente.model';
import { Horacon } from './../../audiencias/horacon.model';
import { Abogado } from './../../audiencias/abogado.model';

import { ConciliaService } from './../../audiencias/concilia.service';
import { ExpedienteService } from './../../audiencias/expediente.service';
import { Pasegl } from './../../audiencias/pasegl.model';
import { Atencion } from './../../audiencias/atencion.model';
import { Datlab } from './../../audiencias/datlab.model';
import { Empleador } from './../../audiencias/empleador.model';
import { Perjuridica } from './../../audiencias/perjuridica.model';
import { Pernatural } from './../../audiencias/pernatural.model';
import { Trabajador } from './../../audiencias/trabajador.model';
import { Docexpedien } from './../../models/docexpedien.model';
import { Tipdocident} from './../../models/tipdocident.model'
import { ResolutorService } from './../../services/resolutor.service';
import { Resolutor } from './../../models/resolutor.model';

@Component({
    selector: 'jhi-documento-asignacion-dialog',
    templateUrl: './asignar-resolutor-dialog.component.html'
})
export class AsignarResolutorDialogComponent implements OnInit {

    es: any;
    fechaRegDoc: Date;
    registro: Date;
    direcciones: any;
    motivos: any;
    audiencia: any;

    concilia: Concilia;
    expediente: Expediente;
    horacon: Horacon;
    pasegl: Pasegl;
    atencion: Atencion;
    datlab: Datlab;
    empleador: Empleador;
    perjuridica: Perjuridica;
    pernaturalEMP: Pernatural;
    pernaturalTRA: Pernatural;
    trabajador: Trabajador;
    docexpedien = new Docexpedien();

    numero_exp: String;
    anio: String;
    varfechahoraconci: String;
    isSaving: boolean;
    fullnametrab: string;
    fullnameemp: string;
    tipprovdisabled: boolean;
    detprovdisabled: boolean;
    oficiodisabled: boolean;
    nro_oficio: string;
    nro_folios: number;
    resolutors: ComboModel[];
    selectedResolutor: ComboModel;
    messagesDocumento: Message[] = [];
    resolutor = new Resolutor();

    constructor(
        public activeModal: NgbActiveModal,
        private messageService: MessageService,
        private eventManager: JhiEventManager,
        private conciliaService: ConciliaService,
        private expedienteService: ExpedienteService,
        private resolutorService: ResolutorService,
        public datepipe: DatePipe,
        private router: Router
    ) {

    }

    ngOnInit() {
        this.es = ES;
        this.loadResolutor();
        this.fechaRegDoc = new Date();
        this.numero_exp = this.expediente.vNumexp + '-' + this.expediente.nAnioexp;
        let mes_exp;
        if (this.expediente.dFecregexp.month < 10) {
            mes_exp = '0' + this.expediente.dFecregexp.month;
        } else {
            mes_exp = this.expediente.dFecregexp.month;
        }

        if (this.expediente.resolutor !== null) {
            this.resolutor = this.expediente.resolutor;
            this.selectedResolutor = new ComboModel(this.resolutor.vNomresol, String(this.resolutor.id), 0);
        }

        this.pasegl = this.expediente.pasegl;
        this.atencion = this.pasegl.atencion;
        this.datlab = this.atencion.datlab;
        this.empleador = this.datlab.empleador;
        this.perjuridica = this.empleador.perjuridica;
        this.pernaturalEMP = this.empleador.pernatural;
        this.trabajador = this.datlab.trabajador;
        this.pernaturalTRA = this.trabajador.pernatural;
        if (this.perjuridica != null) {
            this.expediente.nrodocemp = this.perjuridica.vNumdoc;
            this.expediente.fullnameemp = this.perjuridica.vRazsocial;
        }else {
            this.expediente.nrodocemp = this.pernaturalEMP.vNumdoc;
            this.expediente.fullnameemp = this.pernaturalEMP.vNombres + ' ' + this.pernaturalEMP.vApepat + ' ' + this.pernaturalEMP.vApemat ;
            console.log(this.pernaturalEMP.vNumdoc);
        }
        this.expediente.nrodoctrab = this.pernaturalTRA.vNumdoc;
        this.expediente.fullnametrab = this.pernaturalTRA.vNombres + ' ' + this.pernaturalTRA.vApepat + ' ' + this.pernaturalTRA.vApemat ;

        this.varfechahoraconci = this.expediente.dFecregexp.day + '/' + mes_exp + '/' + this.expediente.dFecregexp.year;

        this.isSaving = false;
        this.tipprovdisabled = true;
        this.detprovdisabled = true;
        this.oficiodisabled = true;

    }

    loadResolutor() {
        this.resolutorService.consultaCbResolutor().subscribe(
            (res: ResponseWrapper) => {
                this.resolutors = [];
                this.resolutors = res.json;
            },
            (res: ResponseWrapper) => { this.onError(res.json); }
        );
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        if (this.validarResolutor() === true ) {

            console.log('Resolutor select');
            console.log(this.selectedResolutor);
            this.expediente.resolutor =  {id: this.selectedResolutor.value};
            this.expediente.nFlgresol = false;
            const fechaResolucion = new Date();
            this.expediente.dFecreso = new Date();

            if (this.expediente.dFecmespar !== null) {
                this.expediente.dFecmespar = {
                    day:  this.datepipe.transform(this.expediente.dFecmespar, 'dd'),
                    month:  this.datepipe.transform(this.expediente.dFecmespar, 'MM'),
                    year:  this.datepipe.transform(this.expediente.dFecmespar, 'yyyy')
                };
            }

            if (this.expediente.dFecArchiv !== null) {
                this.expediente.dFecArchiv = {
                    day:  this.datepipe.transform(this.expediente.dFecArchiv, 'dd'),
                    month:  this.datepipe.transform(this.expediente.dFecArchiv, 'MM'),
                    year:  this.datepipe.transform(this.expediente.dFecArchiv, 'yyyy')
                };
            }

            this.isSaving = true;
            console.log(this.expediente);
                this.subscribeToSaveResponse(
                    this.expedienteService.update(this.expediente));
        }

        console.log(this.messagesDocumento);
    }

    validarResolutor(): boolean {
        if (this.selectedResolutor === undefined) {
            this.onErrorDocumento('Debe seleccionar un resolutor');
            return false;
        }   else {
            return true;
        }
    }

    private subscribeToSaveResponse(result: Observable<Expediente>) {
        result.subscribe((res: Expediente) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: Expediente) {
        console.log('broadcast');
        console.log(this.router.url);
        console.log(this.router.url.indexOf('/defensa/expediente/asignar-resolutor-admin'));
        if (this.router.url.indexOf('/defensa/expediente/asignar-resolutor-admin') === 0) {
            console.log(1);
            this.eventManager.broadcast({ name: 'ExpedienteResolutorAdminListModification', content: 'OK'});
        } else  {
            this.eventManager.broadcast({ name: 'ExpedienteResolutorListModification', content: 'OK'});
            console.log(2);
        }
        // this.eventManager.broadcast({ name: 'ExpedienteResolutorListModification', content: 'OK'});
        console.log(Expediente);
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
        console.log('saveerror');
    }

    private onError(error: any) {
        // this.jhiAlertService.error(error.message, null, null);
    }

    private onErrorDocumento(error: any) {
        this.messagesDocumento = [];
        this.messagesDocumento.push({ severity: 'error', summary: 'Mensaje de Error', detail: error });
    }
}

@Component({
    selector: 'jhi-audiencia-asignacion-popup',
    template: ''
})
export class AsignarResolutorPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private documentoAsignacionPopupService: AsignarResolutorPopupService
    ) { }

    ngOnInit() {
        console.log('Hola');
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.documentoAsignacionPopupService
                    .open(AsignarResolutorDialogComponent as Component, params['id']);
            } else {
                this.documentoAsignacionPopupService
                    .open(AsignarResolutorDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}

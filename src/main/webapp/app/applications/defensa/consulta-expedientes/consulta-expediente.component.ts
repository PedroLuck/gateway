import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationStart, Event as NavigationEvent  } from '@angular/router';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

@Component({
    selector: 'jhi-consulta-expediente',
    templateUrl: './consulta-expediente.component.html'
})
export class ConsultaExpedienteComponent implements OnInit {

    expedientes: any;
    id = '14';
    estado_expediente: number;

    constructor(
        private router: Router,
        private eventManager: JhiEventManager,
        private activatedRoute: ActivatedRoute
    ) {

    }

    ngOnInit() {
        this.VerExpedienteEmitidos();
    }

    onTabChange(indextab) {
        console.log('Index Tab');
        console.log(indextab.index);
        if (indextab.index === 0) {
            this.VerExpedienteEmitidos();
        } else if (indextab.index === 1) {
            this.VerExpedienteParaMultar();
        } else if (indextab.index === 2) {
            this.VerExpedienteMultados();
        }
    }

    VerExpedienteEmitidos() {
        // this.router.navigate(['/defensa/expediente/consulta', { outlets: { emitidos: ['exp-emitidos'] } }]);
        this.estado_expediente = 0;
        this.router.navigate(['/defensa/expediente/consulta', 'exp-emitidos']);
    }

    VerExpedienteParaMultar() {
        this.estado_expediente = 1;
        // this.router.navigate(['/defensa/expediente/consulta']);
        // this.router.navigate(['/defensa/expediente/consulta', { outlets: { multados: ['exp-multados'] } }]);
        this.router.navigate(['/defensa/expediente/consulta', 'exp-paramultar']);
    }

    VerExpedienteMultados() {
        this.estado_expediente = 2;
        // this.router.navigate(['/defensa/expediente/consulta']);
        // this.router.navigate(['/defensa/expediente/consulta', { outlets: { multados: ['exp-multados'] } }]);
        this.router.navigate(['/defensa/expediente/consulta', 'exp-multados']);
    }
}

import { ES } from './../../../applications.constant';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';
import { DatePipe } from '@angular/common'

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ResolucionSubdirectoralPopupService } from './resolucion-subdirectoral-popup.service';
import { ResponseWrapper } from './../../../../shared';

import { Message } from 'primeng/components/common/api';
import { MessageService } from 'primeng/components/common/messageservice';
import { ComboModel } from './../../../general/combobox.model';
import { Concilia } from './../../audiencias/concilia.model';
import { Expediente } from './../../audiencias/expediente.model';
import { Horacon } from './../../audiencias/horacon.model';
import { Abogado } from './../../audiencias/abogado.model';
import { AbogadoService } from './../../audiencias/abogado.service';
import { ConciliaService } from './../../audiencias/concilia.service';
import { Pasegl } from './../../audiencias/pasegl.model';
import { Atencion } from './../../audiencias/atencion.model';
import { Datlab } from './../../audiencias/datlab.model';
import { Empleador } from './../../audiencias/empleador.model';
import { Perjuridica } from './../../audiencias/perjuridica.model';
import { Pernatural } from './../../audiencias/pernatural.model';
import { Trabajador } from './../../audiencias/trabajador.model';
import { Dirperjuri } from './../../audiencias/dirperjuri.model';
import { DirperjuriService } from './../../audiencias/dirperjuri.service';
import { Dirpernat } from './../../audiencias/dirpernat.model';
import { DirpernatService } from './../../audiencias/dirpernat.service';
import { Tipdocident} from './../../../../entities/tipdocident/tipdocident.model';
import { Motatenofic } from './../../audiencias/motatenofic.model';
import { Resulconci } from '../../../../entities/resulconci/index';
import { Tipresconc } from './../../audiencias/tipresconc.model';
import { Resolucrd } from './../../../../entities/resolucrd/resolucrd.model'
import { ResolucrdService } from './../../../../entities/resolucrd/resolucrd.service';
import { Multaconci } from './../../../../entities/multaconci/multaconci.model';
import { MultaconciService} from './../../../../entities/multaconci/multaconci.service';

@Component({
    selector: 'jhi-audiencia-consulta-dialog',
    templateUrl: './resolucion-subdirectoral-dialog.component.html'
})

export class ResolucionSubdirectoralDialogComponent implements OnInit {

    es: any;
    fechaAudiencia: Date;
    fechaMP: Date;
    registro: Date;
    selectedValues1: string[];
    selectedValues2: string[];
    selectedValues3: string[];
    selectedModelos: any;
    direcciones: any;
    motivos: any;
    audiencia: any;
    disabledSegInst: boolean;
    disabledInstJud: boolean;
    resolucionModelos: any[];

    // CLases
    concilia: Concilia;
    expediente: Expediente;
    horacon: Horacon;
    pasegl: Pasegl;
    atencion: Atencion;
    datlab: Datlab;
    empleador: Empleador;
    perjuridica: Perjuridica;
    pernaturalEMP: Pernatural;
    pernaturalTRA: Pernatural;
    trabajador: Trabajador;
    abogado: Abogado;
    tipdocidentEMP: Tipdocident;
    tipdocidentTRA: Tipdocident;
    resolucrd = new Resolucrd();
    multaconci = new Multaconci();

    // CLases Lista Concilia
    expedienteCon: Expediente;
    horaconCon: Horacon;
    abogadoCon: Abogado;
    resulconciCon: Resulconci;
    tipresconcCon: Tipresconc;

    // Listas
    dirperjuris: any[];
    dirpernatsEmp: any[];
    dirpernatsTra: Dirpernat[];
    motatenofics: any[];
    selectmotatenofic: Motatenofic[];
    selectmotatenoficstring: String[];
    conciliasCon: Concilia[];
    // arraydireccion: any[];

    // Variables
    numero_exp: string;
    varfechaexp: string;
    fullnameemp: string;
    tipdocemp: string;
    numdocemp: string;
    nomemp: string;
    nomtrab: string;
    varfecconci: string;
    varhorconci: string;
    varfecmp: string;
    banPersona: string; // Empleador 1: PersonaJuridica 2:PersonaNatural

    // nuevas
    varfojasconc: number;
    varcelnotificacion: string;
    fechanotificacion: Date;
    varfojascelula: number;
    disabledfojas: boolean;
    selectedDirEmp: any;
    checkaviso: boolean;
    varfojasaviso: string;
    varmontomulta: number;
    messagesResolucion: Message[] = [];
    isSaving: boolean;
    isVistaPrevia: boolean;
    nroresolucionsd: string;
    fecharesolucionsd: string;
    msgs: Message[] = [];
    dirperjuri: Dirperjuri;
    codigo_Resolucion: number;
    tipo_modelo: number;
    parrafo_resolucion: string;
    parametroString: string;
    nombrereporte: string;

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private datePipe: DatePipe,
        private dirperjuriService: DirperjuriService,
        private dirpernatService: DirpernatService,
        private conciliaService: ConciliaService,
        private resolucrdService: ResolucrdService,
        private multaconciService: MultaconciService,
        private eventManager: JhiEventManager
    ) {

    }

    loadAudiencias(id: any) {
        this.conciliaService.consultaAudienciaExpediente(id).subscribe(
            (res: ResponseWrapper) => {
                this.conciliasCon = res.json;
                this.conciliasCon.forEach((item, index) => {
                    this.DatosFaltantesConciliacion(item, index);
                });
                // console.log(JSON.stringify(this.dirpernat));
            },
            (res: ResponseWrapper) => { this.onError(res.json); }
        );
    }

    loadDirecEmpleadorPerJur(id: any) {
        this.dirperjuriService.buscarDireccionesPerJur(id).subscribe(
            (res: ResponseWrapper) => {
                this.dirperjuris = res.json;
                this.loadResolucionConciliacion();
            },
            (res: ResponseWrapper) => { this.onError(res.json); }
        );
    }

    loadDirecEmpleadorPerNatu(id: any) {
        this.dirpernatService.buscarDireccionesEmpleador(id).subscribe(
            (res: ResponseWrapper) => {
                this.dirpernatsEmp = res.json;
                this.loadResolucionConciliacion();
            },
            (res: ResponseWrapper) => { this.onError(res.json); }
        );
    }

    loadDirecTrabajadorPerNatu(id: any) {
        this.dirpernatService.buscarDireccionesTrabajador(id).subscribe(
            (res: ResponseWrapper) => {
                this.dirpernatsTra = res.json;
                console.log('persona natural dir');
                console.log(this.dirpernatsTra);
            },
            (res: ResponseWrapper) => { this.onError(res.json); }
        );
    }

    ValidadCeroidmotate() {
        this.motatenofics.forEach((item, index) => {
            const concilia = item;
            if (concilia.idmotpase == null ) {
                this.motatenofics[index].idmotpase = 0;
                this.motatenofics[index].motivselec = false;
            }else {
                this.motatenofics[index].motivselec = true;
            }
        });
    }

    loadResolucionConciliacion() {
        this.resolucrdService.findConcilia(this.concilia.id).subscribe((resolucrd) => {
            this.codigo_Resolucion = 0;
            if (resolucrd.id !== 0) {
                this.resolucrd.tFecreg = this.datePipe
                        .transform(resolucrd.tFecreg, 'yyyy-MM-ddTHH:mm:ss');
                this.resolucrd = resolucrd;

                let name_modelo;
                switch (this.resolucrd.nCodtiprsd) {
                    case 1: name_modelo = 'Modelo Normal'; this.tipo_modelo = 1; break;
                    case 2: name_modelo = 'Modelo Escrito'; this.tipo_modelo = 2; this.parrafo_resolucion = this.resolucrd.vParrafo1; break;
                    case 3: name_modelo = 'Modelo Judicial'; this.tipo_modelo = 3; this.parrafo_resolucion = this.resolucrd.vParrafo2; break;
                }
                this.selectedModelos =  {name: name_modelo, code: String(this.resolucrd.nCodtiprsd)};
                this.varfojasconc = Number(this.resolucrd.nFojaconc);
                this.varfojascelula = Number(this.resolucrd.nFojanot);
                this.varcelnotificacion = this.resolucrd.vNumnotifi;
                this.fechanotificacion = this.resolucrd.dFechanoti;
                this.varmontomulta = Number(this.resolucrd.fMonmulta);
                this.codigo_Resolucion = this.resolucrd.id;

                if (this.perjuridica != null) {
                    this.dirperjuris.forEach((item, index) => {
                        const direccion = item;
                        if (direccion.direc.id === this.resolucrd.nCoddirec) {
                            this.selectedDirEmp = direccion;
                        }
                    });
                } else {
                    this.dirpernatsEmp.forEach((item, index) => {
                        const direccion = item;
                        if (direccion.direc.id === this.resolucrd.nCoddirec) {
                            this.selectedDirEmp = direccion;
                        }
                    });
                }
            } else {
                this.selectedModelos =  {name: 'Modelo Normal', code: '1'};
                this.tipo_modelo = 1;
                console.log('select inicial');
                console.log(this.selectedModelos);
            }
        });
    }

    ngOnInit() {
        this.isSaving = false;
        this.resolucionModelos = [
            {name: 'Modelo Normal', code: '1'},
            {name: 'Modelo Escrito', code: '2'},
            {name: 'Modelo Judicial', code: '3'}
        ];

        this.es = ES;
        this.disabledfojas = true;
        this.banPersona = '';
        this.checkaviso = false;
        this.disabledSegInst = true;
        this.expediente = this.concilia.expediente;
        this.pasegl = this.expediente.pasegl;
        this.horacon = this.concilia.horacon;
        this.abogado = this.concilia.abogado;
        this.pasegl = this.expediente.pasegl;
        this.atencion = this.pasegl.atencion;
        this.datlab = this.atencion.datlab;
        this.empleador = this.datlab.empleador;
        this.perjuridica = this.empleador.perjuridica;
        this.pernaturalEMP = this.empleador.pernatural;
        this.trabajador = this.datlab.trabajador;
        this.pernaturalTRA = this.trabajador.pernatural;
        this.tipdocidentTRA = this.pernaturalTRA.tipdocident;
        // this.loadDirecTrabajadorPerNatu(this.pernaturalTRA.id);
        // this.loadAudiencias(this.expediente.id);

        this.numero_exp = this.expediente.vNumexp + '-' + this.expediente.nAnioexp;
        this.varfechaexp = this.datePipe.transform(this.expediente.dFecregexp, 'dd-MM-yyyy');
        if (this.perjuridica != null) {
            this.tipdocemp = 'RUC:';
            this.numdocemp = this.perjuridica.vNumdoc;
            this.nomemp = this.perjuridica.vRazsocial;
            this.loadDirecEmpleadorPerJur(this.perjuridica.id);
            this.banPersona = '1';
        }else {
            this.tipdocidentEMP = this.pernaturalEMP.tipdocident;
            this.tipdocemp = this.tipdocidentEMP.vDescorta;
            this.numdocemp = this.pernaturalEMP.vNumdoc;
            this.nomemp = this.pernaturalEMP.vApepat + ' ' + this.pernaturalEMP.vApepat + ' ' + this.pernaturalEMP.vNombres;
            this.loadDirecEmpleadorPerNatu(this.pernaturalEMP.id);
            this.banPersona = '2';
        }
/*
        console.log('**************');
        this.loadResolucionConciliacion();

        if (this.resolucrd != null) {
            console.log('Resolucion SD')
            console.log(this.resolucrd);
            // this.selectedDirEmp.id = this.resolucrd.nCoddirec;
            // console.log(this.resolucrd.nFojavis);
        console.log('**************');
        }*/

        this.nomtrab = this.pernaturalTRA.vApepat + ' ' + this.pernaturalTRA.vApepat + ' ' + this.pernaturalTRA.vNombres;
        this.varfecconci = this.concilia.dFecconci.day + '/' + this.concilia.dFecconci.month + '/' + this.concilia.dFecconci.year;
        this.varhorconci = this.horacon.vDescrip + ':00';
        this.varfecmp = this.expediente.dFecmespar == null ? '' : this.datePipe.transform(this.expediente.dFecmespar, 'dd-MM-yyyy');
    }

    ObtenerTipoReporte() {
        this.tipo_modelo = Number(this.selectedModelos.code);
        this.NuevoReporte();
    }

    NuevoReporte() {
        if (this.tipo_modelo === 1) {
            this. parrafo_resolucion = '';

        } else if (this.tipo_modelo === 2) {

                 this.parrafo_resolucion = 'Que, mediante escrito con registro Nº 105221-111777, la supuesta representante de ' +
                                           'la invitada a conciliar, presenta justificación indicando que la persona a la que le ' +
                                           'otorgó poder se le observo la carta poder toda vez que estaba dirigida a SUNAFIL y no ' +
                                           'a la Sub Dirección de Defensa Legal Gratuita y Asesoría al trabajador y solicita una ' +
                                           'nueva fecha para la segunda conciliación. Que, el numeral 30.1 del artículo 30º del ' +
                                           'Decreto Legislativo nº 910, Ley General de la Inspección del Trabajo y Defensa del ' +
                                           'Trabajador (en adelante “La Ley”) dispone que: “si el empleador o el trabajador no ' +
                                           'asisten a la conciliación por incapacidad física, caso fortuito o fuerza mayor, ' +
                                           'deben acreditar por escrito su inasistencia, dentro del segundo día hábil posterior ' +
                                           'a la fecha señalada para la misma”. Que, del escrito presentado se observa que no se ' +
                                           'justifica la inasistencia a la audiencia de conciliación por las causales previstas en el ' +
                                            'numeral 30.1 del artículo 30º de la Ley, la designación para representar en la audiencia ' +
                                            'de conciliación programada por esta dependencia, constara en una carta poder simple ' +
                                            'con facultades expresas para conciliar; lo que no se advierte en la carta poder ' +
                                            'que se anexa al escrito Nº 105221-17. '

        } else if (this.tipo_modelo === 3) {
            this.parrafo_resolucion = 'Que mediante Oficio Nº 0969-2017-MTPE/1/6 de fecha 21 de junio de 2017, el Procurador ' +
                                      'Público  del Ministerio de Trabajo y Promoción del Empleo, pone en conocimiento de ' +
                                      'este despacho lo resulto por el Quinto Juzgado Especializado en lo Contencioso ' +
                                      'Administrativo de la Corte Superior de Justicia de Lima, mediante Resolución Número ' +
                                      'Veinte de fecha 31 de marzo del 2017, que resuelve declarar nula de pleno derecho la ' +
                                      'Resolución Sub Directoral Nº 0093-2011-MTPE/1/20.22 de fecha 14 de febrero de 2011; ' +
                                      'ordenado a esta Autoridad administrativa de trabajo, cumplir  con emitir un acto ' +
                                      'administrativo expreso que dé cumplimiento a lo ordenado en la sentencia de vista; ' +
                                      'en los seguidos por el trabajador BENITES PUYEN WALTER JOVANY con INVESTIGACION Y ' +
                                      'EXTENSION AGRARIA – INIEA), sobre audiencia de conciliación solicitada y autorizada ' +
                                      'por el Liquidador del Servicio de Liquidaciones de la Sub dirección de Defensa Legal ' +
                                      'Gratuita y Asesoría al trabajador; y, '
        }
    }

    VistaPrevia() {

        if (this.validarResolucion() === true ) {
        let direccionempleador;
        let fojasconciiacion;
        let fechalitnotificacion;
        let avisolit;
        let fojasavisocelula;
        let multa;

        fojasconciiacion = this.Millones(this.varfojasconc).toLowerCase() + ' (' + this.varfojasconc + ')';
        fechalitnotificacion = Number(this.datePipe.transform(this.fechanotificacion, 'dd')) + ' de ' +
                               this.Meses(Number(this.datePipe.transform(this.fechanotificacion, 'MM'))) +
                               ' del ' + this.datePipe.transform(this.fechanotificacion, 'yyyy');
        avisolit = this.checkaviso === true ? 'el aviso y ' : ' ';
        fojasavisocelula = this.checkaviso === true ? this.Millones(this.varfojasaviso).toLowerCase() + ' (' + this.varfojasaviso +
                           ') y ' + this.Millones(this.varfojascelula).toLowerCase() + ' (' + this.varfojasaviso +
                           ')' : this.Millones(this.varfojascelula).toLowerCase() + ' (' + this.varfojascelula + ')';
        direccionempleador = this.selectedDirEmp.direc.vDircomple + ' - ' + this.selectedDirEmp.dist;
        direccionempleador = direccionempleador.replace('.', '*');
        multa = 'S* ' + this.varmontomulta + ' ('  + this.Millones(this.varmontomulta).toUpperCase() + 'Y 00*100 SOLES)';
        // http://localhost:8030/api/reporte/reportPrueba/codDenuncia|146251^
        this.parametroString = 'ID_CON|' + this.concilia.id + '^' +
                          'MULTA|' + multa + '^' +
                          'FOJAS|' + fojasconciiacion + '^' +
                          'FEC_NOTIFICA|' + fechalitnotificacion + '^' +
                          'AVISO|' + avisolit + '^' +
                          'CEL_NOT|' + this.varcelnotificacion.trim() + '^' +
                          'FOJAS_NOT|' + fojasavisocelula + '^' +
                          'DOM_EMP|' + direccionempleador + '^';

        console.log(this.parametroString);
        this.resolucrd.dFecconcil = null;
        this.resolucrd.dFecconcil = {
            day: this.concilia.dFecconci.day,
            month:  this.concilia.dFecconci.month,
            year:  this.concilia.dFecconci.year
        }

        this.resolucrd.dFechanoti = {
            day:  this.datePipe.transform(this.fechanotificacion, 'dd'),
            month:  this.datePipe.transform(this.fechanotificacion, 'MM'),
            year:  this.datePipe.transform(this.fechanotificacion, 'yyyy')
        };

        this.resolucrd.concilia =  {id: this.concilia.id};
        this.resolucrd.fMonmulta = this.varmontomulta;
        this.resolucrd.nFlgactivo = true;
        this.resolucrd.tippersona =  {id: this.banPersona};
        // this.resolucrd.tippersona.id = this.banPersona;
        this.resolucrd.vAvisos = avisolit;
        this.resolucrd.vDireccion = this.selectedDirEmp.direc.vDircomple + ' - ' + this.selectedDirEmp.dist;
        this.resolucrd.vExpanio = String(this.expediente.nAnioexp);
        this.resolucrd.vFojaconc = fojasconciiacion;
        this.resolucrd.vFojanot = fojasconciiacion;
        this.resolucrd.vHorconcil = this.horacon.vDescrip;
        this.resolucrd.vMultalit = multa;
        this.resolucrd.vNomemplea = this.nomemp;
        this.resolucrd.vNomtrabaj = this.nomtrab;
        this.resolucrd.vNumdocemp = this.numdocemp;
        this.resolucrd.vNumnotifi = this.varcelnotificacion.trim();
        this.resolucrd.nFojaconc = this.varfojasconc;
        this.resolucrd.nFojanot = this.varfojascelula;
        this.resolucrd.nCoddirec = this.selectedDirEmp.direc.id;
        this.resolucrd.nFojavis = this.varfojasaviso === null ? Number(0) : Number(this.varfojasaviso);
        // console.log(this.pernaturalTRA);
        if (this.pernaturalEMP === null) {
            this.resolucrd.vTelefono = '';
        } else {
            this.resolucrd.vTelefono = this.perjuridica.vTelefono === null ? '' : this.perjuridica.vTelefono;
        }

        this.resolucrd.vTipdocemp = this.tipdocemp;
        this.resolucrd.nCodtiprsd = Number(this.selectedModelos.code);
        console.log('codigo tip res');
        console.log(this.resolucrd.nCodtiprsd);

        if (this.resolucrd.nCodtiprsd === 1) {
            this.resolucrd.vParrafo1 = undefined;
            this.resolucrd.vParrafo2 = undefined;
            this.nombrereporte = 'ResolucionSDModelo1';
        } else if (this.resolucrd.nCodtiprsd === 2) {
            this.resolucrd.vParrafo1 = this.parrafo_resolucion;
            this.resolucrd.vParrafo2 = undefined;
            this.nombrereporte = 'ResolucionSDModelo2';
        } else if (this.resolucrd.nCodtiprsd === 3) {
            this.resolucrd.vParrafo1 = undefined;
            this.resolucrd.vParrafo2 = this.parrafo_resolucion;
            this.nombrereporte = 'ResolucionSDModelo3';
        }

        console.log(this.resolucrd);
        // this.resolucrd.id = this.codigo_Resolucion;

        if (this.resolucrd.id === 0 || this.resolucrd.id === undefined) {
            this.subscribeToSaveResponsePreliminar(
                this.resolucrdService.createPreliminar(this.resolucrd));
        } else {
            this.resolucrd.tFecreg = this.datePipe
                        .transform(this.resolucrd.tFecreg, 'yyyy-MM-ddTHH:mm:ss');
            this.resolucrd.tFecupd = null;
            this.subscribeToSaveResponsePreliminar(
                this.resolucrdService.updatePreliminar(this.resolucrd));
        }

        }
    }

    Save() {
        if (this.validarResolucion() === true ) {
        this.isSaving = true;
        let direccionempleador;
        let fojasconciiacion;
        let fechalitnotificacion;
        let avisolit;
        let fojasavisocelula;
        let multa;

        fojasconciiacion = this.Millones(this.varfojasconc).toLowerCase() + ' (' + this.varfojasconc + ')';
        fechalitnotificacion = Number(this.datePipe.transform(this.fechanotificacion, 'dd')) + ' de ' +
                               this.Meses(Number(this.datePipe.transform(this.fechanotificacion, 'MM'))) +
                               ' del ' + this.datePipe.transform(this.fechanotificacion, 'yyyy');
        avisolit = this.checkaviso === true ? 'el aviso y ' : ' ';
        fojasavisocelula = this.checkaviso === true ? this.Millones(this.varfojasaviso).toLowerCase() + ' (' + this.varfojasaviso +
                           ') y ' + this.Millones(this.varfojascelula).toLowerCase() + ' (' + this.varfojasaviso +
                           ')' : this.Millones(this.varfojascelula).toLowerCase() + ' (' + this.varfojascelula + ')';
        direccionempleador = this.selectedDirEmp.direc.vDircomple + ' - ' + this.selectedDirEmp.dist;
        direccionempleador = direccionempleador.replace('.', '*');
        multa = 'S* ' + this.varmontomulta + ' ('  + this.Millones(this.varmontomulta).toUpperCase() + 'Y 00*100 SOLES)';
        // http://localhost:8030/api/reporte/reportPrueba/codDenuncia|146251^

        this.resolucrd.dFecconcil = {
            day: this.concilia.dFecconci.day,
            month:  this.concilia.dFecconci.month,
            year:  this.concilia.dFecconci.year
        }

        this.resolucrd.dFechanoti = {
            day:  this.datePipe.transform(this.fechanotificacion, 'dd'),
            month:  this.datePipe.transform(this.fechanotificacion, 'MM'),
            year:  this.datePipe.transform(this.fechanotificacion, 'yyyy')
        };

        this.resolucrd.dFecresosd = {
            day:  this.datePipe.transform(new Date(), 'dd'),
            month:  this.datePipe.transform(new Date(), 'MM'),
            year:  this.datePipe.transform(new Date(), 'yyyy')
        };

        this.resolucrd.concilia =  {id: this.concilia.id};
        this.resolucrd.fMonmulta = this.varmontomulta;
        this.resolucrd.nFlgactivo = true;
        this.resolucrd.tippersona =  {id: this.banPersona};
        // this.resolucrd.tippersona.id = this.banPersona;
        this.resolucrd.vAvisos = avisolit;
        this.resolucrd.vDireccion = this.selectedDirEmp.direc.vDircomple + ' - ' + this.selectedDirEmp.dist;
        this.resolucrd.vExpanio = String(this.expediente.nAnioexp);
        this.resolucrd.vFojaconc = fojasconciiacion;
        this.resolucrd.vFojanot = fojasconciiacion;
        this.resolucrd.vHorconcil = this.horacon.vDescrip;
        this.resolucrd.vMultalit = multa;
        this.resolucrd.vNomemplea = this.nomemp;
        this.resolucrd.vNomtrabaj = this.nomtrab;
        this.resolucrd.vNumdocemp = this.numdocemp;
        this.resolucrd.vNumnotifi = this.varcelnotificacion.trim();
        // console.log(this.pernaturalTRA);
        if (this.pernaturalEMP === null) {
            this.resolucrd.vTelefono = '';
        } else {
            this.resolucrd.vTelefono = this.perjuridica.vTelefono === null ? '' : this.perjuridica.vTelefono;
        }

        /*this.resolucrd.vTelefono = this.banPersona === '1' ? (this.pernaturalEMP.vCelular === null ?
                                   (this.pernaturalEMP.vTelefono === null ? '' : this.pernaturalEMP.vTelefono) :
                                   this.pernaturalEMP.vCelular) : (this.perjuridica.vTelefono === null ? '' : this.perjuridica.vTelefono);*/
        this.resolucrd.vTipdocemp = this.tipdocemp;
        this.resolucrd.nCodtiprsd = Number(this.selectedModelos.code);
        console.log('codigo tip res');
        console.log(this.resolucrd.nCodtiprsd);

        if (this.resolucrd.nCodtiprsd === 1) {
            this.resolucrd.vParrafo1 = undefined;
            this.resolucrd.vParrafo2 = undefined;
        } else if (this.resolucrd.nCodtiprsd === 2) {
            this.resolucrd.vParrafo1 = this.parrafo_resolucion;
            this.resolucrd.vParrafo2 = undefined;
        } else if (this.resolucrd.nCodtiprsd === 3) {
            this.resolucrd.vParrafo1 = undefined;
            this.resolucrd.vParrafo2 = this.parrafo_resolucion;
        }

        let parametroString;
        parametroString = 'ID_CON|' + this.concilia.id + '^' +
                          'MULTA|' + multa + '^' +
                          'FOJAS|' + fojasconciiacion + '^' +
                          'FEC_NOTIFICA|' + fechalitnotificacion + '^' +
                          'AVISO|' + avisolit + '^' +
                          'CEL_NOT|' + this.varcelnotificacion.trim() + '^' +
                          'FOJAS_NOT|' + fojasavisocelula + '^' +
                          'DOM_EMP|' + direccionempleador + '^';

        if (this.resolucrd.id === 0 || this.resolucrd.id === undefined) {
            this.subscribeToSaveResponse(
                this.resolucrdService.create(this.resolucrd));
        } else {
            this.resolucrd.tFecreg = this.datePipe
                        .transform(this.resolucrd.tFecreg, 'yyyy-MM-ddTHH:mm:ss');
            this.resolucrd.tFecupd = null;
            this.subscribeToSaveResponse(
                this.resolucrdService.update(this.resolucrd));
        }
        }
    }

    private subscribeToSaveResponse(result: Observable<Resolucrd>) {
        result.subscribe((res: Resolucrd) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: Resolucrd) {
        this.nroresolucionsd = result.vNumresosd;
        this.fecharesolucionsd = result.dFecresosd;
        let mensajeExito;
        mensajeExito = 'Número de Resolución SD:' + result.vNumresosd + '\n' + 'Fecha Resolución SD: ' + this.datePipe.transform(result.dFecresosd, 'dd-MM-yyyy');
        this.isSaving = true;
        this.isVistaPrevia = true;
        this.messagesResolucion = [];
        this.msgs = [];
        this.messagesResolucion.push({ severity: 'success', summary: 'Se Guardo con éxito', detail: mensajeExito });

        this.multaconci.dFecresosd = {
            day:  this.datePipe.transform(result.dFecresosd, 'dd'),
            month:  this.datePipe.transform(result.dFecresosd, 'MM'),
            year:  this.datePipe.transform(result.dFecresosd, 'yyyy')
        };

        this.multaconci.fMonmulta = result.fMonmulta;
        this.multaconci.nFlgactivo = true;
        this.multaconci.vNumresosd = result.vNumresosd;
        this.multaconci.resolucrd =  {id: result.id};
        // this.activeModal.dismiss(result);
        this.subscribeToSaveResponseMulta(
        this.multaconciService.create(this.multaconci));

        this.eventManager.broadcast({ name: 'ExpedienteParaMultarListModification', content: 'OK'});
    }

    private onSaveError() {
        this.isSaving = false;
        console.log('saveerror');
    }

    private subscribeToSaveResponseMulta(result: Observable<Multaconci>) {
        result.subscribe((res: Multaconci) =>
            this.onSaveSuccessMulta(res), (res: Response) => this.onSaveErrorMulta());
    }

    private onSaveSuccessMulta(result: Multaconci) {
        console.log('broadcast');
        console.log(result);
    }

    private onSaveErrorMulta() {
        this.isSaving = false;
        console.log('saveerrorMulta');
    }

    private subscribeToSaveResponsePreliminar(result: Observable<Resolucrd>) {
        result.subscribe((res: Resolucrd) =>
            this.onSaveSuccessPreliminar(res), (res: Response) => this.onSaveErrorPreliminar());
    }

    private onSaveSuccessPreliminar(result: Multaconci) {
        console.log('broadcast');
        console.log(result);
        this.resolucrd.id = result.id;
        this.resolucrd.tFecreg = this.datePipe
                        .transform(result.tFecreg, 'yyyy-MM-ddTHH:mm:ss');
        this.resolucrd.nUsuareg = result.nUsuareg;
        this.resolucrd.nSedereg = result.nSedereg;
        this.conciliaService.getReporte(this.nombrereporte, this.parametroString);
        // this.resolucrd.id = result.id;
    }

    private onSaveErrorPreliminar() {
        this.isSaving = false;
        console.log('saveerrorMulta');
    }

    Meses(mes) {
        switch (mes) {
            case 1: return 'enero';
            case 2: return 'febrero';
            case 3: return 'marzo';
            case 4: return 'abril';
            case 5: return 'mayo';
            case 6: return 'junio';
            case 7: return 'julio';
            case 8: return 'agosto';
            case 9: return 'septiembre';
            case 10: return 'octubre';
            case 11: return 'noviembre';
            case 12: return 'diciembre';
        }
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    validarResolucion(): boolean {
        this.messagesResolucion = [];
        if (this.selectedDirEmp === undefined) {
            this.onErrorResolucion('Debe seleccionar una dirección');
            return false;
        } else if (this.varfojasconc === undefined) {
            this.onErrorResolucion('Debe ingresar foja de conciliación');
            return false;
        } else if (this.varcelnotificacion === undefined) {
            this.onErrorResolucion('Debe ingresar célula de notificación');
            return false;
        } else if (this.fechanotificacion === undefined) {
            this.onErrorResolucion('Debe ingresar fecha de notificación');
            return false;
        } else if (this.varfojasaviso === undefined && this.checkaviso === true) {
            this.onErrorResolucion('Debe ingresar foja de aviso');
            return false;
        } else if (this.varfojascelula === undefined) {
            this.onErrorResolucion('Debe ingresar foja de cedula de notificación');
            return false;
        } else if (this.varmontomulta === undefined) {
            this.onErrorResolucion('Debe ingresar multa');
            return false;
        } else {
            return true;
        }
    }

    EventchekAviso(e) {
        this.disabledfojas = e.checked === true ? false : true;
    }

    private onError(error: any) {
        // this.messages = [];
        // this.messages.push({ severity: 'error', summary: 'Mensaje de Error', detail: error.message });
    }

    private onErrorResolucion(error: any) {
        this.messagesResolucion = [];
        this.messagesResolucion.push({ severity: 'error', summary: 'Mensaje de Error', detail: error });
    }

    private DatosFaltantesConciliacion(item, index) {
        const concilia = item;
        this.expedienteCon = concilia.expediente;
        if (concilia.resulconci != null) {
            this.resulconciCon = concilia.resulconci;
            this.tipresconcCon = this.resulconciCon.tipresconc;
        }
        this.horaconCon = concilia.horacon;
        this.abogadoCon = concilia.abogado;
        this.conciliasCon[index].fechaconci = this.datePipe.transform(concilia.dFecconci, 'dd/MM/yyyy');
        this.conciliasCon[index].horaconci =  this.horaconCon.vDescrip + ':00';
    }

    private Unidades(num) {

        switch (num) {
            case 1: return 'UN';
            case 2: return 'DOS';
            case 3: return 'TRES';
            case 4: return 'CUATRO';
            case 5: return 'CINCO';
            case 6: return 'SEIS';
            case 7: return 'SIETE';
            case 8: return 'OCHO';
            case 9: return 'NUEVE';
        }

        return '';
    }// Unidades()

    Decenas(num) {

        let decena;
        decena = Math.floor(num / 10);
        let unidad
        unidad = num - (decena * 10);

        switch (decena) {
            case 1:
                switch (unidad) {
                    case 0: return 'DIEZ';
                    case 1: return 'ONCE';
                    case 2: return 'DOCE';
                    case 3: return 'TRECE';
                    case 4: return 'CATORCE';
                    case 5: return 'QUINCE';
                    default: return 'DIECI' + this.Unidades(unidad);
                }
            case 2:
                switch (unidad) {
                    case 0: return 'VEINTE';
                    default: return 'VEINTI' + this.Unidades(unidad);
                }
            case 3: return this.DecenasY('TREINTA', unidad);
            case 4: return this.DecenasY('CUARENTA', unidad);
            case 5: return this.DecenasY('CINCUENTA', unidad);
            case 6: return this.DecenasY('SESENTA', unidad);
            case 7: return this.DecenasY('SETENTA', unidad);
            case 8: return this.DecenasY('OCHENTA', unidad);
            case 9: return this.DecenasY('NOVENTA', unidad);
            case 0: return this.Unidades(unidad);
        }
    }// Unidades()

    DecenasY(strSin, numUnidades) {
        if (numUnidades > 0) {
            return strSin + ' Y ' + this.Unidades(numUnidades);
        }

        return strSin;
    }// DecenasY()

    Centenas(num) {
        let centenas;
        centenas = Math.floor(num / 100);
        let decenas;
        decenas = num - (centenas * 100);

        switch (centenas) {
            case 1:
                if (decenas > 0) {
                    return 'CIENTO ' + this.Decenas(decenas); }
                return 'CIEN';
            case 2: return 'DOSCIENTOS ' + this.Decenas(decenas);
            case 3: return 'TRESCIENTOS ' + this.Decenas(decenas);
            case 4: return 'CUATROCIENTOS ' + this.Decenas(decenas);
            case 5: return 'QUINIENTOS ' + this.Decenas(decenas);
            case 6: return 'SEISCIENTOS ' + this.Decenas(decenas);
            case 7: return 'SETECIENTOS ' + this.Decenas(decenas);
            case 8: return 'OCHOCIENTOS ' + this.Decenas(decenas);
            case 9: return 'NOVECIENTOS ' + this.Decenas(decenas);
        }

        return this.Decenas(decenas);
    }// Centenas()

    Seccion(num, divisor, strSingular, strPlural) {
        let cientos;
        cientos = Math.floor(num / divisor)
        let resto;
        resto = num - (cientos * divisor)

        let letras = '';

        if (cientos > 0) {
            if (cientos > 1) {
                letras = this.Centenas(cientos) + ' ' + strPlural;
            } else {
                letras = strSingular;
            }
        }

        if (resto > 0) {
            letras += '';
        }

        return letras;
    }// Seccion()

    Miles(num) {
        let divisor;
        divisor = 1000;
        let cientos;
        cientos = Math.floor(num / divisor)
        let resto;
        resto = num - (cientos * divisor)

        let strMiles;
        strMiles = this.Seccion(num, divisor, 'UN MIL', 'MIL');
        let strCentenas;
        strCentenas = this.Centenas(resto);

        if (strMiles === '') {
            return strCentenas;
        }
        return strMiles + ' ' + strCentenas;
    }// Miles()

    Millones(num) {
        let divisor;
        divisor = 1000000;
        let cientos;
        cientos = Math.floor(num / divisor)
        let resto;
        resto = num - (cientos * divisor)

        let strMillones;
        strMillones = this.Seccion(num, divisor, 'UN MILLON DE', 'MILLONES DE');
        let strMiles;
        strMiles = this.Miles(resto);

        if (strMillones === '') {
            return strMiles;
        }

        return strMillones + ' ' + strMiles;
    }// Millones()
}

@Component({
    selector: 'jhi-audiencia-popup',
    template: ''
})
export class ResolucionSubdirectoralPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private resolucionsubdirectoralPopupService: ResolucionSubdirectoralPopupService
    ) { }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.resolucionsubdirectoralPopupService
                    .open(ResolucionSubdirectoralDialogComponent as Component, params['id']);
            } else {
                this.resolucionsubdirectoralPopupService
                    .open(ResolucionSubdirectoralDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}

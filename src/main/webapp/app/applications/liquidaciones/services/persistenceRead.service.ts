import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
// Models
import { ResponseWrapper } from '../../../shared';
import { Empleador } from '../models/empleador.model'

@Injectable()
export abstract class PersistenceReadService {

  /**
   * Tipo de Documento de Identidad
   * @returns Observable
   */
  abstract consultaTipoDocIdentidad(): Observable<ResponseWrapper>;

  /**
   * Buscar Empleador por tipo de documento de identidad y nùmero de documento
   * @param  {number} tipodoc
   * @param  {String} numdoc
   * @returns Observable
   */
  abstract findEmpleadorsByDocIdent(tipodoc: number, numdoc: String, tipper: number): Observable<Empleador>
}

import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
// Service Interface
import { PersistenceReadService } from '../services/persistenceRead.service';
// Utilitarios
import { JhiDateUtils } from 'ng-jhipster';
// Models
import { ResponseWrapper } from '../../../shared';
import { Tipdocident } from '../models/tipdocident.model';
import { Empleador } from '../models/empleador.model'

@Injectable()
export class PersistenceReadPrivateService implements PersistenceReadService {

  private resource = '/liquidaciones/api/';
  private resourceTipdocident = this.resource + 'tipdocidents';
  private resourceEmpleador   = this.resource + 'empleadors';

  constructor(
    private http: Http,
    private dateUtils: JhiDateUtils
  ) { }

  // Obteniendo la lista de tipos de documentos de identidad
  consultaTipoDocIdentidad(): Observable<ResponseWrapper> {
      return this.http.get(this.resourceTipdocident, null)
          .map((res: Response) => this.convertResponseTipdocident(res));
  };
  private convertResponseTipdocident(res: Response): ResponseWrapper {
      const jsonResponse = res.json();
      const result = [];
      for (let i = 0; i < jsonResponse.length; i++) {
          result.push(this.convertItemFromServerTipdocident(jsonResponse[i]));
      };
      return new ResponseWrapper(res.headers, result, res.status);
  };
  private convertItemFromServerTipdocident(json: any): Tipdocident {
      const entity: Tipdocident = Object.assign(new Tipdocident(), json);
      return entity;
  };

  // Obteniendo al Empleador por tipo de documento, numero de documento, y tipo de persona
  findEmpleadorsByDocIdent(tipodoc: number, numdoc: String, tipper: number): Observable<Empleador> {
      return this.http.get(`${this.resourceEmpleador}/tipdoc/${tipodoc}/numdoc/${numdoc}/bandperjuri/${tipper}`)
          .map((res: Response) => {
              const jsonResponse = res.json();
              return this.convertItemFromServerEmpleador(jsonResponse);
          });
  };
  private convertItemFromServerEmpleador(json: any): Empleador {
      const entity: Empleador = Object.assign(new Empleador(), json);
      entity.tFecreg = this.dateUtils.convertDateTimeFromServer(json.tFecreg);
      entity.tFecupd = this.dateUtils.convertDateTimeFromServer(json.tFecupd);
      return entity;
  };

};

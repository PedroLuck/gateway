import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
// Servicios
import { PersistenceReadService } from '../../services/persistenceRead.service';
// Modelos
import { ResponseWrapper } from '../../../../shared';
import { Tipdocident } from '../../models/tipdocident.model';
import { Empleador } from '../../models/empleador.model';
import { Atencion } from '../../models/atencion.model';
// Message para error
import { Message } from 'primeng/components/common/api';

@Component({
    selector: 'jhi-empleador',
    templateUrl: './empleador.component.html'
})

export class EmpleadorComponent implements OnInit {

    messages: Message[] = [];
    listaTipdocident: Tipdocident[] = [];
    empleador: Empleador;
    atencion: Atencion;

    constructor(
      private _persistenceReadService: PersistenceReadService,
    ) {}

    ngOnInit() {
      this.cargarListaComboTipoDocumento();
    }

    // Boton Buscar Empleador
    btnBuscarEmpleador(tipDoc, numDoc) {
      this.cargarEmpleadorSolicitado(tipDoc, numDoc);
    };

    // PersistenceRead: ---------------------------------------------------------------------------------
    private cargarEmpleadorSolicitado(tipDoc, numDoc) {
      let tipPer = 0;
      if (tipDoc === 4) { tipPer = 1 }; // TipPer indica si la persona es Natural (0) o Juridica (1)
      this._persistenceReadService.findEmpleadorsByDocIdent(tipDoc, numDoc, tipPer).subscribe((empleador) => {
        console.log('Empleador Buscado; ' + JSON.stringify(empleador));
        this.empleador = empleador;
        this.atencion.empleador = empleador;
        },
        (res: Response) => {
          this.onError(res.json);
        });
    };

    private cargarListaComboTipoDocumento() {
      this._persistenceReadService.consultaTipoDocIdentidad().subscribe((res: ResponseWrapper) => {
        this.listaTipdocident = res.json;
        },
        (res: ResponseWrapper) => {
          this.onError(res.json);
        });
    };

    private onError(error: any) {
        this.messages = [];
        this.messages.push({ severity: 'error', summary: 'Mensaje de Error', detail: error.message });
    };
};

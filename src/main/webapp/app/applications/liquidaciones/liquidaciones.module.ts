import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { customHttpProvider } from '../../blocks/interceptor/http.provider';
import { RegistroAtencionModule } from './registro-atencion/registro-atencion.module';
import { ConsultasModule } from './consultas/consultas.module';
import { CalculoLiquidacionModule } from './calculo-liquidacion/calculo-liquidacion.module';
import { PersistenceReadService } from './services/persistenceRead.service';
import { PersistenceReadPrivateService } from './services/persistenceReadPrivate.service';

@NgModule({
    imports: [
        RegistroAtencionModule,
        ConsultasModule,
        CalculoLiquidacionModule
    ],
    providers: [
        customHttpProvider(),
        { provide: PersistenceReadService, useClass: PersistenceReadPrivateService }
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GatewayLiquidacionesModule {}

import { BaseEntity } from '../../../shared';
import { Pernatural } from './pernatural.model';

export class Trabajador implements BaseEntity {
    constructor(
        public id?: number,
        public nFlgsuces?: boolean,
        public nUsuareg?: number,
        public tFecreg?: any,
        public nFlgactivo?: boolean,
        public nSedereg?: number,
        public nUsuaupd?: number,
        public tFecupd?: any,
        public nSedeupd?: number,
        public cartrab?: BaseEntity,
        public pernatural?: Pernatural,
        public expedientes?: BaseEntity[],
        public datlabs?: BaseEntity[],
        public atencions?: BaseEntity[],
        public sucesors?: BaseEntity[],
    ) {
        this.nFlgsuces = false;
        this.nFlgactivo = false;
    }
}

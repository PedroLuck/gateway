import { BaseEntity } from '../../../shared';

export class Calmes {
    constructor(
        public numero?: number,
        public mes?: string,
        public anio?: number,
        public remuneracion?: number,
    ) {
        this.numero = 0;
        this.mes = '';
        this.anio = 0;
        this.remuneracion = 0.00;
    }
}

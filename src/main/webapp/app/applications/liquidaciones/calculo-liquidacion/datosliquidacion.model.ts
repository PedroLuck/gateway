import { BaseEntity } from '../../../shared';

export class Datosliquidacion {
    constructor(
        public nomTrabajador?: string,
        public razonSocialEmp?: string,
        public desRegimen?: string,
        public desCategoriaTrabajador?: string,
        public razonSocialBanco?: string,
        public desMoneda?: string,
        public fecIngreso?: string,
        public fecCese?: string,
    ) {
        this.nomTrabajador = '';
        this.razonSocialEmp = '';
        this.desRegimen = '';
        this.desCategoriaTrabajador = '';
        this.razonSocialBanco = '';
        this.desMoneda = '';
        this.fecIngreso = '';
        this.fecCese = '';
    }
}

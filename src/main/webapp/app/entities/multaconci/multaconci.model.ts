import { BaseEntity } from './../../shared';

export class Multaconci implements BaseEntity {
    constructor(
        public id?: number,
        public fMonmulta?: number,
        public vNumresosd?: string,
        public dFecresosd?: any,
        public vCodigo?: string,
        public nUsuareg?: number,
        public tFecreg?: any,
        public nFlgactivo?: boolean,
        public nSedereg?: number,
        public nUsuaupd?: number,
        public tFecupd?: any,
        public nSedeupd?: number,
        public fullnametrab?: string,
        public nrodoctrab ?: string,
        public fullnameemp?: string,
        public nrodocemp ?: string,
        public nomemp ?: string,
        public nomtrab ?: string,
        public fechahoraconci?: String,
        public fechaResolucion?: String,
        public multa?: String,
        public fechaconci?: String,
        public horaconci?: String,
        public fecregistro?: String,
        public resolucrd?: BaseEntity,
    ) {
        this.nFlgactivo = false;
    }
}

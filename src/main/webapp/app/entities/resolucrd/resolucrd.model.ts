import { BaseEntity } from './../../shared';

export class Resolucrd implements BaseEntity {
    constructor(
        public id?: number,
        public vNumresosd?: string,
        public dFecresosd?: any,
        public vNomemplea?: string,
        public vNomtrabaj?: string,
        public vDireccion?: string,
        public vTelefono?: string,
        public dFecconcil?: any,
        public vHorconcil?: string,
        public dFechanoti?: any,
        public vNumnotifi?: string,
        public fMonmulta?: number,
        public nUsuareg?: number,
        public tFecreg?: any,
        public nFlgactivo?: boolean,
        public nSedereg?: number,
        public nUsuaupd?: number,
        public tFecupd?: any,
        public nSedeupd?: number,
        public nFojanot?: number,
        public nFojaconc?: number,
        public nCoddirec?: number,
        public nFojavis?: number,
        public tippersona?: BaseEntity,
        public concilia?: BaseEntity,
        public multaconcis?: BaseEntity[],
        public vExpanio?: string,
        public vFojaconc?: string,
        public vAvisos ?: string,
        public vTipdocemp ?: string,
        public vNumdocemp ?: string,
        public vFojanot ?: string,
        public vMultalit ?: string,
        public nCodtiprsd ?: number,
        public vParrafo1 ?: string,
        public vParrafo2 ?: string

    ) {
        this.nFlgactivo = false;
    }
}

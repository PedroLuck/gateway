export * from './mentidad.model';
export * from './mentidad-popup.service';
export * from './mentidad.service';
export * from './mentidad-dialog.component';
export * from './mentidad-delete-dialog.component';
export * from './mentidad-detail.component';
export * from './mentidad.component';
export * from './mentidad.route';

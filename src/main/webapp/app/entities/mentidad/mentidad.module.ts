import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GatewaySharedModule } from '../../shared';
import {
    MentidadService,
    MentidadPopupService,
    MentidadComponent,
    MentidadDetailComponent,
    // MentidadDialogComponent,
    MentidadPopupComponent,
    MentidadDeletePopupComponent,
    MentidadDeleteDialogComponent,
    mentidadRoute,
    mentidadPopupRoute,
} from './';
import { MentidadDialogComponent } from './mentidad-dialog.component';

const ENTITY_STATES = [
    ...mentidadRoute,
    ...mentidadPopupRoute,
];

@NgModule({
    imports: [
        GatewaySharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        MentidadComponent,
        MentidadDetailComponent,
        MentidadDialogComponent,
        MentidadDeleteDialogComponent,
        MentidadPopupComponent,
        MentidadDeletePopupComponent,
    ],
    entryComponents: [
        MentidadComponent,
        MentidadPopupComponent,
        MentidadDeleteDialogComponent,
        MentidadDeletePopupComponent,
    ],
    providers: [
        MentidadService,
        MentidadPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GatewayMentidadModule {}

import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { JhiDateUtils } from 'ng-jhipster';

import { Mentidad } from './mentidad.model';
import { ResponseWrapper, createRequestOption, AuthServerProvider } from '../../shared';

@Injectable()
export class MentidadService {

    private resourceUrl = '/seguridad/api/modulo-entidades';
    private resourceSearchUrl = '/seguridad/api/_search/modulo-entidades';

    constructor(private http: Http, private dateUtils: JhiDateUtils, private authServerProvider: AuthServerProvider) { }

    create(mEntidad: Mentidad): Observable<Mentidad> {
        const token = this.authServerProvider.getToken();
        mEntidad.numEliminar = 1;
        mEntidad.varUsuarioLog = token.substring(token.length - 20);
        const copy = this.convert(mEntidad);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(mEntidad: Mentidad): Observable<Mentidad> {
        const copy = this.convert(mEntidad);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<Mentidad> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to Moduloentidad.
     */
    private convertItemFromServer(json: any): Mentidad {
        const entity: Mentidad = Object.assign(new Mentidad(), json);
        entity.datFechaLog = this.dateUtils
            .convertDateTimeFromServer(json.datFechaLog);
        return entity;
    }

    /**
     * Convert a ModuloEntidad to a JSON which can be sent to the server.
     */
    private convert(mEntidad: Mentidad): Mentidad {
        const copy: Mentidad = Object.assign({}, mEntidad);

        copy.datFechaLog = this.dateUtils.toDate(mEntidad.datFechaLog);
        return copy;
    }
}

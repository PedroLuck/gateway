import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { MentidadComponent } from './mentidad.component';
import { MentidadDetailComponent } from './mentidad-detail.component';
import { MentidadPopupComponent } from './mentidad-dialog.component';
import { MentidadDeletePopupComponent } from './mentidad-delete-dialog.component';

@Injectable()
export class MentidadResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
        };
    }
}

export const mentidadRoute: Routes = [
    {
        path: 'modulo-entidad',
        component: MentidadComponent,
        resolve: {
            'pagingParams': MentidadResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.mEntidad.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'modulo-entidad/:id',
        component: MentidadDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.mEntidad.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const mentidadPopupRoute: Routes = [
    {
        path: 'modulo-entidad-new',
        component: MentidadPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.mEntidad.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'modulo-entidad/:id/edit',
        component: MentidadPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.mEntidad.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'modulo-entidad/:id/delete',
        component: MentidadDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.mEntidad.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];

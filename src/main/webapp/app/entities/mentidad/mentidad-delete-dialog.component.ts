import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Mentidad } from './mentidad.model';
import { MentidadPopupService } from './mentidad-popup.service';
import { MentidadService } from './mentidad.service';

@Component({
    selector: 'jhi-mentidad-delete-dialog',
    templateUrl: './mentidad-delete-dialog.component.html'
})
export class MentidadDeleteDialogComponent {

    mEntidad: Mentidad;

    constructor(
        private mEntidadService: MentidadService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(mEntidad) {
        mEntidad.numEliminar = 0;
        this.mEntidadService.update(mEntidad).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'mEntidadListModification',
                content: 'Deleted an mEntidad'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-mentidad-delete-popup',
    template: ''
})
export class MentidadDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private mEntidadPopupService: MentidadPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.mEntidadPopupService
                .open(MentidadDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}

import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { Mentidad } from './mentidad.model';
import { MentidadService } from './mentidad.service';

@Component({
    selector: 'jhi-mentidad-detail',
    templateUrl: './mentidad-detail.component.html'
})
export class MentidadDetailComponent implements OnInit, OnDestroy {

    mEntidad: Mentidad;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private mEntidadService: MentidadService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInMEntidads();
    }

    load(id) {
        this.mEntidadService.find(id).subscribe((mEntidad) => {
            this.mEntidad = mEntidad;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInMEntidads() {
        this.eventSubscriber = this.eventManager.subscribe(
            'mEntidadListModification',
            (response) => this.load(this.mEntidad.id)
        );
    }
}

import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Mentidad } from './mentidad.model';
import { MentidadPopupService } from './mentidad-popup.service';
import { MentidadService } from './mentidad.service';
import { Modulo, ModuloService } from '../modulo';
import { Entidad, EntidadService } from '../entidad';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-mentidad-dialog',
    templateUrl: './mentidad-dialog.component.html'
})
export class MentidadDialogComponent implements OnInit {

    mEntidad: Mentidad;
    isSaving: boolean;

    modulos: Modulo[];

    entidads: Entidad[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private mEntidadService: MentidadService,
        private moduloService: ModuloService,
        private entidadService: EntidadService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.moduloService.query()
            .subscribe((res: ResponseWrapper) => { this.modulos = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.entidadService.query()
            .subscribe((res: ResponseWrapper) => { this.entidads = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.mEntidad.id !== undefined) {
            this.subscribeToSaveResponse(
                this.mEntidadService.update(this.mEntidad));
        } else {
            this.subscribeToSaveResponse(
                this.mEntidadService.create(this.mEntidad));
        }
    }

    private subscribeToSaveResponse(result: Observable<Mentidad>) {
        result.subscribe((res: Mentidad) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: Mentidad) {
        this.eventManager.broadcast({ name: 'mEntidadListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackModuloById(index: number, item: Modulo) {
        return item.id;
    }

    trackEntidadById(index: number, item: Entidad) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-mentidad-popup',
    template: ''
})
export class MentidadPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private mEntidadPopupService: MentidadPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.mEntidadPopupService
                    .open(MentidadDialogComponent as Component, params['id']);
            } else {
                this.mEntidadPopupService
                    .open(MentidadDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}

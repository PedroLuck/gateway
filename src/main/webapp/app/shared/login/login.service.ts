import { Injectable } from '@angular/core';
import { JhiLanguageService } from 'ng-jhipster';

import { Principal } from '../auth/principal.service';
import { AuthServerProvider } from '../auth/auth-jwt.service';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { ResponseWrapper, createRequestOption } from '../../shared';
import { Tipdocident } from '../../entities/tipdocident';
import { ComboModel } from '../../applications/general/combobox.model';
import { Pernatural } from '../../entities/pernatural';
import { Tipvia } from '../../entities/tipvia';
import { Tipzona } from '../../entities/tipzona';

@Injectable()
export class LoginService {
    private resourceRegPernat = '/denuncias/api/nuevodenunciante';
    private resourceRegDirdenunciante = '/denuncias/api/direcciondenunciante';
    private resourcePersonaValidarServicio = '/api/validarpersonaservicio';
    private resourceNotificacionUsuario = '/api/notificausuario';
    private resourceDepa = '/denuncias/api/departamentos';
    private resourceProv = '/denuncias/api/provincias';
    private resourceDist = '/denuncias/api/distritos';
    private resourceTVia = '/denuncias/api/tipvias';
    private resourceTZona = '/denuncias/api/tipzonas';
    private resourceTipoDoc = '/denuncias/api/tipdocidents';

    constructor(
        private languageService: JhiLanguageService,
        private principal: Principal,
        private authServerProvider: AuthServerProvider,
        private http: Http
    ) {}
    consultaTipoDocIdentidad(): Observable<ResponseWrapper> {
        return this.http.get(this.resourceTipoDoc, null)
            .map((res: Response) => this.convertResponseTipoDocIdentidad(res));
    }
    consultaDepas(): any {
        return this.http.get(`${this.resourceDepa}`).map((res: Response) => {
            const jsonResponse = res.json();
            return jsonResponse;
        });
    }

    consultaDepa(depas: any): any {
        return this.http.get(`${this.resourceDepa}/${depas}`).map((res: Response) => {
            const jsonResponse = res.json();
            return jsonResponse;
        });
    }

    consultaProvs(depas: any): any {
        return this.http.get(`${this.resourceProv}/${depas}`).map((res: Response) => {
            const jsonResponse = res.json();
            return jsonResponse;
        });
    }

    consultaProv(depas: any, prov: any): any {
        return this.http.get(`${this.resourceProv}/${depas}/${prov}`).map((res: Response) => {
            const jsonResponse = res.json();
            return jsonResponse;
        });
    }

    consultaDist(depas: any, prov: any, dist: any): any {
        return this.http.get(`${this.resourceDist}/${depas}/${prov}/${dist}`).map((res: Response) => {
            const jsonResponse = res.json();
            return jsonResponse;
        });
    }

    consultaDists(depas: any, prov: any): any {
        return this.http.get(`${this.resourceDist}/${depas}/${prov}`).map((res: Response) => {
            const jsonResponse = res.json();
            return jsonResponse;
        });
    }

    regNuevoDenunciante(personaNatural: any): Observable<ResponseWrapper> {
        return this.http.post(this.resourceRegPernat, personaNatural).map((res: Response) => {
            const jsonResponse = res.json();
            return jsonResponse;
        });
    }

    regDireccionDenunciante(dirdenun: any): Observable<ResponseWrapper> {
        return this.http.post(this.resourceRegDirdenunciante, dirdenun).map((res: Response) => {
            const jsonResponse = res.json();
            return jsonResponse;
        });
    }

    consultaPersonaValidaServicio(personaNatural: any): Observable<Pernatural> {
        return this.http.post(this.resourcePersonaValidarServicio, personaNatural).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertPersonaFromServer(jsonResponse);
        });
    }

    consultaTVia(): Observable<ResponseWrapper> {
        return this.http.get(this.resourceTVia, null)
        .map((res: Response) => this.convertResponseTipvia(res));
    }

    consultaTZona(): Observable<ResponseWrapper> {
        return this.http.get(this.resourceTZona, null)
        .map((res: Response) => this.convertResponseTipzona(res));
    }

    login(credentials, callback?) {
        const cb = callback || function() {};

        return new Promise((resolve, reject) => {
            this.authServerProvider.login(credentials).subscribe((data) => {
                this.principal.identity(true).then((account) => {
                    // After the login the language will be changed to
                    // the language selected by the user during his registration
                    if (account !== null) {
                        this.languageService.changeLanguage(account.langKey);
                    }
                    resolve(data);
                });
                return cb();
            }, (err) => {
                this.logout();
                reject(err);
                return cb(err);
            });
        });
    }

    loginWithToken(jwt, rememberMe) {
        return this.authServerProvider.loginWithToken(jwt, rememberMe);
    }

    logout() {
        this.authServerProvider.logout().subscribe();
        this.principal.authenticate(null);
    }

    private convertResponseTipvia(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];

        for (let i = 0; i < jsonResponse.length; i++) {

            const cm: Tipvia = this.convertTViaFromServer(jsonResponse[i]);
            result.push(new ComboModel(cm.vDesccorta, cm.id.toString(), 0));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    private convertResponseTipzona(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];

        for (let i = 0; i < jsonResponse.length; i++) {

            const cm: Tipzona = this.convertTZonaFromServer(jsonResponse[i]);
            result.push(new ComboModel(cm.vDesccorta, cm.id.toString(), 0));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    private convertTipoDocFromServer(json: any): Tipdocident {
        const entity: Tipdocident = Object.assign(new Tipdocident(), json);
        return entity;
    }
    private convertPersonaFromServer(json: any): Pernatural {
        const entity: Pernatural = Object.assign(new Pernatural(), json);
        return entity;
    }

    private convertTViaFromServer(json: any): Tipvia {
        const entity: Tipvia = Object.assign(new Tipvia(), json);
        return entity;
    }

    private convertTZonaFromServer(json: any): Tipzona {
        const entity: Tipzona = Object.assign(new Tipzona(), json);
        return entity;
    }
    private convertResponseTipoDocIdentidad(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];

        for (let i = 0; i < jsonResponse.length; i++) {

            const cm: Tipdocident = this.convertTipoDocFromServer(jsonResponse[i]);
            result.push(new ComboModel(cm.vDescorta, cm.id.toString(), cm.nNumdigi));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }
}

import { BaseEntity } from '../shared';
export class AccountLog {
    constructor(
        public activated?: boolean,
        public authorities?: any,
        public email?: string,
        public firstName?: string,
        public lastName?: string,
        public login?: string,
        public menus?: any
    ) {
        this.login = '';
        this.email = '';
        this.firstName = '';
        this.lastName = '';
    }
}

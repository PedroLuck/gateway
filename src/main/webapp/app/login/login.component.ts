import { Component, AfterViewInit, Renderer, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { JhiEventManager } from 'ng-jhipster';

import { LoginService } from '../shared/login/login.service';
import { StateStorageService } from '../shared/auth/state-storage.service';
import { ComboModel } from '../applications/general/combobox.model';
import { Pernatural } from '../entities/pernatural';
import { Dirdenun } from '../entities/dirdenun';
import { Message } from 'primeng/components/common/api';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../shared';

@Component({
    selector: 'jhi-login',
    templateUrl: 'login.component.html',
    styleUrls: ['./login.component.css']
})

export class LoginComponent implements AfterViewInit {
    messages: Message[] = [];
    authenticationError: boolean;
    password: string;
    rememberMe: boolean;
    username: string;
    credentials: any;
    displayNuevoUsuario: boolean;
    tipodocs: ComboModel[];
    selectedTipodoc: ComboModel;
    pernatural: Pernatural;
    block: boolean;
    messageList: any;
    cambiaDir: boolean;
    departs: ComboModel[];
    provins: ComboModel[];
    distris: ComboModel[];
    tviasLista: ComboModel[];
    tzonasLista: ComboModel[];
    tviaSelected: ComboModel;
    tzonaSelected: ComboModel;
    dirdenunDirec: Dirdenun;
    selectedDeparts: ComboModel;
    selectedProvins: ComboModel;
    selectedDistris: ComboModel;
    messagesForm: Message[] = [];
    indexTab: number;
    disableTab1: boolean;
    disableTab2: boolean;

    constructor(
        private eventManager: JhiEventManager,
        private loginService: LoginService,
        private stateStorageService: StateStorageService,
        private elementRef: ElementRef,
        private renderer: Renderer,
        private router: Router,
    ) {
        this.pernatural = new Pernatural();
        this.displayNuevoUsuario = false;
        this.block = false;
        this.indexTab = 0;
        this.disableTab1 = false;
        this.disableTab2 = true;
        this.cambiaDir = false;
        this.pernatural = new Pernatural();
        this.dirdenunDirec = new Dirdenun();
        this.departs = [];
        this.provins = [];
        this.distris = [];
        this.tviasLista = [];
        this.tzonasLista = [];
    }

    ngAfterViewInit() {
        this.renderer.invokeElementMethod(this.elementRef.nativeElement.querySelector('#username'), 'focus', []);
    }

    login() {
        this.loginService.login({
            username: this.username,
            password: this.password,
            rememberMe: this.rememberMe
        }).then(() => {
            this.authenticationError = false;
            // this.activeModal.dismiss('login success');
            this.router.navigate(['/']);
            if (this.router.url === '/register' || (/^\/activate\//.test(this.router.url)) ||
                (/^\/reset\//.test(this.router.url))) {
                this.router.navigate(['']);
            }

            this.eventManager.broadcast({
                name: 'authenticationSuccess',
                content: 'Sending Authentication Success'
            });

            // // previousState was set in the authExpiredInterceptor before being redirected to login modal.
            // // since login is succesful, go to stored previousState and clear previousState
            const redirect = this.stateStorageService.getUrl();
            if (redirect) {
                this.stateStorageService.storeUrl(null);
                this.router.navigate([redirect]);
            }
        }).catch(() => {
            this.authenticationError = true;
            this.onError({ message: 'Usuario Incorrecto.'});
        });
    }

    submitNuevoUsuario() {
    }

    validarDatoPersonaDireccion() {
        this.block = true;
        this.messageList = [];
        if (this.cambiaDir === true && this.tviaSelected === undefined) {
            this.messageList.push({ severity: 'error', summary: 'Mensaje de Error', detail: 'Debe seleccionar un tipo de via' });
            this.block = false;
        } else if (this.cambiaDir === true && (this.dirdenunDirec.vDesvia === '' || this.dirdenunDirec.vDesvia === undefined)) {
            this.messageList.push({ severity: 'error', summary: 'Mensaje de Error', detail: 'Debe ingresar la descripción de la via.' });
            this.block = false;
        } else if (this.cambiaDir === true && this.tzonaSelected === undefined) {
            this.messageList.push({ severity: 'error', summary: 'Mensaje de Error', detail: 'Debe seleccionar un tipo de zona' });
            this.block = false;
        } else if (this.cambiaDir === true && (this.dirdenunDirec.vDeszona === '' || this.dirdenunDirec.vDesvia === undefined)) {
            this.messageList.push({ severity: 'error', summary: 'Mensaje de Error', detail: 'Debe ingresar la descripción de la zona.' });
            this.block = false;
        } else if (this.cambiaDir === true && (this.dirdenunDirec.vDireccion === '' || this.dirdenunDirec.vDesvia === undefined)) {
            this.messageList.push({ severity: 'error', summary: 'Mensaje de Error', detail: 'Debe ingresar su dirección completa' });
            this.block = false;
        } else if (this.cambiaDir === true && this.selectedDeparts === undefined) {
            this.messageList.push({ severity: 'error', summary: 'Mensaje de Error', detail: 'Debe seleccionar el departamento.' });
            this.block = false;
        } else if (this.cambiaDir === true && this.selectedProvins === undefined) {
            this.messageList.push({ severity: 'error', summary: 'Mensaje de Error', detail: 'Debe seleccionar la provincia' });
            this.block = false;
        } else if (this.cambiaDir === true && this.selectedDistris === undefined) {
            this.messageList.push({ severity: 'error', summary: 'Mensaje de Error', detail: 'Debe seleccionar el distrito' });
            this.block = false;
        } else {
            this.pernatural.fecNacimiento = this.pernatural.dFecnac;
            this.pernatural.dFecnac = new Date(this.pernatural.dFecnac.substring(0, 4), (this.pernatural.dFecnac.substring(4, 6)) - 1, this.pernatural.dFecnac.substring(6, 8));
            this.pernatural.vEstado = '1';
            this.pernatural.nSedereg = 1;
            this.pernatural.nUsuareg = 1;
            this.pernatural.tFecreg = 0;
            this.loginService.regNuevoDenunciante(this.pernatural).subscribe(
                (resp: any) => {
                    if (resp.resultado === true) {
                        this.onError({ message : resp.mensaje });
                        this.dirdenunDirec.nCoddenute = resp.id;
                    // , this.pernatural.codpro, this.pernatural.coddist
                    if (this.cambiaDir === true) {
                        this.dirdenunDirec.nSedereg = 1;
                        this.dirdenunDirec.vUsuareg = '1';
                        this.dirdenunDirec.tFecreg = 0;
                        this.dirdenunDirec.vCoddepart = this.selectedDeparts.value;
                        this.dirdenunDirec.vCodprovin = this.selectedProvins.value;
                        this.dirdenunDirec.vCoddistri = this.selectedDistris.value;
                        this.dirdenunDirec.nCodtipvia = Number(this.tviaSelected.value);
                        this.dirdenunDirec.nCodtzona = Number(this.tzonaSelected.value);
                        this.dirdenunDirec.vDircomple = this.dirdenunDirec.vDireccion;
                    } else {
                        this.dirdenunDirec.nSedereg = 1;
                        this.dirdenunDirec.vUsuareg = '1';
                        this.dirdenunDirec.tFecreg = 0;
                        this.dirdenunDirec.vCoddepart = this.pernatural.coddep;
                        this.dirdenunDirec.vCodprovin = this.pernatural.codpro;
                        this.dirdenunDirec.vCoddistri = this.pernatural.coddist;
                        this.dirdenunDirec.nCodtipvia = 21;
                        this.dirdenunDirec.nCodtzona = 12;
                        this.dirdenunDirec.vDesvia = '';
                        this.dirdenunDirec.vDeszona = '';
                        this.dirdenunDirec.vDireccion = this.pernatural.direccion;
                        this.dirdenunDirec.vDircomple = this.pernatural.direccion;
                    }
                    this.loginService.regDireccionDenunciante(this.dirdenunDirec).subscribe(
                        (respDir: any) => {
                            console.log(respDir);
                            this.displayNuevoUsuario = false;
                            this.block = false;
                        },
                        (respDir: ResponseWrapper) => {
                            console.log(respDir);
                            this.block = false;
                        });
                    } else {
                        this.onError({ message : resp.mensaje });
                    }
                },
                (resp: ResponseWrapper) => {
                    console.log(resp);
                    this.block = false;
                });
        }
        this.onErrorMultiple(this.messageList);
    }
    retrocederVentada() {
        this.indexTab = 0;
        this.disableTab1 = false;
        this.disableTab2 = true;
    }

    avanzarVentana() {
        this.indexTab = 1;
        this.disableTab1 = true;
        this.disableTab2 = false;
    }
    abrirModal() {
        this.loadAll();
        this.displayNuevoUsuario = true;
    }
    loadAll() {
        this.block = true;
        this.loginService.consultaTipoDocIdentidad().subscribe(
            (res: ResponseWrapper) => {
                this.tipodocs = res.json;
                this.block = false;
            },
            (res: ResponseWrapper) => { this.onError(res.json); this.block = false; }
        );
    }

    onChangeDepartamento() {
        this.block = true;
        this.messageList = [];
        if (this.selectedDeparts === undefined) {
            this.onErrorMultiple([{ severity: 'error', summary: 'Mensaje de Error', detail: 'Debe seleccionar un departamento' }]);
            this.block = false;
        } else {
            this.loginService.consultaProvs(this.selectedDeparts.value).subscribe(
                (tprovs: ResponseWrapper) => {
                    this.provins = [];
                    // tslint:disable-next-line:forin
                    for (const i in tprovs) {
                        this.provins.push(new ComboModel(tprovs[i].vDespro, tprovs[i].vCodpro, 0));
                    }
                    this.block = false;
                },
                (tprovs: ResponseWrapper) => { this.onErrorMultiple([{ severity: 'error', summary: 'Mensaje de Error', detail: tprovs.json }]); this.block = false; }
            );
        }
    }

    onChangeProvincia() {
        this.block = true;
        this.messageList = [];
        if (this.selectedDeparts === undefined) {
            this.onErrorMultiple([{ severity: 'error', summary: 'Mensaje de Error', detail: 'Debe seleccionar un departamento' }]);
            this.block = false;
        } else if (this.selectedProvins === undefined) {
            this.onErrorMultiple([{ severity: 'error', summary: 'Mensaje de Error', detail: 'Debe seleccionar una provincia' }]);
            this.block = false;
        } else {
            this.loginService.consultaDists(this.selectedDeparts.value, this.selectedProvins.value).subscribe(
                (tdists: ResponseWrapper) => {
                    this.distris = [];
                    // tslint:disable-next-line:forin
                    for (const i in tdists) {
                        this.distris.push(new ComboModel(tdists[i].vDesdis, tdists[i].vCoddis, 0));
                    }
                    this.block = false;
                },
                (tdists: ResponseWrapper) => { this.onErrorMultiple([{ severity: 'error', summary: 'Mensaje de Error', detail: tdists.json }]); this.block = false; }
            );
        }
    }

    validarDatoPersona() {
        this.messageList = [];
        if (this.selectedTipodoc === undefined) {
            this.messageList.push({ severity: 'error', summary: 'Mensaje de Error', detail: 'Debe seleccionar un tipo de documento.' });
        } else if (this.pernatural.vNumdoc === '') {
            this.messageList.push({ severity: 'error', summary: 'Mensaje de Error', detail: 'Debe ingresar el número de documento.' });
        } else if (this.selectedTipodoc !== undefined && !(this.selectedTipodoc.totaldig === this.pernatural.vNumdoc.trim().length)) {
            this.messageList.push({ severity: 'error', summary: 'Mensaje de Error', detail: 'Debe ingresar un numero de documento valido.' });
        } else if (this.pernatural.vApepat === '') {
            this.messageList.push({ severity: 'error', summary: 'Mensaje de Error', detail: 'Debe ingresar el apellido paterno.' });
        } else if (this.pernatural.vApemat === '') {
            this.messageList.push({ severity: 'error', summary: 'Mensaje de Error', detail: 'Debe ingresar el apellido materno.' });
        } else if (this.pernatural.vNombres === '') {
            this.messageList.push({ severity: 'error', summary: 'Mensaje de Error', detail: 'Debe ingresar sus nombres.' });
        } else if (this.pernatural.vEmailper === '') {
            this.messageList.push({ severity: 'error', summary: 'Mensaje de Error', detail: 'Debe ingresar su correo electronico.' });
        } else if (this.pernatural.vTelefono === '') {
            this.messageList.push({ severity: 'error', summary: 'Mensaje de Error', detail: 'Debe ingresar su telefono.' });
        } else if (this.pernatural.vCelular === '') {
            this.messageList.push({ severity: 'error', summary: 'Mensaje de Error', detail: 'Debe ingresar su celular.' });
        } else {
            console.log(this.pernatural.vApepat);
            console.log(this.pernatural.vApemat);
            console.log(this.pernatural.vNombres);
            if (this.pernatural.vApepat === undefined || this.pernatural.vApepat.trim() === '') {
                this.messageList.push({ severity: 'error', summary: 'Mensaje de Error', detail: 'Debe ingresar su apellido paterno' });
                this.block = false;
            } else if (this.pernatural.vApemat === undefined || this.pernatural.vApemat.trim() === '') {
                this.messageList.push({ severity: 'error', summary: 'Mensaje de Error', detail: 'Debe ingresar su apellido materno' });
                this.block = false;
            } else if (this.pernatural.vNombres === undefined || this.pernatural.vNombres.trim() === '') {
                this.messageList.push({ severity: 'error', summary: 'Mensaje de Error', detail: 'Debe ingresar su nombre completo' });
                this.block = false;
            } else {
                this.block = true;
                this.messageList = [];
                this.loginService.consultaPersonaValidaServicio(
                    {
                        TipoDoc: this.selectedTipodoc.name,
                        vApepat: this.pernatural.vApepat,
                        vApemat: this.pernatural.vApemat,
                        vNombres: this.pernatural.vNombres,
                        vNumdoc: this.pernatural.vNumdoc,
                        vTelefono: this.pernatural.vTelefono,
                        vCelular: this.pernatural.vCelular,
                        vEmailper: this.pernatural.vEmailper
                    }).subscribe(
                    (res: Pernatural) => {
                        this.pernatural = res;
                        if (this.pernatural.Resultado) {
                            this.loginService.consultaDepa(this.pernatural.coddep).subscribe(
                                (dep: any) => {
                                    this.loginService.consultaProv(this.pernatural.coddep, this.pernatural.codpro).subscribe(
                                        (prov: any) => {
                                            this.loginService.consultaDist(this.pernatural.coddep, this.pernatural.codpro, this.pernatural.coddist).subscribe(
                                                (dist: any) => {
                                                    this.dirdenunDirec.vCoddepartDes = (dep.length > 0) ? dep[0].vDesdep : '';
                                                    this.dirdenunDirec.vCodprovinDes = (prov.length > 0) ? prov[0].vDespro : '';
                                                    this.dirdenunDirec.vCoddistriDes = (dist.length > 0) ? dist[0].vDesdis : '';
                                                    this.block = false;
                                                    this.avanzarVentana();
                                                },
                                                (dist: ResponseWrapper) => {
                                                    this.onErrorMultiple([{ severity: 'error', summary: 'Mensaje de Error', detail: prov }]);
                                                    this.block = false;
                                                });
                                        },
                                        (prov: ResponseWrapper) => {
                                            this.onErrorMultiple([{
                                                severity: 'error',
                                                summary: 'Mensaje de Error', detail: prov
                                            }]); this.block = false;
                                        });
                                },
                                (dep: ResponseWrapper) => this.onErrorMultiple([{ severity: 'error', summary: 'Mensaje de Error', detail: res }]));
                        } else {
                            this.onErrorMultiple([{
                                severity: 'error', summary: 'Mensaje de Error',
                                detail: 'Los datos del documento de identidad no corresponden a los ingresados.'
                            }])
                            this.block = false;
                        }
                    },
                    (res: ResponseWrapper) => { this.onErrorMultiple([{ severity: 'error', summary: 'Mensaje de Error', detail: res.json }]); this.block = false; }
                    );
            }
        }
        this.onErrorMultiple(this.messageList);
    }

    cambiaDireccion() {
        this.block = true;
        this.messageList = [];
        this.loginService.consultaDepas().subscribe(
            (deps: any) => {
                this.loginService.consultaTVia().subscribe(
                    (tvias: ResponseWrapper) => {
                        this.loginService.consultaTZona().subscribe(
                            (tzonas: ResponseWrapper) => {
                                // tslint:disable-next-line:forin
                                for (const i in deps) {
                                    this.departs.push(new ComboModel(deps[i].vDesdep, deps[i].vCoddep, 0));
                                }
                                this.tviasLista = tvias.json;
                                this.tzonasLista = tzonas.json;
                                this.cambiaDir = true;
                                this.block = false;
                            },
                            (tzonas: ResponseWrapper) => { this.onErrorMultiple([{ severity: 'error', summary: 'Mensaje de Error', detail: tzonas.json }]); this.block = false; }
                        );
                    },
                    (tvias: ResponseWrapper) => { this.onErrorMultiple([{ severity: 'error', summary: 'Mensaje de Error', detail: tvias.json }]); this.block = false; }
                );
            },
            (res: ResponseWrapper) => { this.onErrorMultiple([{ severity: 'error', summary: 'Mensaje de Error', detail: res.json }]); this.block = false; }
        );
    }

    private onErrorMultiple(errorList: any) {
        for (let i = 0; i < errorList.length; i++) {
            this.messagesForm.push(errorList[i]);
        }
    }
    private onError(error: any) {
        this.messages = [];
        this.messages.push({ severity: 'error', summary: 'Mensaje de Error', detail: error.message });
    }
}

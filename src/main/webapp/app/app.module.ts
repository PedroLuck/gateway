import './vendor.ts';

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Ng2Webstorage } from 'ng2-webstorage';
import { RouterModule } from '@angular/router';

import { GatewaySharedModule, UserRouteAccessService } from './shared';
import { GatewayHomeModule } from './home/home.module';
import { GatewayAccountModule } from './account/account.module';
import { LoginComponent } from './login/login.component';

import { customHttpProvider } from './blocks/interceptor/http.provider';
import { PaginationConfig } from './blocks/config/uib-pagination.config';

import {
    adminRoute,
    consultasRoute,
    patrocinioRoute,
    defensaRoute,
    denunciasRoute,
    entityRoute,
    liquidacionesRoute,
    loginRoute,
    sindicatosRoute,
    conciliacionesRoute,
    retornoRoute,
    dictamenRoute
} from './app.routing';
// import { GatewayprimengModule } from './primeng/primeng.module';

import { GatewayTipoEntidadModule } from './entities/tipo-entidad/tipo-entidad.module';
import { GatewayUsuarioHorarioModule } from './entities/usuario-horario/usuario-horario.module';
import { GatewayMenuModule } from './entities/menu/menu.module';
import { GatewayGrupoModule } from './entities/grupo/grupo.module';
import { GatewayEntidadModule } from './entities/entidad/entidad.module';
import { GatewayAplicacionModule } from './entities/aplicacion/aplicacion.module';
import { GatewayModuloModule } from './entities/modulo/modulo.module';
import { GatewayUsuarioGrupoModule } from './entities/usuario-grupo/usuario-grupo.module';
import { GatewayUsuarioModule } from './entities/usuario/usuario.module';
import { GatewayUsuPerModule } from './entities/usu-per/usu-per.module';
import { GatewayTipoUsuarioModule } from './entities/tipo-usuario/tipo-usuario.module';
import { GatewayPermisoModule } from './entities/permiso/permiso.module';
import { GatewayPerfilModule } from './entities/perfil/perfil.module';
import { GatewayListadoSolicitudesModule } from './applications/dictamenes/listado-solicitudes/listado-solicitudes.module';
import { GatewayRegistroSolicitudesModule } from './applications/dictamenes/registro-solicitudes/registro-solicitudes.module';
import { PanelModule } from 'primeng/primeng';
import { TabViewModule } from 'primeng/primeng';
import { DropdownModule } from 'primeng/primeng';
import {BlockUIModule} from 'primeng/primeng';
import {ButtonModule} from 'primeng/primeng';
import {MessagesModule} from 'primeng/primeng';
import {MessageModule} from 'primeng/primeng';
import {DialogModule, GrowlModule} from 'primeng/primeng';
import {MessageService} from 'primeng/components/common/messageservice';
// jhipster-needle-angular-add-module-import JHipster will add new module here

import {
    JhiMainComponent,
    LayoutRoutingModule,
    NavbarComponent,
    LeftbarComponent,
    TopbarComponent,
    FooterComponent,
    ProfileService,
    PageRibbonComponent,
    ActiveMenuDirective,
    ErrorComponent
} from './layouts';
import { GatewayAsignacionSolicitudesModule } from './applications/dictamenes/asignacion-solicitudes/asignacion-solicitudes.module';
import { GatewayRevisionSolicitudesModule } from './applications/dictamenes/revision-solicitudes/revision-solicitudes.module';
import { GatewayAnalisisSolicitudesModule } from './applications/dictamenes/analisis-solicitudes/analisis-solicitudes.module';
import { GatewayMentidadModule } from './entities/mentidad/mentidad.module';
import { GatewayPagosNacionModule } from './applications/dictamenes/entities/entities/pagosnacion/pagosnacion.module';
import { GatewayTipdocidentModule } from './entities/tipdocident/tipdocident.module';
import { MantenimientoModule } from './applications/conciliaciones/mantenimientos/mantenimiento.module';
import { MantenimientoModule as MantDEF } from './applications/defensa/mantenimientos/mantenimiento.module';
import { GatewayOrganizacioModule } from './applications/dictamenes/entities/entities/organizacio/organizacio.module';
import { GatewayUsusolModule } from './applications/dictamenes/entities/entities/ususol/ususol.module';
import { GatewayPernaturalModule } from './applications/dictamenes/entities/entities/pernatural/pernatural.module';
import { GatewayReglaboralModule } from './applications/dictamenes/entities/entities/reglaboral/reglaboral.module';
import { GatewayPerreglabModule } from './applications/dictamenes/entities/entities/perreglab/perreglab.module';
import { GatewayUsuarioModule as UsuarioMod } from './applications/dictamenes/entities/entities/usuario/usuario.module';

const LAZY_ROUTES = [
    adminRoute,
    entityRoute,
    loginRoute,
    sindicatosRoute,
    consultasRoute,
    conciliacionesRoute,
    liquidacionesRoute,
    defensaRoute,
    patrocinioRoute,
    denunciasRoute,
    retornoRoute,
    dictamenRoute
];

@NgModule({
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        LayoutRoutingModule,
        Ng2Webstorage.forRoot({ prefix: 'jhi', separator: '-'}),
        RouterModule.forRoot(LAZY_ROUTES, { useHash: true }),
        GatewaySharedModule,
        GatewayHomeModule,
        GatewayAccountModule,
        GatewayAplicacionModule,
        GatewayMentidadModule,
        GatewayEntidadModule,
        GatewayTipdocidentModule,
        GatewayGrupoModule,
        GatewayMenuModule,
        GatewayModuloModule,
        GatewayUsuarioHorarioModule,
        GatewayUsuarioGrupoModule,
        GatewayUsuarioModule,
        GatewayUsuPerModule,
        GatewayTipoUsuarioModule,
        GatewayTipoEntidadModule,
        GatewayPermisoModule,
        GatewayPerfilModule,
        GatewayPagosNacionModule,
        GatewayListadoSolicitudesModule,
        GatewayRegistroSolicitudesModule,
        GatewayAsignacionSolicitudesModule,
        GatewayRevisionSolicitudesModule,
        GatewayAnalisisSolicitudesModule,
        MessagesModule,
        MessageModule,
        DialogModule,
        PanelModule,
        TabViewModule,
        DropdownModule,
        BlockUIModule,
        ButtonModule,
        GrowlModule,
        MantenimientoModule,
        MantDEF,
        GatewayOrganizacioModule,
        GatewayUsusolModule,
        GatewayPernaturalModule,
        GatewayReglaboralModule,
        GatewayPerreglabModule,
        UsuarioMod

        // GatewayprimengModule,
        // jhipster-needle-angular-add-module JHipster will add new module here
    ],
    declarations: [
        JhiMainComponent,
        LoginComponent,
        NavbarComponent,
        LeftbarComponent,
        TopbarComponent,
        ErrorComponent,
        PageRibbonComponent,
        ActiveMenuDirective,
        FooterComponent
    ],
    providers: [
        ProfileService,
        customHttpProvider(),
        PaginationConfig,
        UserRouteAccessService
    ],
    bootstrap: [ JhiMainComponent ]
})
export class GatewayAppModule {}

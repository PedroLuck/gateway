import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs/Rx';

import { ProfileService } from '../profiles/profile.service';
import { JhiLanguageService, JhiEventManager } from 'ng-jhipster';
import { Principal, LoginModalService, LoginService } from '../../shared';
import { VERSION, DEBUG_INFO_ENABLED } from '../../app.constants';
import { AccountLog } from '../../account/account.model';

@Component({
    selector: 'jhi-leftbar',
    templateUrl: './leftbar.component.html',
    styleUrls: [
        'leftbar.scss'
    ]
})
export class LeftbarComponent implements OnInit {
    account: AccountLog;
    inProduction: boolean;
    isNavbarCollapsed: boolean;
    languages: any[];
    eventSubscriber: Subscription;
    swaggerEnabled: boolean;
    modalRef: NgbModalRef;
    version: string;
    aplicacion: string;
    accordionDefensa = 'expediente';
    accordionConciliaciones = 'expediente';
    accordionPatrocinio = 'legajo';
    menuConciliaciones = ['expediente', 'audiencia', 'reportes', 'mantenimiento']
    menuDefensa = ['expediente', 'audiencia', 'reportes']
    menuPatrocinio = ['consulta', 'legajo', 'reportes', 'mantenimiento', 'atencion']
    private modules = ['consultas', 'defensa', 'conciliaciones', 'liquidaciones', 'sindicatos', 'dictamenes', 'denuncias', 'seguridad', 'patrocinio'];
    accordionDictamen = 'dictamen';
    menuDictamen = ['dictamen'];
    modulesMenu: { id: string, nombre: string, ruta: any, idPadre: string, nivel: number, tipo: number, rutaC: string }[] = [];
    moduleHijos: { id: string, nombre: string, ruta: any, idPadre: string, nivel: number, tipo: number, rutaC: string }[] = [];
    codModPadre: string;
    constructor(
        private loginService: LoginService,
        private principal: Principal,
        private loginModalService: LoginModalService,
        private profileService: ProfileService,
        private router: Router,
        private route: ActivatedRoute,
        private eventManager: JhiEventManager,
    ) {
        this.version = VERSION ? 'v' + VERSION : '';
        this.isNavbarCollapsed = true;
        this.getRoute(router.url);
    }

    changeRoute(cod: string) {
        // tslint:disable-next-line:no-debugger
        debugger;
        // tslint:disable-next-line:forin
        for (const i in this.modulesMenu) {
            if (this.modulesMenu[i].id === cod) {
                if (this.modulesMenu[i].tipo === 1) {
                    this.router.navigate([this.modulesMenu[i].ruta[0]]);
                } else if (this.modulesMenu[i].tipo === 2) {
                    this.router.navigate([this.modulesMenu[i].ruta[0], { outlets: { 'wizard': [this.modulesMenu[i].rutaC] } }]);
                }
            }
        }
    }

    getRoute(url) {
        // tslint:disable-next-line:no-debugger
        debugger;
        url = url.split('/')[1];
        let codPadre: string;
        codPadre = '';
        if (this.modulesMenu.length === 0) {
            this.principal.identity().then((account) => {
                this.account = account;
                // tslint:disable-next-line:no-debugger
                debugger;
                // tslint:disable-next-line:forin
                for (const i in this.account.menus) {
                    if (this.account.menus[i].url.split('/')[1] === url && this.account.menus[i].nivel === 1) {
                        codPadre = this.account.menus[i].cod;
                    }

                    if (this.account.menus[i].url.split('*').length === 1) {
                        this.modulesMenu.push({
                            'id': this.account.menus[i].cod,
                            'nombre': this.account.menus[i].descripcion,
                            'ruta': [this.account.menus[i].url],
                            'idPadre': this.account.menus[i].codPadre,
                            'nivel': this.account.menus[i].nivel,
                            'tipo': 1,
                            'rutaC': ''
                        });
                    } else if (this.account.menus[i].url.split('*').length > 1) {
                        this.modulesMenu.push({
                            'id': this.account.menus[i].cod,
                            'nombre': this.account.menus[i].descripcion,
                            'ruta': [this.account.menus[i].url.split('*')[0]],
                            'idPadre': this.account.menus[i].codPadre,
                            'nivel': this.account.menus[i].nivel,
                            'tipo': 2,
                            'rutaC': this.account.menus[i].url.split('*')[1]
                        });
                    }
                }
                this.pintarMenu(codPadre);
                /*if (url === '/') {
                    this.aplicacion = 'seguridad';
                } else if (url.indexOf(module) === 1) {
                    if (module === 'denuncias') {
                        this.menuPatrocinioActive(url);
                    }
                    if (module === 'defensa') {
                        this.menuDefensaActive(url);
                    }
                    if (module === 'dictamenes') {
                        this.menuDictamenActive(url);
                    }
                    this.aplicacion = module;
                }*/
            });
        } else {
            for (const module of this.modulesMenu) {
                if (module.ruta[0].split('/')[1] === url && module.nivel === 1) {
                    codPadre = module.id;
                }
            }
            this.pintarMenu(codPadre);
        }
    }

    private pintarMenu(id: string) {
        this.moduleHijos = [];
        if (id !== '') {
            for (const module of this.modulesMenu) {
                if (module.idPadre === id) {
                    this.moduleHijos.push(module);
                }
            }
        }
    }

    ngOnInit() {
        // tslint:disable-next-line:no-debugger
        debugger;
        this.account = new AccountLog();
        this.profileService.getProfileInfo().subscribe((profileInfo) => {
            this.inProduction = profileInfo.inProduction;
            this.swaggerEnabled = profileInfo.swaggerEnabled;
        });
        /*
        this.principal.identity().then((account) => {
            this.account = account;
            // tslint:disable-next-line:forin
            for (const i in this.account.menus) {
                if (this.modulesMenu.length === 0) {
                    this.modulesMenu.push({
                        'id': this.account.menus[i].cod,
                        'nombre': this.account.menus[i].descripcion,
                        'ruta': [this.account.menus[i].url],
                        'idPadre': this.account.menus[i].codPadre,
                        'nivel': this.account.menus[i].nivel
                    });
                } else {
                    for (const module of this.modulesMenu) {
                        if (this.account.menus[i].id !== module.id) {
                            this.modulesMenu.push({
                                'id': this.account.menus[i].cod,
                                'nombre': this.account.menus[i].descripcion,
                                'ruta': [this.account.menus[i].url],
                                'idPadre': this.account.menus[i].codPadre,
                                'nivel': this.account.menus[i].nivel
                            });
                        }
                    }
                }
            }
        });
        */
        this.registerChangeInRoutes();
    }

    registerChangeInRoutes() {
        this.eventSubscriber = this.eventManager.subscribe('changeRoute', (response) => this.getRoute(this.router.url));
    }

    collapseNavbar() {
        this.isNavbarCollapsed = true;
    }

    isAuthenticated() {
        return this.principal.isAuthenticated();
    }

    toggleNavbar() {
        this.isNavbarCollapsed = !this.isNavbarCollapsed;
    }
    menuDefensaActive(url) {
        for (const menu of this.menuDefensa) {
            if (url.indexOf(menu) !== -1) {
                this.accordionDefensa = menu;
            }
        }
    }

    menuPatrocinioActive(url) {
        for (const menu of this.menuPatrocinio) {
            if (url.indexOf(menu) !== -1) {
                this.accordionPatrocinio = menu;
            }
        }
    }

    menuDictamenActive(url) {
        for (const menu of this.menuDictamen) {
            if (url.indexOf(menu) !== -1) {
                this.accordionDictamen = menu;
            }
        }
    }
}

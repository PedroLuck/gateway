package pe.gob.trabajo.web.rest;

import com.codahale.metrics.annotation.Timed;
import pe.gob.trabajo.domain.PagosNacion;

import pe.gob.trabajo.repository.PagosNacionRepository;
import pe.gob.trabajo.repository.search.PagosNacionSearchRepository;
import pe.gob.trabajo.web.rest.errors.BadRequestAlertException;
import pe.gob.trabajo.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing PagosNacion.
 */
@RestController
@RequestMapping("/api")
public class PagosNacionResource {

    private final Logger log = LoggerFactory.getLogger(PagosNacionResource.class);

    private static final String ENTITY_NAME = "pagosnacion";

    private final PagosNacionRepository pagosnacionRepository;

    private final PagosNacionSearchRepository pagosnacionSearchRepository;

    public PagosNacionResource(PagosNacionRepository pagosnacionRepository, PagosNacionSearchRepository pagosnacionSearchRepository) {
        this.pagosnacionRepository = pagosnacionRepository;
        this.pagosnacionSearchRepository = pagosnacionSearchRepository;
    }

    /**
     * POST  /pagosnacions : Create a new pagosnacion.
     *
     * @param pagosnacion the pagosnacion to create
     * @return the ResponseEntity with status 201 (Created) and with body the new pagosnacion, or with status 400 (Bad Request) if the pagosnacion has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/pagosnacions")
    @Timed
    public ResponseEntity<PagosNacion> createPagosNacion(@Valid @RequestBody PagosNacion pagosnacion) throws URISyntaxException {
        log.debug("REST request to save PagosNacion : {}", pagosnacion);
        PagosNacion result = pagosnacionRepository.save(pagosnacion);
        pagosnacionSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/pagosnacions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /pagosnacions : Updates an existing pagosnacion.
     *
     * @param pagosnacion the pagosnacion to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated pagosnacion,
     * or with status 400 (Bad Request) if the pagosnacion is not valid,
     * or with status 500 (Internal Server Error) if the pagosnacion couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/pagosnacions")
    @Timed
    public ResponseEntity<PagosNacion> updatePagosNacion(@Valid @RequestBody PagosNacion pagosnacion) throws URISyntaxException {
        log.debug("REST request to update PagosNacion : {}", pagosnacion);
        PagosNacion result = pagosnacionRepository.save(pagosnacion);
        pagosnacionSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, pagosnacion.getId().toString()))
            .body(result);
    }

    /**
     * GET  /pagosnacions : get all the pagosnacions.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of pagosnacions in body
     */
    @GetMapping("/pagosnacions")
    @Timed
    public List<PagosNacion> getAllPagosNacions() {
        log.debug("REST request to get all PagosNacions");
        return pagosnacionRepository.findAll();
        }

    /**
     * GET  /pagosnacions/:id : get the "id" pagosnacion.
     *
     * @param id the id of the pagosnacion to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the pagosnacion, or with status 404 (Not Found)
     */
    @GetMapping("/pagosnacions/{id}")
    @Timed
    public ResponseEntity<PagosNacion> getPagosNacion(@PathVariable Integer id) {
        log.debug("REST request to get PagosNacion : {}", id);
        PagosNacion pagosnacion = pagosnacionRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(pagosnacion));
    }

    /**
     * DELETE  /pagosnacions/:id : delete the "id" pagosnacion.
     *
     * @param id the id of the pagosnacion to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/pagosnacions/{id}")
    @Timed
    public ResponseEntity<Void> deletePagosNacion(@PathVariable Integer id) {
        log.debug("REST request to delete PagosNacion : {}", id);
        pagosnacionRepository.delete(id);
        pagosnacionSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/pagosnacions?query=:query : search for the pagosnacion corresponding
     * to the query.
     *
     * @param query the query of the pagosnacion search
     * @return the result of the search
     */
    @GetMapping("/_search/pagosnacions")
    @Timed
    public List<PagosNacion> searchPagosNacions(@RequestParam String query) {
        log.debug("REST request to search PagosNacions for query {}", query);
        return StreamSupport
            .stream(pagosnacionSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

    /**
     * GET  /listarUnidadNegocio : get all the pagosnacions.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of listarUnidadNegocio in body
     */
    @GetMapping("/verificarPago")
    @Timed
    public ResponseEntity<Integer> verificarPago(@RequestParam  String vcodcomp,
                                 @RequestParam String vcodofc,
                                 @RequestParam  String vfeccomp,
                                 @RequestParam String vnumdoc,
                                 @RequestParam  String nimpcomp,
                                 @RequestParam  String vcodtrib,
                                 @RequestParam String vcodcaj) {
        log.debug("REST verificar Pago al Banco de la nacion");
        Integer resultado = 0;
        Object obj = pagosnacionRepository.verificarPago(vcodcomp,vcodofc,vfeccomp,vnumdoc,Double.parseDouble(nimpcomp),vcodtrib,vcodcaj);
        if(obj != null) {
            resultado = Integer.parseInt(String.valueOf(obj));
        }
        return ResponseEntity.ok().body(resultado);
    }

    /**
     * GET  /eliminar : listar los anexos laborales por formulario de perfil.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of eliminar in body
     */
    /*@GetMapping("/eliminarUnidad")
    @Timed
    @Transactional
    public Integer eliminar(@RequestParam Integer codFormPerfil) {
        log.debug("REST eliminar");
        return pagosnacionRepository.eliminar(codFormPerfil);
    }*/

}

package pe.gob.trabajo.web.rest;

import com.codahale.metrics.annotation.Timed;

import pe.gob.trabajo.domain.Perfil;
import pe.gob.trabajo.domain.Pernatural;
import pe.gob.trabajo.domain.SisMenu;
import pe.gob.trabajo.domain.SisMenuxperfil;
import pe.gob.trabajo.domain.SisPerfil;
import pe.gob.trabajo.domain.SisPersonal;
import pe.gob.trabajo.domain.SisUsuario;
import pe.gob.trabajo.domain.SisUsuxperfil;
import pe.gob.trabajo.domain.User;
import pe.gob.trabajo.domain.Usuario;
import pe.gob.trabajo.domain.Usuperfil;
import pe.gob.trabajo.repository.UserRepository;
import pe.gob.trabajo.repository.UsuarioRepository;
import pe.gob.trabajo.repository.UsuperfilRepository;
import pe.gob.trabajo.repository.PerfilRepository;
import pe.gob.trabajo.repository.PernaturalRepository;
import pe.gob.trabajo.security.SecurityUtils;
import pe.gob.trabajo.service.MailService;
import pe.gob.trabajo.service.MenuDTO;
import pe.gob.trabajo.service.UserService;
import pe.gob.trabajo.service.dto.UserDTO;
import pe.gob.trabajo.web.rest.errors.*;
import pe.gob.trabajo.web.rest.vm.KeyAndPasswordVM;
import pe.gob.trabajo.web.rest.vm.ManagedUserVM;
import pe.gob.trabajo.repository.SisMenuRepository;
import pe.gob.trabajo.repository.SisMenuxperfilRepository;
import pe.gob.trabajo.repository.SisPerfilRepository;
import pe.gob.trabajo.repository.SisPersonalRepository;
import pe.gob.trabajo.repository.SisUsuarioRepository;
import pe.gob.trabajo.repository.SisUsuxperfilRepository;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

/**
* REST controller for managing the current user's account.
*/
@RestController
@RequestMapping("/api")
public class AccountResource {

    private final Logger log = LoggerFactory.getLogger(AccountResource.class);

    private final UserRepository userRepository;

    private final SisUsuarioRepository sisUsuarioRepository;
    private final SisPersonalRepository sisPersonalRepository;
    private final SisPerfilRepository sisPerfilRepository;
    private final SisUsuxperfilRepository sisUsuxperfilRepository;
    private final SisMenuxperfilRepository sisMenuxperfilRepository;
    private final SisMenuRepository sisMenuRepository;
    private final String codSis = "084";

    private final UserService userService;

    private final MailService mailService;

    public AccountResource(UserRepository userRepository, UserService userService, MailService mailService,
            SisUsuarioRepository sisUsuarioRepository, SisPersonalRepository sisPersonalRepository,
            SisPerfilRepository sisPerfilRepository, SisUsuxperfilRepository sisUsuxperfilRepository,
            SisMenuxperfilRepository sisMenuxperfilRepository, SisMenuRepository sisMenuRepository) {
        this.sisUsuarioRepository = sisUsuarioRepository;
        this.sisPersonalRepository = sisPersonalRepository;
        this.sisPerfilRepository = sisPerfilRepository;
        this.sisUsuxperfilRepository = sisUsuxperfilRepository;
        this.sisMenuxperfilRepository = sisMenuxperfilRepository;
        this.sisMenuRepository = sisMenuRepository;
        this.userRepository = userRepository;
        this.userService = userService;
        this.mailService = mailService;
    }

    /**
    * POST  /register : register the user.
    *
    * @param managedUserVM the managed user View Model
    * @throws InvalidPasswordException 400 (Bad Request) if the password is incorrect
    * @throws EmailAlreadyUsedException 400 (Bad Request) if the email is already used
    * @throws LoginAlreadyUsedException 400 (Bad Request) if the login is already used
    */
    @PostMapping("/register")
    @Timed
    @ResponseStatus(HttpStatus.CREATED)
    public void registerAccount(@Valid @RequestBody ManagedUserVM managedUserVM) {
        if (!checkPasswordLength(managedUserVM.getPassword())) {
            throw new InvalidPasswordException();
        }
        userRepository.findOneByLogin(managedUserVM.getLogin().toLowerCase()).ifPresent(u -> {
            throw new LoginAlreadyUsedException();
        });
        userRepository.findOneByEmailIgnoreCase(managedUserVM.getEmail()).ifPresent(u -> {
            throw new EmailAlreadyUsedException();
        });
        User user = userService.registerUser(managedUserVM);
        mailService.sendActivationEmail(user);
    }

    /**
    * GET  /activate : activate the registered user.
    *
    * @param key the activation key
    * @throws RuntimeException 500 (Internal Server Error) if the user couldn't be activated
    */
    @GetMapping("/activate")
    @Timed
    public void activateAccount(@RequestParam(value = "key") String key) {
        Optional<User> user = userService.activateRegistration(key);
        if (!user.isPresent()) {
            throw new InternalServerErrorException("No user was found for this reset key");
        }
        ;
    }

    /**
    * GET  /authenticate : check if the user is authenticated, and return its login.
    *
    * @param request the HTTP request
    * @return the login if the user is authenticated
    */
    @GetMapping("/authenticate")
    @Timed
    public String isAuthenticated(HttpServletRequest request) {
        log.debug("REST request to check if the current user is authenticated");
        return request.getRemoteUser();
    }

    /**
    * GET  /account : get the current user.
    *
    * @return the current user
    * @throws RuntimeException 500 (Internal Server Error) if the user couldn't be returned
    */
    @GetMapping("/account")
    @Timed
    public UserDTO getAccount() {

        SisUsuario usuario = sisUsuarioRepository.GetUsuarioByLogin(SecurityUtils.getCurrentUserLogin());
        SisPersonal pernatural = null;
        Set<MenuDTO> menus = null;
        if (usuario == null) {
            new InternalServerErrorException("User could not be found");
        } else {
            List<SisUsuxperfil> usuperfils = sisUsuxperfilRepository.GetPerfilByUsu(codSis, usuario.getvCodusu());
            List<SisPerfil> perfils = new ArrayList<>();
            Set<String> strings = new HashSet<String>();
            Set<MenuDTO> objMenus = new HashSet<MenuDTO>();

            List<String> perfilCods = java.util.stream.IntStream.range(0, usuperfils.size())
                    .mapToObj(i -> usuperfils.get(i).getSisUsuxperfilId().getvCodper()).collect(Collectors.toList());

            perfils = sisPerfilRepository.GetPerfilsByCode(codSis, perfilCods);

            pernatural = sisPersonalRepository.GetPersonalByCode(usuario.getvCodpersonal());

            List<SisMenu> menusPadres = sisMenuRepository.GetMenuByPadres(codSis);
            List<SisMenuxperfil> sisMenuxperfils = sisMenuxperfilRepository.GetPerfilsByCode(codSis, perfilCods);

            List<String> menuCods = java.util.stream.IntStream.range(0, sisMenuxperfils.size())
                    .mapToObj(i -> sisMenuxperfils.get(i).getSisMenuxperfilId().getvCodmen())
                    .collect(Collectors.toList());

            List<SisMenu> sisMenus = new ArrayList<SisMenu>();
            for (String string : menuCods) {
            	SisMenu objMenu = sisMenuRepository.GetMenuByOneCode(codSis, string);
            	if(objMenu != null) {
            		sisMenus.add(objMenu);
            	}
            }
            
            List<String> menuPadres = java.util.stream.IntStream.range(0, sisMenus.size())
                    .mapToObj(i -> sisMenus.get(i).getvCodpad())
                    .collect(Collectors.toList());
            
            for (String string : menuPadres.stream().distinct().collect(Collectors.toList())) {
            	SisMenu objMenu = sisMenuRepository.GetMenuByOneCode(codSis, string);
            	if(objMenu != null) {
            		sisMenus.add(objMenu);
            	}
            }

            if (perfils.size() > 0) {
                for (SisPerfil var : perfils) {
                    strings.add(var.getvAbrper());
                }
            }

            if (sisMenus.size() > 0) {
                for (SisMenu sisMenu : sisMenus) {
                    MenuDTO menu = new MenuDTO();
                    menu.cod = sisMenu.getSisMenuId().getvCodmen();
                    menu.codPadre = sisMenu.getvCodpad();
                    menu.orden = sisMenu.getnOrden();
                    menu.nivel = sisMenu.getnNivel();
                    menu.url = sisMenu.getvUrl();
                    menu.descripcion = sisMenu.getvDesmen();
                    objMenus.add(menu);
                }
            }

            UserDTO userDTO = new UserDTO((long) 9999999, usuario.getvCodusu(), pernatural.getvDesnombres(),
                    pernatural.getvDesapepat() + " " + pernatural.getvDesapemat(), pernatural.getvCorreoe(),
                    pernatural.getnFlgactivo() == 1 ? true : false, "", usuario.getvCodusu(), "usuario", Instant.now(),
                    "usuario", Instant.now(), strings, objMenus.stream().distinct().collect(Collectors.toSet()));

            return userDTO;
        }
        return null;
        /*
         * ong id, String login, String firstName, String lastName,
        String email, boolean activated, String imageUrl, String langKey,
        String createdBy, Instant createdDate, String lastModifiedBy, Instant lastModifiedDate,
        Set<String> authorities
         * 
         * {
        "id" : 4,
        "login" : "user",
        "firstName" : "User",
        "lastName" : "User",
        "email" : "user@localhost",
        "imageUrl" : null,
        "activated" : true,
        "langKey" : "es",
        "createdBy" : "system",
        "createdDate" : "2017-10-12T13:50:04Z",
        "lastModifiedBy" : "system",
        "lastModifiedDate" : null,
        "authorities" : [ "ROLE_USER" ]
        }
         * */
        /*return Optional.ofNullable(userService.getUserWithAuthorities())
            .map(UserDTO::new)
            .orElseThrow(() -> new InternalServerErrorException("User could not be found"));*/
    }

    /**
    * POST  /account : update the current user information.
    *
    * @param userDTO the current user information
    * @throws EmailAlreadyUsedException 400 (Bad Request) if the email is already used
    * @throws RuntimeException 500 (Internal Server Error) if the user login wasn't found
    */
    @PostMapping("/account")
    @Timed
    public void saveAccount(@Valid @RequestBody UserDTO userDTO) {
        final String userLogin = SecurityUtils.getCurrentUserLogin();
        Optional<User> existingUser = userRepository.findOneByEmailIgnoreCase(userDTO.getEmail());
        if (existingUser.isPresent() && (!existingUser.get().getLogin().equalsIgnoreCase(userLogin))) {
            throw new EmailAlreadyUsedException();
        }
        Optional<User> user = userRepository.findOneByLogin(userLogin);
        if (!user.isPresent()) {
            throw new InternalServerErrorException("User could not be found");
        }
        userService.updateUser(userDTO.getFirstName(), userDTO.getLastName(), userDTO.getEmail(), userDTO.getLangKey(),
                userDTO.getImageUrl());
    }

    /**
    * POST  /account/change-password : changes the current user's password
    *
    * @param password the new password
    * @throws InvalidPasswordException 400 (Bad Request) if the new password is incorrect
    */
    @PostMapping(path = "/account/change-password")
    @Timed
    public void changePassword(@RequestBody String password) {
        if (!checkPasswordLength(password)) {
            throw new InvalidPasswordException();
        }
        userService.changePassword(password);
    }

    /**
    * POST   /account/reset-password/init : Send an email to reset the password of the user
    *
    * @param mail the mail of the user
    * @throws EmailNotFoundException 400 (Bad Request) if the email address is not registered
    */
    @PostMapping(path = "/account/reset-password/init")
    @Timed
    public void requestPasswordReset(@RequestBody String mail) {
        mailService
                .sendPasswordResetMail(userService.requestPasswordReset(mail).orElseThrow(EmailNotFoundException::new));
    }

    /**
    * POST   /account/reset-password/finish : Finish to reset the password of the user
    *
    * @param keyAndPassword the generated key and the new password
    * @throws InvalidPasswordException 400 (Bad Request) if the password is incorrect
    * @throws RuntimeException 500 (Internal Server Error) if the password could not be reset
    */
    @PostMapping(path = "/account/reset-password/finish")
    @Timed
    public void finishPasswordReset(@RequestBody KeyAndPasswordVM keyAndPassword) {
        if (!checkPasswordLength(keyAndPassword.getNewPassword())) {
            throw new InvalidPasswordException();
        }
        Optional<User> user = userService.completePasswordReset(keyAndPassword.getNewPassword(),
                keyAndPassword.getKey());

        if (!user.isPresent()) {
            throw new InternalServerErrorException("No user was found for this reset key");
        }
    }

    private static boolean checkPasswordLength(String password) {
        return !StringUtils.isEmpty(password) && password.length() >= ManagedUserVM.PASSWORD_MIN_LENGTH
                && password.length() <= ManagedUserVM.PASSWORD_MAX_LENGTH;
    }
}

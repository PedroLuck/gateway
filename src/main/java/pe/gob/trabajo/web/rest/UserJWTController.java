package pe.gob.trabajo.web.rest;

import pe.gob.trabajo.domain.SisUsuxperfil;
import pe.gob.trabajo.repository.PerfilRepository;
import pe.gob.trabajo.repository.SisMenuRepository;
import pe.gob.trabajo.repository.SisMenuxperfilRepository;
import pe.gob.trabajo.repository.SisPerfilRepository;
import pe.gob.trabajo.repository.SisPersonalRepository;
import pe.gob.trabajo.repository.SisUsuarioRepository;
import pe.gob.trabajo.repository.SisUsuxperfilRepository;
import pe.gob.trabajo.repository.UsuarioRepository;
import pe.gob.trabajo.repository.UsuperfilRepository;
import pe.gob.trabajo.security.jwt.JWTConfigurer;
import pe.gob.trabajo.security.jwt.TokenProvider;
import pe.gob.trabajo.service.CustomAuthenticationManager;
import pe.gob.trabajo.service.UserService;
import pe.gob.trabajo.web.rest.vm.LoginVM;

import com.codahale.metrics.annotation.Timed;
import com.fasterxml.jackson.annotation.JsonProperty;

import org.springframework.http.HttpStatus;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Controller to authenticate users.
 */
@RestController
@RequestMapping("/api")
public class UserJWTController {

	private final UserService userService;
    private final TokenProvider tokenProvider;
    private final SisUsuarioRepository sisUsuarioRepository;
    private final SisPersonalRepository sisPersonalRepository;
    private final SisPerfilRepository sisPerfilRepository;
    private final SisUsuxperfilRepository sisUsuxperfilRepository;
    private final SisMenuxperfilRepository sisMenuxperfilRepository;
    private final SisMenuRepository sisMenuRepository;
    private final AuthenticationManager authenticationManager;
    private CustomAuthenticationManager customAuthenticationManager;

	public UserJWTController(TokenProvider tokenProvider
    		, AuthenticationManager authenticationManager
    		, SisUsuarioRepository sisUsuarioRepository
    		, SisPersonalRepository sisPersonalRepository
            , SisPerfilRepository sisPerfilRepository
            , SisUsuxperfilRepository sisUsuxperfilRepository
            , SisMenuxperfilRepository sisMenuxperfilRepository
            , SisMenuRepository sisMenuRepository
    		, UserService userService
    		) {
        this.tokenProvider = tokenProvider;
        this.authenticationManager = authenticationManager;
        this.sisUsuarioRepository = sisUsuarioRepository;
        this.sisPersonalRepository = sisPersonalRepository;
        this.sisPerfilRepository = sisPerfilRepository;
        this.sisUsuxperfilRepository = sisUsuxperfilRepository;
        this.sisMenuxperfilRepository = sisMenuxperfilRepository;
        this.sisMenuRepository = sisMenuRepository;
        this.userService = userService;
    }

    @PostMapping("/authenticate")
    @Timed
    public ResponseEntity<JWTToken> authorize(@Valid @RequestBody LoginVM loginVM) {

        UsernamePasswordAuthenticationToken authenticationToken =
            new UsernamePasswordAuthenticationToken(loginVM.getUsername(), loginVM.getPassword());//userService.encodePass(loginVM.getPassword()));    
        
        this.customAuthenticationManager = new CustomAuthenticationManager(
            sisUsuarioRepository, 
            sisPersonalRepository, 
            sisPerfilRepository,
            sisUsuxperfilRepository, 
            sisMenuxperfilRepository,
            sisMenuRepository);
        
        Authentication authentication = this.customAuthenticationManager.authenticate(authenticationToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        boolean rememberMe = (loginVM.isRememberMe() == null) ? false : loginVM.isRememberMe();
        String jwt = tokenProvider.createToken(authentication, rememberMe);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(JWTConfigurer.AUTHORIZATION_HEADER, "Bearer " + jwt);
        return new ResponseEntity<>(new JWTToken(jwt), httpHeaders, HttpStatus.OK);
    }

    /**
     * Object to return as body in JWT Authentication.
     */
    static class JWTToken {

        private String idToken;

        JWTToken(String idToken) {
            this.idToken = idToken;
        }

        @JsonProperty("id_token")
        String getIdToken() {
            return idToken;
        }

        void setIdToken(String idToken) {
            this.idToken = idToken;
        }
    }
}

package pe.gob.trabajo.web.rest;

import com.codahale.metrics.annotation.Timed;
import pe.gob.trabajo.domain.SisPersonal;

import pe.gob.trabajo.repository.SisPersonalRepository;
import pe.gob.trabajo.web.rest.errors.BadRequestAlertException;
import pe.gob.trabajo.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Solicitud.
 */
@RestController
@RequestMapping("/api")
public class SispersonalResource {

    private final Logger log = LoggerFactory.getLogger(SolicitudResource.class);

    private static final String ENTITY_NAME = "solicitud";

    private final SisPersonalRepository sispersonalRepository;


    public SispersonalResource(SisPersonalRepository sispersonalRepository) {
        this.sispersonalRepository = sispersonalRepository;
    }

   
    /**
     * GET  /solicituds/:id : get the "id" solicitud.
     *
     * @param id the id of the solicitud to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the solicitud, or with status 404 (Not Found)
     */
    @GetMapping("/sispersonal/{id}")
    @Timed
    public SisPersonal getSolicitud(@PathVariable String id) {
        log.debug("REST request to get Sispersonal : {}", id);
        return sispersonalRepository.GetPersonalByCode(id);
    }

    @GetMapping("/sispersonal/usuario/{id}")
    @Timed
    public List<SisPersonal> getSolicitudUsuario(@PathVariable String id) {
        log.debug("REST request to get Sispersonal : {}", id);
        return sispersonalRepository.GetPersonalUsuarioByCode(id);
    }

}

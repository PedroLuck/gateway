package pe.gob.trabajo.service.util;

import java.util.Hashtable;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

public class ActiveDirectory {

    static final String LDAP_URL = "ldap://trabajo.gob.pe:389";
    static final String base = "DC=trabajo,DC=gob,DC=pe";
    static final String dominio = "trabajo.gob.pe";

    public DirContext login(String username, String password) {
        Hashtable env = new Hashtable();
        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        env.put(Context.PROVIDER_URL, "ldap://trabajo.gob.pe:389");
        env.put(Context.SECURITY_AUTHENTICATION, "simple");
        env.put(Context.SECURITY_PRINCIPAL, username.toUpperCase() + "@" + dominio);
        env.put(Context.SECURITY_CREDENTIALS, password);
        try {
            DirContext ctx = new InitialDirContext(env);
             System.out.println("Success login: ");
            return ctx;
        } catch (NamingException ex) {
            System.out.println("Error login: " + ex);
        }
        return null;
    }

    public SearchResult search(String username, DirContext ldapContext) {
        try {
            SearchControls searchCtls = new SearchControls();
            searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);
            String searchFilter = "(&(objectClass=user)(sAMAccountName=" + username + "))";
            NamingEnumeration<SearchResult> results = ldapContext.search(base, searchFilter, searchCtls);
            SearchResult searchResult = null;
            if (results.hasMoreElements()) {
                searchResult = (SearchResult) results.nextElement();

                if (results.hasMoreElements()) {
                    System.err.println("Matched multiple users for the accountName: " + username);
                    return null;
                }
            }

            return searchResult;
            
        } catch (NamingException ex) {
            System.out.println("Error search: " + ex);
        }

        return null;
    }
    /*
    public static void main(String[] args) {
        String username = "hsanchez";
        String password = "heider150487";
        ActiveDirectory instance = new ActiveDirectory();
        DirContext ldapContext = instance.login(username, password);
        SearchResult result = instance.search("ecueva", ldapContext);
        System.out.println("busqueda: " + result);
    }
    */
}
package pe.gob.trabajo.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.naming.directory.DirContext;
import javax.naming.directory.SearchResult;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import pe.gob.trabajo.domain.SisPerfil;
import pe.gob.trabajo.domain.SisUsuario;
import pe.gob.trabajo.domain.SisUsuxperfil;
import pe.gob.trabajo.domain.Usuario;
import pe.gob.trabajo.domain.Usuperfil;
import pe.gob.trabajo.repository.PerfilRepository;
import pe.gob.trabajo.repository.SisMenuRepository;
import pe.gob.trabajo.repository.SisMenuxperfilRepository;
import pe.gob.trabajo.repository.SisPerfilRepository;
import pe.gob.trabajo.repository.SisPersonalRepository;
import pe.gob.trabajo.repository.SisUsuarioRepository;
import pe.gob.trabajo.repository.SisUsuxperfilRepository;
import pe.gob.trabajo.repository.UsuarioRepository;
import pe.gob.trabajo.repository.UsuperfilRepository;
import pe.gob.trabajo.service.util.ActiveDirectory;

public class CustomAuthenticationManager implements AuthenticationManager {

    private final SisUsuarioRepository sisUsuarioRepository;
    private final SisPersonalRepository sisPersonalRepository;
    private final SisPerfilRepository sisPerfilRepository;
    private final SisUsuxperfilRepository sisUsuxperfilRepository;
    private final SisMenuxperfilRepository sisMenuxperfilRepository;
    private final SisMenuRepository sisMenuRepository;
    private final String codSis = "084";

    private SisUsuario usuario;

    public CustomAuthenticationManager(
              SisUsuarioRepository sisUsuarioRepository
    		, SisPersonalRepository sisPersonalRepository
            , SisPerfilRepository sisPerfilRepository
            , SisUsuxperfilRepository sisUsuxperfilRepository
            , SisMenuxperfilRepository sisMenuxperfilRepository
            , SisMenuRepository sisMenuRepository
    ){
        this.sisUsuarioRepository = sisUsuarioRepository;
        this.sisPersonalRepository = sisPersonalRepository;
        this.sisPerfilRepository = sisPerfilRepository;
        this.sisUsuxperfilRepository = sisUsuxperfilRepository;
        this.sisMenuxperfilRepository = sisMenuxperfilRepository;
        this.sisMenuRepository = sisMenuRepository;
    }

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		try {
			String username = authentication.getPrincipal() + "";
	        String password = authentication.getCredentials() + "";

	        this.usuario = sisUsuarioRepository.GetUsuarioByLogin(username);
	        List<SisPerfil> perfils = new ArrayList<>();
	        
	        ActiveDirectory instance = new ActiveDirectory();
	        DirContext ldapContext = instance.login(username, password);
	        SearchResult result = instance.search("ecueva", ldapContext);
	        
	        if (usuario == null) {
	            throw new BadCredentialsException("1000");
	        }
	        if (usuario.getvFlgact().trim().equals("N")) {
	            throw new DisabledException("1001");
	        }
	        if (ldapContext == null) {
	            throw new BadCredentialsException("1000");
	        }
	        List<SisUsuxperfil> sisUsuxperfils = sisUsuxperfilRepository.GetPerfilByUsu(codSis, usuario.getvCodusu());
	        if (sisUsuxperfils.size() == 0) {
	            throw new DisabledException("1001");
	        }   
	        
	        List<String> perfilCods = java.util.stream.IntStream.range(0, sisUsuxperfils.size())
	                                .mapToObj(i -> sisUsuxperfils.get(i).getSisUsuxperfilId().getvCodper())
	                                .collect(Collectors.toList());

	        perfils = sisPerfilRepository.GetPerfilsByCode(codSis, perfilCods);
	         
	        return new UsernamePasswordAuthenticationToken(username, password, perfils.stream().map(x -> new SimpleGrantedAuthority(x.getvDesper())).collect(Collectors.toList()));
		} catch (Exception e) {
			throw new BadCredentialsException("1000");
		}
	}

}
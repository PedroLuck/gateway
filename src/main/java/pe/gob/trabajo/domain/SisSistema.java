package pe.gob.trabajo.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

@Entity
@Table(name = "SITB_SISTEMA", schema = "SIMINTRA1")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "SITB_SISTEMA")
public class SisSistema {
	
	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "V_CODSIS", nullable = false)
	private String vCodsis;
	@Column(name = "V_NOMSIS")
	private String vNomsis;
	@Column(name = "V_OBSSIST")
	private String vObssist;
	@Column(name = "V_ABRSIS")
	private String vAbrsis;
	@Column(name = "V_FLGACT")
	private String vFlgact;
	@Column(name = "V_TIPOSIS")
	private String vTiposis;
	@Column(name = "V_URL")
	private String vUrl;
	
	public String getvCodsis() {
		return vCodsis;
	}
	public void setvCodsis(String vCodsis) {
		this.vCodsis = vCodsis;
	}
	public String getvNomsis() {
		return vNomsis;
	}
	public void setvNomsis(String vNomsis) {
		this.vNomsis = vNomsis;
	}
	public String getvObssist() {
		return vObssist;
	}
	public void setvObssist(String vObssist) {
		this.vObssist = vObssist;
	}
	public String getvAbrsis() {
		return vAbrsis;
	}
	public void setvAbrsis(String vAbrsis) {
		this.vAbrsis = vAbrsis;
	}
	public String getvFlgact() {
		return vFlgact;
	}
	public void setvFlgact(String vFlgact) {
		this.vFlgact = vFlgact;
	}
	public String getvTiposis() {
		return vTiposis;
	}
	public void setvTiposis(String vTiposis) {
		this.vTiposis = vTiposis;
	}
	public String getvUrl() {
		return vUrl;
	}
	public void setvUrl(String vUrl) {
		this.vUrl = vUrl;
	}
	
	

}

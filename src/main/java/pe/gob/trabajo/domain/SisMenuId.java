package pe.gob.trabajo.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class SisMenuId implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Column(name = "V_CODSIS", nullable = false)
	private String vCodsis;
	@Column(name = "V_CODMEN")
	private String vCodmen;
	
	public String getvCodsis() {
		return vCodsis;
	}
	public void setvCodsis(String vCodsis) {
		this.vCodsis = vCodsis;
	}
	public String getvCodmen() {
		return vCodmen;
	}
	public void setvCodmen(String vCodmen) {
		this.vCodmen = vCodmen;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((vCodmen == null) ? 0 : vCodmen.hashCode());
		result = prime * result + ((vCodsis == null) ? 0 : vCodsis.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SisMenuId other = (SisMenuId) obj;
		if (vCodmen == null) {
			if (other.vCodmen != null)
				return false;
		} else if (!vCodmen.equals(other.vCodmen))
			return false;
		if (vCodsis == null) {
			if (other.vCodsis != null)
				return false;
		} else if (!vCodsis.equals(other.vCodsis))
			return false;
		return true;
	}
	public SisMenuId(String vCodsis, String vCodmen) {
		super();
		this.vCodsis = vCodsis;
		this.vCodmen = vCodmen;
	}
	public SisMenuId() {}
	
	@Override
	public String toString() {
		return "SisMenuId [vCodsis=" + vCodsis + ", vCodmen=" + vCodmen + "]";
	}
	
}

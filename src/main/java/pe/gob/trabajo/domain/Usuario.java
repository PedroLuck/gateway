package pe.gob.trabajo.domain;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "sgtbc_usuario")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "usuario")
public class Usuario {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "n_codusuario")
    private Long id;
    @Column(name = "v_loginusu")
    private String login;
    @Column(name = "v_password")
    private String pass;
    @Column(name = "v_imgusu")
    private String img;
    @Column(name = "v_langkey")
    private String langKey;
    @Column(name = "v_resetkey")
    private String resetKey;
    @Column(name = "n_flgrepass")
    private Boolean flgResetKey;
    @Column(name = "n_codpernat")
    private Long nCodpernat;
    @Column(name = "n_codtipusu")
    private Long nCodtipusu;
    @Column(name = "n_flgactivo")
    private Boolean flgActivo;
    
    @OneToMany(mappedBy = "usuario")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Usuperfil> usuperfils = new HashSet<>();
    
	public Set<Usuperfil> getUsuperfils() {
		return usuperfils;
	}
	public void setUsuperfils(Set<Usuperfil> usuperfils) {
		this.usuperfils = usuperfils;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	public String getImg() {
		return img;
	}
	public void setImg(String img) {
		this.img = img;
	}
	public String getLangKey() {
		return langKey;
	}
	public void setLangKey(String langKey) {
		this.langKey = langKey;
	}
	public String getResetKey() {
		return resetKey;
	}
	public void setResetKey(String resetKey) {
		this.resetKey = resetKey;
	}
	public Boolean getFlgResetKey() {
		return flgResetKey;
	}
	public void setFlgResetKey(Boolean flgResetKey) {
		this.flgResetKey = flgResetKey;
	}
	public Long getnCodpernat() {
		return nCodpernat;
	}
	public void setnCodpernat(Long nCodpernat) {
		this.nCodpernat = nCodpernat;
	}
	public Long getnCodtipusu() {
		return nCodtipusu;
	}
	public void setnCodtipusu(Long nCodtipusu) {
		this.nCodtipusu = nCodtipusu;
	}
	public Boolean getFlgActivo() {
		return flgActivo;
	}
	public void setFlgActivo(Boolean flgActivo) {
		this.flgActivo = flgActivo;
	}
	@Override
	public String toString() {
		return "Usuario [id=" + id + ", login=" + login + ", pass=" + pass + ", img=" + img + ", langKey=" + langKey
				+ ", resetKey=" + resetKey + ", flgResetKey=" + flgResetKey + ", nCodpernat=" + nCodpernat
				+ ", nCodtipusu=" + nCodtipusu + ", flgActivo=" + flgActivo + "]";
	}
}
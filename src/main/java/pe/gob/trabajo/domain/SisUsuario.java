package pe.gob.trabajo.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

@Entity
@Table(name = "SITB_USUARIO", schema = "SIMINTRA1")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "SITB_USUARIO")
public class SisUsuario {
	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "V_CODUSU", nullable = false)
	private String vCodusu;
	@Column(name = "V_CODPERSONAL")
	private String vCodpersonal;
	@Column(name = "V_PASSUSU")
	private String vPassusu;
	@Column(name = "V_FLGACT")
	private String vFlgact;
	@Column(name = "V_CODPEREXT")
	private String vCodperext;
	@Column(name = "V_FLGTIPO")
	private String vFlgtipo;
	@Column(name = "V_FLGESTEXT")
	private String vFlgestext;
	
	public String getvCodusu() {
		return vCodusu;
	}
	public void setvCodusu(String vCodusu) {
		this.vCodusu = vCodusu;
	}
	public String getvCodpersonal() {
		return vCodpersonal;
	}
	public void setvCodpersonal(String vCodpersonal) {
		this.vCodpersonal = vCodpersonal;
	}
	public String getvPassusu() {
		return vPassusu;
	}
	public void setvPassusu(String vPassusu) {
		this.vPassusu = vPassusu;
	}
	public String getvFlgact() {
		return vFlgact;
	}
	public void setvFlgact(String vFlgact) {
		this.vFlgact = vFlgact;
	}
	public String getvCodperext() {
		return vCodperext;
	}
	public void setvCodperext(String vCodperext) {
		this.vCodperext = vCodperext;
	}
	public String getvFlgtipo() {
		return vFlgtipo;
	}
	public void setvFlgtipo(String vFlgtipo) {
		this.vFlgtipo = vFlgtipo;
	}
	public String getvFlgestext() {
		return vFlgestext;
	}
	public void setvFlgestext(String vFlgestext) {
		this.vFlgestext = vFlgestext;
	}
	
	
}

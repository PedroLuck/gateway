package pe.gob.trabajo.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A PagosNacion.
 */
@Entity(name="PagosNacion")
@Table(name = "sitb_pagosnacion")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "sitb_pagosnacion")
public class PagosNacion implements Serializable {

    private static final long serialVersionUID = 1L;

    @Transient
    private Long id;

    @EmbeddedId
    private PagosNacionId pagosNacionId;

    @Size(max = 5)
    @Column(name = "v_codtrib", length = 5)
    private String vCodtrib;

    @Size(max = 5)
    @Column(name = "v_codmtpe", length = 5)
    private String vCodmtpe;

    @Size(max = 1)
    @Column(name = "v_tipdoc", length = 1)
    private String vTipdoc;

    @Size(max = 15)
    @Column(name = "v_numdoc", length = 15)
    private String vNumdoc;

    @Column(name = "n_impcomp")
    private Double nImpcomp;

    @Column(name = "d_feccomp")
    private Instant dFeccomp;

    @Size(max = 4)
    @Column(name = "v_codcaj", length = 4)
    private String vCodcaj;

    @Size(max = 1)
    @Column(name = "v_flgcon", length = 1)
    private String vFlgcon;

    @Size(max = 12)
    @Column(name = "v_numdocmul", length = 12)
    private String vNumdocmul;

    @Size(max = 100)
    @Column(name = "v_estado", length = 100)
    private String vEstado;

    @Size(max = 100)
    @Column(name = "v_pago", length = 100)
    private String vPago;

    @Size(max = 100)
    @Column(name = "v_digito", length = 100)
    private String vDigito;

    @Size(max = 100)
    @Column(name = "v_afp", length = 100)
    private String vAfp;

    @Size(max = 100)
    @Column(name = "v_motivo_ext", length = 100)
    private String vMotivo_ext;

    @Size(max = 100)
    @Column(name = "v_otros", length = 100)
    private String vOtros;

    @Size(max = 100)
    @Column(name = "v_banco", length = 100)
    private String vBanco;

    @Size(max = 100)
    @Column(name = "v_cheque", length = 100)
    private String vCheque;

    @Size(max = 100)
    @Column(name = "v_juzgado", length = 100)
    private String vJuzgado;

    @Size(max = 100)
    @Column(name = "v_registros", length = 100)
    private String vRegistros;

    @Size(max = 100)
    @Column(name = "v_expdte", length = 100)
    private String vExpdte;

    @Size(max = 10)
    @Column(name = "v_ano_movim", length = 10)
    private String vAno_movim;

    @Size(max = 10)
    @Column(name = "v_mes_movim", length = 10)
    private String vMes_movim;

    @Size(max = 10)
    @Column(name = "v_dia_movim", length = 10)
    private String vDia_movim;

    @Size(max = 10)
    @Column(name = "v_hh_movim", length = 10)
    private String vHh_movim;

    @Size(max = 10)
    @Column(name = "v_mm_movim", length = 10)
    private String vMm_movim;

    @Size(max = 10)
    @Column(name = "v_ss_movim", length = 10)
    private String vSs_movim;

    @Size(max = 30)
    @Column(name = "v_trans", length = 30)
    private String vTrans;

    @Size(max = 30)
    @Column(name = "v_tributo2", length = 30)
    private String vTributo2;

    @Size(max = 30)
    @Column(name = "v_benefi", length = 30)
    private String vBenefi;

    @Size(max = 30)
    @Column(name = "v_digchk", length = 30)
    private String vDigchk;

    @Size(max = 30)
    @Column(name = "v_expdte1", length = 30)
    private String vExpdte1;

    @Size(max = 30)
    @Column(name = "v_doc_pgo", length = 30)
    private String vDoc_pgo;

    @Size(max = 30)
    @Column(name = "v_pgo", length = 30)
    private String vPgo;

    @Size(max = 30)
    @Column(name = "v_region", length = 30)
    private String vRegion;

    @Size(max = 30)
    @Column(name = "v_pagos", length = 30)
    private String vPagos;

    @Size(max = 30)
    @Column(name = "v_adm", length = 30)
    private String vAdm;

    @Size(max = 30)
    @Column(name = "v_agencia_d", length = 30)
    private String vAgencia_d;

    @Size(max = 30)
    @Column(name = "v_ano_recau", length = 30)
    private String vAno_recau;

    @Size(max = 30)
    @Column(name = "v_mes_recau", length = 30)
    private String vMes_recau;

    @Size(max = 30)
    @Column(name = "v_dia_recau", length = 30)
    private String vDia_recau;

    @Size(max = 30)
    @Column(name = "v_ubigeo", length = 30)
    private String vUbigeo;

    @Size(max = 30)
    @Column(name = "v_fuente", length = 30)
    private String vFuente;

    @Size(max = 30)
    @Column(name = "v_contrib", length = 30)
    private String vContrib;

    @Size(max = 30)
    @Column(name = "v_supervisor", length = 30)
    private String vSupervisor;

    @Size(max = 30)
    @Column(name = "v_hhmovim", length = 30)
    private String vHhmovim;

    @Size(max = 30)
    @Column(name = "v_proceso", length = 30)
    private String vProceso;

    @Column(name = "d_fecreg")
    private Instant dFecreg;

    @Size(max = 11)
    @Column(name = "v_codbanco", length = 11)
    private String vCodbanco;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
   
    public Long getId() {
        return id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
    
    public String getvCodtrib() {
        return vCodtrib;
    }
    
    public String getvCodmtpe() {
        return vCodmtpe;
    }
    
    public String getvTipdoc() {
        return vTipdoc;
    }
    
    public String getvNumdoc() {
        return vNumdoc;
    }
    
    public Double getnImpcomp() {
        return nImpcomp;
    }
    
    public Instant getdFeccomp() {
        return dFeccomp;
    }
    
    public String getvCodcaj() {
        return vCodcaj;
    }
    
    public String getvFlgcon() {
        return vFlgcon;
    }
    
    public String getvNumdocmul() {
        return vNumdocmul;
    }
    
    public String getvEstado() {
        return vEstado;
    }
    
    public String getvPago() {
        return vPago;
    }
    
    public String getvDigito() {
        return vDigito;
    }
    
    public String getvAfp() {
        return vAfp;
    }
    
    public String getvMotivo_ext() {
        return vMotivo_ext;
    }
    
    public String getvOtros() {
        return vOtros;
    }
    
    public String getvBanco() {
        return vBanco;
    }
    
    public String getvCheque() {
        return vCheque;
    }
    
    public String getvJuzgado() {
        return vJuzgado;
    }
    
    public String getvRegistros() {
        return vRegistros;
    }
    
    public String getvExpdte() {
        return vExpdte;
    }
    
    public String getvAno_movim() {
        return vAno_movim;
    }
    
    public String getvMes_movim() {
        return vMes_movim;
    }
    
    public String getvDia_movim() {
        return vDia_movim;
    }
    
    public String getvHh_movim() {
        return vHh_movim;
    }
    
    public String getvMm_movim() {
        return vMm_movim;
    }
    
    public String getvSs_movim() {
        return vSs_movim;
    }
    
    public String getvTrans() {
        return vTrans;
    }
    
    public String getvTributo2() {
        return vTributo2;
    }
    
    public String getvBenefi() {
        return vBenefi;
    }
    
    public String getvDigchk() {
        return vDigchk;
    }
    
    public String getvExpdte1() {
        return vExpdte1;
    }
    
    public String getvDoc_pgo() {
        return vDoc_pgo;
    }
    
    public String getvPgo() {
        return vPgo;
    }
    
    public String getvRegion() {
        return vRegion;
    }
    
    public String getvPagos() {
        return vPagos;
    }
    
    public String getvAdm() {
        return vAdm;
    }
    
    public String getvAgencia_d() {
        return vAgencia_d;
    }
    
    public String getvAno_recau() {
        return vAno_recau;
    }
    
    public String getvMes_recau() {
        return vMes_recau;
    }
    
    public String getvDia_recau() {
        return vDia_recau;
    }
    
    public String getvUbigeo() {
        return vUbigeo;
    }
    
    public String getvFuente() {
        return vFuente;
    }
    
    public String getvContrib() {
        return vContrib;
    }
    
    public String getvSupervisor() {
        return vSupervisor;
    }
    
    public String getvHhmovim() {
        return vHhmovim;
    }
    
    public String getvProceso() {
        return vProceso;
    }
    
    public Instant getdFecreg() {
        return dFecreg;
    }
    
    public String getvCodbanco() {
        return vCodbanco;
    }
    
   
    
    public void setvCodtrib(String vCodtrib) {
        this.vCodtrib = vCodtrib;
    }
    
    public void setvCodmtpe(String vCodmtpe) {
        this.vCodmtpe = vCodmtpe;
    }
    
    public void setvTipdoc(String vTipdoc) {
        this.vTipdoc = vTipdoc;
    }
    
    public void setvNumdoc(String vNumdoc) {
        this.vNumdoc = vNumdoc;
    }
    
    public void setnImpcomp(Double nImpcomp) {
        this.nImpcomp = nImpcomp;
    }
    
    public void setdFeccomp(Instant dFeccomp) {
        this.dFeccomp = dFeccomp;
    }
    
    public void setvCodcaj(String vCodcaj) {
        this.vCodcaj = vCodcaj;
    }
    
    public void setvFlgcon(String vFlgcon) {
        this.vFlgcon = vFlgcon;
    }
    
    public void setvNumdocmul(String vNumdocmul) {
        this.vNumdocmul = vNumdocmul;
    }
    
    public void setvEstado(String vEstado) {
        this.vEstado = vEstado;
    }
    
    public void setvPago(String vPago) {
        this.vPago = vPago;
    }
    
    public void setvDigito(String vDigito) {
        this.vDigito = vDigito;
    }
    
    public void setvAfp(String vAfp) {
        this.vAfp = vAfp;
    }
    
    public void setvMotivo_ext(String vMotivo_ext) {
        this.vMotivo_ext = vMotivo_ext;
    }
    
    public void setvOtros(String vOtros) {
        this.vOtros = vOtros;
    }
    
    public void setvBanco(String vBanco) {
        this.vBanco = vBanco;
    }
    
    public void setvCheque(String vCheque) {
        this.vCheque = vCheque;
    }
    
    public void setvJuzgado(String vJuzgado) {
        this.vJuzgado = vJuzgado;
    }
    
    public void setvRegistros(String vRegistros) {
        this.vRegistros = vRegistros;
    }
    
    public void setvExpdte(String vExpdte) {
        this.vExpdte = vExpdte;
    }
    
    public void setvAno_movim(String vAno_movim) {
        this.vAno_movim = vAno_movim;
    }
    
    public void setvMes_movim(String vMes_movim) {
        this.vMes_movim = vMes_movim;
    }
    
    public void setvDia_movim(String vDia_movim) {
        this.vDia_movim = vDia_movim;
    }
    
    public void setvHh_movim(String vHh_movim) {
        this.vHh_movim = vHh_movim;
    }
    
    public void setvMm_movim(String vMm_movim) {
        this.vMm_movim = vMm_movim;
    }
    
    public void setvSs_movim(String vSs_movim) {
        this.vSs_movim = vSs_movim;
    }
    
    public void setvTrans(String vTrans) {
        this.vTrans = vTrans;
    }
    
    public void setvTributo2(String vTributo2) {
        this.vTributo2 = vTributo2;
    }
    
    public void setvBenefi(String vBenefi) {
        this.vBenefi = vBenefi;
    }
    
    public void setvDigchk(String vDigchk) {
        this.vDigchk = vDigchk;
    }
    
    public void setvExpdte1(String vExpdte1) {
        this.vExpdte1 = vExpdte1;
    }
    
    public void setvDoc_pgo(String vDoc_pgo) {
        this.vDoc_pgo = vDoc_pgo;
    }
    
    public void setvPgo(String vPgo) {
        this.vPgo = vPgo;
    }
    
    public void setvRegion(String vRegion) {
        this.vRegion = vRegion;
    }
    
    public void setvPagos(String vPagos) {
        this.vPagos = vPagos;
    }
    
    public void setvAdm(String vAdm) {
        this.vAdm = vAdm;
    }
    
    public void setvAgencia_d(String vAgencia_d) {
        this.vAgencia_d = vAgencia_d;
    }
    
    public void setvAno_recau(String vAno_recau) {
        this.vAno_recau = vAno_recau;
    }
    
    public void setvMes_recau(String vMes_recau) {
        this.vMes_recau = vMes_recau;
    }
    
    public void setvDia_recau(String vDia_recau) {
        this.vDia_recau = vDia_recau;
    }
    
    public void setvUbigeo(String vUbigeo) {
        this.vUbigeo = vUbigeo;
    }
    
    public void setvFuente(String vFuente) {
        this.vFuente = vFuente;
    }
    
    public void setvContrib(String vContrib) {
        this.vContrib = vContrib;
    }
    
    public void setvSupervisor(String vSupervisor) {
        this.vSupervisor = vSupervisor;
    }
    
    public void setvHhmovim(String vHhmovim) {
        this.vHhmovim = vHhmovim;
    }
    
    public void setvProceso(String vProceso) {
        this.vProceso = vProceso;
    }
    
    public void setdFecreg(Instant dFecreg) {
        this.dFecreg = dFecreg;
    }
    
    public void setvCodbanco(String vCodbanco) {
        this.vCodbanco = vCodbanco;
    }
    
   
    
    public PagosNacion vCodtrib(String vCodtrib) {
        this.vCodtrib = vCodtrib;
        return this;
    }
    
    public PagosNacion vCodmtpe(String vCodmtpe) {
        this.vCodmtpe = vCodmtpe;
        return this;
    }
    
    public PagosNacion vTipdoc(String vTipdoc) {
        this.vTipdoc = vTipdoc;
        return this;
    }
    
    public PagosNacion vNumdoc(String vNumdoc) {
        this.vNumdoc = vNumdoc;
        return this;
    }
    
    public PagosNacion nImpcomp(Double nImpcomp) {
        this.nImpcomp = nImpcomp;
        return this;
    }
    
    public PagosNacion dFeccomp(Instant dFeccomp) {
        this.dFeccomp = dFeccomp;
        return this;
    }
    
    public PagosNacion vCodcaj(String vCodcaj) {
        this.vCodcaj = vCodcaj;
        return this;    
    }
    
    public PagosNacion vFlgcon(String vFlgcon) {
        this.vFlgcon = vFlgcon;
        return this;
    }
    
    public PagosNacion vNumdocmul(String vNumdocmul) {
        this.vNumdocmul = vNumdocmul;
        return this;
    }
    
    public PagosNacion vEstado(String vEstado) {
        this.vEstado = vEstado;
        return this;
    }
    
    public PagosNacion vPago(String vPago) {
        this.vPago = vPago;
        return this;
    }
    
    public PagosNacion vDigito(String vDigito) {
        this.vDigito = vDigito;
        return this;
    }
    
    public PagosNacion vAfp(String vAfp) {
        this.vAfp = vAfp;
        return this;
    }
    
    public PagosNacion vMotivo_ext(String vMotivo_ext) {
        this.vMotivo_ext = vMotivo_ext;
        return this;
    }
    
    public PagosNacion vOtros(String vOtros) {
        this.vOtros = vOtros;
        return this;
    }
    
    public PagosNacion vBanco(String vBanco) {
        this.vBanco = vBanco;
        return this;
    }
    
    public PagosNacion vCheque(String vCheque) {
        this.vCheque = vCheque;
        return this;
    }
    
    public PagosNacion vJuzgado(String vJuzgado) {
        this.vJuzgado = vJuzgado;
        return this;
    }
    
    public PagosNacion vRegistros(String vRegistros) {
        this.vRegistros = vRegistros;
        return this;
    }
    
    public PagosNacion vExpdte(String vExpdte) {
        this.vExpdte = vExpdte;
        return this;
    }
    
    public PagosNacion vAno_movim(String vAno_movim) {
        this.vAno_movim = vAno_movim;
        return this;
    }
    
    public PagosNacion vMes_movim(String vMes_movim) {
        this.vMes_movim = vMes_movim;
        return this;
    }
    
    public PagosNacion vDia_movim(String vDia_movim) {
        this.vDia_movim = vDia_movim;
        return this;
    }
    
    public PagosNacion vHh_movim(String vHh_movim) {
        this.vHh_movim = vHh_movim;
        return this;
    }
    
    public PagosNacion vMm_movim(String vMm_movim) {
        this.vMm_movim = vMm_movim;
        return this;
    }
    
    public PagosNacion vSs_movim(String vSs_movim) {
        this.vSs_movim = vSs_movim;
        return this;
    }
    
    public PagosNacion vTrans(String vTrans) {
        this.vTrans = vTrans;
        return this;
    }
    
    public PagosNacion vTributo2(String vTributo2) {
        this.vTributo2 = vTributo2;
        return this;
    }
    
    public PagosNacion vBenefi(String vBenefi) {
        this.vBenefi = vBenefi;
        return this;
    }
    
    public PagosNacion vDigchk(String vDigchk) {
        this.vDigchk = vDigchk;
        return this;
    }
    
    public PagosNacion vExpdte1(String vExpdte1) {
        this.vExpdte1 = vExpdte1;
        return this;
    }
    
    public PagosNacion vDoc_pgo(String vDoc_pgo) {
        this.vDoc_pgo = vDoc_pgo;
        return this;
    }
    
    public PagosNacion vPgo(String vPgo) {
        this.vPgo = vPgo;
        return this;
    }
    
    public PagosNacion vRegion(String vRegion) {
        this.vRegion = vRegion;
        return this;
    }
    
    public PagosNacion vPagos(String vPagos) {
        this.vPagos = vPagos;
        return this;
    }
    
    public PagosNacion vAdm(String vAdm) {
        this.vAdm = vAdm;
        return this;
    }
    
    public PagosNacion vAgencia_d(String vAgencia_d) {
        this.vAgencia_d = vAgencia_d;
        return this;
    }
    
    public PagosNacion vAno_recau(String vAno_recau) {
        this.vAno_recau = vAno_recau;
        return this;
    }
    
    public PagosNacion vMes_recau(String vMes_recau) {
        this.vMes_recau = vMes_recau;
        return this;
    }
    
    public PagosNacion vDia_recau(String vDia_recau) {
        this.vDia_recau = vDia_recau;
        return this;
    }
    
    public PagosNacion vUbigeo(String vUbigeo) {
        this.vUbigeo = vUbigeo;
        return this;
    }
    
    public PagosNacion vFuente(String vFuente) {
        this.vFuente = vFuente;
        return this;
    }
    
    public PagosNacion vContrib(String vContrib) {
        this.vContrib = vContrib;
        return this;
    }
    
    public PagosNacion vSupervisor(String vSupervisor) {
        this.vSupervisor = vSupervisor;
        return this;
    }
    
    public PagosNacion vHhmovim(String vHhmovim) {
        this.vHhmovim = vHhmovim;
        return this;
    }
    
    public PagosNacion vProceso(String vProceso) {
        this.vProceso = vProceso;
        return this;
    }
    
    public PagosNacion dFecreg(Instant dFecreg) {
        this.dFecreg = dFecreg;
        return this;
    }
    
    public PagosNacion vCodbanco(String vCodbanco) {
        this.vCodbanco = vCodbanco;
        return this;
    }

    public PagosNacionId getpagosNacionId() {
        return pagosNacionId;
    }

    public PagosNacion pagosNacionId(PagosNacionId pagosNacionId) {
        this.pagosNacionId = pagosNacionId;
        return this;
    }
 
    public void setpagosNacionId(PagosNacionId pagosNacionId) {
        this.pagosNacionId = pagosNacionId;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PagosNacion pagosNacion = (PagosNacion) o;
        if (pagosNacion.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), pagosNacion.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PagosNacion{" + "id=" + getId() + 
        "vAnocomp=" + getpagosNacionId().getvAnocomp() + ", " + 
        "vCodcomp=" + getpagosNacionId().getvCodcomp() + ", " + 
        "vCodofc=" + getpagosNacionId().getvCodofc() + ", " + 
        "vFeccomp=" + getpagosNacionId().getvFeccomp() + ", " + 
        "vCodtrib=" + getvCodtrib() + ", " + 
        "vCodmtpe=" + getvCodmtpe() + ", " + 
        "vTipdoc=" + getvTipdoc() + ", " + 
        "vNumdoc=" + getvNumdoc() + ", " + 
        "nImpcomp=" + getnImpcomp() + ", " + 
        "dFeccomp=" + getdFeccomp() + ", " + 
        "vCodcaj=" + getvCodcaj() + ", " + 
        "vFlgcon=" + getvFlgcon() + ", " + 
        "vNumdocmul=" + getvNumdocmul() + ", " + 
        "vEstado=" + getvEstado() + ", " + 
        "vPago=" + getvPago() + ", " + 
        "vDigito=" + getvDigito() + ", " + 
        "vAfp=" + getvAfp() + ", " + 
        "vMotivo_ext=" + getvMotivo_ext() + ", " + 
        "vOtros=" + getvOtros() + ", " + 
        "vBanco=" + getvBanco() + ", " + 
        "vCheque=" + getvCheque() + ", " + 
        "vJuzgado=" + getvJuzgado() + ", " + 
        "vRegistros=" + getvRegistros() + ", " + 
        "vExpdte=" + getvExpdte() + ", " + 
        "vAno_movim=" + getvAno_movim() + ", " + 
        "vMes_movim=" + getvMes_movim() + ", " + 
        "vDia_movim=" + getvDia_movim() + ", " + 
        "vHh_movim=" + getvHh_movim() + ", " + 
        "vMm_movim=" + getvMm_movim() + ", " + 
        "vSs_movim=" + getvSs_movim() + ", " + 
        "vTrans=" + getvTrans() + ", " + 
        "vTributo2=" + getvTributo2() + ", " + 
        "vBenefi=" + getvBenefi() + ", " + 
        "vDigchk=" + getvDigchk() + ", " + 
        "vExpdte1=" + getvExpdte1() + ", " + 
        "vDoc_pgo=" + getvDoc_pgo() + ", " + 
        "vPgo=" + getvPgo() + ", " + 
        "vRegion=" + getvRegion() + ", " + 
        "vPagos=" + getvPagos() + ", " + 
        "vAdm=" + getvAdm() + ", " + 
        "vAgencia_d=" + getvAgencia_d() + ", " + 
        "vAno_recau=" + getvAno_recau() + ", " + 
        "vMes_recau=" + getvMes_recau() + ", " + 
        "vDia_recau=" + getvDia_recau() + ", " + 
        "vUbigeo=" + getvUbigeo() + ", " + 
        "vFuente=" + getvFuente() + ", " + 
        "vContrib=" + getvContrib() + ", " + 
        "vSupervisor=" + getvSupervisor() + ", " + 
        "vHhmovim=" + getvHhmovim() + ", " + 
        "vProceso=" + getvProceso() + ", " + 
        "dFecreg=" + getdFecreg() + ", " + 
        "vCodbanco=" + getvCodbanco() + "}";
    }
}

package pe.gob.trabajo.domain;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

@Entity
@Table(name = "SITB_MENUXPERFIL", schema = "SIMINTRA1")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "SITB_MENUXPERFIL")
public class SisMenuxperfil {
	
	@EmbeddedId
	private SisMenuxperfilId sisMenuxperfilId;
	/*
	@Id
	@Column(name = "V_CODSIS")
	private String vCodsis;
	@Id
	@Column(name = "V_CODMEN")
	private String vCodmen;
	@Id
	@Column(name = "V_CODPER")
	private String vCodper;
	*/
	@Column(name = "V_FLGACT")
	private String vFlgact;
		
	public SisMenuxperfilId getSisMenuxperfilId() {
		return sisMenuxperfilId;
	}
	public void setSisMenuxperfilId(SisMenuxperfilId sisMenuxperfilId) {
		this.sisMenuxperfilId = sisMenuxperfilId;
	}
	public String getvFlgact() {
		return vFlgact;
	}
	public void setvFlgact(String vFlgact) {
		this.vFlgact = vFlgact;
	}
}

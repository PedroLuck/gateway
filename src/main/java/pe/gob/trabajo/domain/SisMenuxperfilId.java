package pe.gob.trabajo.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

@Embeddable
public class SisMenuxperfilId implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Column(name = "v_codsis")
	private String vCodsis;
	@Column(name = "v_codmen")
	private String vCodmen;
	@Column(name = "v_codper")
	private String vCodper;
	
	public String getvCodsis() {
		return vCodsis;
	}
	public void setvCodsis(String vCodsis) {
		this.vCodsis = vCodsis;
	}
	public String getvCodmen() {
		return vCodmen;
	}
	public void setvCodmen(String vCodmen) {
		this.vCodmen = vCodmen;
	}
	public String getvCodper() {
		return vCodper;
	}
	public void setvCodper(String vCodper) {
		this.vCodper = vCodper;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((vCodmen == null) ? 0 : vCodmen.hashCode());
		result = prime * result + ((vCodper == null) ? 0 : vCodper.hashCode());
		result = prime * result + ((vCodsis == null) ? 0 : vCodsis.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SisMenuxperfilId other = (SisMenuxperfilId) obj;
		if (vCodmen == null) {
			if (other.vCodmen != null)
				return false;
		} else if (!vCodmen.equals(other.vCodmen))
			return false;
		if (vCodper == null) {
			if (other.vCodper != null)
				return false;
		} else if (!vCodper.equals(other.vCodper))
			return false;
		if (vCodsis == null) {
			if (other.vCodsis != null)
				return false;
		} else if (!vCodsis.equals(other.vCodsis))
			return false;
		return true;
	}
	public SisMenuxperfilId(String vCodsis, String vCodmen, String vCodper) {
		super();
		this.vCodsis = vCodsis;
		this.vCodmen = vCodmen;
		this.vCodper = vCodper;
	}
	
	public SisMenuxperfilId() {};
}

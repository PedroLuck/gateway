package pe.gob.trabajo.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

@Entity
@Table(name = "sgtbc_usuperfil")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "usuperfil")
public class Usuperfil {
	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "n_codusuperf")
	private Long id;
	@Column(name = "n_codusuario")
	private Long nCodusuario;
	@Column(name = "n_codperfil")
	private Long nCodperfil;
	@Column(name = "n_flgactivo")
	private Boolean flgActivo;
	@ManyToOne
    @JoinColumn(name = "n_codusuario", insertable = false, updatable = false)
    private Usuario usuario;
	@ManyToOne
    @JoinColumn(name = "n_codperfil", insertable = false, updatable = false)
    private Perfil perfil;
	
	
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getnCodusuario() {
		return nCodusuario;
	}
	public void setnCodusuario(Long nCodusuario) {
		this.nCodusuario = nCodusuario;
	}
	public Long getnCodperfil() {
		return nCodperfil;
	}
	public void setnCodperfil(Long nCodperfil) {
		this.nCodperfil = nCodperfil;
	}
	public Boolean getFlgActivo() {
		return flgActivo;
	}
	public void setFlgActivo(Boolean flgActivo) {
		this.flgActivo = flgActivo;
	}
	
	@Override
	public String toString() {
		return "Usuperfil [id=" + id + ", nCodusuario=" + nCodusuario + ", nCodperfil=" + nCodperfil + ", flgActivo="
				+ flgActivo + "]";
	}
}

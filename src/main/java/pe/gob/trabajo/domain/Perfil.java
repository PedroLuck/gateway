package pe.gob.trabajo.domain;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "sgtbc_perfil")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "perfil")
public class Perfil {
	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "n_codperfil")
	private Long id;
	@Column(name = "v_descrip")
	private String descrip;
	@Column(name = "n_flgactivo")
	private Boolean flgActivo;
	
	@OneToMany(mappedBy = "perfil")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Usuperfil> usuperfils = new HashSet<>();
	
	
	public Set<Usuperfil> getUsuperfils() {
		return usuperfils;
	}
	public void setUsuperfils(Set<Usuperfil> usuperfils) {
		this.usuperfils = usuperfils;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDescrip() {
		return descrip;
	}
	public void setDescrip(String descrip) {
		this.descrip = descrip;
	}
	public Boolean getFlgActivo() {
		return flgActivo;
	}
	public void setFlgActivo(Boolean flgActivo) {
		this.flgActivo = flgActivo;
	}
	
	@Override
	public String toString() {
		return "Perfil [id=" + id + ", descrip=" + descrip + ", flgActivo=" + flgActivo + "]";
	}
	
	
	
}

package pe.gob.trabajo.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Pernatural.
 */
@Entity
@Table(name = "gltbc_pernatural")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "gltbc_pernatural")
public class Pernatural implements Serializable {

    private static final long serialVersionUID = 1L;  
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "n_codpernat", nullable = false)
    private Long id;

    @Transient
    public String fecNacimiento;

    @NotNull
    @Size(max = 100)
    @Column(name = "v_nombres", length = 100, nullable = false)
    private String vNombres;

    @NotNull
    @Size(max = 100)
    @Column(name = "v_apepat", length = 100, nullable = false)
    private String vApepat;

    @NotNull
    @Size(max = 100)
    @Column(name = "v_apemat", length = 100, nullable = false)
    private String vApemat;

    @NotNull
    @Size(max = 15)
    @Column(name = "v_numdoc", length = 15, nullable = false)
    private String vNumdoc;

    @Size(max = 15)
    @Column(name = "v_telefono", length = 15)
    private String vTelefono;

    @Size(max = 15)
    @Column(name = "v_celular", length = 15)
    private String vCelular;

    @Size(max = 100)
    @Column(name = "v_emailper", length = 100)
    private String vEmailper;

    @Column(name = "d_fecnac")
    private LocalDate dFecnac;

    @NotNull
    @Size(max = 1)
    @Column(name = "v_sexoper", length = 1, nullable = false)
    private String vSexoper;

    @NotNull
    @Size(max = 1)
    @Column(name = "v_estado", length = 1, nullable = false)
    private String vEstado;

    @NotNull
    @Column(name = "n_usuareg", nullable = false)
    private Integer nUsuareg;

    @NotNull
    @Column(name = "t_fecreg", nullable = false)
    private Instant tFecreg;

    @NotNull
    @Column(name = "n_flgactivo", nullable = false)
    private Boolean nFlgactivo;

    @NotNull
    @Column(name = "n_sedereg", nullable = false)
    private Integer nSedereg;

    @Column(name = "n_usuaupd")
    private Integer nUsuaupd;

    @Column(name = "t_fecupd")
    private Instant tFecupd;

    @Column(name = "n_sedeupd")
    private Integer nSedeupd;

    @NotNull
    @Column(name = "n_codtdiden", nullable = false)
    private Long nCodtdiden;
    
    

    public String getFecNacimiento() {
		return fecNacimiento;
	}

	public void setFecNacimiento(String fecNacimiento) {
		this.fecNacimiento = fecNacimiento;
	}

	public Long getnCodtdiden() {
		return nCodtdiden;
	}

	public void setnCodtdiden(Long nCodtdiden) {
		this.nCodtdiden = nCodtdiden;
	}

	public Boolean getnFlgactivo() {
		return nFlgactivo;
	}

	// jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getvNombres() {
        return vNombres;
    }

    public Pernatural vNombres(String vNombres) {
        this.vNombres = vNombres;
        return this;
    }

    public void setvNombres(String vNombres) {
        this.vNombres = vNombres;
    }

    public String getvApepat() {
        return vApepat;
    }

    public Pernatural vApepat(String vApepat) {
        this.vApepat = vApepat;
        return this;
    }

    public void setvApepat(String vApepat) {
        this.vApepat = vApepat;
    }

    public String getvApemat() {
        return vApemat;
    }

    public Pernatural vApemat(String vApemat) {
        this.vApemat = vApemat;
        return this;
    }

    public void setvApemat(String vApemat) {
        this.vApemat = vApemat;
    }

    public String getvNumdoc() {
        return vNumdoc;
    }

    public Pernatural vNumdoc(String vNumdoc) {
        this.vNumdoc = vNumdoc;
        return this;
    }

    public void setvNumdoc(String vNumdoc) {
        this.vNumdoc = vNumdoc;
    }

    public String getvTelefono() {
        return vTelefono;
    }

    public Pernatural vTelefono(String vTelefono) {
        this.vTelefono = vTelefono;
        return this;
    }

    public void setvTelefono(String vTelefono) {
        this.vTelefono = vTelefono;
    }

    public String getvCelular() {
        return vCelular;
    }

    public Pernatural vCelular(String vCelular) {
        this.vCelular = vCelular;
        return this;
    }

    public void setvCelular(String vCelular) {
        this.vCelular = vCelular;
    }

    public String getvEmailper() {
        return vEmailper;
    }

    public Pernatural vEmailper(String vEmailper) {
        this.vEmailper = vEmailper;
        return this;
    }

    public void setvEmailper(String vEmailper) {
        this.vEmailper = vEmailper;
    }

    public LocalDate getdFecnac() {
        return dFecnac;
    }

    public Pernatural dFecnac(LocalDate dFecnac) {
        this.dFecnac = dFecnac;
        return this;
    }

    public void setdFecnac(LocalDate dFecnac) {
        this.dFecnac = dFecnac;
    }

    public String getvSexoper() {
        return vSexoper;
    }

    public Pernatural vSexoper(String vSexoper) {
        this.vSexoper = vSexoper;
        return this;
    }

    public void setvSexoper(String vSexoper) {
        this.vSexoper = vSexoper;
    }

    public String getvEstado() {
        return vEstado;
    }

    public Pernatural vEstado(String vEstado) {
        this.vEstado = vEstado;
        return this;
    }

    public void setvEstado(String vEstado) {
        this.vEstado = vEstado;
    }

    public Integer getnUsuareg() {
        return nUsuareg;
    }

    public Pernatural nUsuareg(Integer nUsuareg) {
        this.nUsuareg = nUsuareg;
        return this;
    }

    public void setnUsuareg(Integer nUsuareg) {
        this.nUsuareg = nUsuareg;
    }

    public Instant gettFecreg() {
        return tFecreg;
    }

    public Pernatural tFecreg(Instant tFecreg) {
        this.tFecreg = tFecreg;
        return this;
    }

    public void settFecreg(Instant tFecreg) {
        this.tFecreg = tFecreg;
    }

    public Boolean isnFlgactivo() {
        return nFlgactivo;
    }

    public Pernatural nFlgactivo(Boolean nFlgactivo) {
        this.nFlgactivo = nFlgactivo;
        return this;
    }

    public void setnFlgactivo(Boolean nFlgactivo) {
        this.nFlgactivo = nFlgactivo;
    }

    public Integer getnSedereg() {
        return nSedereg;
    }

    public Pernatural nSedereg(Integer nSedereg) {
        this.nSedereg = nSedereg;
        return this;
    }

    public void setnSedereg(Integer nSedereg) {
        this.nSedereg = nSedereg;
    }

    public Integer getnUsuaupd() {
        return nUsuaupd;
    }

    public Pernatural nUsuaupd(Integer nUsuaupd) {
        this.nUsuaupd = nUsuaupd;
        return this;
    }

    public void setnUsuaupd(Integer nUsuaupd) {
        this.nUsuaupd = nUsuaupd;
    }

    public Instant gettFecupd() {
        return tFecupd;
    }

    public Pernatural tFecupd(Instant tFecupd) {
        this.tFecupd = tFecupd;
        return this;
    }

    public void settFecupd(Instant tFecupd) {
        this.tFecupd = tFecupd;
    }

    public Integer getnSedeupd() {
        return nSedeupd;
    }

    public Pernatural nSedeupd(Integer nSedeupd) {
        this.nSedeupd = nSedeupd;
        return this;
    }

    public void setnSedeupd(Integer nSedeupd) {
        this.nSedeupd = nSedeupd;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Pernatural pernatural = (Pernatural) o;
        if (pernatural.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), pernatural.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Pernatural{" + "id=" + getId() + ", vNombres='" + getvNombres() + "'" + ", vApepat='" + getvApepat()
                + "'" + ", vApemat='" + getvApemat() + "'" + ", vNumdoc='" + getvNumdoc() + "'" + ", vTelefono='"
                + getvTelefono() + "'" + ", vCelular='" + getvCelular() + "'" + ", vEmailper='" + getvEmailper() + "'"
                + ", dFecnac='" + getdFecnac() + "'" + ", vSexoper='" + getvSexoper() + "'" + ", vEstado='"
                + getvEstado() + "'" + ", nUsuareg='" + getnUsuareg() + "'" + ", tFecreg='" + gettFecreg() + "'"
                + ", nFlgactivo='" + isnFlgactivo() + "'" + ", nSedereg='" + getnSedereg() + "'" + ", nUsuaupd='"
                + getnUsuaupd() + "'" + ", tFecupd='" + gettFecupd() + "'" + ", nSedeupd='" + getnSedeupd() + "'" + "}";
    }
}

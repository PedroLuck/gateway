package pe.gob.trabajo.domain;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

@Entity
@Table(name = "SITB_MENUS", schema = "SIMINTRA1")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "SITB_MENUS")
public class SisMenu {
	
	@EmbeddedId
	private SisMenuId sisMenuId;
	
	@Column(name = "N_NIVEL")
	private Integer nNivel;
	@Column(name = "V_CODPAD")
	private String vCodpad;
	@Column(name = "V_DESMEN")
	private String vDesmen;
	@Column(name = "V_URL")
	private String vUrl;
	@Column(name = "V_FLGACT")
	private String vFlgact;
	@Column(name = "N_ORDEN")
	private Integer nOrden;
	@Column(name = "V_ICONO")
	private Integer vIcono;
	
	public SisMenuId getSisMenuId() {
		return sisMenuId;
	}
	public void setSisMenuId(SisMenuId sisMenuId) {
		this.sisMenuId = sisMenuId;
	}
	public Integer getnNivel() {
		return nNivel;
	}
	public void setnNivel(Integer nNivel) {
		this.nNivel = nNivel;
	}
	public String getvCodpad() {
		return vCodpad;
	}
	public void setvCodpad(String vCodpad) {
		this.vCodpad = vCodpad;
	}
	public String getvDesmen() {
		return vDesmen;
	}
	public void setvDesmen(String vDesmen) {
		this.vDesmen = vDesmen;
	}
	public String getvUrl() {
		return vUrl;
	}
	public void setvUrl(String vUrl) {
		this.vUrl = vUrl;
	}
	public String getvFlgact() {
		return vFlgact;
	}
	public void setvFlgact(String vFlgact) {
		this.vFlgact = vFlgact;
	}
	public Integer getnOrden() {
		return nOrden;
	}
	public void setnOrden(Integer nOrden) {
		this.nOrden = nOrden;
	}
	public Integer getvIcono() {
		return vIcono;
	}
	public void setvIcono(Integer vIcono) {
		this.vIcono = vIcono;
	}
}

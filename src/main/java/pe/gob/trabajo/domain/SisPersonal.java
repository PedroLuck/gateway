package pe.gob.trabajo.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

@Entity
@Table(name = "PRTBC_PERSONAL", schema = "SIMINTRA1")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "PRTBC_PERSONAL")
public class SisPersonal {
	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "V_CODPERSONAL")
	private String vCodpersonal;
	@Column(name = "V_CODTIPO")
	private String vCodtipo;
	@Column(name = "V_DESAPEPAT")
	private String vDesapepat;
	@Column(name = "V_DESAPEMAT")
	private String vDesapemat;
	@Column(name = "V_DESNOMBRES")
	private String vDesnombres;
	@Column(name = "V_CODESCALA")
	private String vCodescala;
	@Column(name = "V_DESCARNIV")
	private String vDescarniv;
	@Column(name = "N_NUMDEP")
	private Integer nNumdep;
	@Column(name = "N_FLGACTIVO")
	private Integer nFlgactivo;
	@Column(name = "V_CORREOE")
	private String vCorreoe;
	@Column(name = "V_CODESCALAREMUN")
	private String vCodescalaremun;
	@Column(name = "V_CODCARGO")
	private String vCodcargo;
	@Column(name = "V_FLGRESTUPA")
	private String vFlgrestupa;
	@Column(name = "V_FLGALERT")
	private String vFlgalert;
	@Column(name = "V_FLGCONG")
	private String vFlgcong;
	@Column(name = "V_FLGARCH")
	private String vFlgarch;
	@Column(name = "V_FLGBNDASIG")
	private String vFlgbndasig;
	@Column(name = "V_CODASIST")
	private String vCodasist;
	
	public String getvCodpersonal() {
		return vCodpersonal;
	}
	public void setvCodpersonal(String vCodpersonal) {
		this.vCodpersonal = vCodpersonal;
	}
	public String getvCodtipo() {
		return vCodtipo;
	}
	public void setvCodtipo(String vCodtipo) {
		this.vCodtipo = vCodtipo;
	}
	public String getvDesapepat() {
		return vDesapepat;
	}
	public void setvDesapepat(String vDesapepat) {
		this.vDesapepat = vDesapepat;
	}
	public String getvDesapemat() {
		return vDesapemat;
	}
	public void setvDesapemat(String vDesapemat) {
		this.vDesapemat = vDesapemat;
	}
	public String getvDesnombres() {
		return vDesnombres;
	}
	public void setvDesnombres(String vDesnombres) {
		this.vDesnombres = vDesnombres;
	}
	public String getvCodescala() {
		return vCodescala;
	}
	public void setvCodescala(String vCodescala) {
		this.vCodescala = vCodescala;
	}
	public String getvDescarniv() {
		return vDescarniv;
	}
	public void setvDescarniv(String vDescarniv) {
		this.vDescarniv = vDescarniv;
	}
	public Integer getnNumdep() {
		return nNumdep;
	}
	public void setnNumdep(Integer nNumdep) {
		this.nNumdep = nNumdep;
	}
	public Integer getnFlgactivo() {
		return nFlgactivo;
	}
	public void setnFlgactivo(Integer nFlgactivo) {
		this.nFlgactivo = nFlgactivo;
	}
	public String getvCorreoe() {
		return vCorreoe;
	}
	public void setvCorreoe(String vCorreoe) {
		this.vCorreoe = vCorreoe;
	}
	public String getvCodescalaremun() {
		return vCodescalaremun;
	}
	public void setvCodescalaremun(String vCodescalaremun) {
		this.vCodescalaremun = vCodescalaremun;
	}
	public String getvCodcargo() {
		return vCodcargo;
	}
	public void setvCodcargo(String vCodcargo) {
		this.vCodcargo = vCodcargo;
	}
	public String getvFlgrestupa() {
		return vFlgrestupa;
	}
	public void setvFlgrestupa(String vFlgrestupa) {
		this.vFlgrestupa = vFlgrestupa;
	}
	public String getvFlgalert() {
		return vFlgalert;
	}
	public void setvFlgalert(String vFlgalert) {
		this.vFlgalert = vFlgalert;
	}
	public String getvFlgcong() {
		return vFlgcong;
	}
	public void setvFlgcong(String vFlgcong) {
		this.vFlgcong = vFlgcong;
	}
	public String getvFlgarch() {
		return vFlgarch;
	}
	public void setvFlgarch(String vFlgarch) {
		this.vFlgarch = vFlgarch;
	}
	public String getvFlgbndasig() {
		return vFlgbndasig;
	}
	public void setvFlgbndasig(String vFlgbndasig) {
		this.vFlgbndasig = vFlgbndasig;
	}
	public String getvCodasist() {
		return vCodasist;
	}
	public void setvCodasist(String vCodasist) {
		this.vCodasist = vCodasist;
	}
	
	

}

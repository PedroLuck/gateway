package pe.gob.trabajo.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

@Embeddable
public class SisUsuxperfilId implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Column(name = "v_codusu")
	private String vCodusu;
	@Column(name = "v_codper")
	private String vCodper;
	@Column(name = "v_codsis")
	private String vCodsis;
	public String getvCodusu() {
		return vCodusu;
	}
	public void setvCodusu(String vCodusu) {
		this.vCodusu = vCodusu;
	}
	public String getvCodper() {
		return vCodper;
	}
	public void setvCodper(String vCodper) {
		this.vCodper = vCodper;
	}
	public String getvCodsis() {
		return vCodsis;
	}
	public void setvCodsis(String vCodsis) {
		this.vCodsis = vCodsis;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((vCodper == null) ? 0 : vCodper.hashCode());
		result = prime * result + ((vCodsis == null) ? 0 : vCodsis.hashCode());
		result = prime * result + ((vCodusu == null) ? 0 : vCodusu.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SisUsuxperfilId other = (SisUsuxperfilId) obj;
		if (vCodper == null) {
			if (other.vCodper != null)
				return false;
		} else if (!vCodper.equals(other.vCodper))
			return false;
		if (vCodsis == null) {
			if (other.vCodsis != null)
				return false;
		} else if (!vCodsis.equals(other.vCodsis))
			return false;
		if (vCodusu == null) {
			if (other.vCodusu != null)
				return false;
		} else if (!vCodusu.equals(other.vCodusu))
			return false;
		return true;
	}
	public SisUsuxperfilId() {
	}
	public SisUsuxperfilId(String vCodusu, String vCodper, String vCodsis) {
		super();
		this.vCodusu = vCodusu;
		this.vCodper = vCodper;
		this.vCodsis = vCodsis;
	}
	
	
}

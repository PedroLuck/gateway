package pe.gob.trabajo.domain;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

@Entity
@Table(name = "SITB_USUXPERFIL", schema = "SIMINTRA1") 
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "SITB_USUXPERFIL")
public class SisUsuxperfil {
	@EmbeddedId
	private SisUsuxperfilId sisUsuxperfilId;	
	
	/*@Column(name = "V_CODUSU")
	private String vCodusu;
	@Id
	@Column(name = "V_CODPER")
	private String vCodper;
	@Id
	@Column(name = "V_CODSIS")
	private String vCodsis;*/
	@Column(name = "V_FLGACT")
	private String vFlgact;

	public SisUsuxperfilId getSisUsuxperfilId() {
		return sisUsuxperfilId;
	}

	public void setSisUsuxperfilId(SisUsuxperfilId sisUsuxperfilId) {
		this.sisUsuxperfilId = sisUsuxperfilId;
	}

	public String getvFlgact() {
		return vFlgact;
	}

	public void setvFlgact(String vFlgact) {
		this.vFlgact = vFlgact;
	}
	
}

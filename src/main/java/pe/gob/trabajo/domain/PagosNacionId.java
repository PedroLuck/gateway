package pe.gob.trabajo.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A PagosNacionId.
 */
@Embeddable
public class PagosNacionId implements Serializable {

    private static final long serialVersionUID = 1L;

    @Transient
    private Long id;

    @Size(max = 4)
    @Column(name = "v_anocomp", length = 4)
    private String vAnocomp;

    @Size(max = 10)
    @Column(name = "v_codcomp", length = 10)
    private String vCodcomp;

    @Size(max = 4)
    @Column(name = "v_codofc", length = 4)
    private String vCodofc;

    @Size(max = 8)
    @Column(name = "v_feccomp", length = 8)
    private String vFeccomp;
    

    public String getvAnocomp() {
        return vAnocomp;
    }
    
    public String getvCodcomp() {
        return vCodcomp;
    }
    
    public String getvCodofc() {
        return vCodofc;
    }
    
    public String getvFeccomp() {
        return vFeccomp;
    }

    public void setvAnocomp(String vAnocomp) {
        this.vAnocomp = vAnocomp;
    }
    
    public void setvCodcomp(String vCodcomp) {
        this.vCodcomp = vCodcomp;
    }
    
    public void setvCodofc(String vCodofc) {
        this.vCodofc = vCodofc;
    }
    
    public void setvFeccomp(String vFeccomp) {
        this.vFeccomp = vFeccomp;
    }

    public PagosNacionId vAnocomp(String vAnocomp) {
        this.vAnocomp = vAnocomp;
        return this;
    }
    
    public PagosNacionId vCodcomp(String vCodcomp) {
        this.vCodcomp = vCodcomp;
        return this;
    }
    
    public PagosNacionId vCodofc(String vCodofc) {
        this.vCodofc = vCodofc;
        return this;
    }
    
    public PagosNacionId vFeccomp(String vFeccomp) {
        this.vFeccomp = vFeccomp;
        return this;
    }

}

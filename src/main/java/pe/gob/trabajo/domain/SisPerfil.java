package pe.gob.trabajo.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

@Entity
@Table(name = "SITB_PERFIL", schema = "SIMINTRA1")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "SITB_PERFIL")
public class SisPerfil {
	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "V_CODSIS", nullable = false)
	private String vCodsis;
	@Column(name = "V_CODPER")
	private String vCodper;
	@Column(name = "V_DESPER")
	private String vDesper;
	@Column(name = "V_ABRPER")
	private String vAbrper;
	@Column(name = "V_FLGLEC")
	private String vFlglec;
	@Column(name = "V_FLGACT")
	private String vFlgact;
	@Column(name = "V_CODZON")
	private String vCodzon;
	
	public String getvCodsis() {
		return vCodsis;
	}
	public void setvCodsis(String vCodsis) {
		this.vCodsis = vCodsis;
	}
	public String getvCodper() {
		return vCodper;
	}
	public void setvCodper(String vCodper) {
		this.vCodper = vCodper;
	}
	public String getvDesper() {
		return vDesper;
	}
	public void setvDesper(String vDesper) {
		this.vDesper = vDesper;
	}
	public String getvAbrper() {
		return vAbrper;
	}
	public void setvAbrper(String vAbrper) {
		this.vAbrper = vAbrper;
	}
	public String getvFlglec() {
		return vFlglec;
	}
	public void setvFlglec(String vFlglec) {
		this.vFlglec = vFlglec;
	}
	public String getvFlgact() {
		return vFlgact;
	}
	public void setvFlgact(String vFlgact) {
		this.vFlgact = vFlgact;
	}
	public String getvCodzon() {
		return vCodzon;
	}
	public void setvCodzon(String vCodzon) {
		this.vCodzon = vCodzon;
	}
	
	
}

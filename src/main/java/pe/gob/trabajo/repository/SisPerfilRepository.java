package pe.gob.trabajo.repository;

import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.*;
import java.util.List;
import org.springframework.data.repository.query.Param;
import pe.gob.trabajo.domain.Sectorecoperjuridica;
import pe.gob.trabajo.domain.SisMenu;
import pe.gob.trabajo.domain.SisMenuxperfil;
import pe.gob.trabajo.domain.SisPerfil;

/**
 * Spring Data JPA repository for the Anexlaboral entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SisPerfilRepository extends JpaRepository<SisPerfil, String> {
    @Query("select SisPerfil from SisPerfil SisPerfil where SisPerfil.vCodsis = :codsis and  SisPerfil.vCodper in :ids and SisPerfil.vFlgact = 'S'")
    public List<SisPerfil> GetPerfilsByCode(@Param("codsis") String codsis, @Param("ids") List<String> codperfils);
}
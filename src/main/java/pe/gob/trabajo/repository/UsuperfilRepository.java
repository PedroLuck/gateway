package pe.gob.trabajo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import pe.gob.trabajo.domain.Usuperfil;

@Repository
public interface UsuperfilRepository extends JpaRepository<Usuperfil, Long> {
	@Query("select usuperfil from Usuperfil usuperfil where usuperfil.nCodusuario=?1")
    List<Usuperfil> GetUsuarioValid(Long idUsuario);
}

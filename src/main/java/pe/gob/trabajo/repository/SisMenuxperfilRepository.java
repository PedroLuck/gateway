package pe.gob.trabajo.repository;

import org.springframework.stereotype.Repository;

import pe.gob.trabajo.domain.SisMenuxperfil;
import pe.gob.trabajo.domain.SisMenuxperfilId;

import org.springframework.data.jpa.repository.*;
import java.util.List;
import org.springframework.data.repository.query.Param;

/**
 * Spring Data JPA repository for the Anexlaboral entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SisMenuxperfilRepository extends JpaRepository<SisMenuxperfil, SisMenuxperfilId> {
    @Query("select m from SisMenuxperfil m where m.sisMenuxperfilId.vCodsis = :codsis " + 
    		" and m.sisMenuxperfilId.vCodper in :ids and m.vFlgact = 'S'")
    public List<SisMenuxperfil> GetPerfilsByCode(@Param("codsis") String codsis, @Param("ids") List<String> codperfils);

    @Query("select m from SisMenuxperfil m where m.sisMenuxperfilId.vCodsis = :codsis " + 
    		" and m.sisMenuxperfilId.vCodper in :ids and m.vFlgact = 'S'")
    public SisMenuxperfil GetPerfilsByCodeOne(@Param("codsis") String codsis, @Param("ids") String codperfils);
}
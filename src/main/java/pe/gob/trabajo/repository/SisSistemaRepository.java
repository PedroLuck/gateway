package pe.gob.trabajo.repository;

import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.*;
import java.util.List;
import org.springframework.data.repository.query.Param;
import pe.gob.trabajo.domain.Sectorecoperjuridica;
import pe.gob.trabajo.domain.SisMenu;
import pe.gob.trabajo.domain.SisMenuxperfil;
import pe.gob.trabajo.domain.SisPerfil;
import pe.gob.trabajo.domain.SisPersonal;
import pe.gob.trabajo.domain.SisSistema;

/**
 * Spring Data JPA repository for the Anexlaboral entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SisSistemaRepository extends JpaRepository<SisSistema, String> {
}
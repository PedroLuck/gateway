package pe.gob.trabajo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import pe.gob.trabajo.domain.Usuario;

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

    @Query("select usuario from Usuario usuario where usuario.login=?1")
    Usuario GetUsuarioValid(String login);
}
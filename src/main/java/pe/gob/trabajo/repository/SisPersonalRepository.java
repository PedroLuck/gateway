package pe.gob.trabajo.repository;

import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.*;
import java.util.List;
import org.springframework.data.repository.query.Param;
import pe.gob.trabajo.domain.Sectorecoperjuridica;
import pe.gob.trabajo.domain.SisMenu;
import pe.gob.trabajo.domain.SisMenuxperfil;
import pe.gob.trabajo.domain.SisPerfil;
import pe.gob.trabajo.domain.SisPersonal;

/**
 * Spring Data JPA repository for the Anexlaboral entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SisPersonalRepository extends JpaRepository<SisPersonal, String> {

    @Query("select SisPersonal from SisPersonal SisPersonal where SisPersonal.vCodpersonal=?1")
    public SisPersonal GetPersonalByCode(String vCodpersonal);

    @Query("select new map (SP as sispersona, U.vCodusu as usuario) from SisPersonal SP left join SisUsuario U on U.vCodpersonal = SP.vCodpersonal where SP.vCodpersonal=?1")
    public List<SisPersonal> GetPersonalUsuarioByCode(String vCodpersonal);
}
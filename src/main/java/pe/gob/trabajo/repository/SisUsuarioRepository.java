package pe.gob.trabajo.repository;

import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.*;
import java.util.List;
import org.springframework.data.repository.query.Param;
import pe.gob.trabajo.domain.Sectorecoperjuridica;
import pe.gob.trabajo.domain.SisMenu;
import pe.gob.trabajo.domain.SisMenuxperfil;
import pe.gob.trabajo.domain.SisPerfil;
import pe.gob.trabajo.domain.SisPersonal;
import pe.gob.trabajo.domain.SisSistema;
import pe.gob.trabajo.domain.SisUsuario;

/**
 * Spring Data JPA repository for the Anexlaboral entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SisUsuarioRepository extends JpaRepository<SisUsuario, String> {
    @Query("select SisUsuario from SisUsuario SisUsuario where  SisUsuario.vCodusu=?1")
    public SisUsuario GetUsuarioByLogin(String vCodusu);
}
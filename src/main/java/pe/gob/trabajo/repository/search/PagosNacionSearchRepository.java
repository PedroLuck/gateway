package pe.gob.trabajo.repository.search;

import pe.gob.trabajo.domain.PagosNacion;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the PagosNacion entity.
 */
public interface PagosNacionSearchRepository extends ElasticsearchRepository<PagosNacion, Integer> {
}

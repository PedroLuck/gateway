package pe.gob.trabajo.repository;

import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.*;
import java.util.List;
import org.springframework.data.repository.query.Param;
import pe.gob.trabajo.domain.Sectorecoperjuridica;
import pe.gob.trabajo.domain.SisMenu;
import pe.gob.trabajo.domain.SisMenuxperfil;
import pe.gob.trabajo.domain.SisPerfil;
import pe.gob.trabajo.domain.SisPersonal;
import pe.gob.trabajo.domain.SisSistema;
import pe.gob.trabajo.domain.SisUsuario;
import pe.gob.trabajo.domain.SisUsuxperfil;
import pe.gob.trabajo.domain.SisUsuxperfilId;

/**
 * Spring Data JPA repository for the Anexlaboral entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SisUsuxperfilRepository extends JpaRepository<SisUsuxperfil, SisUsuxperfilId> {
    @Query("select m from SisUsuxperfil m where m.sisUsuxperfilId.vCodsis=?1 and " +
    		" m.sisUsuxperfilId.vCodusu=?2 and m.vFlgact = 'S'")
    public List<SisUsuxperfil> GetPerfilByUsu(String vCodsis, String vCodusu);
}
package pe.gob.trabajo.repository;

import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.*;
import java.util.List;
import org.springframework.data.repository.query.Param;
import pe.gob.trabajo.domain.Sectorecoperjuridica;
import pe.gob.trabajo.domain.SisMenu;

/**
 * Spring Data JPA repository for the Anexlaboral entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SisMenuRepository extends JpaRepository<SisMenu, String> {
    @Query("select SisMenu from SisMenu SisMenu where SisMenu.sisMenuId.vCodsis = :codsis and  SisMenu.sisMenuId.vCodmen in :ids and SisMenu.vFlgact = 'S'")
    public List<SisMenu> GetMenuByCode(@Param("codsis") String codsis, @Param("ids") List<String> codmenus);
    
    @Query("select SisMenu from SisMenu SisMenu where SisMenu.sisMenuId.vCodsis = :codsis and  SisMenu.sisMenuId.vCodmen in :ids and SisMenu.vFlgact = 'S'")
    public SisMenu GetMenuByOneCode(@Param("codsis") String codsis, @Param("ids") String codmenus);

    @Query("select sisMenu from SisMenu sisMenu where sisMenu.sisMenuId.vCodsis = :codsis and sisMenu.nNivel = 1 and sisMenu.vFlgact = 'S'")
    public List<SisMenu> GetMenuByPadres(@Param("codsis") String codsis);
}
package pe.gob.trabajo.repository;

import pe.gob.trabajo.domain.PagosNacion;
import org.springframework.stereotype.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;


/**
 * Spring Data JPA repository for the PagosNacion entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PagosNacionRepository extends JpaRepository<PagosNacion, Integer> {

    @Query("select count(p.pagosNacionId.vCodcomp) from PagosNacion p " +
            "where p.pagosNacionId.vCodcomp = :vcodcomp " +
            "and p.pagosNacionId.vCodofc = :vcodofc " +
            "and p.pagosNacionId.vFeccomp = :vfeccomp " +
            "and p.vNumdoc = :vnumdoc " +
            "and p.nImpcomp = :nimpcomp " +
            "and p.vCodtrib = :vcodtrib " +
            "and p.vCodcaj = :vcodcaj " +
            "and p.vFlgcon = 'N'")
    public Object verificarPago(@Param("vcodcomp") String vcodcomp,
                                     @Param("vcodofc") String vcodofc,
                                     @Param("vfeccomp") String vfeccomp,
                                     @Param("vnumdoc") String vnumdoc,
                                     @Param("nimpcomp") Double nimpcomp,
                                     @Param("vcodtrib") String vcodtrib,
                                     @Param("vcodcaj") String vcodcaj); 

}

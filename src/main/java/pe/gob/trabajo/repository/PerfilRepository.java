package pe.gob.trabajo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import pe.gob.trabajo.domain.Perfil;

@SuppressWarnings("unused")
@Repository
public interface PerfilRepository extends JpaRepository<Perfil, Long> {
	@Query("select perfil from Perfil perfil where perfil.id=?1")
	Perfil GetPerfils(Long id);
}